
# Contribution Guide

Contact [Tim Aitken](aitken.tim@gmail.com) for project access.

## Getting Started

To get started with the project, you will need to obtain the project code,
its dependencies, and some build tools. Currently development is supported
on Windows, MacOS, and Linux operating systems.

Note that the instructions below anticipate only the most common configurations
for most developers on a given platform. If you have a different preferred
toolset, then the below guide will need to be modified.

### Windows

1. Install [VisualStudio][] as your IDE.
   * This should make `msbuild` available in powershell.
2. Install [Git][] as your version-control system.
   * This should make `git` available in powershell.
3. Install [CMake][] as your build tool.
   * This should make `cmake` and `cmake-gui` in powershell.
4. Use `git` to clone a copy of the [VilnLibs][] project and its submodules.
5. Navigate into the project and run the **configure.bat** script. This
   should open the CMake GUI. Click *generate* and then *open project* to
   load up the project in VisualStudio.

### MacOS

1. Install [Xcode][] as your IDE.
   * This should make `git` and `xcodebuild` available on the command line.
2. Install [CMake][] as your build tool.
   * This should make `cmake` and `cmake-gui` available on the command line.
3. Use `git` to clone a copy of the [VilnLibs][] project and its submodules.
4. Navigate into the project and run the **configure.sh** script. This
   should open the CMake GUI. Click *generate* and then *open project* to
   load up the project in Xcode.

### Linux

The following instructions are for OpenSUSE, and should be modified as
appropriate to your distro.

1. Install development tools and dependencies with `zypper`:

   ```bash
   zypper in git-core doxygen gcc-c++ make cmake
   ```

2. Clone the VilnLibs project using `git`:

   ```bash
   git clone https://gitlab.com/villainous/struct.git viln
   cd viln
   git submodule init
   git submodule update
   ```

3. Generate makefiles using `cmake`:

   ```bash
   mkdir _build
   cd _build
   cmake ..
   ```

4. Build the project using `make` from the **_build/** directory. Available
   make targets can be found via CMake help:

   ```bash
   cmake --build .. --target help
   ```

## Management

This project uses a [gitflow][] workflow, with issues tracked using
[GitLab Issues][].
Contributors are expected to create or be assigned an issue from the tracker,
and create a feature-branch for developemt related to said issue. When
complete, a merge-request should be created for the branch, and assigned
to the project owner for review.

The project uses [GitLab-CI][] for continuous integration purposes. Contributors
are expected to monitor the status of the CI pipeline associated with their
feature-branches, and ensure that the pipeline succeeds prior to submitting
their contributions for review.

This project's **dev** and **master** branches are protected. The **dev**
branch is used to integrate new features for the next upcoming release,
while the **master** branch is used for the releases themselves. Project
releases are permanently assigned tags. Project version numbering follows
[semver][] conventions.

## Coding Conventions

### Data Ownership

See the [public guide](README.md#data-ownership) for an overview of data
ownership.

All code contributed to this project must follow these conventions for data
ownership notation. Furthermore, all classes must make the contract for
pointer members clear, and delete all owned data upon completion.

### Variable Naming

See the [public guide](README.md#naming-conventions) for an overview of naming
conventions.

All code contributed to this project must meet these naming conventions in order
to be considered for inclusion in released versions of the software.

### Source Files

Files in this project use an *80-character line width*. Keeping a standard
width ensures that multiple files can be veiwed side-by-side, and discourages
over-complicating any single line of code.

Each provided class should provide its own header/souce pair.
Templated types may use an inline file to replace the source file.

Header names should be the lowercase underscore-separated version of the class
name, and use the ".hpp" extension.
They should provide a fenced `#ifdef` block, and declare any types
or functions within the `viln` namespace.
Headers may `#include` other headers, but *not* inline files.

```cpp
// example_class.hpp
#ifndef VILN_EXAMPLE_CLASS_HPP
#define VILN_EXAMPLE_CLASS_HPP

#include <another_class.hpp>

namespace viln
{
  // code...
}

#endif
```

Source file names should be the lowercase underscore-separated version of the
class name, and use the ".cpp" extension.
Source files may specify the `viln` namespace via the `using` keyword.
Source files should `#include` the corresponding header class header file,
as well as any required headers or inlines.

```cpp
// example_class.cpp

#include <example_class.hpp>
#include <another_class.inl>

using namespace viln;

// code...
```

Inline-source names should be the lowercase underscore-separated version of the
class name, and use the ".inl" extension.
They should provide a fenced `#ifdef` block, and declare any types
or functions within the `viln` namespace.
Inline files should `#include` the corresponding header class header file,
as well as any required headers or inlines (prefer headers).

```cpp
// example_class.inl
#ifndef VILN_EXAMPLE_CLASS_INL
#define VILN_EXAMPLE_CLASS_INL

#include <example_class.hpp>
#include <another_class.inl>

namespace viln
{
  // code...
}

#endif
```

## Documentation

All code contributed to the project should provide public documentation.
Documentation should use Doxygen-compatible identifiers.

At minimum, each function, method, and type definition that has global, public,
or protected scope should provide a one-line short documentation block
using `///` describing what it does in the header where it is declared:

```cpp
// example_class.hpp

/// This is an example type.
typedef char char3_t[3];

/// This is an example class.
class ExampleClass
{
public:
  /// This is an example public method.
  char3_t publicExample(void);
protected:
  /// This is an example protected method.
  void protectedExample(char3_t c3);
private:
  char3_t data_;
};
```

Furthermore, classes and methods with nontrivial behavior should provide
long comments using `/**` along with their definition in a .cpp or .inl file.
These comments should include `@param` and `@return` documentation for all
parameters and return values, as well as a `@note` detailing performance
when nontrivial.

```cpp
// example_class.cpp

/**
A long description of the public example method.

@return  an example return value
*/
char3_t ExampleClass::publicExample(void)
{
  return c3;
}

/**
A long description of the protected example method.

@param c3  an example parameter
*/
void ExampleClass::protectedExample(char3_t c3)
{
  this->data_ = c3;
}
```

Additional inline comments may be left as appropriate notes for future
developers, using `/*` or `//`. Test methods should be expecially heavily
documented with inline comments, as their goals are frequently inscrutable.

## Testing

New code should have unit tests associated with it when possible. Each class
added should have one or more tests associated with it. The project CI
process provides a test coverage report. As a general rule, new code should
not reduce the coverage percentage reported.

Tests should focus primarily on the public interface, and not implementation
details. This means that the tests should verify that the classes and methods
being testded have behavior matching their *documentation*, not their *design*.

Tests should have documentation describing what is being tested for each test
case. Tests should also have sufficient inline documentation to explain how the
test goes about verifying that the behavior is as expected. Remember that
unit-tests will be used in the future to diagnose potential coding issues,
and having good documentation here will make that process much less difficult!

[Git]: https://git-scm.com/downloads
[CMake]: https://cmake.org/download/
[Xcode]: https://apps.apple.com/ca/app/xcode/id497799835?mt=12
[VisualStudio]: https://visualstudio.microsoft.com/downloads/
[vcpkg]: https://github.com/microsoft/vcpkg

[VilnLibs]: https://gitlab.com/villainous/struct.git
[GitLab Issues]: https://gitlab.com/villainous/struct/-/issues
[GitLab-CI]: https://gitlab.com/villainous/struct/-/pipelines

[gitflow]: https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow
[semver]: https://semver.org/
