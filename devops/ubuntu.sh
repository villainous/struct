#!/bin/bash -e

# build the devops image
cd $(readlink -f ${0%/*})
docker build -f Dockerfile-ubuntu -t vcoll-devops .

# clone the repository
VCOLL_REMOTE=$(git remote get-url origin)
VCOLL_BRANCH=$(git branch --show-current)
VCOLL_GITDIR=${PWD}/_ignore/vcoll
if [[ -d $VCOLL_GITDIR/.git ]]
then
  cd $VCOLL_GITDIR
  git fetch
else
  git clone $VCOLL_REMOTE $VCOLL_GITDIR
  cd $VCOLL_GITDIR
fi
git checkout $VCOLL_BRANCH
git submodule init
git submodule update

# start the devops container
docker run -ti \
  -v /tmp/.X11-unix:/tmp/.X11-unix \
  -v $HOME/.Xauthority:/home/root/.Xauthority:ro \
  -v $VCOLL_GITDIR:/vcoll \
  -e DISPLAY=$DISPLAY \
  -h $HOSTNAME \
  vcoll-devops bash
