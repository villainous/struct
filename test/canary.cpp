#include "canary.hpp"

using namespace viln;

int Canary::constructor = 0;
int Canary::move = 0;
int Canary::copy = 0;
int Canary::destructor = 0;

Canary::Canary(int health) :
  data(health ? health : 1)
{
  ++constructor;
}

Canary::Canary(Canary&& o)
{
  data = o.data;
  o.data = 0;
  ++move;
}

Canary::Canary(const Canary& o)
{
  data = o.data;
  ++copy;
}

Canary::~Canary(void)
{
  if (data)
  {
    data = 0;
    ++destructor;
  }
}

void Canary::reset(void)
{
  constructor = 0;
  move = 0;
  copy = 0;
  destructor = 0;
}
