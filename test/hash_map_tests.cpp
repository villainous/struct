
#include <utility>
#include "hash_map.inl"

// check this compiles BEFORE gtest leaks symbols
template class viln::HashMap<double, double>;

#include <gtest/gtest.h>
#include "array_list.inl"

using namespace viln;

/**
Demonstrate the behavior of "add" both with and without collisions.
*/
TEST(HashMap, Add)
{
  // add some values to a map
  HashMap<int,int> hm;
  const HashMap<int, int>& chm = hm;
  int i10 = 10;
  ASSERT_TRUE(hm.add(1, i10));
  ASSERT_TRUE(hm.add(2, 20));
  ASSERT_TRUE(hm.add(3, new int(30)));
  ASSERT_EQ(hm.size(), 3);
  // can't add a value if the key already exists
  EXPECT_FALSE(hm.add(1, 0));
  EXPECT_EQ(hm.size(), 3);
  // check values are as expected
  EXPECT_EQ(hm[1], 10);
  EXPECT_EQ(hm[2], 20);
  EXPECT_EQ(chm[3], 30);
}

/**
Demonstrate combining maps using "add all" and show that collisions
are handled by skipping the collided keys.
*/
TEST(HashMap, AddAll)
{
  // create two maps with an overlapping key
  HashMap<int, char> hmA;
  ASSERT_TRUE(hmA.add(1, 'A'));
  ASSERT_TRUE(hmA.add(2, 'A'));
  HashMap<int, char> hmB;
  ASSERT_TRUE(hmB.add(2, 'B'));
  ASSERT_TRUE(hmB.add(3, 'B'));
  // add B to A
  EXPECT_TRUE(hmA.addAll(hmB));
  // check that we have added an element from B
  EXPECT_EQ(hmA.size(), 3);
  // check the values to verify the "add" behavior
  EXPECT_EQ(hmA[1], 'A');
  EXPECT_EQ(hmA[2], 'A'); // ignored from B
  EXPECT_EQ(hmA[3], 'B'); // added from B
}

/**
Demonstrate the behavior of "put" both with and without collisions.
*/
TEST(HashMap, Put)
{
  // put some values in a map
  HashMap<int, int> hm;
  int i10 = 10;
  hm.put(1, i10);
  hm.put(2, 20);
  hm.put(3, new int(30));
  ASSERT_EQ(hm.size(), 3);
  // putting a value in a map overrides it
  hm.put(1, 0);
  EXPECT_EQ(hm.size(), 3);
  // check values are as expected
  EXPECT_EQ(hm[1], 0);
  EXPECT_EQ(hm[2], 20);
  EXPECT_EQ(hm[3], 30);
}

/**
Demonstrate combining maps using "put all" and show that collisions
are handled by overwriting the collided keys.
*/
TEST(HashMap, PutAll)
{
  // create two maps with an overlapping key
  HashMap<int, char> hmA;
  hmA.put(1, 'A');
  hmA.put(2, 'A');
  HashMap<int, char> hmB;
  hmB.put(2, 'B');
  hmB.put(3, 'B');
  // put B to A
  hmA.putAll(hmB);
  // check that we have added an element from B
  EXPECT_EQ(hmA.size(), 3);
  // check the values to verify the "put" behavior
  EXPECT_EQ(hmA[1], 'A');
  EXPECT_EQ(hmA[2], 'B'); // overridden by B
  EXPECT_EQ(hmA[3], 'B'); // puted from B
}

/**
Demonstrate attempting to "remove" both keys in and not in the map.
*/
TEST(HashMap, Remove)
{
  // add some values to a map
  HashMap<int, int> hm;
  ASSERT_TRUE(hm.add(1, 10));
  ASSERT_TRUE(hm.add(2, 20));
  ASSERT_EQ(hm.size(), 2);
  // removing a key not in the map does nothing
  EXPECT_FALSE(hm.remove(3));
  EXPECT_EQ(hm.size(), 2);
  // removing an element behaves as expected
  EXPECT_TRUE(hm.remove(1));
  EXPECT_EQ(hm.size(), 1);
  EXPECT_EQ(hm[2], 20);
  // removing the last element results in an empty map
  EXPECT_TRUE(hm.remove(2));
  EXPECT_EQ(hm.size(), 0);
  EXPECT_TRUE(hm.isEmpty());
  // adding an element to a map that had its last element removed
  ASSERT_TRUE(hm.add(1, 100));
  EXPECT_EQ(hm.size(), 1);
  EXPECT_EQ(hm[1], 100);
}

/**
Demonstrate removing a collection of keys from a map.
*/
TEST(HashMap, RemoveAll)
{
  // add some values to a map
  HashMap<int, int> hm;
  ASSERT_TRUE(hm.add(1, 10));
  ASSERT_TRUE(hm.add(2, 20));
  ASSERT_TRUE(hm.add(3, 30));
  ASSERT_TRUE(hm.add(4, 40));
  ASSERT_EQ(hm.size(), 4);
  // attempt to remove some elements not in the map
  ArrayList<int> misses{ 5, 6 };
  EXPECT_FALSE(hm.removeAll(misses));
  EXPECT_EQ(hm.size(), 4);
  // remove a mix of elements in the map and not
  ArrayList<int> mix{ 5, 1 };
  EXPECT_TRUE(hm.removeAll(mix));
  EXPECT_EQ(hm.size(), 3);
  // the element that should have been removed is no longer present
  EXPECT_FALSE(hm.contains(1));
}

/**
Demonstrate attempting to "get" both keys in and not in the map.
*/
TEST(HashMap, Get)
{
  // add some values to a map
  HashMap<int, int> hm;
  const HashMap<int, int>& chm = hm;
  ASSERT_TRUE(hm.add(1, 10));
  ASSERT_TRUE(hm.add(2, 20));
  ASSERT_EQ(hm.size(), 2);
  // getting keys in the map results in the values
  const int* p10 = hm.get(1);
  const int* p20 = chm.get(2);
  ASSERT_TRUE(p10);
  ASSERT_TRUE(p20);
  EXPECT_EQ(*p10, 10);
  EXPECT_EQ(*p20, 20);
  // getting keys not in the map results in null
  const int* pxx = hm.get(1000);
  ASSERT_FALSE(pxx);
}

/**
Demonstrate that "contains all" can check multiple element containment.
*/
TEST(HashMap, ContainsAll)
{
  // add some values to a map
  HashMap<int, int> hm;
  ASSERT_TRUE(hm.add(1, 10));
  ASSERT_TRUE(hm.add(2, 20));
  ASSERT_TRUE(hm.add(3, 30));
  ASSERT_TRUE(hm.add(4, 40));
  ASSERT_EQ(hm.size(), 4);
  // all misses
  ArrayList<int> misses{ 5, 6 };
  EXPECT_FALSE(hm.containsAll(misses));
  // mix of hits and misses
  ArrayList<int> mix{ 5, 1 };
  EXPECT_FALSE(hm.containsAll(mix));
  // mix of hits and misses
  ArrayList<int> hits{ 1, 4 };
  EXPECT_TRUE(hm.containsAll(hits));
}

/**
Demonstrate that the core iterator can visit all the elements.
*/
TEST(HashMap, Iterator)
{
  // add some values to a map, NOT in ascending order
  HashMap<int, char> hm;
  for (int key = 0; key < 10; key += 2)
  {
    ASSERT_TRUE(hm.add(key, 'e')); // even
  }
  for (int key = 1; key < 10; key += 2)
  {
    ASSERT_TRUE(hm.add(key, 'o')); // odd
  }
  ASSERT_EQ(hm.size(), 10);
  // get an iterator over the map
  Iterator<KeyVal<int, char>>* itr = hm.iterator();
  // check that the values we added are all present and in-order
  int count = 0;
  while (itr->hasNext())
  {
    const KeyVal<int, char>& kvp = itr->next();
    // should visit keys in ascending order
    EXPECT_EQ(kvp.key(), count++);
    // we added the evens as 'e' and odds as 'o'
    if (kvp.key() % 2 == 0) EXPECT_EQ(kvp.value(), 'e');
    else EXPECT_EQ(kvp.value(), 'o');
  }
  // make sure we actually visited all elements
  EXPECT_EQ(count, 10);
  delete itr;
}

/**
Demonstrate how to access elements via the Cterator.
*/
TEST(HashMap, ConstIterable)
{
  // add some values to a map, NOT in ascending order
  HashMap<int, char> hm;
  for (int key = 0; key < 10; key += 2)
  {
    ASSERT_TRUE(hm.add(key, 'e')); // even
  }
  for (int key = 1; key < 10; key += 2)
  {
    ASSERT_TRUE(hm.add(key, 'o')); // odd
  }
  ASSERT_EQ(hm.size(), 10);
  // check that the values we added are all present and in-order
  int count = 0;
  for (const KeyVal<int, char>& kvp : hm)
  {
    // should visit keys in ascending order
    EXPECT_EQ(kvp.key(), count++);
    // we added the evens as 'e' and odds as 'o'
    if (kvp.key() % 2 == 0) EXPECT_EQ(kvp.value(), 'e');
    else EXPECT_EQ(kvp.value(), 'o');
  }
  // make sure we actually visited all elements
  EXPECT_EQ(count, 10);
}

/**
Demonstrate the ability to iterate over the keys of the map.
*/
TEST(HashMap, KeyIterator)
{
  // add some entries to a map
  HashMap<int, int> hm;
  ASSERT_TRUE(hm.add(1, 10));
  ASSERT_TRUE(hm.add(3, 30));
  ASSERT_TRUE(hm.add(2, 20));
  // get an iterator over the map keys
  Iterator<const int>* kitr = hm.keyIterator();
  // check that the keys we entered are returned in order
  int count = 0;
  int prevkey = 0;
  while (kitr->hasNext())
  {
    int key = kitr->next();
    EXPECT_GT(key, prevkey);
    prevkey = key;
    ++count;
  }
  EXPECT_EQ(count, 3);
  delete kitr;
}

/**
Demonstrate the ability to iterate over and update the values of the map.
*/
TEST(HashMap, ValueIterator)
{
  // add some entries to a map
  HashMap<int, int> hm;
  ASSERT_TRUE(hm.add(1, 10));
  ASSERT_TRUE(hm.add(3, 30));
  ASSERT_TRUE(hm.add(2, 20));
  // get an iterator over the map keys
  Iterator<int>* vitr = hm.valueIterator();
  // check that the keys we entered are returned in order
  int count = 0;
  int prevval = 0;
  while (vitr->hasNext())
  {
    int& val = vitr->next();
    EXPECT_GT(val, prevval);
    prevval = val;
    // update value
    val = val / 2;
    ++count;
  }
  EXPECT_EQ(count, 3);
  delete vitr;
  // check that we updated the values as desired
  EXPECT_EQ(hm[1], 5);
  EXPECT_EQ(hm[2], 10);
  EXPECT_EQ(hm[3], 15);
}

/**
Demonstrate the copy constructor duplicating the keys and values.
*/
TEST(HashMap, CopyConstructor)
{
  // add some entries to a map
  HashMap<int, int> hm1;
  ASSERT_TRUE(hm1.add(1, 100));
  ASSERT_TRUE(hm1.add(3, 30));
  ASSERT_TRUE(hm1.add(2, 2));
  {
    // copy the map
    HashMap<int, int> hm2(hm1);
    // elements in the copy should match the original
    EXPECT_EQ(hm2[1], 100);
    EXPECT_EQ(hm2[2], 2);
    EXPECT_EQ(hm2[3], 30);
    // changing the copy does not affect the original
    hm2[3] = 33;
    EXPECT_EQ(hm1[3], 30);
    EXPECT_EQ(hm2[3], 33);
  }
  // copy falls out of scope but original unaffected
  EXPECT_EQ(hm1[3], 30);
}

/**
Demonstrate the assignment operator duplicating the keys and values.
*/
TEST(HashMap, AssignmentOperator)
{
  // add some entries to a map
  HashMap<int, int> hm1;
  ASSERT_TRUE(hm1.add(1, 100));
  ASSERT_TRUE(hm1.add(3, 30));
  ASSERT_TRUE(hm1.add(2, 2));
  {
    // create a map and put a value in it
    HashMap<int, int> hm2;
    hm2.add(1, 1);
    hm2.add(4, 4);
    hm2 = hm1;
    // elements in the copy should match the original
    EXPECT_EQ(hm2[1], 100);
    EXPECT_EQ(hm2[2], 2);
    EXPECT_EQ(hm2[3], 30);
    EXPECT_FALSE(hm2.contains(4));
    // changing the copy does not affect the original
    hm2[3] = 33;
    EXPECT_EQ(hm1[3], 30);
    EXPECT_EQ(hm2[3], 33);
  }
  // copy falls out of scope but original unaffected
  EXPECT_EQ(hm1[3], 30);
}

/*
Demonstrate the move-assign operator via map-swap.
*/
TEST(HashMap, MoveSwap)
{
  // create a pair of maps with overlap
  HashMap<int, int> tm1;
  ASSERT_TRUE(tm1.add(1, 10));
  ASSERT_TRUE(tm1.add(2, 20));
  HashMap<int, int> tm2;
  ASSERT_TRUE(tm2.add(2, 200));
  ASSERT_TRUE(tm2.add(3, 300));
  ASSERT_TRUE(tm2.add(4, 400));
  // the lists match initial values
  for (const KeyVal<int, int>& kv : tm1)
  {
    ASSERT_EQ(kv.value(), kv.key() * 10);
  }
  for (const KeyVal<int, int>& kv : tm2)
  {
    ASSERT_EQ(kv.value(), kv.key() * 100);
  }
  // perform swap
  std::swap(tm1, tm2);
  // the lists are now swapped
  EXPECT_EQ(tm1.size(), 3);
  EXPECT_EQ(tm2.size(), 2);
  for (const KeyVal<int, int>& kv : tm1)
  {
    EXPECT_EQ(kv.value(), kv.key() * 100);
  }
  for (const KeyVal<int, int>& kv : tm2)
  {
    EXPECT_EQ(kv.value(), kv.key() * 10);
  }
}

/**
Demonstrate that a map with 0 initial capacity behaves as expected.
*/
TEST(HashMap, Capacity0)
{
  // start with a map with 0 requested capacity
  HashMap<int, int> blarg(0);
  ASSERT_EQ(blarg.size(), 0);
  ASSERT_FALSE(blarg.contains(999));
  ASSERT_EQ(blarg.get(999), nullptr);
  // add an element to the empty map
  ASSERT_TRUE(blarg.add(999, 0));
  ASSERT_EQ(blarg.size(), 1);
  ASSERT_TRUE(blarg.contains(999));
  ASSERT_NE(blarg.get(999), nullptr);
  ASSERT_EQ(blarg.at(999), 0);
}

/*
Demonstrate swapping the values associated with two keys.
*/
TEST(HashMap, swap)
{
  // add some entries to a map
  HashMap<int, int> hm1;
  ASSERT_TRUE(hm1.add(1, 100));
  ASSERT_TRUE(hm1.add(3, 30));
  ASSERT_TRUE(hm1.add(2, 2));
  EXPECT_EQ(hm1.size(), 3);
  // self-swap does nothing
  EXPECT_FALSE(hm1.swap(1, 1));
  // swapping missing keys does nothing
  EXPECT_FALSE(hm1.swap(0, 0));
  // swap some values
  EXPECT_TRUE(hm1.swap(1, 3));
  EXPECT_TRUE(hm1.swap(2, 3));
  // size matches elements
  EXPECT_EQ(hm1.size(), 3);
  // check contents are remapped
  EXPECT_EQ(hm1[1], 30);
  EXPECT_EQ(hm1[2], 100);
  EXPECT_EQ(hm1[3], 2);
}

/*
Demonstrate swapping the values associated with one of two keys.
*/
TEST(HashMap, swap_half)
{
  // add some entries to a map
  HashMap<int, int> hm1;
  ASSERT_TRUE(hm1.add(1, 100));
  ASSERT_TRUE(hm1.add(3, 30));
  ASSERT_TRUE(hm1.add(2, 2));
  // one-sided swaps
  EXPECT_TRUE(hm1.swap(0, 1)); // left
  EXPECT_TRUE(hm1.swap(3, 9)); // right
  // size unchanged
  ASSERT_EQ(hm1.size(), 3);
  // moved keys no longer present
  EXPECT_FALSE(hm1.contains(1));
  EXPECT_FALSE(hm1.contains(3));
  // valused present under new keys
  EXPECT_EQ(hm1[0], 100);
  EXPECT_EQ(hm1[2], 2);
  EXPECT_EQ(hm1[9], 30);
}

/*
Demonstrate that releasing a key extracts the value from the map.
*/
TEST(HashMap, release)
{
  // add some entries to a map
  HashMap<int, int> hm1;
  ASSERT_TRUE(hm1.add(1, 100));
  ASSERT_TRUE(hm1.add(3, 30));
  ASSERT_TRUE(hm1.add(2, 2));
  EXPECT_EQ(hm1.size(), 3);
  // release missing keys does nothing
  EXPECT_EQ(hm1.release(0), nullptr);
  // release a value extracts it from the map
  int* val = nullptr;
  ASSERT_TRUE((val = hm1.release(1)));
  EXPECT_EQ(*val, 100);
  delete val;
  // size matches elements
  EXPECT_EQ(hm1.size(), 2);
  // check contents altered
  EXPECT_FALSE(hm1.contains(1));
  EXPECT_EQ(hm1[3], 30);
  EXPECT_EQ(hm1[2], 2);
}

/*
Demonstrate iterating over structured-bindings of the key/value pair.
*/
TEST(HashMap, structured_bindings)
{
  // add some entries to a map
  HashMap<int, int> hm1;
  ASSERT_TRUE(hm1.add(1, 100));
  ASSERT_TRUE(hm1.add(3, 30));
  ASSERT_TRUE(hm1.add(2, 2));
  EXPECT_EQ(hm1.size(), 3);
  // iterate over struct-bindings of elements
  int acc = 0;
  for (const auto& [k, v] : hm1)
  {
    // k/v match map contents
    ASSERT_TRUE(hm1.contains(k));
    EXPECT_EQ(hm1[k], v);
    ++acc;
  }
  // visited each element
  EXPECT_EQ(acc, hm1.size());
}
