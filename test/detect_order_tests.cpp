
#include "detect_order.inl"

// check this compiles BEFORE gtest leaks symbols
template class viln::DetectOrder<double>;

#include <string>
#include <gtest/gtest.h>

using namespace viln;

/**
Demonstrate default signed-integer "<".
*/
TEST(DetectOrder, DefaultIntLessThan)
{
  // signed integers
  DetectOrder<int> dlt;
  // equal values are not less
  ASSERT_FALSE(dlt.order(1, 1));
  // non-equal values have different values
  ASSERT_TRUE(dlt.order(-1, 1));
  ASSERT_FALSE(dlt.order(1, -1));
}

/**
Demonstrate that std::string (and thereby other compliant less-than
implementations from stl) behave as expected.
*/
TEST(DetectOrder, StdString)
{
  DetectOrder<std::string> dlts;
  // equal strings are not less than one another
  ASSERT_FALSE(dlts.order("hello", "hello"));
  // check lexographic ordering of strings
  ASSERT_FALSE(dlts.order("hello", "goodbye"));
  ASSERT_TRUE(dlts.order("goodbye", "hello"));
}

struct misc_t
{
  int data;
  char distraction;
};
class DetectOrderOverride : public DetectOrder<misc_t>
{
public:
  virtual bool order(const misc_t& o1, const misc_t& o2) const
  {
    return o1.data < o2.data;
  }
};

/**
Demonstrate that we can successfully override the less-than function dlttection.
*/
TEST(DetectOrder, LessOverride)
{
  DetectOrderOverride dlt;
  misc_t dlt1{ 1, 'a' };
  misc_t dlt2{ 1, 'b' };
  misc_t dlt3{ 2, 'a' };
  // data is the same in both, so expect equality (show with both directions)
  ASSERT_FALSE(dlt.order(dlt1, dlt2));
  ASSERT_FALSE(dlt.order(dlt2, dlt1));
  // different data should have a proper inequality
  ASSERT_TRUE(dlt.order(dlt1, dlt3));
  ASSERT_FALSE(dlt.order(dlt3, dlt1));
}

struct lt_op_t
{
  int data;
  char distraction;
  bool operator<(const lt_op_t& o) const { return data < o.data; }
};

struct lt_free_t
{
  int data;
  char distraction;
};
bool operator<(const lt_free_t& o1, const lt_free_t& o2)
{
  return o1.data < o2.data;
}

struct gt_op_t
{
  int data;
  char distraction;
  bool operator>(const gt_op_t& o) const { return data > o.data; }
};

struct gt_free_t
{
  int data;
  char distraction;
};
bool operator>(const gt_free_t& o1, const gt_free_t& o2)
{
  return o1.data > o2.data;
}

struct order_t
{
  int data;
  char distraction;
};
bool order(const order_t& o1, const order_t& o2)
{
  return o1.data < o2.data;
}

using vector_typelist = testing::Types<
  lt_op_t,
  lt_free_t,
  gt_op_t,
  gt_free_t,
  order_t>;
template<class> struct DetectOrder_sources : testing::Test {};
TYPED_TEST_SUITE(DetectOrder_sources, vector_typelist);

/*
Demonstrate detection of order via each of the available source-types.
*/
TYPED_TEST(DetectOrder_sources, detection)
{
  DetectOrder<TypeParam> dlt;
  TypeParam mlt1{ 233, 'a' };
  TypeParam mlt2{ 233, 'b' };
  TypeParam mlt3{ 234, 'a' };
  // data is the same in both, so expect equality (show with both directions)
  ASSERT_FALSE(dlt.order(mlt1, mlt2));
  ASSERT_FALSE(dlt.order(mlt2, mlt1));
  // different data should have a proper inequality
  ASSERT_TRUE(dlt.order(mlt1, mlt3));
  ASSERT_FALSE(dlt.order(mlt3, mlt1));
}
