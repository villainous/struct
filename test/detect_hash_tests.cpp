
#include "detect_hash.inl"

// check this compiles BEFORE gtest leaks symbols
template class viln::DetectHash<double>;

#include <string>
#include <gtest/gtest.h>

using namespace viln;

/*
Demonstrate default signed-integer hash.
*/
TEST(DetectHash, DefaultIntHash)
{
  // signed integers
  DetectHash<int> dhi;
  // hash of positive numbers are positive numbers
  ASSERT_EQ(dhi.hash(1), 1);
  // different integers have different hashes
  ASSERT_NE(dhi.hash(-1), dhi.hash(1));
}

/*
Demonstrate default unsigned-integer hash.
*/
TEST(DetectHash, DefaultUIntHash)
{
  // unsigned integers
  DetectHash<unsigned char> dhc;
  // hash of positive numbers are positive numbers
  ASSERT_EQ(dhc.hash('a'), (uint64_t)'a');
  // different integers have different hashes
  ASSERT_NE(dhc.hash('z'), dhc.hash('\0'));
}

/*
Demonstrate default floating-point hash.
*/
TEST(DetectHash, DefaultFloatsHash)
{
  // floating point numbers
  DetectHash<float> dhf;
  // same value provides same hash
  ASSERT_EQ(dhf.hash(2.5f), dhf.hash(2.5f));
  // different floats have different hashes
  ASSERT_NE(dhf.hash(24000.0f), dhf.hash(24000.1f));
}

/*
Demonstrate that automatic detection of std::hash template-object can create
a working hash method for std::string objects.
*/
TEST(DetectHash, StdString)
{
  DetectHash<std::string> dhs;
  // show that we can compute a hash code at all
  ASSERT_NE(dhs.hash("!"), 0);
  // hash of equal strings must be equal
  ASSERT_EQ(dhs.hash("hello"), dhs.hash("hello"));
  // hash of unequal strings must not be equal
  ASSERT_NE(dhs.hash("hello"), dhs.hash("goodbye"));
}

/*
The `char` type is distinct from `signed char` and `unsigned char`. It is weird
so check that it works.
*/
TEST(DetectHash, UnspecifiedChar)
{
  DetectHash<char> dhc;
  // null-char maps to zero
  ASSERT_EQ(dhc.hash('\0'), 0);
  // some other char does not map to zero
  ASSERT_NE(dhc.hash('a'), 0);
}

struct misc_t
{
  uint64_t data;
};
class DetectHashOverride : DetectHash<misc_t>
{
public:
  virtual uint64_t hash(const misc_t& o) const { return o.data; }
};

/*
Demonstrate that we can successfully override the hash function detection.
*/
TEST(DetectHash, HashOverride)
{
  DetectHashOverride dh;
  misc_t fh1{ 1 };
  misc_t fh2{ 2 };
  // hash function we implemented should return the data directly
  ASSERT_EQ(dh.hash(fh1), 1);
  // different objects should hash to different values (usually)
  ASSERT_NE(dh.hash(fh1), dh.hash(fh2));
}

namespace viln
{
  template<typename>
  struct misc_tt {};

  // partial specialization
  template<typename T>
  class DetectHash<misc_tt<T>> 
  {
  public:
    virtual uint64_t hash(const misc_tt<T>& t) const
    {
      return 5;
    } 
  };

  // explicit specialization
  template<>
  class DetectHash<misc_tt<float>> 
  {
  public:
    virtual uint64_t hash(const misc_tt<float>& t) const
    {
      return 6;
    } 
  };
}

/*
Demonstrate the ability to use template-specialization in order to
override the default
*/
TEST(DetectHash, HashTemplateSpecial)
{
  // partial template specialization
  DetectHash<misc_tt<int>> ah_other_int;
  EXPECT_EQ(ah_other_int.hash(misc_tt<int>()), 5);
  // explicit template specialization
  DetectHash<misc_tt<float>> ah_other_float;
  EXPECT_EQ(ah_other_float.hash(misc_tt<float>()), 6);
}

struct hash_op_t
{
  uint64_t data;
  uint64_t hash(void) const { return data; }
};

struct hash_free_t
{
  uint64_t data;
};
uint64_t hash(const hash_free_t& o) { return o.data; }

using vector_typelist = testing::Types<
  hash_op_t,
  hash_free_t>;
template<class> struct DetectHash_sources : testing::Test {};
TYPED_TEST_SUITE(DetectHash_sources, vector_typelist);

/*
Demonstrate detection of hash-function via each of the available source-types.
*/
TYPED_TEST(DetectHash_sources, detection)
{
  DetectHash<TypeParam> dh;
  TypeParam fh1{ 12 };
  TypeParam fh2{ 13 };
  // hash function we implemented should return the data directly
  ASSERT_EQ(dh.hash(fh1), 12);
  // different objects should hash to different values (usually)
  ASSERT_NE(dh.hash(fh1), dh.hash(fh2));
}

/*
Show that DetectHash does *not* require STL headers. This requires the actual
test guts to be located in another file, because the gtest headers leak STL
stuff all over, breaking the test.
*/
#include "detect_hash_nostring.hpp"
TEST(DetectHash, NoString)
{
  ASSERT_TRUE(detect_hash_nostring());
}
