// cannot use gtest header because it leaks <string>
#include "detect_hash.inl"
#include "detect_hash_nostring.hpp"

using namespace viln;

/**
Show that DetectHash can be compiled if no STL headers are provided.
*/
bool detect_hash_nostring(void)
{
  DetectHash<int> dh;
  if (dh.hash(0)) return false;
  if (!dh.hash(1)) return false;
  return true;
}
