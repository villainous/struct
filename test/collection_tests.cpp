#include "collection.inl"

// check this compiles BEFORE gtest leaks symbols
template class viln::Collection<double>;

#include <gtest/gtest.h>
#include "array_iterator.inl"

using namespace viln;

#define EXAMPLE_SIZE_MAX 1024

/**
Example `Collection` implementation that uses a static array to provide
storage of integers. The resulting tests below are intended to verify the
default implementations of several methods provided in the `Collection`
interface itself. Usually these would be overridden with more performant
implementations specific to the collection implementation itself, and as such
ensuring test coverage for these methods needs this little workaround.
*/
class ExampleCollection : public Collection<int>
{
public:
  ExampleCollection(size_t size):
    data_(),
    size_(size < EXAMPLE_SIZE_MAX ? size : EXAMPLE_SIZE_MAX)
  {
    for (int idx = 0; idx < size_; ++idx)
    {
      data_[idx] = idx;
    }
  }

  virtual ~ExampleCollection(void)
  {
    // no-op
  }

  virtual Iterator<const int>* iterator(void) const override
  {
    return new ArrayIterator<const int>(data_, size_, 0);
  }

  bool addInt(int e)
  {
    if (size_ >= EXAMPLE_SIZE_MAX)
    {
      return false;
    }
    data_[size_++] = e;
    return true;
  }

  virtual bool add(const int& e) override
  {
    return addInt(e);
  }

  virtual bool add(int&& e) override
  {
    return addInt(e);
  }

  virtual bool remove(const int& e) override
  {
    for (int idx = 0; idx < size_; ++idx)
    {
      if (data_[idx] == e)
      {
        memmove(
          data_ + idx,
          data_ + idx + 1,
          sizeof(int) * (size_ - idx - 1));
        --size_;
        return true;
      }
    }
    return false;
  }

  virtual void clear(void) override
  {
    size_ = 0;
  }

  virtual bool contains(const int& e) const override
  {
    for (int idx = 0; idx < size_; ++idx)
    {
      if (data_[idx] == e)
      {
        return true;
      }
    }
    return false;
  }

  virtual size_t size(void) const override
  {
    return size_;
  }

  const int* getData(void)
  {
    return data_;
  }

  size_t getSize(void)
  {
    return size_;
  }

private:
  int data_[1024];
  size_t size_;
};

/**
Show that we can add all the elements from a specified collection to our
collection.
*/
TEST(Collection, AddAll)
{
  ExampleCollection coll(0);
  ASSERT_EQ(coll.size(), 0);
  ExampleCollection coll3(3);
  // add some elements
  ASSERT_TRUE(coll.addAll(coll3));
  ASSERT_EQ(coll.size(), 3);
  ASSERT_TRUE(coll.addAll(coll3));
  ASSERT_EQ(coll.size(), 6);
  // self-add
  ASSERT_TRUE(coll.addAll(coll));
  ASSERT_EQ(coll.size(), 12);
  const int* data = coll.getData();
  for (int idx = 0; idx < coll.size(); ++idx)
  {
    EXPECT_EQ(data[idx], idx % 3);
  }
}

/**
Show that removing all the elements of a specified collection from our
collection behaves as expected.
*/
TEST(Collection, RemoveAll)
{
  ExampleCollection coll(8);
  // remove some elements
  ExampleCollection other(0);
  other.add(3);
  other.add(7);
  other.add(5);
  ASSERT_TRUE(coll.removeAll(other));
  ASSERT_EQ(coll.size(), 5);
  // repeat does nothing
  ASSERT_FALSE(coll.removeAll(other));
  ASSERT_EQ(coll.size(), 5);
  // check that the values are as expected
  int expected5[5] = { 0,1,2,4,6 };
  const int* data5 = coll.getData();
  for (int idx = 0; idx < 5; ++idx)
  {
    EXPECT_EQ(data5[idx], expected5[idx]);
  }
  // self-removal
  ASSERT_TRUE(coll.removeAll(coll));
  ASSERT_TRUE(coll.isEmpty());
  // empty self-removal is a no-op
  ASSERT_FALSE(coll.removeAll(coll));
  ASSERT_TRUE(coll.isEmpty());
}

/**
Check that one collection being a "subset" of another works.
*/
TEST(Collection, ContainsAll)
{
  ExampleCollection coll(8);
  ExampleCollection other(0);
  // always contains empty set
  EXPECT_TRUE(coll.containsAll(other));
  // check containment of one element
  other.add(5);
  EXPECT_TRUE(coll.containsAll(other));
  // repeats should work fine
  other.add(5);
  other.add(5);
  other.add(5);
  EXPECT_TRUE(coll.containsAll(other));
  // an element not contained
  other.add(999);
  EXPECT_FALSE(coll.containsAll(other));
  // self containment
  EXPECT_TRUE(coll.containsAll(coll));
}

/**
Check that emptiness notion makes sense.
*/
TEST(Collection, IsEmpty)
{
  ExampleCollection coll(0);
  EXPECT_TRUE(coll.isEmpty());
  coll.add(5);
  EXPECT_FALSE(coll.isEmpty());
  coll.remove(5);
  EXPECT_TRUE(coll.isEmpty());
  coll.add(1);
  coll.add(3445);
  EXPECT_FALSE(coll.isEmpty());
  coll.clear();
  EXPECT_TRUE(coll.isEmpty());
}

/**
Check that we can create a duplicate of the underlying data.
*/
TEST(Collection, ToArray)
{
  ExampleCollection coll(12);
  int* dataCopy = coll.toArray();
  const int* data = coll.getData();
  ASSERT_NE(dataCopy, data);
  for (int idx = 0; idx < coll.size(); ++idx)
  {
    EXPECT_EQ(dataCopy[idx], data[idx]);
  }
  delete[] dataCopy;
}
