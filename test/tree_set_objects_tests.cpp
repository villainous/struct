#include <gtest/gtest.h>
#include <math.h>
#include "canary.hpp"
#include "tree_set.inl"

using namespace viln;

class CanaryTreeSet : public TreeSet<Canary>
{
public:
  // allow root node access for debugging
  const TreeNode<Canary>* root(void) const
  {
    return TreeSet<Canary>::root();
  }
protected:
  // provide an override of the default "less-than" detection
  bool order(const Canary& c1, const Canary& c2) const override
  {
    return c1.data < c2.data;
  }
};

/**
Check that when we directly add elements into the set constructed on the
heap they are *not* copied or moved.
*/
TEST(TreeSetObjects, DirectAddition)
{
  Canary c1 = Canary(1);
  Canary c2 = Canary(2);
  Canary c3 = Canary(3);
  {
    CanaryTreeSet coalmine;
    // RESET!
    Canary::reset();
    // add some elements to the set
    ASSERT_TRUE(coalmine.add(new Canary(1)));
    ASSERT_TRUE(coalmine.add(new Canary(2)));
    ASSERT_TRUE(coalmine.add(new Canary(3)));
    // check that the elements actually got put in correctly
    EXPECT_TRUE(coalmine.contains(c1));
    EXPECT_TRUE(coalmine.contains(c2));
    EXPECT_TRUE(coalmine.contains(c3));
    // the actual constructor should be called only once per element
    EXPECT_EQ(Canary::constructor, 3);
    // no moves!
    EXPECT_EQ(Canary::move, 0);
    // no copies!
    EXPECT_EQ(Canary::copy, 0);
    // elements should not be deleted until the set falls out of scope
    EXPECT_EQ(Canary::destructor, 0);
  }
  // once the set falls out of scope, the elements should be deleted
  EXPECT_EQ(Canary::destructor, 3);
}

/**
Check that element construction and destruction is correctly being invoked by
members of a set. Specifically, that the move-constructor is being correctly
invoked when adding elements to the set.
*/
TEST(TreeSetObjects, MoveConstructor)
{
  Canary c1 = Canary(1);
  Canary c2 = Canary(2);
  Canary c3 = Canary(3);
  {
    CanaryTreeSet coalmine;
    // RESET!
    Canary::reset();
    // add some elements to the set
    ASSERT_TRUE(coalmine.add(Canary(1)));
    ASSERT_TRUE(coalmine.add(Canary(2)));
    ASSERT_TRUE(coalmine.add(Canary(3)));
    // check that the elements actually got put in correctly
    EXPECT_TRUE(coalmine.contains(c1));
    EXPECT_TRUE(coalmine.contains(c2));
    EXPECT_TRUE(coalmine.contains(c3));
    // the actual constructor should be called only once per element
    EXPECT_EQ(Canary::constructor, 3);
    // since we started with capacity 3, only 3 moves should be needed
    EXPECT_EQ(Canary::move, 3);
    // move was called, NOT copy
    EXPECT_EQ(Canary::copy, 0);
    // elements should not be deleted until the set falls out of scope
    EXPECT_EQ(Canary::destructor, 0);
  }
  // once the set falls out of scope, the elements should be deleted
  EXPECT_EQ(Canary::destructor, 3);
}

/**
Check that element construction and destruction is correctly being invoked by
members of a set. Specifically, that the copy-constructor is being correctly
invoked when adding elements to the set.
*/
TEST(TreeSetObjects, CopyConstructor)
{
  Canary c1 = Canary(1);
  Canary c2 = Canary(2);
  Canary c3 = Canary(3);
  {
    CanaryTreeSet coalmine;
    // RESET!
    Canary::reset();
    // copy some elements into the set
    ASSERT_TRUE(coalmine.add(c1));
    ASSERT_TRUE(coalmine.add(c2));
    ASSERT_TRUE(coalmine.add(c3));
    // check that the elements actually got put in correctly
    EXPECT_TRUE(coalmine.contains(c1));
    EXPECT_TRUE(coalmine.contains(c2));
    EXPECT_TRUE(coalmine.contains(c3));
    // the elements were originally created prior to the reset
    EXPECT_EQ(Canary::constructor, 0);
    // since we started with capacity 3, only 3 copies should be needed
    EXPECT_EQ(Canary::copy, 3);
    // used copy, NOT move
    EXPECT_EQ(Canary::move, 0);
    // elements should not be deleted until the set falls out of scope
    EXPECT_EQ(Canary::destructor, 0);
  }
  // once the set falls out of scope, its elements are deleted
  EXPECT_EQ(Canary::destructor, 3);
}
