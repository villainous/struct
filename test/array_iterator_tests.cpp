#include "array_iterator.inl"

// check this compiles BEFORE gtest leaks symbols
template class viln::ArrayIterator<double>;

#include <gtest/gtest.h>

using namespace viln;

/**
Check that forward-iteration methods behave as expected.
*/
TEST(ArrayIterator, Forward)
{
  int arr1[] = { 1 };
  // a 0-length array would have no next element
  ArrayIterator<int> arit0(arr1, 0, 0);
  EXPECT_FALSE(arit0.hasNext());
  // a 1-length array can iterate exactly once (from the start)
  ArrayIterator<int> arit1(arr1, 1, 0);
  EXPECT_TRUE(arit1.hasNext());
  EXPECT_EQ(arit1.next(), 1);
  EXPECT_FALSE(arit1.hasNext());
}

/**
Check that reverse-iteration methods behave as expected.
*/
TEST(ArrayIterator, Reverse)
{
  int arr1[] = { 1 };
  // a 0-length array would have no next element
  ArrayIterator<int> arit0(arr1, 0, 0);
  EXPECT_FALSE(arit0.hasPrevious());
  // a 1-length array can iterate exactly sonce (from the end)
  ArrayIterator<int> arit1(arr1, 1, 1);
  EXPECT_TRUE(arit1.hasPrevious());
  EXPECT_EQ(arit1.previous(), 1);
  EXPECT_FALSE(arit1.hasPrevious());
}

/**
Check that lists of pointer types work as expected.
*/
TEST(ArrayIterator, StringArray)
{
  const char* arr2[] = { "alice", "bob" };
  ArrayIterator<const char*> strit(arr2, 2, 0);
  EXPECT_TRUE(strit.hasNext());
  EXPECT_STREQ(strit.next(), "alice");
  EXPECT_TRUE(strit.hasNext());
  EXPECT_STREQ(strit.next(), "bob");
  EXPECT_FALSE(strit.hasNext());
}

/**
Check that iterating over a list of elements accesses those elements correctly.
*/
TEST(ArrayIterator, Loop5)
{
  int arr5[] = { 1, 2, 999000, 4, 5 };
  int idx = 0;
  ArrayIterator<int> arit(arr5, 5, 0);
  EXPECT_TRUE(arit.hasNext());
  while (arit.hasNext())
  {
    EXPECT_EQ(arit.next(), arr5[idx++]);
  }
  EXPECT_FALSE(arit.hasNext());
  EXPECT_EQ(idx, 5);
}

/**
Check that indexing operations `nextIndex()` and `previousIndex()` can return
correct indices.
*/
TEST(ArrayIterator, Indexing)
{
  int arr[] = { 0, 1, 2, 3, 4 };
  // the first element has a next element but not a previous one
  ArrayIterator<int> arit0(arr, 5, 0);
  EXPECT_TRUE(arit0.hasNext());
  EXPECT_FALSE(arit0.hasPrevious());
  EXPECT_EQ(arit0.nextIndex(), 0);
  // the last element has a previous element but not a next one
  ArrayIterator<int> arit5(arr, 5, 5);
  EXPECT_FALSE(arit5.hasNext());
  EXPECT_TRUE(arit5.hasPrevious());
  EXPECT_EQ(arit5.previousIndex(), 4);
  // elements in the middle have both a previous and next element
  ArrayIterator<int> arit2(arr, 5, 2);
  EXPECT_TRUE(arit2.hasNext());
  EXPECT_TRUE(arit2.hasPrevious());
  EXPECT_EQ(arit2.nextIndex(), 2);
  EXPECT_EQ(arit2.previousIndex(), 1);
}

/**
Demonstrate that copying an array-iterator results in a copy with the same state
as the original.
*/
TEST(ArrayIterator, Copy)
{
  int arr[] = { 0, 1, 2, 3, 4 };
  // create an iterator over the list
  ArrayIterator<int> itr0(arr, 5);
  // check that a copy of the iterator visits each value
  ArrayIterator<int> itr1(itr0);
  int acc1;
  ASSERT_TRUE(itr1.hasNext());
  for (acc1 = 0; itr1.hasNext(); ++acc1) EXPECT_EQ(acc1, itr1.next());
  EXPECT_EQ(acc1, 5);
  EXPECT_FALSE(itr1.hasNext());
  // check that we can still use the original iterator
  int acc0;
  ASSERT_TRUE(itr0.hasNext());
  for (acc0 = 0; itr0.hasNext(); ++acc0) EXPECT_EQ(acc0, itr0.next());
  EXPECT_EQ(acc0, 5);
  EXPECT_FALSE(itr0.hasNext());
}
