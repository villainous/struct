
#include <gtest/gtest.h>
#include "vcrd.inl"

using namespace viln;

/*
Demonstrate that equality operators behave as expected.
*/
TEST(VCrd2, Equality)
{
  // self-equality
  vcrd_2i crd1A{ 1, 2 };
  EXPECT_TRUE(crd1A == crd1A);
  EXPECT_FALSE(crd1A != crd1A);
  // equals
  vcrd_2i crd1B{ 1, 2 };
  EXPECT_TRUE(crd1A == crd1B);
  EXPECT_FALSE(crd1A != crd1B);
  // not equals
  vcrd_2i crd2{ 3, 4 };
  EXPECT_FALSE(crd1A == crd2);
  EXPECT_TRUE(crd1A != crd2);
  // element-equality
  vcrd_2i crd3{ 9, 9 };
  EXPECT_TRUE(crd3 == 9);
  EXPECT_FALSE(crd3 != 9);
  vcrd_2i crd4{ 9, 8 };
  EXPECT_FALSE(crd4 == 9);
  EXPECT_TRUE(crd4 != 9);
}

/*
Demonstrate assignment to vcrd[2] from various types.
*/
TEST(VCrd2, Convert)
{
  vcrd_2i crd = {};
  EXPECT_EQ(crd, (vcrd_2i{ 0, 0 }));
  // vcrd-assign
  const vcrd_2i crdA = { 11, 111 };
  EXPECT_EQ((crd = crdA), (vcrd_2i{ 11, 111 }));
  EXPECT_EQ(crd, (vcrd_2i{ 11, 111 }));
  // ucrd-assign
  EXPECT_EQ(crd = ucrd::Y, (vcrd_2i{ 0, 1 }));
  EXPECT_EQ(crd, (vcrd_2i{ 0, 1 }));
  // element-assign
  EXPECT_EQ(crd = 234, (vcrd_2i{ 234, 234 }));
  EXPECT_EQ(crd, (vcrd_2i{ 234, 234 }));
  // vcrd<-assign
  const vcrd<int,1> crd1 = { 11 };
  EXPECT_EQ(crd = crd1, (vcrd_2i{ 11, 0 }));
  EXPECT_EQ(crd, (vcrd_2i{ 11, 0 }));
  // vcrd>-assign
  const vcrd<int,5> crd5 = { 5, 4, 3, 2, 1 };
  EXPECT_EQ(crd = crd5, (vcrd_2i{ 5, 4 }));
  EXPECT_EQ(crd, (vcrd_2i{ 5, 4 }));
}

/*
Verify that the `+` `+=` `++` operators behave as expected.
*/
TEST(VCrd2, Plus)
{
  const vcrd_2i crd1{ 1, 2 };
  const vcrd_2i crd2{ 3, 4 };
  vcrd_2i crd = {};
  // plus
  EXPECT_EQ((crd1 + crd2), (vcrd_2i{ 4, 6 }));
  // ucrd plus
  EXPECT_EQ((crd1 + ucrd::X), (vcrd_2i{ 2, 2 }));
  // element plus
  EXPECT_EQ((crd1 + 5), (vcrd_2i{ 6, 7 }));
  // assign-plus
  crd = crd1;
  EXPECT_EQ((crd += crd2), (vcrd_2i{ 4, 6 }));
  EXPECT_EQ(crd, (vcrd_2i{ 4, 6 }));
  // ucrd assign-plus
  crd = crd1;
  EXPECT_EQ((crd += ucrd::Y), (vcrd_2i{ 1, 3 }));
  EXPECT_EQ(crd, (vcrd_2i{ 1, 3 }));
  // element assign-plus
  crd = crd1;
  EXPECT_EQ((crd += 5), (vcrd_2i{ 6, 7 }));
  EXPECT_EQ(crd, (vcrd_2i{ 6, 7 }));
  // left-increment
  crd = crd1;
  EXPECT_EQ(++crd, (vcrd_2i{ 2, 3 }));
  EXPECT_EQ(crd, (vcrd_2i{ 2, 3 }));
}

/*
Verify that the `-` `-=` `--` operators behave as expected.
*/
TEST(VCrd2, Minus)
{
  const vcrd_2i crd1{ 1, 2 };
  const vcrd_2i crd2{ 3, 4 };
  vcrd_2i crd = {};
  // negate
  EXPECT_EQ(-crd1, (vcrd_2i{ -1, -2 }));
  // minus
  EXPECT_EQ((crd1 - crd2), (vcrd_2i{ -2, -2 }));
  // ucrd minus
  EXPECT_EQ((crd1 - ucrd::X), (vcrd_2i{ 0, 2 }));
  // element minus
  EXPECT_EQ((crd1 - 5), (vcrd_2i{ -4, -3 }));
  // assign-minus
  crd = crd1;
  EXPECT_EQ((crd -= crd2), (vcrd_2i{ -2, -2 }));
  EXPECT_EQ(crd, (vcrd_2i{ -2, -2 }));
  // ucrd assign-minus
  crd = crd1;
  EXPECT_EQ((crd -= ucrd::Y), (vcrd_2i{ 1, 1 }));
  EXPECT_EQ(crd, (vcrd_2i{ 1, 1 }));
  // element assign-minus
  crd = crd1;
  EXPECT_EQ((crd -= 5), (vcrd_2i{ -4, -3 }));
  EXPECT_EQ(crd, (vcrd_2i{ -4, -3 }));
  // left-decrement
  crd = crd1;
  EXPECT_EQ(--crd, (vcrd_2i{ 0, 1 }));
  EXPECT_EQ(crd, (vcrd_2i{ 0, 1 }));
}

/*
Verify that the `*` `*=` operators behave as expected.
*/
TEST(VCrd2, Multiply)
{
  const vcrd_2i crd1{ 1, 2 };
  const vcrd_2i crd2{ 3, -1 };
  const int fac = 12;
  vcrd_2i crd = {};
  // multiply
  EXPECT_EQ((crd1 * crd2), (vcrd_2i{ 3, -2 }));
  // element multiply
  EXPECT_EQ((crd1 * fac), (vcrd_2i{ 12, 24 }));
  // assign-multiply
  crd = crd1;
  EXPECT_EQ((crd *= crd2), (vcrd_2i{ 3, -2 }));
  EXPECT_EQ(crd, (vcrd_2i{ 3, -2 }));
  // element assign-multiply
  crd = crd1;
  EXPECT_EQ((crd *= fac), (vcrd_2i{ 12, 24 }));
  EXPECT_EQ(crd, (vcrd_2i{ 12, 24 }));
}

/*
Verify that the `/` `/=` operators and `div_floor` behave as expected.
*/
TEST(VCrd2, Divide)
{
  const vcrd_2i crd1{ 12, 24 };
  const vcrd_2i crd2{ 11, -11 };
  const int fac = 11;
  vcrd_2i crd = {};
  // div
  EXPECT_EQ((crd1 / crd2), (vcrd_2i{ 1, -2 }));
  // element div
  EXPECT_EQ((crd1 / fac), (vcrd_2i{ 1, 2 }));
  // div-assign
  crd = crd1;
  EXPECT_EQ((crd /= crd2), (vcrd_2i{ 1, -2 }));
  EXPECT_EQ(crd, (vcrd_2i{ 1, -2 }));
  // element div-assign
  crd = crd1;
  EXPECT_EQ((crd /= fac), (vcrd_2i{ 1, 2 }));
  EXPECT_EQ(crd, (vcrd_2i{ 1, 2 }));
  // div_floor
  EXPECT_EQ(div_floor(crd1, crd2), (vcrd_2i{ 1, -3 }));
  // element div_floor
  EXPECT_EQ(div_floor(crd1, fac), (vcrd_2i{ 1, 2 }));
}

/*
Verify that the `%` `%=` operators and `mod_floor` behave as expected.
*/
TEST(VCrd2, Mod)
{
  const vcrd_2i crd1{ 12, 24 };
  const vcrd_2i crd2{ 11, -11 };
  const int fac = 11;
  vcrd_2i crd = {};
  // mod
  EXPECT_EQ((crd1 % crd2), (vcrd_2i{ 1, 2 }));
  // element mod
  EXPECT_EQ((crd1 % fac), (vcrd_2i{ 1, 2 }));
  // mod-assign
  crd = crd1;
  EXPECT_EQ((crd %= crd2), (vcrd_2i{ 1, 2 }));
  EXPECT_EQ(crd, (vcrd_2i{ 1, 2 }));
  // element mod-assign
  crd = crd1;
  EXPECT_EQ((crd %= fac), (vcrd_2i{ 1, 2 }));
  EXPECT_EQ(crd, (vcrd_2i{ 1, 2 }));
  // mod_floor
  EXPECT_EQ(mod_floor(crd1, crd2), (vcrd_2i{ 1, -9 }));
  // element mod_floor
  EXPECT_EQ(mod_floor(crd1, fac), (vcrd_2i{ 1, 2 }));
}

/*
Demonstrate math operations.
*/
TEST(VCrd2, Math)
{
  const vcrd_2i crd1{ 12, -24 };
  const vcrd_2i crd2{ 40, -40 };
  // abs
  EXPECT_EQ(abs(crd1), (vcrd_2i{ 12, 24 }));
  EXPECT_EQ(abs(-crd1), (vcrd_2i{ 12, 24 }));
  // sgn
  EXPECT_EQ(sgn(crd1), (vcrd_2i{ 1, -1 }));
  EXPECT_EQ(sgn(-crd1), (vcrd_2i{ -1, 1 }));
  // max1
  EXPECT_EQ(max(crd1), 12);
  // max2
  EXPECT_EQ(max(crd1, crd2), (vcrd_2i{ 40, -24 }));
  EXPECT_EQ(max(crd1, 11), (vcrd_2i{ 12, 11 }));
  // min1
  EXPECT_EQ(min(crd1), -24);
  // min2
  EXPECT_EQ(min(crd1, crd2), (vcrd_2i{ 12, -40 }));
  EXPECT_EQ(min(crd1, 11), (vcrd_2i{ 11, -24 }));
}

/*
Demonstrate vector-math operations.
*/
TEST(VCrd2, Vector)
{
  const vcrd_2i crd1{ 12, -24 };
  const vcrd_2i crd2{ 3, 10 };
  // volume
  EXPECT_EQ(volume(crd1), 288);
  EXPECT_EQ(volume(-crd1), 288);
  // dot
  EXPECT_EQ(dot(crd1, crd2), -204);
  // lengths
  EXPECT_EQ(length_max(crd1), 24);
  EXPECT_EQ(length1(crd1), 36);
  EXPECT_EQ(length(crd1), 26);
  EXPECT_GE(length_oct(crd1), length_max(crd1));
  EXPECT_LE(length_oct(crd1), length1(crd1));
}

/*
Demonstrate 2d vector creation and access.
*/
TEST(VCrd2, 2x2)
{
  vcrd<vcrd<int, 2>, 2> crd2x2{
    vcrd_2i{  1,  2 },
    vcrd_2i{  5,  6 } };
  // spot-check the values are accessible
  EXPECT_EQ(crd2x2[0][0], 1);
  EXPECT_EQ(crd2x2[0][1], 2);
  EXPECT_EQ(crd2x2[1][0], 5);
  EXPECT_EQ(crd2x2[1][1], 6);
}

/*
Demonstrate single-line construction of a coord from a value.
*/
TEST(VCrd2, make_vcrd)
{
  vcrd<int, 2> crd1 = make_vcrd(2344);
  EXPECT_EQ(crd1[0], 2344);
  EXPECT_EQ(crd1[1], 2344);
}
