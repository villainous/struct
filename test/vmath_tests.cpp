
#include <limits.h>
#include <gtest/gtest.h>
#include "vmath.inl"

using namespace viln;

/*
Demonstrate the min/max functions correctly identify minimum and maximum
values for both float and integer inputs.
*/
TEST(VMath, MinMax)
{
  // integers
  EXPECT_EQ(viln::min(-24, 24), -24);
  EXPECT_EQ(viln::max(-24, 24), 24);
  EXPECT_EQ(viln::min(5, 5), 5);
  EXPECT_EQ(viln::max(5, 5), 5);
  EXPECT_EQ(viln::min<int32_t>(INT32_MIN, INT32_MAX), INT32_MIN);
  EXPECT_EQ(viln::max<int32_t>(INT32_MIN, INT32_MAX), INT32_MAX);
  // floats
  EXPECT_EQ(viln::min(-24.0f, 24.0f), -24.0f);
  EXPECT_EQ(viln::max(-24.0f, 24.0f), 24.0f);
  EXPECT_EQ(viln::min(4.0f, 4.0f), 4.0f);
  EXPECT_EQ(viln::max(4.0f, 4.0f), 4.0f);
  EXPECT_EQ(viln::min<float>(-HUGE_VALF, HUGE_VALF), -HUGE_VALF);
  EXPECT_EQ(viln::max<float>(-HUGE_VALF, HUGE_VALF), HUGE_VALF);
}

/*
Demonstrate the abs/sgn functions correctly identify absolute-value and sign
values for both float and integer inputs.
*/
TEST(VMath, AbsVal)
{
  // integers
  EXPECT_EQ(viln::abs(24), 24);
  EXPECT_EQ(viln::sgn(24), 1);
  EXPECT_EQ(viln::abs(-24), 24);
  EXPECT_EQ(viln::sgn(-24), -1);
  EXPECT_EQ(viln::abs(0), 0);
  EXPECT_EQ(viln::sgn(0), 0);
  // floats
  EXPECT_EQ(viln::abs(24.0f), 24.0f);
  EXPECT_EQ(viln::sgn(24.0f), 1.0f);
  EXPECT_EQ(viln::abs(-24.0f), 24.0f);
  EXPECT_EQ(viln::sgn(-24.0f), -1.0f);
  EXPECT_EQ(viln::abs(0.0f), 0.0f);
  EXPECT_EQ(viln::sgn(0.0f), 0.0f);
  EXPECT_EQ(viln::abs<float>(HUGE_VALF), HUGE_VALF);
  EXPECT_EQ(viln::sgn<float>(HUGE_VALF), 1.0f);
  EXPECT_EQ(viln::abs<float>(-HUGE_VALF), HUGE_VALF);
  EXPECT_EQ(viln::sgn<float>(-HUGE_VALF), -1.0f);
}

/*
Demonstrate modulus specializations for int/float/double.
*/
TEST(VMath, Mod)
{
  // check some specific positive number
  EXPECT_EQ(mod(290, 3), 2);
  // negative denominator
  EXPECT_EQ(mod(290, -3), 2);
  // negative numerator
  EXPECT_EQ(mod(-290, 3), -2);
  // both negative
  EXPECT_EQ(mod(-290, -3), -2);
}

/*
Check `div_floor` provides the floor of the dividend.
*/
TEST(VMath, DivFloor)
{
  // check some specific large number
  EXPECT_EQ(div_floor(290000, 3), 96666);
  // negative denominator
  EXPECT_EQ(div_floor(290000, -3), -96667);
  // negative numerator
  EXPECT_EQ(div_floor(-290000, 3), -96667);
  // both negative
  EXPECT_EQ(div_floor(-290000, -3), 96666);
}

/*
Check `div_floor` provides the ceiling of the dividend.
*/
TEST(VMath, DivCeil)
{
  // check some specific large number
  EXPECT_EQ(div_ceil(290000, 3), 96667);
  // negative denominator
  EXPECT_EQ(div_ceil(290000, -3), -96666);
  // negative numerator
  EXPECT_EQ(div_ceil(-290000, 3), -96666);
  // both negative
  EXPECT_EQ(div_ceil(-290000, -3), 96667);
}

/*
Check that `mod_floor` provides the remainder of `div_floor`.
*/
TEST(VMath, ModFloor)
{
  // check some specific positive number
  EXPECT_EQ(mod_floor(290, 3), 2);
  EXPECT_EQ((3 * div_floor(290, 3)) + mod_floor(290, 3), 290);
  // negative denominator
  EXPECT_EQ(mod_floor(290, -3), -1);
  EXPECT_EQ((-3 * div_floor(290, -3)) + mod_floor(290, -3), 290);
  // negative numerator
  EXPECT_EQ(mod_floor(-290, 3), 1);
  EXPECT_EQ((3 * div_floor(-290, 3)) + mod_floor(-290, 3), -290);
  // both negative
  EXPECT_EQ(mod_floor(-290, -3), -2);
  EXPECT_EQ((-3 * div_floor(-290, -3)) + mod_floor(-290, -3), -290);
}

/*
Check that `mod_ceil` provides the remainder of `div_ceil`.
*/
TEST(VMath, ModCeil)
{
  // check some specific positive number
  EXPECT_EQ(mod_ceil(290, 3), -1);
  EXPECT_EQ((3 * div_ceil(290, 3)) + mod_ceil(290, 3), 290);
  // negative denominator
  EXPECT_EQ(mod_ceil(290, -3), 2);
  EXPECT_EQ((-3 * div_ceil(290, -3)) + mod_ceil(290, -3), 290);
  // negative numerator
  EXPECT_EQ(mod_ceil(-290, 3), -2);
  EXPECT_EQ((3 * div_ceil(-290, 3)) + mod_ceil(-290, 3), -290);
  // both negative
  EXPECT_EQ(mod_ceil(-290, -3), 1);
  EXPECT_EQ((-3 * div_ceil(-290, -3)) + mod_ceil(-290, -3), -290);
}

/*
Check that `mod_floor` and `div_floor` provide consistent results.
*/
TEST(VMath, XYFloor)
{
  // check some specific large number
  EXPECT_EQ((div_floor(290000, 3) * 3) + mod_floor(290000, 3), 290000);
  // negative denominator
  EXPECT_EQ((div_floor(290000, -3) * -3) + mod_floor(290000, -3), 290000);
  // negative numerator
  EXPECT_EQ((div_floor(-290000, 3) * 3) + mod_floor(-290000, 3), -290000);
  // both negative
  EXPECT_EQ((div_floor(-290000, -3) * -3) + mod_floor(-290000, -3), -290000);
}

/*
Check that `clamp(float...)` can limit to a clopen range.
*/
TEST(VMath, ClampF)
{
  // < min
  EXPECT_EQ(clamp(0.0F, 1.0F, -1000.0F), 0.0F);
  // = min
  EXPECT_EQ(clamp(0.0F, 1.0F, 0.0F), 0.0F);
  // middle
  EXPECT_EQ(clamp(0.0F, 1.0F, 0.999F), 0.999F);
  // = max
  float c1 = clamp(0.0F, 1.0F, 1.0F);
  EXPECT_LT(c1, 1.0);
  EXPECT_GT(c1, 0.9999F);
  // > max
  float c2 = clamp(0.0F, 1.0F, 1000.0F);
  EXPECT_LT(c2, 1.0);
  EXPECT_GT(c2, 0.9999F);
}

/*
Check that `clamp(double...)` can limit to a clopen range.
*/
TEST(VMath, ClampD)
{
  // < min
  EXPECT_EQ(clamp(0.0, 1.0, -1000.0), 0.0);
  // = min
  EXPECT_EQ(clamp(0.0, 1.0, 0.0), 0.0);
  // middle
  EXPECT_EQ(clamp(0.0, 1.0, 0.999), 0.999);
  // = max
  double c1 = clamp(0.0, 1.0, 1.0);
  EXPECT_LT(c1, 1.0);
  EXPECT_GT(c1, 0.99999999);
  // > max
  double c2 = clamp(0.0, 1.0, 1000.0);
  EXPECT_LT(c2, 1.0);
  EXPECT_GT(c2, 0.99999999);
}

/*
Check that `clamp(int...)` can limit to a clopen range.
*/
TEST(VMath, ClampI)
{
  // < min
  EXPECT_EQ(clamp(0, 10, -1000), 0);
  // = min
  EXPECT_EQ(clamp(0, 10, 0), 0);
  // middle
  EXPECT_EQ(clamp(0, 10, 9), 9);
  // = max
  EXPECT_EQ(clamp(0, 10, 10), 9);
  // > max
  EXPECT_EQ(clamp(0, 10, 1000), 9);
}

/*
Demonstrate the ability to hash all the builtin numerics.
*/
TEST(VMath, Hash)
{
  EXPECT_NE(hash(uint64_t(24)), 0);
  EXPECT_NE(hash(int64_t(24)), 0);
  EXPECT_NE(hash(uint32_t(24)), 0);
  EXPECT_NE(hash(int32_t(24)), 0);
  EXPECT_NE(hash(uint16_t(24)), 0);
  EXPECT_NE(hash(int16_t(24)), 0);
  EXPECT_NE(hash(uint8_t(24)), 0);
  EXPECT_NE(hash(int8_t(24)), 0);
  EXPECT_NE(hash((unsigned char)(24)), 0);
  EXPECT_NE(hash(char(24)), 0);
  EXPECT_NE(hash(float(24.0f)), 0);
  EXPECT_NE(hash(double(24.0)), 0);
}
