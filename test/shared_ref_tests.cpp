#include "shared_ref.inl"

// check this compiles BEFORE gtest leaks symbols
template class viln::SharedRef<double>;

#include <gtest/gtest.h>
#include "hash_map.inl"
#include "canary.hpp"

using namespace viln;

/**
Demonstrate that the data stored in a shared-ref can be accessed.
*/
TEST(SharedRef, Access)
{
  SharedRef<int32_t> derp = new int32_t(777);
  EXPECT_EQ(derp, 777);
}

/**
Demonstrate that a copy of a shared-ref has access to the data of the original.
*/
TEST(SharedRef, Copy)
{
  SharedRef<int32_t> i1 = new int32_t(456);
  SharedRef<int32_t> i2 = i1;
  // values match
  EXPECT_EQ(i1, 456);
  EXPECT_EQ(i2, 456);
}

/**
Demonstrate that two refs to the same data affect one another.
*/
TEST(SharedRef, DataMod)
{
  SharedRef<int32_t> i1 = new int32_t(456);
  SharedRef<int32_t> i2 = i1;
  // values match
  EXPECT_EQ(i1, 456);
  EXPECT_EQ(i2, 456);
  // alter the value
  ++i2.data();
  // changes reflected in BOTH
  EXPECT_EQ(i1, 457);
  EXPECT_EQ(i2, 457);
  // points to same address
  EXPECT_EQ(&((int32_t&)i1), &((int32_t&)i2));
}

/**
Demonstrate that the shared data is not deleted when the original ref is
deleted if there are other refs in scope.
*/
TEST(SharedRef, SwitchOut)
{
  SharedRef<int32_t>* i1p = new SharedRef<int32_t>(new int32_t(456));
  SharedRef<int32_t> i2 = *i1p;
  // values match
  EXPECT_EQ(*i1p, 456);
  EXPECT_EQ(i2, 456);
  // delete the original
  delete i1p;
  // values match
  EXPECT_EQ(i2, 456);
}

/**
Check that shared memory is deleted exactly once, even if many references
are created and destroyed.
*/
TEST(SharedRef, Objects)
{
  // RESET!
  Canary::reset();
  {
    // first ref created
    SharedRef<Canary> i1 = new Canary(23);
    // constructor called once, nothing else
    EXPECT_EQ(Canary::constructor, 1);
    EXPECT_EQ(Canary::move, 0);
    EXPECT_EQ(Canary::copy, 0);
    EXPECT_EQ(Canary::destructor, 0);
    // cycle through a bunch of refs
    for (int idx = 0; idx < 1000; ++idx)
    {
      SharedRef<Canary> ii = i1;
      EXPECT_EQ(ii.data().data, 23);
    }
    // still the same as the first construction
    EXPECT_EQ(Canary::constructor, 1);
    EXPECT_EQ(Canary::move, 0);
    EXPECT_EQ(Canary::copy, 0);
    EXPECT_EQ(Canary::destructor, 0);
  }
  // original fell out of scope so destructor now called
  EXPECT_EQ(Canary::destructor, 1);
}

/**
Demonstrate that an object which keeps at least one living reference is "good"
until the last reference falls out of scope when many references are created.
*/
TEST(SharedRef, RollingObjects)
{
  // RESET!
  Canary::reset();
  // first ref created
  SharedRef<Canary>* i1p = new SharedRef<Canary>(new Canary(-66));
  // constructor called once, nothing else
  EXPECT_EQ(Canary::constructor, 1);
  EXPECT_EQ(Canary::move, 0);
  EXPECT_EQ(Canary::copy, 0);
  EXPECT_EQ(Canary::destructor, 0);
  // now roll over the shared-ref repeatedly
  for (int idx = 0; idx < 100; ++idx)
  {
    SharedRef<Canary>* i2p = new SharedRef<Canary>(*i1p);
    // create extra intermediate copies as we go
    for (int jdx = 0; jdx < idx; ++jdx)
    {
      const SharedRef<Canary> i3 = *i2p;
      EXPECT_EQ(i3.data().data, -66);
    }
    // roll-over to another copy
    delete i1p;
    i1p = i2p;
  }
  // still the same as the first construction
  EXPECT_EQ(Canary::constructor, 1);
  EXPECT_EQ(Canary::move, 0);
  EXPECT_EQ(Canary::copy, 0);
  EXPECT_EQ(Canary::destructor, 0);
  delete i1p;
  // deleted, so destructor now called, even though this is not the original ref
  EXPECT_EQ(Canary::destructor, 1);
}


class TestInt
{
public:
  TestInt(int dat) : data(dat) {}
  TestInt(const TestInt& o) = delete;
  bool operator==(const TestInt& o) const { return o.data == data; }

  int data;
};

/**
Demonstrate use of shared-ref within a map object.
*/
TEST(SharedRef, HashMap)
{
  // RESET!
  Canary::reset();
  // add some values to a map
  HashMap<int32_t, SharedRef<Canary>> m1;
  // add a shared-ref
  m1.add(1, SharedRef<Canary>(new Canary(111)));
  // implicit conversion from canary-pointer
  m1.add(2, new Canary(22));
  EXPECT_EQ(m1[1].data().data, 111);
  EXPECT_EQ(m1[2].data().data, 22);
  // constructor called twice, nothing else
  EXPECT_EQ(Canary::constructor, 2);
  EXPECT_EQ(Canary::move, 0);
  EXPECT_EQ(Canary::copy, 0);
  // create a copy of the map
  HashMap<int32_t, SharedRef<Canary>> m2(m1);
  EXPECT_EQ(m2[1].data().data, 111);
  EXPECT_EQ(m2[2].data().data, 22);
  // nothing new since construction of first map
  EXPECT_EQ(Canary::constructor, 2);
  EXPECT_EQ(Canary::move, 0);
  EXPECT_EQ(Canary::copy, 0);
  // clearing the first map does not affect second
  m1.clear();
  EXPECT_TRUE(m1.isEmpty());
  EXPECT_EQ(m2[1].data().data, 111);
  EXPECT_EQ(m2[2].data().data, 22);
  // nothing yet destroyed
  EXPECT_EQ(Canary::destructor, 0);
  // clearing the second map finally calls the destructor
  m2.clear();
  EXPECT_EQ(Canary::destructor, 2);
}

/**
Demonstrate that shared-ref equality is by pointer, not value.
*/
TEST(SharedRef, Equal)
{
  // create a pair of refs to separate pointers but with the same value
  SharedRef<int> s1(new int(2245));
  SharedRef<int> s2(new int(2245));
  SharedRef<int> s3(new int(-1));
  // verify self-equality
  EXPECT_EQ(s1, s1);
  EXPECT_EQ(s2, s2);
  EXPECT_EQ(s3, s3);
  // unequal VALUES and unequal REFS
  EXPECT_NE((int&)(s1), (int&)(s3));
  EXPECT_NE(s1, s3);
  // equal VALUES but unequal REFS
  EXPECT_EQ((int&)(s1), (int&)(s2));
  EXPECT_NE(s1, s2);
  // make two refs that share a pool with s1
  SharedRef<int> s1a = s1;
  SharedRef<int> s1b = s1;
  // each ref in the same pool is equal in both VALUE and REF
  EXPECT_EQ((int&)(s1), (int&)(s1a));
  EXPECT_EQ((int&)(s1), (int&)(s1b));
  EXPECT_EQ((int&)(s1a), (int&)(s1b));
  EXPECT_EQ(s1, s1a);
  EXPECT_EQ(s1, s1b);
  EXPECT_EQ(s1a, s1b);
}

/**
Demonstrate equality between a ref and a naked element-reference.
*/
TEST(SharedRef, EqualElement)
{
  // create a ref from a pointer (takes ownership!!!)
  int* s1p = new int(1337);
  SharedRef<int> s1 = s1p;
  // a variety of ways of getting a reference to the same address
  int& s1pr = *s1p;
  EXPECT_EQ(s1, s1pr);
  EXPECT_EQ(s1pr, s1);
  int& s1r = s1;
  EXPECT_EQ(s1, s1r);
  EXPECT_EQ(s1r, s1);
  SharedRef<int> s1a(new int(1337));
  int& s1ar = s1a;
  EXPECT_EQ(s1, s1ar);
  EXPECT_EQ(s1ar, s1);
  // a different value on the stack
  int s2s = 1337;
  EXPECT_EQ(s1, s2s);
  EXPECT_EQ(s2s, s1);
}
