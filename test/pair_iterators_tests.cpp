#include "pair_iterators.inl"

template class viln::ConstKeyIterator<double, double>;
template class viln::ConstValueIterator<double, double>;
template class viln::MutableValueIterator<double, double>;
