#include "tree_priority_queue.inl"

// check this compiles BEFORE gtest leaks symbols
template class viln::TreePriorityQueue<double, double>;

#include <gtest/gtest.h>

using namespace viln;

/**
Demonstrate the behavior of "add" without collisions.
*/
TEST(TreePriorityQueue, SimpleAdd)
{
  // add some values to a queue
  TreePriorityQueue<int,int> pq;
  int i10 = 10;
  pq.add(1, i10);
  pq.add(3, new int(30));
  pq.add(2, 20);
  ASSERT_EQ(pq.size(), 3);
  // check values are as expected
  EXPECT_EQ(pq.next(), 10);
  EXPECT_EQ(pq.next(), 20);
  EXPECT_EQ(pq.next(), 30);
  ASSERT_EQ(pq.size(), 0);
  ASSERT_FALSE(pq.hasNext());
}

/**
Demonstrate the behavior of "add" with collisions.
*/
TEST(TreePriorityQueue, AddCollisions)
{
  // add values to the queue, mixing priorities
  TreePriorityQueue<int, int> pq;
  pq.add(3, 8);
  pq.add(1, 0);
  pq.add(2, 4);
  pq.add(1, 1);
  pq.add(2, 5);
  pq.add(1, 2);
  pq.add(2, 6);
  pq.add(1, 3);
  pq.add(2, 7);
  ASSERT_EQ(pq.size(), 9);
  // keep track of which integers have been returned
  bool checks[9] = { false };
  // check that the values with priority 1 are returned first
  for (int idx = 0; idx < 4; ++idx)
  {
    ASSERT_TRUE(pq.hasNext());
    checks[pq.next()] = true;
  }
  for (int idx = 0; idx < 4; ++idx)
  {
    EXPECT_TRUE(checks[idx]);
  }
  // check that the values with priority 2 are returned next
  for (int idx = 4; idx < 8; ++idx)
  {
    ASSERT_TRUE(pq.hasNext());
    checks[pq.next()] = true;
  }
  for (int idx = 4; idx < 8; ++idx)
  {
    EXPECT_TRUE(checks[idx]);
  }
  // check that the value we put in with priority 3 is returned last
  EXPECT_FALSE(checks[8]);
  ASSERT_TRUE(pq.hasNext());
  EXPECT_EQ(pq.next(), 8);
  // queue should now be empty
  ASSERT_EQ(pq.size(), 0);
  ASSERT_FALSE(pq.hasNext());
}

/**
Check that we can empty out a queue and then re-populate it.
*/
TEST(TreePriorityQueue, Refill)
{
  // put some values in a queue
  TreePriorityQueue<int, int> pq;
  pq.add(1, 100);
  pq.add(2, 200);
  ASSERT_EQ(pq.size(), 2);
  // consume all the values in the queue
  ASSERT_TRUE(pq.hasNext());
  EXPECT_EQ(pq.next(), 100);
  ASSERT_TRUE(pq.hasNext());
  EXPECT_EQ(pq.next(), 200);
  ASSERT_EQ(pq.size(), 0);
  ASSERT_FALSE(pq.hasNext());
  // put some more values in the queue
  pq.add(1, 1000);
  pq.add(2, 2000);
  ASSERT_EQ(pq.size(), 2);
  // carefully check the repopulated queue
  ASSERT_TRUE(pq.hasNext());
  EXPECT_EQ(pq.next(), 1000);
  ASSERT_TRUE(pq.hasNext());
  EXPECT_EQ(pq.next(), 2000);
  ASSERT_EQ(pq.size(), 0);
  ASSERT_FALSE(pq.hasNext());
}

/**
Check that we can empty out a queue and then re-populate it.
*/
TEST(TreePriorityQueue, Clear)
{
  // put some values in a queue
  TreePriorityQueue<int, int> pq;
  pq.add(1, 100);
  pq.add(2, 200);
  ASSERT_EQ(pq.size(), 2);
  // clear the elements from the queue
  pq.clear();
  ASSERT_EQ(pq.size(), 0);
  ASSERT_FALSE(pq.hasNext());
  // put some more values in the queue
  pq.add(1, 1000);
  pq.add(2, 2000);
  ASSERT_EQ(pq.size(), 2);
  // carefully check the repopulated queue
  ASSERT_TRUE(pq.hasNext());
  EXPECT_EQ(pq.next(), 1000);
  ASSERT_TRUE(pq.hasNext());
  EXPECT_EQ(pq.next(), 2000);
  ASSERT_EQ(pq.size(), 0);
  ASSERT_FALSE(pq.hasNext());
}
