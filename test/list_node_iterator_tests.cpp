#include "list_node_iterator.inl"

// check this compiles BEFORE gtest leaks symbols
template class viln::ListNodeIterator<double>;

#include <gtest/gtest.h>
#include "list_node.inl"

using namespace viln;

/**
Simple list of integers from 0 to N for testing. Primaily helpful for
cleaning up `ListNode`s after test completion.
*/
class IntList
{
public:
  IntList(int max)
  {
    ListNode<int>* prev = new ListNode<int>(new int(0));
    first_ = prev;
    for (int idx = 1; idx <= max; ++idx)
    {
      ListNode<int>* next = new ListNode<int>(new int(idx));
      prev->append(next);
      prev = next;
    }
  }

  ~IntList(void)
  {
    delete first_;
  }

  ListNode<int>* first(void)
  {
    return first_;
  }

private:
  ListNode<int>* first_;
};

/**
Test simple iteration over elements.
*/
TEST(ListNodeIterator, SimpleIteration)
{
  IntList list5(5);
  ListNodeIterator<int> itr(nullptr, list5.first());
  int acc = 0;
  // iterate forwards, checking each value
  ASSERT_TRUE(itr.hasNext());
  ASSERT_FALSE(itr.hasPrevious());
  while (itr.hasNext())
  {
    EXPECT_EQ(itr.next(), acc++);
  }
  // we traversed 6 elements
  ASSERT_EQ(acc, 6);
  // iterate backwards, checking each value
  ASSERT_FALSE(itr.hasNext());
  ASSERT_TRUE(itr.hasPrevious());
  while (itr.hasPrevious())
  {
    EXPECT_EQ(itr.previous(), --acc);
  }
  // should return to initial state
  ASSERT_EQ(acc, 0);
  ASSERT_TRUE(itr.hasNext());
  ASSERT_FALSE(itr.hasPrevious());
}

/**
Demonstrate that iterator-snapping can repair the state of the iterator
following the adding of elements in the middle of the iterator position.
*/
TEST(ListNodeIterator, Snap)
{
  IntList list5(5);
  // create an inconsistent iterator
  ListNodeIterator<int> itr(
    list5.first(),
    list5.first()->nextNode()->nextNode());
  // snap the iterator to the first node
  itr.snapToPrevious();
  // the next should be 1, not 2
  ASSERT_EQ(itr.next(), 1);
  ASSERT_EQ(itr.previous(), 1);
  // add an element to make the list inconsistent again
  list5.first()->append(new ListNode<int>(new int(99)));
  // snap the iterator to the first node again
  itr.snapToNext();
  ASSERT_EQ(itr.previous(), 99);
  ASSERT_EQ(itr.next(), 99);
}
