
include( GoogleTest )

set( PWD "${CMAKE_CURRENT_SOURCE_DIR}" )

file( GLOB_RECURSE CPP_HEADERS "${PWD}" "*.hpp" )
file( GLOB_RECURSE CPP_SOURCES "${PWD}" "*.cpp" )
file( GLOB_RECURSE CPP_INLINES "${PWD}" "*.inl" )

target_sources(
  VilnTests
  PUBLIC ${CPP_HEADERS} ${CPP_INLINES}
  PRIVATE ${CPP_SOURCES} )

gtest_add_tests(
  TARGET VilnTests
  SOURCES ${CPP_SOURCES} )
