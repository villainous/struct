
#include <utility>
#include "tree_map.inl"

// check this compiles BEFORE gtest leaks symbols
template class viln::TreeMap<double,double>;

#include <gtest/gtest.h>
#include "array_list.inl"

using namespace viln;

// expose the protected methods for testing
class TestTreeMap : public TreeMap<int, int>
{
public:
  TreeNode<KeyVal<int, int>>* const root(void)
  {
    return TreeMap<int, int>::root();
  }

  const TreeNode<KeyVal<int, int>>* root(void) const
  {
    return TreeMap<int, int>::root();
  }
};

/*
Demonstrate root node access.
*/
TEST(TreeMap, RootNode)
{
  // create a map
  TestTreeMap tm;
  const TestTreeMap& ctm = tm;
  // if no elements, then no root
  EXPECT_EQ(tm.root(), nullptr);
  // if there is an element then that's the root
  ASSERT_TRUE(tm.add(1, 10));
  TreeNode<KeyVal<int, int>>* rt = tm.root();
  ASSERT_TRUE(rt);
  EXPECT_EQ(rt->data().key(), 1);
  EXPECT_EQ(rt->data().value(), 10);
  // check that the constant root is available as well
  const TreeNode<KeyVal<int, int>>* crt = ctm.root();
  ASSERT_TRUE(crt);
  EXPECT_EQ(crt->data().key(), 1);
  EXPECT_EQ(crt->data().value(), 10);
}

/*
Demonstrate the behavior of "add" both with and without collisions.
*/
TEST(TreeMap, Add)
{
  // add some values to a map
  TreeMap<int,int> tm;
  const TreeMap<int, int>& ctm = tm;
  int i10 = 10;
  ASSERT_TRUE(tm.add(1, i10));
  ASSERT_TRUE(tm.add(2, 20));
  ASSERT_TRUE(tm.add(3, new int(30)));
  ASSERT_EQ(tm.size(), 3);
  // can't add a value if the key already exists
  EXPECT_FALSE(tm.add(1, 0));
  EXPECT_EQ(tm.size(), 3);
  // check values are as expected
  EXPECT_EQ(tm[1], 10);
  EXPECT_EQ(tm[2], 20);
  EXPECT_EQ(ctm[3], 30);
}

/*
Demonstrate combining maps using "add all" and show that collisions
are handled by skipping the collided keys.
*/
TEST(TreeMap, AddAll)
{
  // create two maps with an overlapping key
  TreeMap<int, char> tmA;
  ASSERT_TRUE(tmA.add(1, 'A'));
  ASSERT_TRUE(tmA.add(2, 'A'));
  TreeMap<int, char> tmB;
  ASSERT_TRUE(tmB.add(2, 'B'));
  ASSERT_TRUE(tmB.add(3, 'B'));
  // add B to A
  EXPECT_TRUE(tmA.addAll(tmB));
  // check that we have added an element from B
  EXPECT_EQ(tmA.size(), 3);
  // check the values to verify the "add" behavior
  EXPECT_EQ(tmA[1], 'A');
  EXPECT_EQ(tmA[2], 'A'); // ignored from B
  EXPECT_EQ(tmA[3], 'B'); // added from B
}

/*
Demonstrate the behavior of "put" both with and without collisions.
*/
TEST(TreeMap, Put)
{
  // put some values in a map
  TreeMap<int, int> tm;
  int i10 = 10;
  tm.put(1, i10);
  tm.put(2, 20);
  tm.put(3, new int(30));
  ASSERT_EQ(tm.size(), 3);
  // putting a value in a map overrides it
  tm.put(1, 0);
  EXPECT_EQ(tm.size(), 3);
  // check values are as expected
  EXPECT_EQ(tm[1], 0);
  EXPECT_EQ(tm[2], 20);
  EXPECT_EQ(tm[3], 30);
}

/*
Demonstrate combining maps using "put all" and show that collisions
are handled by overwriting the collided keys.
*/
TEST(TreeMap, PutAll)
{
  // create two maps with an overlapping key
  TreeMap<int, char> tmA;
  tmA.put(1, 'A');
  tmA.put(2, 'A');
  TreeMap<int, char> tmB;
  tmB.put(2, 'B');
  tmB.put(3, 'B');
  // put B to A
  tmA.putAll(tmB);
  // check that we have added an element from B
  EXPECT_EQ(tmA.size(), 3);
  // check the values to verify the "put" behavior
  EXPECT_EQ(tmA[1], 'A');
  EXPECT_EQ(tmA[2], 'B'); // overridden by B
  EXPECT_EQ(tmA[3], 'B'); // puted from B
}

/*
Demonstrate attempting to "remove" both keys in and not in the map.
*/
TEST(TreeMap, Remove)
{
  // add some values to a map
  TreeMap<int, int> tm;
  ASSERT_TRUE(tm.add(1, 10));
  ASSERT_TRUE(tm.add(2, 20));
  ASSERT_EQ(tm.size(), 2);
  // removing a key not in the map does nothing
  EXPECT_FALSE(tm.remove(3));
  EXPECT_EQ(tm.size(), 2);
  // removing an element behaves as expected
  EXPECT_TRUE(tm.remove(1));
  EXPECT_EQ(tm.size(), 1);
  EXPECT_EQ(tm[2], 20);
  // removing the last element results in an empty map
  EXPECT_TRUE(tm.remove(2));
  EXPECT_EQ(tm.size(), 0);
  EXPECT_TRUE(tm.isEmpty());
  // adding an element to a map that had its last element removed
  ASSERT_TRUE(tm.add(1, 100));
  EXPECT_EQ(tm.size(), 1);
  EXPECT_EQ(tm[1], 100);
}

/*
Demonstrate removing a collection of keys from a map.
*/
TEST(TreeMap, RemoveAll)
{
  // add some values to a map
  TreeMap<int, int> tm;
  ASSERT_TRUE(tm.add(1, 10));
  ASSERT_TRUE(tm.add(2, 20));
  ASSERT_TRUE(tm.add(3, 30));
  ASSERT_TRUE(tm.add(4, 40));
  ASSERT_EQ(tm.size(), 4);
  // attempt to remove some elements not in the map
  ArrayList<int> misses{ 5, 6 };
  EXPECT_FALSE(tm.removeAll(misses));
  EXPECT_EQ(tm.size(), 4);
  // remove a mix of elements in the map and not
  ArrayList<int> mix{ 5, 1 };
  EXPECT_TRUE(tm.removeAll(mix));
  EXPECT_EQ(tm.size(), 3);
  // the element that should have been removed is no longer present
  EXPECT_FALSE(tm.contains(1));
}

/*
Demonstrate attempting to "get" both keys in and not in the map.
*/
TEST(TreeMap, Get)
{
  // add some values to a map
  TreeMap<int, int> tm;
  const TreeMap<int, int>& ctm = tm;
  ASSERT_TRUE(tm.add(1, 10));
  ASSERT_TRUE(tm.add(2, 20));
  ASSERT_EQ(tm.size(), 2);
  // getting keys in the map results in the values
  const int* p10 = tm.get(1);
  const int* p20 = ctm.get(2);
  ASSERT_TRUE(p10);
  ASSERT_TRUE(p20);
  EXPECT_EQ(*p10, 10);
  EXPECT_EQ(*p20, 20);
  // getting keys not in the map results in null
  const int* pxx = tm.get(1000);
  ASSERT_FALSE(pxx);
}

/*
Demonstrate that "contains all" can check multiple element containment.
*/
TEST(TreeMap, ContainsAll)
{
  // add some values to a map
  TreeMap<int, int> tm;
  ASSERT_TRUE(tm.add(1, 10));
  ASSERT_TRUE(tm.add(2, 20));
  ASSERT_TRUE(tm.add(3, 30));
  ASSERT_TRUE(tm.add(4, 40));
  ASSERT_EQ(tm.size(), 4);
  // all misses
  ArrayList<int> misses{ 5, 6 };
  EXPECT_FALSE(tm.containsAll(misses));
  // mix of hits and misses
  ArrayList<int> mix{ 5, 1 };
  EXPECT_FALSE(tm.containsAll(mix));
  // mix of hits and misses
  ArrayList<int> hits{ 1, 4 };
  EXPECT_TRUE(tm.containsAll(hits));
}

/*
Demonstrate that the core iterator can visit all the elements.
*/
TEST(TreeMap, Iterator)
{
  // add some values to a map, NOT in ascending order
  TreeMap<int, char> tm;
  for (int key = 0; key < 10; key += 2)
  {
    ASSERT_TRUE(tm.add(key, 'e')); // even
  }
  for (int key = 1; key < 10; key += 2)
  {
    ASSERT_TRUE(tm.add(key, 'o')); // odd
  }
  ASSERT_EQ(tm.size(), 10);
  // get an iterator over the map
  Iterator<KeyVal<int, char>>* itr = tm.iterator();
  // check that the values we added are all present and in-order
  int count = 0;
  while (itr->hasNext())
  {
    const KeyVal<int, char>& kvp = itr->next();
    // should visit keys in ascending order
    EXPECT_EQ(kvp.key(), count++);
    // we added the evens as 'e' and odds as 'o'
    if (kvp.key() % 2 == 0) EXPECT_EQ(kvp.value(), 'e');
    else EXPECT_EQ(kvp.value(), 'o');
  }
  // make sure we actually visited all elements
  EXPECT_EQ(count, 10);
  delete itr;
}

/*
Demonstrate how to access elements via the Cterator.
*/
TEST(TreeMap, ConstIterable)
{
  // add some values to a map, NOT in ascending order
  TreeMap<int, char> tm;
  for (int key = 0; key < 10; key += 2)
  {
    ASSERT_TRUE(tm.add(key, 'e')); // even
  }
  for (int key = 1; key < 10; key += 2)
  {
    ASSERT_TRUE(tm.add(key, 'o')); // odd
  }
  ASSERT_EQ(tm.size(), 10);
  // check that the values we added are all present and in-order
  int count = 0;
  for (const KeyVal<int, char>& kvp : tm)
  {
    // should visit keys in ascending order
    EXPECT_EQ(kvp.key(), count++);
    // we added the evens as 'e' and odds as 'o'
    if (kvp.key() % 2 == 0) EXPECT_EQ(kvp.value(), 'e');
    else EXPECT_EQ(kvp.value(), 'o');
  }
  // make sure we actually visited all elements
  EXPECT_EQ(count, 10);
}

/*
Demonstrate the ability to iterate over the keys of the map.
*/
TEST(TreeMap, KeyIterator)
{
  // add some entries to a map
  TreeMap<int, int> tm;
  ASSERT_TRUE(tm.add(1, 10));
  ASSERT_TRUE(tm.add(3, 30));
  ASSERT_TRUE(tm.add(2, 20));
  // get an iterator over the map keys
  Iterator<const int>* kitr = tm.keyIterator();
  // check that the keys we entered are returned in order
  int count = 0;
  int prevkey = 0;
  while (kitr->hasNext())
  {
    int key = kitr->next();
    EXPECT_GT(key, prevkey);
    prevkey = key;
    ++count;
  }
  EXPECT_EQ(count, 3);
  delete kitr;
}

/*
Demonstrate the ability to iterate over and update the values of the map.
*/
TEST(TreeMap, ValueIterator)
{
  // add some entries to a map
  TreeMap<int, int> tm;
  ASSERT_TRUE(tm.add(1, 10));
  ASSERT_TRUE(tm.add(3, 30));
  ASSERT_TRUE(tm.add(2, 20));
  // get an iterator over the map keys
  Iterator<int>* vitr = tm.valueIterator();
  // check that the keys we entered are returned in order
  int count = 0;
  int prevval = 0;
  while (vitr->hasNext())
  {
    int& val = vitr->next();
    EXPECT_GT(val, prevval);
    prevval = val;
    // update value
    val = val / 2;
    ++count;
  }
  EXPECT_EQ(count, 3);
  delete vitr;
  // check that we updated the values as desired
  EXPECT_EQ(tm[1], 5);
  EXPECT_EQ(tm[2], 10);
  EXPECT_EQ(tm[3], 15);
}

/*
Demonstrate the ability to iterate over and update the values of the map
when the collection is const-qualified.
*/
TEST(TreeMap, ConstValueIterator)
{
  // add some entries to a map
  TreeMap<int, int> tm;
  const TreeMap<int, int>& ctm = tm;
  ASSERT_TRUE(tm.add(1, 100));
  ASSERT_TRUE(tm.add(3, 30));
  ASSERT_TRUE(tm.add(2, 2));
  // get an iterator over the map keys
  Iterator<const int>* vitr = ctm.valueIterator();
  // check that the keys we entered are returned in order
  EXPECT_EQ(vitr->next(), 100);
  EXPECT_EQ(vitr->next(), 2);
  EXPECT_EQ(vitr->next(), 30);
  EXPECT_FALSE(vitr->hasNext());
  delete vitr;
}

/*
Demonstrate the copy constructor duplicating the keys and values.
*/
TEST(TreeMap, CopyConstructor)
{
  // add some entries to a map
  TreeMap<int, int> tm1;
  ASSERT_TRUE(tm1.add(1, 100));
  ASSERT_TRUE(tm1.add(3, 30));
  ASSERT_TRUE(tm1.add(2, 2));
  {
    // copy the map
    TreeMap<int, int> tm2(tm1);
    // elements in the copy should match the original
    EXPECT_EQ(tm2[1], 100);
    EXPECT_EQ(tm2[2], 2);
    EXPECT_EQ(tm2[3], 30);
    // changing the copy does not affect the original
    tm2[3] = 33;
    EXPECT_EQ(tm1[3], 30);
    EXPECT_EQ(tm2[3], 33);
  }
  // copy falls out of scope but original unaffected
  EXPECT_EQ(tm1[3], 30);
}

/*
Demonstrate the assignment operator duplicating the keys and values.
*/
TEST(TreeMap, AssignmentOperator)
{
  // add some entries to a map
  TreeMap<int, int> tm1;
  ASSERT_TRUE(tm1.add(1, 100));
  ASSERT_TRUE(tm1.add(3, 30));
  ASSERT_TRUE(tm1.add(2, 2));
  {
    // create a map and put a value in it
    TreeMap<int, int> tm2;
    tm2.add(1, 1);
    tm2.add(4, 4);
    tm2 = tm1;
    // elements in the copy should match the original
    EXPECT_EQ(tm2[1], 100);
    EXPECT_EQ(tm2[2], 2);
    EXPECT_EQ(tm2[3], 30);
    EXPECT_FALSE(tm2.contains(4));
    // changing the copy does not affect the original
    tm2[3] = 33;
    EXPECT_EQ(tm1[3], 30);
    EXPECT_EQ(tm2[3], 33);
  }
  // copy falls out of scope but original unaffected
  EXPECT_EQ(tm1[3], 30);
}

/*
Demonstrate the move-assign operator via map-swap.
*/
TEST(TreeMap, MoveSwap)
{
  // create a pair of maps with overlap
  TreeMap<int, int> tm1;
  ASSERT_TRUE(tm1.add(1, 10));
  ASSERT_TRUE(tm1.add(2, 20));
  TreeMap<int, int> tm2;
  ASSERT_TRUE(tm2.add(2, 200));
  ASSERT_TRUE(tm2.add(3, 300));
  ASSERT_TRUE(tm2.add(4, 400));
  // the lists match initial values
  for (const KeyVal<int, int>& kv : tm1)
  {
    ASSERT_EQ(kv.value(), kv.key() * 10);
  }
  for (const KeyVal<int, int>& kv : tm2)
  {
    ASSERT_EQ(kv.value(), kv.key() * 100);
  }
  // perform swap
  std::swap(tm1, tm2);
  // the lists are now swapped
  EXPECT_EQ(tm1.size(), 3);
  EXPECT_EQ(tm2.size(), 2);
  for (const KeyVal<int, int>& kv : tm1)
  {
    EXPECT_EQ(kv.value(), kv.key() * 100);
  }
  for (const KeyVal<int, int>& kv : tm2)
  {
    EXPECT_EQ(kv.value(), kv.key() * 10);
  }
}

/*
Demonstrate that a map can be cleared and remain functional.
*/
TEST(TreeMap, Clear)
{
  // create a map
  TreeMap<int, int> tm;
  ASSERT_TRUE(tm.add(1, 100));
  ASSERT_FALSE(tm.isEmpty());
  ASSERT_TRUE(tm.contains(1));
  ASSERT_EQ(tm[1], 100);
  // clearing the map makes it empty
  tm.clear();
  ASSERT_TRUE(tm.isEmpty());
  ASSERT_FALSE(tm.contains(1));
  // adding elements still works
  ASSERT_TRUE(tm.add(1, 10));
  ASSERT_FALSE(tm.isEmpty());
  ASSERT_TRUE(tm.contains(1));
  ASSERT_EQ(tm[1], 10);
}

/*
Demonstrate that the first value of a sorted set can be accessed
and match the least key in the set.
*/
TEST(TreeMap, First)
{
  TreeMap<int, char> tm;
  const TreeMap<int, char>& ctm = tm;
  // if just one element, that's the first one
  tm.add(30, 'c');
  ASSERT_EQ(tm.first().key(), 30);
  ASSERT_EQ(tm.first().value(), 'c');
  ASSERT_EQ(ctm.first().value(), 'c');
  // adding a lesser key makes that the first one
  tm.add(10, 'a');
  ASSERT_EQ(tm.first().key(), 10);
  ASSERT_EQ(tm.first().value(), 'a');
  ASSERT_EQ(ctm.first().value(), 'a');
  // adding another key in the middle does not change anything
  tm.add(20, 'b');
  ASSERT_EQ(tm.first().key(), 10);
  ASSERT_EQ(tm.first().value(), 'a');
  ASSERT_EQ(ctm.first().value(), 'a');
}

/*
Demonstrate that the last value of a map can be accessed
and match the greatest key in the set.
*/
TEST(TreeMap, Last)
{
  TreeMap<int, char> tm;
  const TreeMap<int, char>& ctm = tm;
  // if just one element, that's the last one
  tm.add(10, 'a');
  ASSERT_EQ(tm.last().key(), 10);
  ASSERT_EQ(tm.last().value(), 'a');
  ASSERT_EQ(ctm.last().value(), 'a');
  // adding a greater key replaces the last key
  tm.add(30, 'c');
  ASSERT_EQ(tm.last().key(), 30);
  ASSERT_EQ(tm.last().value(), 'c');
  ASSERT_EQ(ctm.last().value(), 'c');
  // adding another key in the middle does not change anything
  tm.add(20, 'b');
  ASSERT_EQ(tm.last().key(), 30);
  ASSERT_EQ(tm.last().value(), 'c');
  ASSERT_EQ(ctm.last().value(), 'c');
}

/*
Demonstrate that the predecessor value to some specified key can be found
in the set. Show that if there is no predecessor, the return value is the
first value in the map.
*/
TEST(TreeMap, Predecessor)
{
  TreeMap<int, char> tm;
  const TreeMap<int, char>& ctm = tm;
  tm.add(10, 'a');
  tm.add(20, 'b');
  tm.add(30, 'c');
  // elements before set return first element
  ASSERT_EQ(tm.predecessor(-1000).key(), 10);
  ASSERT_EQ(tm.predecessor(-1000).value(), 'a');
  ASSERT_EQ(ctm.predecessor(-1).value(), 'a');
  // first element returns first element
  ASSERT_EQ(tm.predecessor(10).key(), 10);
  ASSERT_EQ(tm.predecessor(10).value(), 'a');
  ASSERT_EQ(ctm.predecessor(10).value(), 'a');
  // elements in the set return previous element, not self
  ASSERT_EQ(tm.predecessor(20).key(), 10);
  ASSERT_EQ(tm.predecessor(20).value(), 'a');
  ASSERT_EQ(ctm.predecessor(20).value(), 'a');
  // elements between set elements return the previous element
  ASSERT_EQ(tm.predecessor(22).key(), 20);
  ASSERT_EQ(tm.predecessor(21).value(), 'b');
  ASSERT_EQ(ctm.predecessor(21).value(), 'b');
  // elements past the end return last element
  ASSERT_EQ(tm.predecessor(101).key(), 30);
  ASSERT_EQ(tm.predecessor(101).value(), 'c');
  ASSERT_EQ(ctm.predecessor(1000).value(), 'c');
}

/*
Demonstrate that the successor value to some specified key can be found
in the set. Show that if there is no successor, the return value is the
last value in the map.
*/
TEST(TreeMap, Successor)
{
  TreeMap<int, char> tm;
  const TreeMap<int, char>& ctm = tm;
  tm.add(10, 'a');
  tm.add(20, 'b');
  tm.add(30, 'c');
  // elements before set return first element
  ASSERT_EQ(tm.successor(-1000).key(), 10);
  ASSERT_EQ(tm.successor(-1000).value(), 'a');
  ASSERT_EQ(ctm.successor(-1).value(), 'a');
  // elements in the set return next element, not self
  ASSERT_EQ(tm.successor(10).key(), 20);
  ASSERT_EQ(tm.successor(10).value(), 'b');
  ASSERT_EQ(ctm.successor(10).value(), 'b');
  // elements between set elements return the previous element
  ASSERT_EQ(tm.successor(21).key(), 30);
  ASSERT_EQ(tm.successor(21).value(), 'c');
  ASSERT_EQ(ctm.successor(21).value(), 'c');
  // last element returns first element
  ASSERT_EQ(tm.successor(30).key(), 30);
  ASSERT_EQ(tm.successor(30).value(), 'c');
  ASSERT_EQ(ctm.successor(30).value(), 'c');
  // elements past the end return last element
  ASSERT_EQ(tm.successor(101).key(), 30);
  ASSERT_EQ(tm.successor(101).value(), 'c');
  ASSERT_EQ(ctm.successor(1000).value(), 'c');
}

/*
Demonstrate interaction with a Map's keys via the KeyView.
*/
TEST(TreeMap, KeyView)
{
  // add some entries to a map
  TreeMap<int, int> tm1;
  ASSERT_TRUE(tm1.add(1, 100));
  ASSERT_TRUE(tm1.add(3, 30));
  ASSERT_TRUE(tm1.add(2, 2));
  int acc = 0;
  // check iteration over the keys provides expected keys (sorted)
  int keys[3] = { 1, 2, 3 };
  for (const int& k : tm1.keys())
  {
    EXPECT_EQ(k, keys[acc]);
    ++acc;
  }
  // check the number of keys
  EXPECT_EQ(acc, 3);
}

/*
Demonstrate interaction with a Map's keys via the KeyView.
*/
TEST(TreeMap, ValueView)
{
  // add some entries to a map
  TreeMap<int, int> tm1;
  ASSERT_TRUE(tm1.add(1, 100));
  ASSERT_TRUE(tm1.add(3, 30));
  ASSERT_TRUE(tm1.add(2, 2));
  int acc = 0;
  // check iteration over the keys provides expected values (sorted by keys)
  int values[3] = { 100, 2, 30 };
  for (const int& v : tm1.values())
  {
    EXPECT_EQ(v, values[acc]);
    ++acc;
  }
  // check the number of keys
  EXPECT_EQ(acc, 3);
}

/*
Demonstrate swapping the values associated with two keys.
*/
TEST(TreeMap, swap)
{
  // add some entries to a map
  TreeMap<int, int> tm1;
  ASSERT_TRUE(tm1.add(1, 100));
  ASSERT_TRUE(tm1.add(3, 30));
  ASSERT_TRUE(tm1.add(2, 2));
  // self-swap does nothing
  EXPECT_FALSE(tm1.swap(1, 1));
  // swapping missing keys does nothing
  EXPECT_FALSE(tm1.swap(0, 0));
  EXPECT_FALSE(tm1.swap(0, 9));
  EXPECT_FALSE(tm1.swap(9, 0));
  // swap some values
  EXPECT_TRUE(tm1.swap(1, 3));
  EXPECT_TRUE(tm1.swap(2, 3));
  // size matches elements
  ASSERT_EQ(tm1.size(), 3);
  // check iteration over the keys provides expected values (sorted by keys)
  int acc = 0;
  int values[3] = { 30, 100, 2 };
  for (const int& v : tm1.values())
  {
    EXPECT_EQ(v, values[acc]);
    ++acc;
  }
  // check the number of keys is unchanged
  EXPECT_EQ(acc, 3);
}

/*
Demonstrate swapping the values associated with one of two keys.
*/
TEST(TreeMap, swap_half)
{
  // add some entries to a map
  TreeMap<int, int> tm1;
  ASSERT_TRUE(tm1.add(1, 100));
  ASSERT_TRUE(tm1.add(3, 30));
  ASSERT_TRUE(tm1.add(2, 2));
  // one-sided swaps
  EXPECT_TRUE(tm1.swap(0, 1)); // left
  EXPECT_TRUE(tm1.swap(3, 9)); // right
  // size unchanged
  ASSERT_EQ(tm1.size(), 3);
  // moved keys no longer present
  EXPECT_FALSE(tm1.contains(1));
  EXPECT_FALSE(tm1.contains(3));
  // valused present under new keys
  EXPECT_EQ(tm1[0], 100);
  EXPECT_EQ(tm1[2], 2);
  EXPECT_EQ(tm1[9], 30);
}

/*
Demonstrate that releasing a key extracts the value from the map.
*/
TEST(TreeMap, release)
{
  // add some entries to a map
  TreeMap<int, int> tm1;
  ASSERT_TRUE(tm1.add(1, 100));
  ASSERT_TRUE(tm1.add(3, 30));
  ASSERT_TRUE(tm1.add(2, 2));
  EXPECT_EQ(tm1.size(), 3);
  // release missing keys does nothing
  EXPECT_EQ(tm1.release(0), nullptr);
  // release a value extracts it from the map
  int* val = nullptr;
  ASSERT_TRUE((val = tm1.release(1)));
  EXPECT_EQ(*val, 100);
  delete val;
  // size matches elements
  ASSERT_EQ(tm1.size(), 2);
  // check iteration over the keys provides expected values (sorted by keys)
  int acc = 0;
  int values[2] = { 2, 30 };
  for (const int& v : tm1.values())
  {
    EXPECT_EQ(v, values[acc]);
    ++acc;
  }
  // check the iteration count
  EXPECT_EQ(acc, 2);
}
