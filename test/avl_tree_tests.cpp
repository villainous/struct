#include "avl_tree.inl"

// check this compiles BEFORE gtest leaks symbols
template class viln::AvlTree<double>;

#include <gtest/gtest.h>

using namespace viln;

// make protected methods of avl-tree public so that testing is easier
class TestAvlTree : public AvlTree<int>
{
public:
  // constructor
  TestAvlTree(int* e) : AvlTree<int>(e) {}
  // rebalance
  bool rebalance(TreeNode<int>* const nod)
  {
    return AvlTree<int>::rebalance(nod);
  }
  // rotate left
  void rotateLeft(TreeNode<int>* const nod)
  {
    return AvlTree<int>::rotateLeft(nod);
  }
  // rotate right
  void rotateRight(TreeNode<int>* const nod)
  {
    return AvlTree<int>::rotateRight(nod);
  }
};

/**
Demonstrate a left rotation.
```
        y                          x
       / \     Right Rotation     / \
      x   T3   - - - - - - - >   T1  y
     / \       < - - - - - - -      / \
    T1  T2     Left Rotation       T2  T3
```
*/
TEST(AvlTree, RotateLeft)
{
  // construct a tree
  TestAvlTree tree(new int(1));
  // subtrees
  TreeNode<int>* t1 = new TreeNode<int>(new int(1));
  TreeNode<int>* t2 = new TreeNode<int>(new int(2));
  TreeNode<int>* t3 = new TreeNode<int>(new int(3));
  // nodes being rotated
  TreeNode<int>* y = new TreeNode<int>(new int(4));
  y->setLeft(t2);
  y->setRight(t3);
  TreeNode<int>* x = tree.root();
  x->setLeft(t1);
  x->setRight(y);
  // perform the rotation
  tree.rotateLeft(x);
  // check that all linkages are as expected
  EXPECT_EQ(tree.root(), y);
  EXPECT_EQ(t1->parent(), x);
  EXPECT_EQ(t2->parent(), x);
  EXPECT_EQ(t3->parent(), y);
  EXPECT_EQ(y->leftChild(), x);
  EXPECT_EQ(y->rightChild(), t3);
  EXPECT_EQ(x->parent(), y);
  EXPECT_EQ(x->leftChild(), t1);
  EXPECT_EQ(x->rightChild(), t2);
  // verify heights
  EXPECT_EQ(x->height(), 1);
  EXPECT_EQ(y->height(), 2);
}

/**
Demonstrate a right rotation.
```
        y                          x
       / \     Right Rotation     / \
      x   T3   - - - - - - - >   T1  y
     / \       < - - - - - - -      / \
    T1  T2     Left Rotation       T2  T3
```
*/
TEST(AvlTree, RotateRight)
{
  // construct a tree
  TestAvlTree tree(new int(0));
  // subtrees
  TreeNode<int>* t1 = new TreeNode<int>(new int(1));
  TreeNode<int>* t2 = new TreeNode<int>(new int(2));
  TreeNode<int>* t3 = new TreeNode<int>(new int(3));
  // nodes being rotated
  TreeNode<int>* x = new TreeNode<int>(new int(4));
  x->setLeft(t1);
  x->setRight(t2);
  TreeNode<int>* y = tree.root();
  y->setLeft(x);
  y->setRight(t3);
  // perform the rotation
  tree.rotateRight(y);
  // check that all linkages are as expected
  EXPECT_EQ(tree.root(), x);
  EXPECT_EQ(t1->parent(), x);
  EXPECT_EQ(t2->parent(), y);
  EXPECT_EQ(t3->parent(), y);
  EXPECT_EQ(y->parent(), x);
  EXPECT_EQ(y->leftChild(), t2);
  EXPECT_EQ(y->rightChild(), t3);
  EXPECT_EQ(x->leftChild(), t1);
  EXPECT_EQ(x->rightChild(), y);
  // verify heights
  EXPECT_EQ(x->height(), 2);
  EXPECT_EQ(y->height(), 1);
}

/**
Demonstrate rebalance of a supercritical tree with onlb left imbalances.
```
        c                           b
       / \                         / \
      b  T4  Right Rotate (c)    a     c
     / \     - - - - - - - ->   / \   / \
    a  T3                      T1 T2 T3 T4
   / \
  T1 T2
```
*/
TEST(AvlTree, RebalanceRight)
{
  // construct the tree
  TestAvlTree tree(new int(0));
  TreeNode<int>* c = tree.root();
  TreeNode<int>* b = new TreeNode<int>(new int(1));
  TreeNode<int>* a = new TreeNode<int>(new int(2));
  TreeNode<int>* t1 = new TreeNode<int>(new int(3));
  TreeNode<int>* t2 = new TreeNode<int>(new int(4));
  TreeNode<int>* t3 = new TreeNode<int>(new int(5));
  TreeNode<int>* t4 = new TreeNode<int>(new int(6));
  c->setLeft(b);
  c->setRight(t4);
  b->setLeft(a);
  b->setRight(t3);
  a->setLeft(t1);
  a->setRight(t2);
  // rebalance
  ASSERT_TRUE(tree.rebalance(c));
  // verify the results
  EXPECT_EQ(tree.root(), b);
  EXPECT_EQ(a->leftChild(), t1);
  EXPECT_EQ(a->rightChild(), t2);
  EXPECT_EQ(b->leftChild(), a);
  EXPECT_EQ(b->rightChild(), c);
  EXPECT_EQ(c->leftChild(), t3);
  EXPECT_EQ(c->rightChild(), t4);
}

/**
Demonstrate rebalance of a supercritical tree with a left-right imbalance.
```
      c                               c                           b
     / \                             / \                         / \
    a   T4   Left Rotate (a)        b  T4  Right Rotate(c)     a     c
   / \       - - - - - - - ->      / \     - - - - - - - ->   / \   / \
  T1  b                          a  T3                       T1 T2 T3 T4
     / \                        / \
    T2 T3                      T1 T2
```
*/
TEST(AvlTree, RebalanceLeftRight)
{
  // construct the tree
  TestAvlTree tree(new int(0));
  TreeNode<int>* c = tree.root();
  TreeNode<int>* a = new TreeNode<int>(new int(1));
  TreeNode<int>* b = new TreeNode<int>(new int(2));
  TreeNode<int>* t1 = new TreeNode<int>(new int(3));
  TreeNode<int>* t2 = new TreeNode<int>(new int(4));
  TreeNode<int>* t3 = new TreeNode<int>(new int(5));
  TreeNode<int>* t4 = new TreeNode<int>(new int(6));
  c->setLeft(a);
  c->setRight(t4);
  a->setLeft(t1);
  a->setRight(b);
  b->setLeft(t2);
  b->setRight(t3);
  // rebalance
  ASSERT_TRUE(tree.rebalance(c));
  // verify the results
  EXPECT_EQ(tree.root(), b);
  EXPECT_EQ(a->leftChild(), t1);
  EXPECT_EQ(a->rightChild(), t2);
  EXPECT_EQ(b->leftChild(), a);
  EXPECT_EQ(b->rightChild(), c);
  EXPECT_EQ(c->leftChild(), t3);
  EXPECT_EQ(c->rightChild(), t4);
}

/**
Demonstrate rebalance of a supercritical tree with only right imbalances.
```
    a                               b
   / \                             / \
  T1  b      Left Rotate(a)      a     c
     / \     - - - - - - - ->   / \   / \
    T2  c                      T1 T2 T3 T4
       / \
      T3 T4
```
*/
TEST(AvlTree, RebalanceLeft)
{
  // construct the tree
  TestAvlTree tree(new int(0));
  TreeNode<int>* a = tree.root();
  TreeNode<int>* b = new TreeNode<int>(new int(1));
  TreeNode<int>* c = new TreeNode<int>(new int(2));
  TreeNode<int>* t1 = new TreeNode<int>(new int(3));
  TreeNode<int>* t2 = new TreeNode<int>(new int(4));
  TreeNode<int>* t3 = new TreeNode<int>(new int(5));
  TreeNode<int>* t4 = new TreeNode<int>(new int(6));
  a->setLeft(t1);
  a->setRight(b);
  b->setLeft(t2);
  b->setRight(c);
  c->setLeft(t3);
  c->setRight(t4);
  // rebalance
  ASSERT_TRUE(tree.rebalance(a));
  // verify the results
  EXPECT_EQ(tree.root(), b);
  EXPECT_EQ(a->leftChild(), t1);
  EXPECT_EQ(a->rightChild(), t2);
  EXPECT_EQ(b->leftChild(), a);
  EXPECT_EQ(b->rightChild(), c);
  EXPECT_EQ(c->leftChild(), t3);
  EXPECT_EQ(c->rightChild(), t4);
}

/**
Demonstrate rebalance of a supercritical tree with a right-left imbalance.
```
     a                           a                                b
    / \                         / \                              / \
  T1   c     Right Rotate (c)  T1  b       Left Rotate(a)      a     c
      / \    - - - - - - - ->     / \      - - - - - - - ->   / \   / \
     b  T4                       T2  c                       T1 T2 T3 T4
    / \                             / \
   T2 T3                           T3 T4
```
*/
TEST(AvlTree, RebalanceRightLeft)
{
  // construct the tree
  TestAvlTree tree(new int(0));
  TreeNode<int>* a = tree.root();
  TreeNode<int>* c = new TreeNode<int>(new int(1));
  TreeNode<int>* b = new TreeNode<int>(new int(2));
  TreeNode<int>* t1 = new TreeNode<int>(new int(3));
  TreeNode<int>* t2 = new TreeNode<int>(new int(4));
  TreeNode<int>* t3 = new TreeNode<int>(new int(5));
  TreeNode<int>* t4 = new TreeNode<int>(new int(6));
  a->setLeft(t1);
  a->setRight(c);
  c->setLeft(b);
  c->setRight(t4);
  b->setLeft(t2);
  b->setRight(t3);
  // rebalance
  ASSERT_TRUE(tree.rebalance(a));
  // verify the results
  EXPECT_EQ(tree.root(), b);
  EXPECT_EQ(a->leftChild(), t1);
  EXPECT_EQ(a->rightChild(), t2);
  EXPECT_EQ(b->leftChild(), a);
  EXPECT_EQ(b->rightChild(), c);
  EXPECT_EQ(c->leftChild(), t3);
  EXPECT_EQ(c->rightChild(), t4);
}

/**
Demonstrate adding a successor at 1 and 2 levels of depth, both with and
without triggering an rebalance.
*/
TEST(AvlTree, AddSuccessor)
{
  // construct a tree with a root element
  AvlTree<int> tree(new int(1));
  TreeNode<int>* x = tree.root();
  EXPECT_EQ(x->height(), 0);
  // adding one successor does not trigger rebalance
  TreeNode<int>* y = tree.addSuccessor(x, new int(3));
  EXPECT_EQ(tree.root(), x);
  EXPECT_EQ(x->height(), 1);
  EXPECT_EQ(x->rightChild(), y);
  EXPECT_EQ(x->balance(), 1);
  // adding a second successor will cause a rebalance
  TreeNode<int>* w = tree.addSuccessor(x, new int(1));
  EXPECT_NE(tree.root(), x);
  EXPECT_EQ(tree.root()->height(), 1);
  EXPECT_EQ(tree.root()->balance(), 0);
}

/**
Demonstrate adding a predecessor at 1 and 2 levels of depth, both with and
without triggering an rebalance.
*/
TEST(AvlTree, AddPredecessor)
{
  // construct a tree with a root element
  AvlTree<int> tree(new int(1));
  TreeNode<int>* x = tree.root();
  EXPECT_EQ(x->height(), 0);
  // adding one Predecessor does not trigger rebalance
  TreeNode<int>* y = tree.addPredecessor(x, new int(3));
  EXPECT_EQ(tree.root(), x);
  EXPECT_EQ(x->height(), 1);
  EXPECT_EQ(x->leftChild(), y);
  EXPECT_EQ(x->balance(), -1);
  // adding a second Predecessor will cause a rebalance
  TreeNode<int>* w = tree.addPredecessor(x, new int(1));
  EXPECT_NE(tree.root(), x);
  EXPECT_EQ(tree.root()->height(), 1);
  EXPECT_EQ(tree.root()->balance(), 0);
}

/**
Demonstrate removing the last element in the list results in no root node.
*/
TEST(AvlTree, RemoveRoot)
{
  AvlTree<int> tree(new int(1));
  TreeNode<int>* root = tree.root();
  tree.remove(root);
  ASSERT_EQ(tree.root(), nullptr);
}

/**
Demonstrate removing a leaf node other than the root.
*/
TEST(AvlTree, RemoveLeaf)
{
  // create a tree with two leaf nodes
  AvlTree<int> tree(new int(1));
  TreeNode<int>* root = tree.root();
  TreeNode<int>* leafL = new TreeNode<int>(new int(0));
  TreeNode<int>* leafR = new TreeNode<int>(new int(2));
  root->setLeft(leafL);
  root->setRight(leafR);
  // remove the leaves
  tree.remove(leafL);
  tree.remove(leafR);
  // check that the root node looks OK
  ASSERT_EQ(tree.root(), root);
  ASSERT_EQ(root->leftChild(), nullptr);
  ASSERT_EQ(root->rightChild(), nullptr);
}

/**
Demonstrate removing a non-leaf node leading to a rebalance.
```
      a                        a                          c
     / \                      / \                        / \
    x   c      Remove(x)     y   c      Rebalance(a)    a   d
   /   / \     - - - - - ->     / \     - - - - - ->   / \   \
  y   b   d                    b   d                  y   b   e
           \                        \
            e                        e
```
*/
TEST(AvlTree, RemoveWithRebalance)
{
  // construct the tree
  TestAvlTree tree(new int(0));
  TreeNode<int>* a = tree.root();
  TreeNode<int>* b = new TreeNode<int>(new int(1));
  TreeNode<int>* c = new TreeNode<int>(new int(2));
  TreeNode<int>* d = new TreeNode<int>(new int(3));
  TreeNode<int>* e = new TreeNode<int>(new int(4));
  TreeNode<int>* x = new TreeNode<int>(new int(-1));
  TreeNode<int>* y = new TreeNode<int>(new int(-2));
  a->setLeft(x);
  a->setRight(c);
  c->setLeft(b);
  c->setRight(d);
  d->setRight(e);
  x->setLeft(y);
  // rebalance
  tree.remove(x);
  // verify the results
  EXPECT_EQ(tree.root(), c);
  EXPECT_EQ(a->leftChild(), y);
  EXPECT_EQ(a->rightChild(), b);
  EXPECT_EQ(c->leftChild(), a);
  EXPECT_EQ(c->rightChild(), d);
  EXPECT_EQ(d->rightChild(), e);
}
