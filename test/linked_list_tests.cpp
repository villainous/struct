
#include <utility>
#include "linked_list.inl"

// check this compiles BEFORE gtest leaks symbols
template class viln::LinkedList<double>;

#include <gtest/gtest.h>

using namespace viln;

// helper that shortens test definitions
template<typename E>
LinkedList<E> array2list(const E* src, int size)
{
  LinkedList<E> list;
  for (size_t idx = 0; idx < size; ++idx)
  {
    list.add(src[idx]);
  }
  return list;
}

/**
Check that operator[] behaves as expected.
*/
TEST(LinkedList, ArrayAccess)
{
  int arr9[9] = { 1, 2, 3, 912000, 5, 6, 7, 8, 999 };
  LinkedList<int> list9 = array2list(arr9, 9);
  const LinkedList<int>& clist5 = list9;
  for (int idx = 0; idx < 9; ++idx)
  {
    // non-const access
    EXPECT_EQ(list9[idx], arr9[idx]);
    // const access
    EXPECT_EQ(clist5[idx], arr9[idx]);
  }
}

/**
Check that element access via an iterator can visit each element.
*/
TEST(LinkedList, IteratorAccess)
{
  int arr5[5] = { 1, 2, 3, 912000, 5 };
  LinkedList<int> list5 = array2list(arr5, 5);
  // check that iterating over the list allows element access
  ListIterator<int>* itr = list5.iterator();
  int idx = 0;
  while (itr->hasNext())
  {
    EXPECT_EQ(itr->next(), arr5[idx++]);
  }
  delete itr;
  EXPECT_EQ(idx, 5);
}

/**
Check that element access via an iterator can visit each element.
*/
TEST(LinkedList, ConstIteratorAccess)
{
  int arr5[5] = { 11, 22, 33, 44, 55 };
  LinkedList<int> list5 = array2list(arr5, 5);
  // check that the const-qualified iterator allows const access
  const LinkedList<int>& clist5 = list5;
  ListIterator<const int>* citr = clist5.iterator();
  int idx = 0;
  while (citr->hasNext())
  {
    EXPECT_EQ(citr->next(), arr5[idx++]);
  }
  delete citr;
  EXPECT_EQ(idx, 5);
}

/**
Check that iteration in reverse order behaves as expected.
*/
TEST(LinkedList, ReverseIteratorAccess)
{
  int arr9[9] = { 1, 2, 3, 4, -37, 66, 777, 8888, 9999 };
  // check that iterating over the list allows element access
  LinkedList<int> list9 = array2list(arr9, 9);
  ListIterator<int>* ritr = list9.listIterator(9);
  int idx = 9;
  while (ritr->hasPrevious())
  {
    EXPECT_EQ(ritr->previous(), arr9[--idx]);
  }
  delete ritr;
  EXPECT_EQ(idx, 0);
}

/**
Check that single-element append behaves as expected.
*/
TEST(LinkedList, AddElement)
{
  LinkedList<int> list;
  ASSERT_TRUE(list.add(1));
  ASSERT_TRUE(list.add(2));
  ASSERT_TRUE(list.add(3));
  ASSERT_TRUE(list.add(4));
  // check that the content is as expected
  ASSERT_EQ(list.size(), 4);
  EXPECT_EQ(list[0], 1);
  EXPECT_EQ(list[1], 2);
  EXPECT_EQ(list[2], 3);
  EXPECT_EQ(list[3], 4);
}

/**
Check that we can append lists.
*/
TEST(LinkedList, AddCollection)
{
  LinkedList<int> list;
  // adding an empty collection does NOT modify the list
  LinkedList<int> listX;
  ASSERT_FALSE(list.addAll(listX));
  ASSERT_TRUE(list.isEmpty());
  // adding lists DOES modify the list
  LinkedList<int> listA{ 11, 12 };
  LinkedList<int> listB{ 21, 22 };
  ASSERT_TRUE(list.addAll(listA));
  ASSERT_TRUE(list.addAll(listB));
  // check that the content is as expected
  ASSERT_EQ(list.size(), 4);
  // check that the content is as expected
  EXPECT_EQ(list[0], 11);
  EXPECT_EQ(list[1], 12);
  EXPECT_EQ(list[2], 21);
  EXPECT_EQ(list[3], 22);
}

/**
Check that appending a list to itself works.
*/
TEST(LinkedList, AddSelf)
{
  LinkedList<int> list{ 1, 22, 333 };
  ASSERT_TRUE(list.addAll(list));
  ASSERT_EQ(list.size(), 6);
  for (int idx = 0; idx < 3; ++idx)
  {
    ASSERT_EQ(list[idx], list[idx + 3]);
  }
}

/**
Check that single-element insertion at an index behaves as expected.
*/
TEST(LinkedList, IndexedInsertElement)
{
  LinkedList<int> list{ 1, 2, 3 };
  // show that we can insert elements in the middle
  ASSERT_TRUE(list.insert(1, 91));
  ASSERT_TRUE(list.insert(1, 92));
  // show we can insert a variable
  int i93 = 93;
  ASSERT_TRUE(list.insert(1, i93));
  ASSERT_EQ(list.size(), 6);
  // show we can insert an element at the end
  ASSERT_TRUE(list.insert(6, 94));
  // check that the content is inserted as expected
  ASSERT_EQ(list.size(), 7);
  {
    ListIterator<int>* itr = list.iterator();
    EXPECT_EQ(itr->next(), 1);
    EXPECT_EQ(itr->next(), 93);
    EXPECT_EQ(itr->next(), 92);
    EXPECT_EQ(itr->next(), 91);
    EXPECT_EQ(itr->next(), 2);
    EXPECT_EQ(itr->next(), 3);
    EXPECT_EQ(itr->next(), 94);
    EXPECT_FALSE(itr->hasNext());
    delete itr;
  }
}

/**
Check that single-element insertion into an iterator position behaves as
expected.
*/
TEST(LinkedList, IteratorInsertElement)
{
  LinkedList<int> list{ 1, 2, 3 };
  // insert some elements into the list
  {
    ListNodeIterator<int>* itr = list.listIterator(0);
    // insert a new element at the start
    EXPECT_TRUE(list.insert(itr, 101));
    EXPECT_EQ(itr->next(), 1);
    // insert an element in the middle
    EXPECT_TRUE(list.insert(itr, 201));
    // insert an element from a variable
    const int i202 = 202;
    EXPECT_TRUE(list.insert(itr, i202));
    EXPECT_EQ(itr->next(), 2);
    EXPECT_EQ(itr->next(), 3);
    // insert elements at the end
    EXPECT_TRUE(list.insert(itr, 402));
    EXPECT_FALSE(itr->hasNext());
    delete itr;
  }
  ASSERT_EQ(list.size(), 7);
  // check that each element is as expected
  {
    ListIterator<int>* itr = list.iterator();
    EXPECT_EQ(itr->next(), 101);
    EXPECT_EQ(itr->next(), 1);
    EXPECT_EQ(itr->next(), 201);
    EXPECT_EQ(itr->next(), 202);
    EXPECT_EQ(itr->next(), 2);
    EXPECT_EQ(itr->next(), 3);
    EXPECT_EQ(itr->next(), 402);
    EXPECT_FALSE(itr->hasNext());
    delete itr;
  }
}

/**
Check that we can insert one list within another at a given index.
*/
TEST(LinkedList, IndexedInsertCollection)
{
  LinkedList<int> list;
  LinkedList<int> listA;
  ASSERT_TRUE(listA.add(11));
  ASSERT_TRUE(listA.add(12));
  LinkedList<int> listB;
  ASSERT_TRUE(listB.add(21));
  ASSERT_TRUE(listB.add(22));
  LinkedList<int> listC;
  ASSERT_TRUE(listC.add(31));
  ASSERT_TRUE(listC.add(32));
  LinkedList<int> empty;
  // insert into an empty list
  ASSERT_TRUE(list.insertAll((size_t)0, listA));
  // insert into the middle of a list
  ASSERT_TRUE(list.insertAll(1, listB));
  // insert at the end of a list
  ASSERT_TRUE(list.insertAll(4, listC));
  // insert an empty list
  ASSERT_FALSE(list.insertAll((size_t)0, empty));
  // check that each element is as expected
  ASSERT_EQ(list.size(), 6);
  {
    ListIterator<int>* itr = list.iterator();
    EXPECT_EQ(itr->next(), 11);
    EXPECT_EQ(itr->next(), 21);
    EXPECT_EQ(itr->next(), 22);
    EXPECT_EQ(itr->next(), 12);
    EXPECT_EQ(itr->next(), 31);
    EXPECT_EQ(itr->next(), 32);
    EXPECT_FALSE(itr->hasNext());
    delete itr;
  }
}

/**
Check that we can insert one list within another at the location of an
iterator.
*/
TEST(LinkedList, IteratorInsertCollection)
{
  LinkedList<int> list;
  LinkedList<int> listA;
  ASSERT_TRUE(listA.add(11));
  ASSERT_TRUE(listA.add(12));
  LinkedList<int> listB;
  ASSERT_TRUE(listB.add(21));
  ASSERT_TRUE(listB.add(22));
  LinkedList<int> listC;
  ASSERT_TRUE(listC.add(31));
  ASSERT_TRUE(listC.add(32));
  // insert into an empty list using an iterator
  {
    ListNodeIterator<int>* itr = list.listIterator(0);
    EXPECT_TRUE(list.insertAll(itr, listA));
    delete itr;
  }
  ASSERT_EQ(list.size(), 2);
  // insert insert a list into the middle of another list
  {
    ListNodeIterator<int>* itr = list.listIterator(1);
    EXPECT_TRUE(list.insertAll(itr, listB));
    delete itr;
  }
  ASSERT_EQ(list.size(), 4);
  // insert a list at the end
  {
    ListNodeIterator<int>* itr = list.listIterator(4);
    EXPECT_TRUE(list.insertAll(itr, listC));
    delete itr;
  }
  ASSERT_EQ(list.size(), 6);
  // inserting an empty list is a no-op
  LinkedList<int> empty;
  {
    ListNodeIterator<int>* itr = list.listIterator(0);
    EXPECT_FALSE(list.insertAll(itr, empty));
    delete itr;
  }
  ASSERT_EQ(list.size(), 6);
  // check that each element is as expected
  {
    ListIterator<int>* itr = list.iterator();
    EXPECT_EQ(itr->next(), 11);
    EXPECT_EQ(itr->next(), 21);
    EXPECT_EQ(itr->next(), 22);
    EXPECT_EQ(itr->next(), 12);
    EXPECT_EQ(itr->next(), 31);
    EXPECT_EQ(itr->next(), 32);
    EXPECT_FALSE(itr->hasNext());
    delete itr;
  }
}

/**
Check that we can insert a list into itself at a specified index.
*/
TEST(LinkedList, IndexedInsertSelf)
{
  LinkedList<int> list{ 1, 2, 3, 4 };
  // check that the insertion itself succeeds
  ASSERT_TRUE(list.insertAll(2, list));
  // check on the values of the resulting list
  ASSERT_EQ(list.size(), 8);
  {
    ListIterator<int>* itr = list.iterator();
    EXPECT_EQ(itr->next(), 1);
    EXPECT_EQ(itr->next(), 2);
    EXPECT_EQ(itr->next(), 1);
    EXPECT_EQ(itr->next(), 2);
    EXPECT_EQ(itr->next(), 3);
    EXPECT_EQ(itr->next(), 4);
    EXPECT_EQ(itr->next(), 3);
    EXPECT_EQ(itr->next(), 4);
    EXPECT_FALSE(itr->hasNext());
    delete itr;
  }
}

/**
Check that we can insert a list into itself at a specified index.
*/
TEST(LinkedList, IteratorInsertSelf)
{
  LinkedList<int> list;
  ASSERT_TRUE(list.add(1));
  ASSERT_TRUE(list.add(2));
  ASSERT_TRUE(list.add(3));
  ASSERT_TRUE(list.add(4));
  // check that the insertion itself succeeds
  {
    ListNodeIterator<int>* itr = list.listIterator(0);
    EXPECT_EQ(itr->next(), 1);
    EXPECT_EQ(itr->next(), 2);
    EXPECT_TRUE(list.insertAll(itr, list));
    delete itr;
  }
  // validate the new values
  ASSERT_EQ(list.size(), 8);
  {
    ListIterator<int>* itr = list.iterator();
    EXPECT_EQ(itr->next(), 1);
    EXPECT_EQ(itr->next(), 2);
    EXPECT_EQ(itr->next(), 1);
    EXPECT_EQ(itr->next(), 2);
    EXPECT_EQ(itr->next(), 3);
    EXPECT_EQ(itr->next(), 4);
    EXPECT_EQ(itr->next(), 3);
    EXPECT_EQ(itr->next(), 4);
    EXPECT_FALSE(itr->hasNext());
    delete itr;
  }
}

/**
Check that elements can be cleared and then added.
*/
TEST(LinkedList, ClearAdd)
{
  LinkedList<int> list{ 1, 2 };
  ASSERT_FALSE(list.isEmpty());
  list.clear();
  ASSERT_TRUE(list.isEmpty());
  ASSERT_TRUE(list.add(3));
  ASSERT_FALSE(list.isEmpty());
}

/**
Check that we can delete a bunch of scattered elements.
*/
TEST(LinkedList, EraseScatter)
{
  LinkedList<int> list{ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
  // deletion a bunch of elements all over the place
  ASSERT_TRUE(list.erase(9));
  ASSERT_EQ(list.size(), 9);
  ASSERT_TRUE(list.erase(3));
  ASSERT_EQ(list.size(), 8);
  ASSERT_TRUE(list.erase(3));
  ASSERT_EQ(list.size(), 7);
  ASSERT_TRUE(list.erase(4));
  ASSERT_EQ(list.size(), 6);
  ASSERT_TRUE(list.erase(4));
  ASSERT_EQ(list.size(), 5);
  ASSERT_TRUE(list.erase(2));
  ASSERT_EQ(list.size(), 4);
  // check on the values of the resulting list
  EXPECT_EQ(list[0], 1);
  EXPECT_EQ(list[1], 2);
  EXPECT_EQ(list[2], 6);
  EXPECT_EQ(list[3], 9);
}

/**
Check that erasing multiple discontiguous indices works.
*/
TEST(LinkedList, EraseAll)
{
  LinkedList<int> list;
  for (int idx = 0; idx <= 5; ++idx)
  {
    ASSERT_TRUE(list.add(idx));
  }
  ListIndices indices;
  // erasing an empty list of lists is a no-op
  ASSERT_FALSE(list.eraseAll(indices));
  // erase some elements
  indices.add(1);
  indices.add(3);
  indices.add(5);
  ASSERT_TRUE(list.eraseAll(indices));
  // check that all the elements match what we expect
  ASSERT_EQ(list.size(), 3);
  {
    ListIterator<int>* itr = list.iterator();
    EXPECT_EQ(itr->next(), 0);
    EXPECT_EQ(itr->next(), 2);
    EXPECT_EQ(itr->next(), 4);
    EXPECT_FALSE(itr->hasNext());
    delete itr;
  }
}

/**
Check that we can erase elements based on iterator position.
*/
TEST(LinkedList, IteratorErase)
{
  LinkedList<int> list;
  for (int idx = 1; idx <= 5; ++idx)
  {
    ASSERT_TRUE(list.add(idx));
  }
  // erase elements 2 and 3
  {
    ListNodeIterator<int>* itr = list.listIterator(0);
    EXPECT_EQ(itr->next(), 1);
    EXPECT_TRUE(list.eraseNext(itr));
    EXPECT_EQ(itr->next(), 3);
    EXPECT_TRUE(list.erasePrevious(itr));
    delete itr;
  }
  // validate the new values
  ASSERT_EQ(list.size(), 3);
  {
    ListIterator<int>* itr = list.iterator();
    EXPECT_EQ(itr->next(), 1);
    EXPECT_EQ(itr->next(), 4);
    EXPECT_EQ(itr->next(), 5);
    EXPECT_FALSE(itr->hasNext());
    delete itr;
  }
}

/**
Demonstrate that erasing the first element by iterator position results in the
iterator remaining in a reasonable state.
*/
TEST(LinkedList, IteratorEraseFirst)
{
  // create a list of 2 elements
  LinkedList<int> list;
  list.add(1);
  list.add(2);

  // create an iterator pointing past the last element
  ListNodeIterator<int>* itr = list.listIterator(0);
  // check that the iterator is initially in a reasonable state
  ASSERT_EQ(list.size(), 2);
  EXPECT_EQ(list[0], 1);
  EXPECT_EQ(list[1], 2);
  EXPECT_TRUE(itr->hasNext());
  EXPECT_FALSE(itr->hasPrevious());
  // erasing the nonexistent previous-element does nothing
  EXPECT_FALSE(list.erasePrevious(itr));
  // erase the last element and check we are still pointing past the start
  EXPECT_TRUE(list.eraseNext(itr));
  ASSERT_EQ(list.size(), 1);
  EXPECT_EQ(list[0], 2);
  EXPECT_TRUE(itr->hasNext());
  EXPECT_FALSE(itr->hasPrevious());
  // erase the last element and check that we have exhausted the list contents
  EXPECT_TRUE(list.eraseNext(itr));
  ASSERT_EQ(list.size(), 0);
  EXPECT_FALSE(itr->hasNext());
  EXPECT_FALSE(itr->hasPrevious());
  // attempting to erase another element does nothing
  EXPECT_FALSE(list.eraseNext(itr));
  delete itr;
}

/**
Demonstrate that erasing the last element by iterator position results in the
iterator remaining in a reasonable state.
*/
TEST(LinkedList, IteratorEraseLast)
{
  // create a list of 2 elements
  LinkedList<int> list;
  list.add(1);
  list.add(2);

  // create an iterator pointing past the last element
  ListNodeIterator<int>* itr = list.listIterator(list.size());
  // check that the iterator is initially in a reasonable state
  ASSERT_EQ(list.size(), 2);
  EXPECT_EQ(list[0], 1);
  EXPECT_EQ(list[1], 2);
  EXPECT_FALSE(itr->hasNext());
  EXPECT_TRUE(itr->hasPrevious());
  // erasing the nonexistent next-element does nothing
  EXPECT_FALSE(list.eraseNext(itr));
  // erase the last element and check we are still pointing past the end
  EXPECT_TRUE(list.erasePrevious(itr));
  ASSERT_EQ(list.size(), 1);
  EXPECT_EQ(list[0], 1);
  EXPECT_FALSE(itr->hasNext());
  EXPECT_TRUE(itr->hasPrevious());
  // erase the last element and check that we have exhausted the list contents
  EXPECT_TRUE(list.erasePrevious(itr));
  ASSERT_EQ(list.size(), 0);
  EXPECT_FALSE(itr->hasNext());
  EXPECT_FALSE(itr->hasPrevious());
  // attempting to erase another element does nothing
  EXPECT_FALSE(list.erasePrevious(itr));
  delete itr;
}

/**
Check that we can remove elements one at a time.
*/
TEST(LinkedList, RemoveElement)
{
  LinkedList<int> list{ 1, 2, 3, 2, 1 };
  // removing elements that do not exist does nothing
  ASSERT_FALSE(list.remove(999));
  ASSERT_EQ(list.size(), 5);
  // removing an element removes all instances
  ASSERT_TRUE(list.remove(2));
  ASSERT_EQ(list.size(), 3);
  EXPECT_NE(list[1], 2);
  EXPECT_NE(list[2], 2);
  // check that all the elements match what we expect
  EXPECT_EQ(list[0], 1);
  EXPECT_EQ(list[1], 3);
  EXPECT_EQ(list[2], 1);
}

/**
Check that we can remove several elements. Specific attention to the behavior
of repeated elements.
*/
TEST(LinkedList, RemoveCollection)
{
  LinkedList<int> list{ 1, 2, 2, 3, 3, 3, 4, 4, 4, 4 };
  // removing elements not in the list has no effect
  int arrX[3] = { 991, 992, 993 };
  LinkedList<int> toRemoveX = array2list(arrX, 3);
  ASSERT_FALSE(list.removeAll(toRemoveX));
  ASSERT_EQ(list.size(), 10);
  // removing mixed elements and non-elements removes only the elements
  int arrM[3] = { 1, 3, 999 };
  LinkedList<int> toRemoveMixed = array2list(arrM, 3);
  ASSERT_TRUE(list.removeAll(toRemoveMixed));
  ASSERT_EQ(list.size(), 6);
  {
    ListIterator<int>* itr = list.iterator();
    EXPECT_EQ(itr->next(), 2);
    EXPECT_EQ(itr->next(), 2);
    EXPECT_EQ(itr->next(), 4);
    EXPECT_EQ(itr->next(), 4);
    EXPECT_EQ(itr->next(), 4);
    EXPECT_EQ(itr->next(), 4);
    EXPECT_FALSE(itr->hasNext());
    delete itr;
  }
}

/**
Check that self-removal clears the list.
*/
TEST(LinkedList, RemoveSelf)
{
  LinkedList<int> list{ 1, 2, 3, 4, 5 };
  ASSERT_TRUE(list.removeAll(list));
  ASSERT_TRUE(list.isEmpty());
}

/**
Check that retaining an empty list results in an empty list.
*/
TEST(LinkedList, Clear)
{
  LinkedList<int> list{ 1, 2, 2, 3, 3, 3, 4, 4, 4, 4 };
  ASSERT_FALSE(list.isEmpty());
  list.clear();
  ASSERT_TRUE(list.isEmpty());
}

/**
Demonstrate that the equality operator returns true only if the lists contain
the same elements in the same order.
*/
TEST(LinkedList, EqualityOperator)
{
  // self-equality
  LinkedList<int> list1A{ 1, 2, 3 };
  EXPECT_TRUE(list1A == list1A);
  EXPECT_FALSE(list1A != list1A);
  // lists with the same elements and order are equal
  LinkedList<int> list1B{ 1, 2, 3 };
  EXPECT_TRUE(list1A == list1B);
  EXPECT_FALSE(list1A != list1B);
  EXPECT_TRUE(list1B == list1A);
  EXPECT_FALSE(list1B != list1A);
  // lists with different elements are not equal
  LinkedList<int> list2{ 9, 8, 7 };
  EXPECT_FALSE(list1A == list2);
  EXPECT_TRUE(list1A != list2);
  EXPECT_FALSE(list2 == list1A);
  EXPECT_TRUE(list2 != list1A);
  // lists with the same elements but different order are not equal
  LinkedList<int> list3{ 1, 3, 2 };
  EXPECT_FALSE(list1A == list3);
  EXPECT_TRUE(list1A != list3);
  EXPECT_FALSE(list3 == list1A);
  EXPECT_TRUE(list3 != list1A);
  // lists with a different number of elements are not equal
  LinkedList<int> list4{ 1, 2, 3, 4 };
  EXPECT_FALSE(list1A == list4);
  EXPECT_TRUE(list1A != list4);
  EXPECT_FALSE(list4 == list1A);
  EXPECT_TRUE(list4 != list1A);
}

/**
Demonstrate that the assignment operator makes the LHS set contain the same
elements as the RHS.
*/
TEST(LinkedList, AssignmentOperator)
{
  LinkedList<int> list1{ 1, 2 };
  LinkedList<int> list2{ 1, 2, 3 };
  // the lists are not the same
  ASSERT_TRUE(list1 != list2);
  // perform assignment
  list2 = list1;
  // the lists are now the same
  EXPECT_TRUE(list1 == list2);
}

/*
Demonstrate the move-assign operator via list-swap.
*/
TEST(LinkedList, MoveSwap)
{
  const LinkedList<int> list1c{ 1, 2 };
  LinkedList<int> list1(list1c);
  const LinkedList<int> list2c{ 1, 2, 3 };
  LinkedList<int> list2(list2c);
  // the lists match initial values
  ASSERT_EQ(list1, list1c);
  ASSERT_EQ(list2, list2c);
  // perform swap
  std::swap(list1, list2);
  // the lists are now swapped
  EXPECT_EQ(list1.size(), 3);
  EXPECT_EQ(list2.size(), 2);
  EXPECT_EQ(list2, list1c);
  EXPECT_EQ(list1, list2c);
}

/**
Demonstrate adding an element in the set to itself behaves as expected.
*/
TEST(LinkedList, OwnedElementInsert)
{
  LinkedList<int> list{ 1, 2, 3, 4, 5 };
  // add an element in the set to the set
  ASSERT_TRUE(list.insert(2, list[3]));
  // all elements got added successfully
  EXPECT_EQ(list[2], 4); // there it is!
  // check the rest of the elements
  LinkedList<int> expect{ 1, 2, 4, 3, 4, 5 };
  EXPECT_TRUE(list == expect);
}

/**
Demonstrate iteration over a list allowing modification of elements.
*/
TEST(LinkedList, NonConstIteration)
{
  LinkedList<int> list{ 1, 2, 3 };
  // double each element
  for (int& elem : list)
  {
    elem *= 2;
  }
  // check the values of the elements got doubled
  LinkedList<int> expect{ 2, 4, 6 };
  EXPECT_TRUE(list == expect);
}

/**
Demonstrate that we can successfully obtain the first and last index of
elements.
*/
TEST(LinkedList, FirstLastIndexOf)
{
  LinkedList<int> list{ 1, 999, 3, 1, 999 };
  // one of the "1" instances is the first element
  EXPECT_EQ(list.firstIndexOf(1), 0);
  EXPECT_EQ(list.lastIndexOf(1), 3);
  // one of the "999" instances is the last element
  EXPECT_EQ(list.firstIndexOf(999), 1);
  EXPECT_EQ(list.lastIndexOf(999), 4);
  // only one instance of element "3"
  EXPECT_EQ(list.firstIndexOf(3), 2);
  EXPECT_EQ(list.lastIndexOf(3), 2);
  // no ionstances of element "0"
  EXPECT_EQ(list.firstIndexOf(0), 5);
  EXPECT_EQ(list.lastIndexOf(0), 5);
}

/**
Demonstrate sorting a list using the default sort-function.
*/
TEST(LinkedList, Sort)
{
  // create an un-order list
  LinkedList<int> list{ 0, 9, 1, 8, 2, 7, 3, 6, 4, 5 };
  ASSERT_EQ(list.size(), 10);
  // sort the list
  list.sort();
  // check that the elements are now sorted
  int acc = 0;
  for (int val : list)
  {
    EXPECT_EQ(val, acc++);
  }
  // check there are the correct number of elements
  ASSERT_EQ(acc, 10);
}

/**
Demonstrate that sorting an empty list does not cause errors.
*/
TEST(LinkedList, Sort0)
{
  LinkedList<int> list;
  ASSERT_EQ(list.size(), 0);
  list.sort();
  ASSERT_EQ(list.size(), 0);
}

// a comparison-operation for sorting
static bool greater_than(const int& a, const int& b)
{
  return a > b;
}

/**
Demonstrate sorting a list using a custom sort-function.
*/
TEST(LinkedList, CustomSort)
{
  // create an un-order list
  LinkedList<int> list{ -16000, 1000, 100, 9, 100, 7, 7, 8, 7, 2 };
  ASSERT_EQ(list.size(), 10);
  // sort the list
  list.sort(greater_than);
  int expected[10] = { 1000, 100, 100, 9, 8, 7, 7, 7, 2, -16000 };
  // check that the elements are now sorted
  int acc = 0;
  for (int val : list)
  {
    EXPECT_EQ(val, expected[acc++]);
  }
  // check there are the correct number of elements
  ASSERT_EQ(acc, 10);
}

/**
Demonstrate sorting a large list of elements.
*/
TEST(LinkedList, BigSort)
{
  // create a list to sort
  LinkedList<int> list;
  // put a jumbled mess in the list
  for (int idx = 0; idx < 2500; ++idx)
  {
    list.add(idx & 0xff);
  }
  for (int idx = 0; idx < 2500; ++idx)
  {
    list.add(idx >> 1);
  }
  for (int idx = 0; idx < 2500; ++idx)
  {
    list.add(idx & 0xff);
  }
  for (int idx = 0; idx < 2500; ++idx)
  {
    list.add(idx);
  }
  ASSERT_EQ(list.size(), 10000);
  // sort the list
  list.sort();
  int acc = 0;
  int prev = -1;
  // check that the elements are now listed in ascending order
  for (int val : list)
  {
    ASSERT_LE(prev, val);
    prev = val;
    ++acc;
  }
  // check the list contains the correct number of elements
  ASSERT_EQ(acc, 10000);
}

/**
Demonstrate accessing the first and last elements of the list.
*/
TEST(LinkedList, FirstLast)
{
  LinkedList<int> list;
  for (int idx = 0; idx < 100; ++idx) list.add(idx);

  // const access
  const LinkedList<int>& clist = list;
  EXPECT_EQ(clist.first(), 0);
  EXPECT_EQ(clist.last(), 99);

  // non-const access
  EXPECT_EQ(list.first(), 0);
  EXPECT_EQ(list.last(), 99);

  // change elements at first/last index
  list.first() = 700;
  list.last() = -700;
  EXPECT_EQ(list.first(), 700);
  EXPECT_EQ(list.last(), -700);
}
