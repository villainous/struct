
#include "vcrd.hpp"
#include "detect_equal.hpp"
#include "detect_hash.hpp"

// we can *declare* these without available implementations (yet)
static viln::DetectHash<viln::vcrd_2i>* dhp;
static viln::DetectEqual<viln::vcrd_2i>* dep;

#include "vcrd.inl"
#include "detect_equal.inl"
#include "detect_hash.inl"

#include <gtest/gtest.h>

using namespace viln;

/*
Demonstrate that `DetectHash` works as expected for `vcrd`.
*/
TEST(DetectVcrd, Hash)
{
  vcrd_2i v11 = { 1, 1 };

  // check that vcrd hash works by itself
  uint64_t h11;
  EXPECT_NE((h11 = hash(v11)), 0);

  // check that DetectHash matches the operator== results
  DetectHash<vcrd_2i> dh;
  EXPECT_EQ(dh.hash(v11), h11);
}

/*
Demonstrate that `DetectEqual` works as expected for `vcrd`.
*/
TEST(DetectVcrd, Equal)
{
  vcrd_2i v11 = { 1, 1 };

  // check that vcrd operator== works by itself
  EXPECT_TRUE((v11 == v11));
  EXPECT_TRUE((v11 == vcrd_2i{ 1, 1 }));
  EXPECT_FALSE((v11 == vcrd_2i{ 1, 2 }));

  // check that DetectEqual matches the operator== results
  DetectEqual<vcrd_2i> de;
  EXPECT_TRUE(de.equal(v11, v11));
  EXPECT_TRUE(de.equal(v11, vcrd_2i{ 1, 1 }));
  EXPECT_FALSE(de.equal(v11, vcrd_2i{ 1, 2 }));
}
