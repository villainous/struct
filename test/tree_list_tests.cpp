
#include <utility>
#include "tree_list.inl"

// check this compiles BEFORE gtest leaks symbols
template class viln::TreeList<double>;

#include <gtest/gtest.h>

using namespace viln;

// helper that shortens test definitions
template<typename E>
TreeList<E> array2list(const E* src, int size)
{
  TreeList<E> list;
  for (size_t idx = 0; idx < size; ++idx)
  {
    list.add(src[idx]);
  }
  return list;
}

/**
Check that operator[] behaves as expected.
*/
TEST(TreeList, ArrayAccess)
{
  int arr9[9] = { 1, 2, 3, 912000, 5, 6, 7, 8, 999 };
  TreeList<int> list9 = array2list(arr9, 9);
  const TreeList<int>& clist5 = list9;
  for (int idx = 0; idx < 9; ++idx)
  {
    // non-const access
    EXPECT_EQ(list9[idx], arr9[idx]);
    // const access
    EXPECT_EQ(clist5[idx], arr9[idx]);
  }
}

/**
Check that element access via an iterator can visit each element.
*/
TEST(TreeList, IteratorAccess)
{
  int arr5[5] = { 1, 2, 3, 912000, 5 };
  TreeList<int> list5 = array2list(arr5, 5);
  // check that iterating over the list allows element access
  ListIterator<int>* itr = list5.iterator();
  int idx = 0;
  while (itr->hasNext())
  {
    EXPECT_EQ(itr->next(), arr5[idx++]);
  }
  delete itr;
  EXPECT_EQ(idx, 5);
}

/**
Check that element access via an iterator can visit each element.
*/
TEST(TreeList, ConstIteratorAccess)
{
  int arr5[5] = { 11, 22, 33, 44, 55 };
  TreeList<int> list5 = array2list(arr5, 5);
  // check that the const-qualified iterator allows const access
  const TreeList<int>& clist5 = list5;
  ListIterator<const int>* citr = clist5.iterator();
  int idx = 0;
  while (citr->hasNext())
  {
    EXPECT_EQ(citr->next(), arr5[idx++]);
  }
  delete citr;
  EXPECT_EQ(idx, 5);
}

/**
Check that iteration in reverse order behaves as expected.
*/
TEST(TreeList, ReverseIteratorAccess)
{
  int arr9[9] = { 1, 2, 3, 4, -37, 66, 777, 8888, 9999 };
  // check that iterating over the list allows element access
  TreeList<int> list9 = array2list(arr9, 9);
  ListIterator<int>* ritr = list9.listIterator(9);
  int idx = 9;
  while (ritr->hasPrevious())
  {
    EXPECT_EQ(ritr->previous(), arr9[--idx]);
  }
  delete ritr;
  EXPECT_EQ(idx, 0);
}

/**
Check that single-element append behaves as expected.
*/
TEST(TreeList, AddElement)
{
  TreeList<int> list;
  ASSERT_TRUE(list.add(1));
  ASSERT_TRUE(list.add(2));
  ASSERT_TRUE(list.add(3));
  ASSERT_TRUE(list.add(4));
  ASSERT_EQ(list.size(), 4);
  EXPECT_EQ(list[0], 1);
  EXPECT_EQ(list[1], 2);
  EXPECT_EQ(list[2], 3);
  EXPECT_EQ(list[3], 4);
}

/**
Check that we can append lists.
*/
TEST(TreeList, AddCollection)
{
  TreeList<int> list;
  // adding an empty collection does NOT modify the list
  TreeList<int> listX;
  ASSERT_FALSE(list.addAll(listX));
  ASSERT_TRUE(list.isEmpty());
  // adding lists DOES modify the list
  int arrA[2] = { 11, 12 };
  int arrB[2] = { 21, 22 };
  TreeList<int> listA = array2list(arrA, 2);
  TreeList<int> listB = array2list(arrB, 2);
  ASSERT_TRUE(list.addAll(listA));
  ASSERT_TRUE(list.addAll(listB));
  ASSERT_EQ(list.size(), 4);
  // check that the content is as expected
  EXPECT_EQ(list[0], 11);
  EXPECT_EQ(list[1], 12);
  EXPECT_EQ(list[2], 21);
  EXPECT_EQ(list[3], 22);
}

/**
Check that appending a list to itself works.
*/
TEST(TreeList, AddSelf)
{
  int arr[3] = { 1, 22, 333 };
  TreeList<int> list = array2list(arr, 3);
  ASSERT_TRUE(list.addAll(list));
  ASSERT_EQ(list.size(), 6);
  for (int idx = 0; idx < 3; ++idx)
  {
    ASSERT_EQ(list[idx], list[idx + 3]);
  }
}

/**
Check that single-element insertion behaves as expected.
*/
TEST(TreeList, InsertElement)
{
  TreeList<int> list{ 1, 2, 3 };
  // show that we can insert elements in the middle
  ASSERT_TRUE(list.insert(1, 91));
  ASSERT_TRUE(list.insert(1, 92));
  ASSERT_TRUE(list.insert(1, 93));
  ASSERT_EQ(list.size(), 6);
  // show we can insert an element at the end
  ASSERT_TRUE(list.insert(6, 94));
  ASSERT_EQ(list.size(), 7);
  // check that the content is inserted as expected
  EXPECT_EQ(list[0], 1);
  EXPECT_EQ(list[1], 93);
  EXPECT_EQ(list[2], 92);
  EXPECT_EQ(list[3], 91);
  EXPECT_EQ(list[4], 2);
  EXPECT_EQ(list[5], 3);
  EXPECT_EQ(list[6], 94);
}

/**
Check that we can insert one list within another.
*/
TEST(TreeList, InsertCollection)
{
  TreeList<int> listA{ 11, 12 };
  TreeList<int> listB{ 21, 22 };
  TreeList<int> list(listA);
  ASSERT_TRUE(list.insertAll(1, listB));
  ASSERT_EQ(list.size(), 4);
  EXPECT_EQ(list[0], 11);
  EXPECT_EQ(list[1], 21);
  EXPECT_EQ(list[2], 22);
  EXPECT_EQ(list[3], 12);
}

/**
Check that we can insert a list into itself.
*/
TEST(TreeList, InsertSelf)
{
  TreeList<int> list{ 1, 2, 3, 4 };
  // check that the insertion itself succeeds
  ASSERT_TRUE(list.insertAll(2, list));
  ASSERT_EQ(list.size(), 8);
  // validate the new values
  EXPECT_EQ(list[0], 1);
  EXPECT_EQ(list[1], 2);
  EXPECT_EQ(list[2], 1);
  EXPECT_EQ(list[3], 2);
  EXPECT_EQ(list[4], 3);
  EXPECT_EQ(list[5], 4);
  EXPECT_EQ(list[6], 3);
  EXPECT_EQ(list[7], 4);
}

/**
Check that elements can be cleared and then added.
*/
TEST(TreeList, ClearAdd)
{
  TreeList<int> list{ 1, 2 };
  ASSERT_FALSE(list.isEmpty());
  list.clear();
  ASSERT_TRUE(list.isEmpty());
  ASSERT_TRUE(list.add(3));
  ASSERT_FALSE(list.isEmpty());
}

/**
Check that we can delete the first element.
*/
TEST(TreeList, EraseFirst)
{
  TreeList<int> list{ 1, 22, 333, 4444 };
  // deletion of the last element
  ASSERT_TRUE(list.erase(0));
  ASSERT_EQ(list.size(), 3);
  // check on the values of the resulting list
  EXPECT_EQ(list[0], 22);
  EXPECT_EQ(list[1], 333);
  EXPECT_EQ(list[2], 4444);
}

/**
Check that we can delete elements in the middle.
*/
TEST(TreeList, EraseMiddle)
{
  TreeList<int> list{ 1, 22, 333, 4444 };
  // deletion of the last element
  ASSERT_TRUE(list.erase(2));
  ASSERT_EQ(list.size(), 3);
  // check on the values of the resulting list
  EXPECT_EQ(list[0], 1);
  EXPECT_EQ(list[1], 22);
  EXPECT_EQ(list[2], 4444);
}

/**
Check that we can delete the last element.
*/
TEST(TreeList, EraseLast)
{
  TreeList<int> list{ 1, 22, 333, 4444 };
  // deletion of the last element
  ASSERT_TRUE(list.erase(3));
  ASSERT_EQ(list.size(), 3);
  // check on the values of the resulting list
  EXPECT_EQ(list[0], 1);
  EXPECT_EQ(list[1], 22);
  EXPECT_EQ(list[2], 333);
}

/**
Check that we can delete a bunch of scattered elements.
*/
TEST(TreeList, EraseScatter)
{
  TreeList<int> list{ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
  // deletion a bunch of elements all over the place
  ASSERT_TRUE(list.erase(9));
  ASSERT_EQ(list.size(), 9);
  ASSERT_TRUE(list.erase(3));
  ASSERT_EQ(list.size(), 8);
  ASSERT_TRUE(list.erase(3));
  ASSERT_EQ(list.size(), 7);
  ASSERT_TRUE(list.erase(4));
  ASSERT_EQ(list.size(), 6);
  ASSERT_TRUE(list.erase(4));
  ASSERT_EQ(list.size(), 5);
  ASSERT_TRUE(list.erase(2));
  ASSERT_EQ(list.size(), 4);
  // check on the values of the resulting list
  EXPECT_EQ(list[0], 1);
  EXPECT_EQ(list[1], 2);
  EXPECT_EQ(list[2], 6);
  EXPECT_EQ(list[3], 9);
}

/**
Check that erasing multiple contiguous indices works.
*/
TEST(TreeList, EraseMultiContiguous)
{
  TreeList<int> list{ 1, 2, 3, 4 };
  ListIndices indices;
  ASSERT_TRUE(indices.add(1));
  ASSERT_TRUE(indices.add(2));
  ASSERT_TRUE(list.eraseAll(indices));
  ASSERT_EQ(list.size(), 2);
  EXPECT_EQ(list[0], 1);
  EXPECT_EQ(list[1], 4);
}

/**
Check that erasing multiple discontiguous indices works.
*/
TEST(TreeList, EraseMultiDiscontiguous)
{
  TreeList<int> list{ 1, 2, 3, 4, 5 };
  ListIndices indices;
  ASSERT_TRUE(indices.add(1));
  ASSERT_TRUE(indices.add(3));
  ASSERT_TRUE(list.eraseAll(indices));
  ASSERT_EQ(list.size(), 3);
  EXPECT_EQ(list[0], 1);
  EXPECT_EQ(list[1], 3);
  EXPECT_EQ(list[2], 5);
}

/**
Check that we can delete a more complex list of indices.
*/
TEST(TreeList, EraseMultiScatter)
{
  TreeList<int> list{ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
  // deletion a bunch of elements all over the place
  ListIndices indices;
  ASSERT_TRUE(indices.add(2));
  ASSERT_TRUE(indices.add(3));
  ASSERT_TRUE(indices.add(4));
  ASSERT_TRUE(indices.add(6));
  ASSERT_TRUE(indices.add(7));
  ASSERT_TRUE(indices.add(9));
  ASSERT_TRUE(list.eraseAll(indices));
  ASSERT_EQ(list.size(), 4);
  // check on the values of the resulting list
  EXPECT_EQ(list[0], 1);
  EXPECT_EQ(list[1], 2);
  EXPECT_EQ(list[2], 6);
  EXPECT_EQ(list[3], 9);
}

/**
Check that we can remove elements one at a time.
*/
TEST(TreeList, RemoveElement)
{
  TreeList<int> list{ 1, 2, 3, 2, 1 };
  // removing elements that do not exist does nothing
  ASSERT_FALSE(list.remove(999));
  ASSERT_EQ(list.size(), 5);
  // removing an element removes all instances
  ASSERT_TRUE(list.remove(2));
  ASSERT_EQ(list.size(), 3);
  EXPECT_NE(list[1], 2);
  EXPECT_NE(list[2], 2);
  // check that all the elements match what we expect
  EXPECT_EQ(list[0], 1);
  EXPECT_EQ(list[1], 3);
  EXPECT_EQ(list[2], 1);
}

/**
Check that we can remove several elements. Specific attention to the behavior
of repeated elements.
*/
TEST(TreeList, RemoveCollection)
{
  TreeList<int> list{ 1, 2, 2, 3, 3, 3, 4, 4, 4, 4 };
  // removing elements not in the list has no effect
  int arrX[3] = { 991, 992, 993 };
  TreeList<int> toRemoveX = array2list(arrX, 3);
  ASSERT_FALSE(list.removeAll(toRemoveX));
  ASSERT_EQ(list.size(), 10);
  // removing mixed elements and non-elements removes only the elements
  int arrM[3] = { 1, 3, 999 };
  TreeList<int> toRemoveMixed = array2list(arrM, 3);
  ASSERT_TRUE(list.removeAll(toRemoveMixed));
  ASSERT_EQ(list.size(), 6);
  // check that all the values come up as expected
  EXPECT_EQ(list[0], 2);
  EXPECT_EQ(list[1], 2);
  EXPECT_EQ(list[2], 4);
  EXPECT_EQ(list[3], 4);
  EXPECT_EQ(list[4], 4);
  EXPECT_EQ(list[5], 4);
}

/**
Check that self-removal clears the list.
*/
TEST(TreeList, RemoveSelf)
{
  TreeList<int> list{ 1, 2, 3, 4, 5 };
  ASSERT_TRUE(list.removeAll(list));
  ASSERT_TRUE(list.isEmpty());
}

/**
Check that retaining an empty list results in an empty list.
*/
TEST(TreeList, Clear)
{
  TreeList<int> list{ 1, 2, 2, 3, 3, 3, 4, 4, 4, 4 };
  ASSERT_FALSE(list.isEmpty());
  list.clear();
  ASSERT_TRUE(list.isEmpty());
}

/**
Demonstrate that the equality operator returns true only if the lists contain
the same elements in the same order.
*/
TEST(TreeList, EqualityOperator)
{
  // self-equality
  TreeList<int> list1A{ 1, 2, 3 };
  EXPECT_TRUE(list1A == list1A);
  EXPECT_FALSE(list1A != list1A);
  // lists with the same elements and order are equal
  TreeList<int> list1B{ 1, 2, 3 };
  EXPECT_TRUE(list1A == list1B);
  EXPECT_FALSE(list1A != list1B);
  EXPECT_TRUE(list1B == list1A);
  EXPECT_FALSE(list1B != list1A);
  // lists with different elements are not equal
  TreeList<int> list2{ 9, 8, 7 };
  EXPECT_FALSE(list1A == list2);
  EXPECT_TRUE(list1A != list2);
  EXPECT_FALSE(list2 == list1A);
  EXPECT_TRUE(list2 != list1A);
  // lists with the same elements but different order are not equal
  TreeList<int> list3{ 1, 3, 2 };
  EXPECT_FALSE(list1A == list3);
  EXPECT_TRUE(list1A != list3);
  EXPECT_FALSE(list3 == list1A);
  EXPECT_TRUE(list3 != list1A);
  // lists with a different number of elements are not equal
  TreeList<int> list4{ 1, 2, 3, 4 };
  EXPECT_FALSE(list1A == list4);
  EXPECT_TRUE(list1A != list4);
  EXPECT_FALSE(list4 == list1A);
  EXPECT_TRUE(list4 != list1A);
}

/**
Demonstrate that the assignment operator makes the LHS set contain the same
elements as the RHS.
*/
TEST(TreeList, AssignmentOperator)
{
  TreeList<int> list1{ 1, 2 };
  TreeList<int> list2{ 1, 2, 3 };
  // the lists are not the same
  ASSERT_TRUE(list1 != list2);
  // perform assignment
  list2 = list1;
  // the lists are now the same
  EXPECT_TRUE(list1 == list2);
}

/*
Demonstrate the move-assign operator via list-swap.
*/
TEST(TreeList, MoveSwap)
{
  const TreeList<int> list1c{ 1, 2 };
  TreeList<int> list1(list1c);
  const TreeList<int> list2c{ 1, 2, 3 };
  TreeList<int> list2(list2c);
  // the lists match initial values
  ASSERT_EQ(list1, list1c);
  ASSERT_EQ(list2, list2c);
  // perform swap
  std::swap(list1, list2);
  // the lists are now swapped
  EXPECT_EQ(list1.size(), 3);
  EXPECT_EQ(list2.size(), 2);
  EXPECT_EQ(list2, list1c);
  EXPECT_EQ(list1, list2c);
}

/**
Demonstrate adding an element in the set to itself behaves as expected.
*/
TEST(TreeList, OwnedElementInsert)
{
  TreeList<int> list{ 1, 2, 3, 4, 5 };
  // add an element in the set to the set
  ASSERT_TRUE(list.insert(2, list[3]));
  // all elements got added successfully
  EXPECT_EQ(list[2], 4); // there it is!
  // check the rest of the elements
  TreeList<int> expect{ 1, 2, 4, 3, 4, 5 };
  EXPECT_TRUE(list == expect);
}

/**
Demonstrate iteration over a list allowing modification of elements.
*/
TEST(TreeList, NonConstIteration)
{
  TreeList<int> list{ 1, 2, 3 };
  // double each element
  for (int& elem : list)
  {
    elem *= 2;
  }
  // check the values of the elements got doubled
  TreeList<int> expect{ 2, 4, 6 };
  EXPECT_TRUE(list == expect);
}

/**
Demonstrate that we can successfully obtain the first and last index of
elements.
*/
TEST(TreeList, FirstLastIndexOf)
{
  TreeList<int> list{ 1, 999, 3, 1, 999 };
  // one of the "1" instances is the first element
  EXPECT_EQ(list.firstIndexOf(1), 0);
  EXPECT_EQ(list.lastIndexOf(1), 3);
  // one of the "999" instances is the last element
  EXPECT_EQ(list.firstIndexOf(999), 1);
  EXPECT_EQ(list.lastIndexOf(999), 4);
  // only one instance of element "3"
  EXPECT_EQ(list.firstIndexOf(3), 2);
  EXPECT_EQ(list.lastIndexOf(3), 2);
  // no ionstances of element "0"
  EXPECT_EQ(list.firstIndexOf(0), 5);
  EXPECT_EQ(list.lastIndexOf(0), 5);
}


/**
Demonstrate sorting a list using the default sort-function.
*/
TEST(TreeList, Sort)
{
  // create an un-order list
  TreeList<int> list{ 0, 9, 1, 8, 2, 7, 3, 6, 4, 5 };
  ASSERT_EQ(list.size(), 10);
  // sort the list
  list.sort();
  // check that the elements are now sorted
  int acc = 0;
  for (int val : list)
  {
    EXPECT_EQ(val, acc++);
  }
  // check there are the correct number of elements
  ASSERT_EQ(acc, 10);
}

// a comparison-operation for sorting
static bool greater_than(const int& a, const int& b)
{
  return a > b;
}

/**
Demonstrate sorting a list using a custom sort-function.
*/
TEST(TreeList, CustomSort)
{
  // create an un-order list
  TreeList<int> list{ -16000, 1000, 100, 9, 100, 7, 7, 8, 7, 2 };
  ASSERT_EQ(list.size(), 10);
  // sort the list
  list.sort(greater_than);
  int expected[10] = { 1000, 100, 100, 9, 8, 7, 7, 7, 2, -16000 };
  // check that the elements are now sorted
  int acc = 0;
  for (int val : list)
  {
    EXPECT_EQ(val, expected[acc++]);
  }
  // check there are the correct number of elements
  ASSERT_EQ(acc, 10);
}

/**
Demonstrate sorting a large list of elements.
*/
TEST(TreeList, BigSort)
{
  // create a list to sort
  TreeList<int> list;
  // put a jumbled mess in the list
  for (int idx = 0; idx < 2500; ++idx)
  {
    list.add(idx & 0xff);
  }
  for (int idx = 0; idx < 2500; ++idx)
  {
    list.add(idx >> 1);
  }
  for (int idx = 0; idx < 2500; ++idx)
  {
    list.add(idx & 0xff);
  }
  for (int idx = 0; idx < 2500; ++idx)
  {
    list.add(idx);
  }
  ASSERT_EQ(list.size(), 10000);
  // sort the list
  list.sort();
  int acc = 0;
  int prev = -1;
  // check that the elements are now listed in ascending order
  for (int val : list)
  {
    ASSERT_LE(prev, val);
    prev = val;
    ++acc;
  }
  // check the list contains the correct number of elements
  ASSERT_EQ(acc, 10000);
}
