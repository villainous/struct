#ifndef VILN_CANARY_HPP
#define VILN_CANARY_HPP

#include "list_node.inl"

#include "list_node_iterator.hpp"

/**
Create an object to test copy/move constructor behavior. Tracks the number of
calls to each constructor/destructor type. Has `health` as a member and `data`
as a pointer-member to demonstrate both types of data can be managed in a list.
*/
class Canary
{
public:
  Canary(int health = 1);
  Canary(Canary&& o);
  Canary(const Canary& o);
  virtual ~Canary(void);
  static void reset(void);

  static int constructor;
  static int move;
  static int copy;
  static int destructor;
  int data;
};

#endif
