
#include <gtest/gtest.h>
#include "vcrd.inl"

using namespace viln;

/*
Demonstrate that equality operators behave as expected.
*/
TEST(VCrd3, Equality)
{
  // self-equality
  vcrd_3i crd1A{ 1, 2, 3 };
  EXPECT_TRUE(crd1A == crd1A);
  EXPECT_FALSE(crd1A != crd1A);
  // equals
  vcrd_3i crd1B{ 1, 2, 3 };
  EXPECT_TRUE(crd1A == crd1B);
  EXPECT_FALSE(crd1A != crd1B);
  // not equals
  vcrd_3i crd2{ 3, 4, 5 };
  EXPECT_FALSE(crd1A == crd2);
  EXPECT_TRUE(crd1A != crd2);
  // element-equality
  vcrd_3i crd3{ 9, 9, 9 };
  EXPECT_TRUE(crd3 == 9);
  EXPECT_FALSE(crd3 != 9);
  vcrd_3i crd4{ 9, 9, 8 };
  EXPECT_FALSE(crd4 == 9);
  EXPECT_TRUE(crd4 != 9);
}

/*
Demonstrate assignment to vcrd[3] from various types.
*/
TEST(VCrd3, Convert)
{
  vcrd_3i crd = {};
  EXPECT_EQ(crd, (vcrd_3i{ 0, 0, 0 }));
  // vcrd-assign
  const vcrd_3i crdA = { 11, 111, 1 };
  EXPECT_EQ((crd = crdA), (vcrd_3i{ 11, 111, 1 }));
  EXPECT_EQ(crd, (vcrd_3i{ 11, 111, 1 }));
  // ucrd-assign
  EXPECT_EQ(crd = ucrd::Y, (vcrd_3i{ 0, 1, 0 }));
  EXPECT_EQ(crd, (vcrd_3i{ 0, 1, 0 }));
  // element-assign
  EXPECT_EQ(crd = 234, (vcrd_3i{ 234, 234, 234 }));
  EXPECT_EQ(crd, (vcrd_3i{ 234, 234, 234 }));
  // vcrd<-assign
  const vcrd<int,1> crd1 = { 11 };
  EXPECT_EQ(crd = crd1, (vcrd_3i{ 11, 0, 0 }));
  EXPECT_EQ(crd, (vcrd_3i{ 11, 0, 0 }));
  // vcrd>-assign
  const vcrd<int,5> crd5 = { 5, 4, 3, 2, 1 };
  EXPECT_EQ(crd = crd5, (vcrd_3i{ 5, 4, 3 }));
  EXPECT_EQ(crd, (vcrd_3i{ 5, 4, 3 }));
}

/*
Verify that the `+` `+=` `++` operators behave as expected.
*/
TEST(VCrd3, Plus)
{
  const vcrd_3i crd1{ 1, 2, 3 };
  const vcrd_3i crd2{ 3, 4, 5 };
  vcrd_3i crd = {};
  // plus
  EXPECT_EQ((crd1 + crd2), (vcrd_3i{ 4, 6, 8 }));
  // ucrd plus
  EXPECT_EQ((crd1 + ucrd::X), (vcrd_3i{ 2, 2, 3 }));
  // element plus
  EXPECT_EQ((crd1 + 5), (vcrd_3i{ 6, 7, 8 }));
  // assign-plus
  crd = crd1;
  EXPECT_EQ((crd += crd2), (vcrd_3i{ 4, 6, 8 }));
  EXPECT_EQ(crd, (vcrd_3i{ 4, 6, 8 }));
  // ucrd assign-plus
  crd = crd1;
  EXPECT_EQ((crd += ucrd::Z), (vcrd_3i{ 1, 2, 4 }));
  EXPECT_EQ(crd, (vcrd_3i{ 1, 2, 4 }));
  // element assign-plus
  crd = crd1;
  EXPECT_EQ((crd += 5), (vcrd_3i{ 6, 7, 8 }));
  EXPECT_EQ(crd, (vcrd_3i{ 6, 7, 8 }));
  // left-increment
  crd = crd1;
  EXPECT_EQ(++crd, (vcrd_3i{ 2, 3, 4 }));
  EXPECT_EQ(crd, (vcrd_3i{ 2, 3, 4 }));
}

/*
Verify that the `-` `-=` `--` operators behave as expected.
*/
TEST(VCrd3, Minus)
{
  const vcrd_3i crd1{ 1, 2, 3 };
  const vcrd_3i crd2{ 3, 4, 5 };
  vcrd_3i crd = {};
  // negate
  EXPECT_EQ(-crd1, (vcrd_3i{ -1, -2, -3 }));
  // minus
  EXPECT_EQ((crd1 - crd2), (vcrd_3i{ -2, -2, -2 }));
  // ucrd minus
  EXPECT_EQ((crd1 - ucrd::X), (vcrd_3i{ 0, 2, 3 }));
  // element minus
  EXPECT_EQ((crd1 - 5), (vcrd_3i{ -4, -3, -2 }));
  // assign-minus
  crd = crd1;
  EXPECT_EQ((crd -= crd2), (vcrd_3i{ -2, -2, -2 }));
  EXPECT_EQ(crd, (vcrd_3i{ -2, -2, -2 }));
  // ucrd assign-minus
  crd = crd1;
  EXPECT_EQ((crd -= ucrd::Z), (vcrd_3i{ 1, 2, 2 }));
  EXPECT_EQ(crd, (vcrd_3i{ 1, 2, 2 }));
  // element assign-minus
  crd = crd1;
  EXPECT_EQ((crd -= 5), (vcrd_3i{ -4, -3, -2 }));
  EXPECT_EQ(crd, (vcrd_3i{ -4, -3, -2 }));
  // left-decrement
  crd = crd1;
  EXPECT_EQ(--crd, (vcrd_3i{ 0, 1, 2 }));
  EXPECT_EQ(crd, (vcrd_3i{ 0, 1, 2 }));
}

/*
Verify that the `*` `*=` operators behave as expected.
*/
TEST(VCrd3, Multiply)
{
  const vcrd_3i crd1{ 1, 2, 3 };
  const vcrd_3i crd2{ 3, -1, 0 };
  const int fac = 12;
  vcrd_3i crd = {};
  // multiply
  EXPECT_EQ((crd1 * crd2), (vcrd_3i{ 3, -2, 0 }));
  // element multiply
  EXPECT_EQ((crd1 * fac), (vcrd_3i{ 12, 24, 36 }));
  // assign-multiply
  crd = crd1;
  EXPECT_EQ((crd *= crd2), (vcrd_3i{ 3, -2, 0 }));
  EXPECT_EQ(crd, (vcrd_3i{ 3, -2, 0 }));
  // element assign-multiply
  crd = crd1;
  EXPECT_EQ((crd *= fac), (vcrd_3i{ 12, 24, 36 }));
  EXPECT_EQ(crd, (vcrd_3i{ 12, 24, 36 }));
}

/*
Verify that the `/` `/=` operators and `div_floor` behave as expected.
*/
TEST(VCrd3, Divide)
{
  const vcrd_3i crd1{ 12, 24, 36 };
  const vcrd_3i crd2{ 11, -11, 1 };
  const int fac = 11;
  vcrd_3i crd = {};
  // div
  EXPECT_EQ((crd1 / crd2), (vcrd_3i{ 1, -2, 36 }));
  // element div
  EXPECT_EQ((crd1 / fac), (vcrd_3i{ 1, 2, 3 }));
  // div-assign
  crd = crd1;
  EXPECT_EQ((crd /= crd2), (vcrd_3i{ 1, -2, 36 }));
  EXPECT_EQ(crd, (vcrd_3i{ 1, -2, 36 }));
  // element div-assign
  crd = crd1;
  EXPECT_EQ((crd /= fac), (vcrd_3i{ 1, 2, 3 }));
  EXPECT_EQ(crd, (vcrd_3i{ 1, 2, 3 }));
}

/*
Verify that the div_floor` and `div_ceil` functions behave as expected.
*/
TEST(VCrd3, Divide_Special)
{
  const vcrd_3i crd1{ 12, 24, 36 };
  const vcrd_3i crd2{ 11, -11, 1 };
  const int fac = 11;
  vcrd_3i crd = {};
  // div_floor
  EXPECT_EQ(div_floor(crd1, crd2), (vcrd_3i{ 1, -3, 36 }));
  // element div_floor
  EXPECT_EQ(div_floor(crd1, fac), (vcrd_3i{ 1, 2, 3 }));
  // div_ceil
  EXPECT_EQ(div_ceil(crd1, crd2), (vcrd_3i{ 2, -2, 36 }));
  // element div_ceil
  EXPECT_EQ(div_ceil(crd1, fac), (vcrd_3i{ 2, 3, 4 }));
}

/*
Verify that the `%` `%=` operators and `mod_floor` behave as expected.
*/
TEST(VCrd3, Mod)
{
  const vcrd_3i crd1{ 12, 24, 36 };
  const vcrd_3i crd2{ 11, -11, 1 };
  const int fac = 11;
  vcrd_3i crd = {};
  // mod
  EXPECT_EQ((crd1 % crd2), (vcrd_3i{ 1, 2, 0 }));
  // element mod
  EXPECT_EQ((crd1 % fac), (vcrd_3i{ 1, 2, 3 }));
  // mod-assign
  crd = crd1;
  EXPECT_EQ((crd %= crd2), (vcrd_3i{ 1, 2, 0 }));
  EXPECT_EQ(crd, (vcrd_3i{ 1, 2, 0 }));
  // element mod-assign
  crd = crd1;
  EXPECT_EQ((crd %= fac), (vcrd_3i{ 1, 2, 3 }));
  EXPECT_EQ(crd, (vcrd_3i{ 1, 2, 3 }));
  // mod_floor
  EXPECT_EQ(mod_floor(crd1, crd2), (vcrd_3i{ 1, -9, 0 }));
  // element mod_floor
  EXPECT_EQ(mod_floor(crd1, fac), (vcrd_3i{ 1, 2, 3 }));
}

/*
Demonstrate math operations.
*/
TEST(VCrd3, Math)
{
  const vcrd_3i crd1{ 12, -24, 36 };
  const vcrd_3i crd2{ 40, -40, 40 };
  // abs
  EXPECT_EQ(abs(crd1), (vcrd_3i{ 12, 24, 36 }));
  EXPECT_EQ(abs(-crd1), (vcrd_3i{ 12, 24, 36 }));
  // sgn
  EXPECT_EQ(sgn(crd1), (vcrd_3i{ 1, -1, 1 }));
  EXPECT_EQ(sgn(-crd1), (vcrd_3i{ -1, 1, -1 }));
  // max1
  EXPECT_EQ(max(crd1), 36);
  // max2
  EXPECT_EQ(max(crd1, crd2), (vcrd_3i{ 40, -24, 40 }));
  EXPECT_EQ(max(crd1, 11), (vcrd_3i{ 12, 11, 36 }));
  // min1
  EXPECT_EQ(min(crd1), -24);
  // min2
  EXPECT_EQ(min(crd1, crd2), (vcrd_3i{ 12, -40, 36 }));
  EXPECT_EQ(min(crd1, 11), (vcrd_3i{ 11, -24, 11 }));
}

/*
Demonstrate vector-math operations.
*/
TEST(VCrd3, Vector)
{
  const vcrd_3i crd1{ 12, -24, 1 };
  const vcrd_3i crd2{ 3, 10, 0 };
  // volume
  EXPECT_EQ(volume(crd1), 288);
  EXPECT_EQ(volume(-crd1), 288);
  // dot
  EXPECT_EQ(dot(crd1, crd2), -204);
  // lengths
  EXPECT_EQ(length_max(crd1), 24);
  EXPECT_EQ(length1(crd1), 37);
  EXPECT_EQ(length(crd1), 26);
  EXPECT_GE(length_oct(crd1), length_max(crd1));
  EXPECT_LE(length_oct(crd1), length1(crd1));
}

/*
Demonstrate 2d vector creation and access.
*/
TEST(VCrd3, 3x3)
{
  vcrd<vcrd<int, 3>, 3> crd3x3{
    vcrd_3i{  1,  2,  3 },
    vcrd_3i{  5,  6,  7 },
    vcrd_3i{  9, 10, 11 } };
  // spot-check the values are accessible
  EXPECT_EQ(crd3x3[0][0], 1);
  EXPECT_EQ(crd3x3[0][1], 2);
  EXPECT_EQ(crd3x3[1][0], 5);
  EXPECT_EQ(crd3x3[1][1], 6);
  EXPECT_EQ(crd3x3[2][2], 11);
}
