
#include <gtest/gtest.h>
#include "bool_ords.hpp"

using namespace viln;

// cardinal and ordinal directions must be consistent
static_assert(bool_6c::N == bool_6o_t(bool_6o::N));
static_assert(bool_6c::S == bool_6o_t(bool_6o::S));
static_assert(bool_6c::E == bool_6o_t(bool_6o::E));
static_assert(bool_6c::W == bool_6o_t(bool_6o::W));
static_assert(bool_6c::U == bool_6o_t(bool_6o::U));
static_assert(bool_6c::D == bool_6o_t(bool_6o::D));

/*
Demonstrate conversions from cardinal-directions to characters.
*/
TEST(BoolOrds, CardToChar)
{
  using namespace bool_card_bits;
  // conversions for simple directions
  EXPECT_EQ(to_char(N), 'N');
  EXPECT_EQ(to_char(S), 'S');
  EXPECT_EQ(to_char(E), 'E');
  EXPECT_EQ(to_char(W), 'W');
  EXPECT_EQ(to_char(U), 'U');
  EXPECT_EQ(to_char(D), 'D');
  // error for out-of-range values
  EXPECT_EQ(to_char(BAD), '\0');
  EXPECT_EQ(to_char(bool_6c(0xff)), '\0');
}

/*
Demonstrate conversions from characters to cardinal-directions.
*/
TEST(BoolOrds, CardFromChar)
{
  using namespace bool_card_bits;
  // conversions for simple directions
  EXPECT_EQ(from_char('N'), N);
  EXPECT_EQ(from_char('S'), S);
  EXPECT_EQ(from_char('E'), E);
  EXPECT_EQ(from_char('W'), W);
  EXPECT_EQ(from_char('U'), U);
  EXPECT_EQ(from_char('D'), D);
  // case insensitivity
  EXPECT_EQ(from_char('n'), N);
  EXPECT_EQ(from_char('s'), S);
  EXPECT_EQ(from_char('e'), E);
  EXPECT_EQ(from_char('w'), W);
  EXPECT_EQ(from_char('u'), U);
  EXPECT_EQ(from_char('d'), D);
  // error for unknown characters
  EXPECT_EQ(from_char('z'), BAD);
  EXPECT_EQ(from_char('?'), BAD);
}

/*
Check that cardinal-directions can be flipped in all directions.
*/
TEST(BoolOrds, CardFlip)
{
  using namespace bool_card_bits;
  // single values
  EXPECT_EQ(flip(N), S);
  EXPECT_EQ(flip(S), N);
  EXPECT_EQ(flip(E), W);
  EXPECT_EQ(flip(W), E);
  EXPECT_EQ(flip(U), D);
  EXPECT_EQ(flip(D), U);
}

/*
Check that conversions of the ordinals results in the
correct resulting string values.
*/
TEST(BoolOrds, OrdsToString)
{
  using namespace bool_ords_bits;
  char buf[8];
  // conversions for simple directions
  EXPECT_GT(snprint(buf, 8, N), 0);
  EXPECT_STREQ(buf, "N");
  EXPECT_GT(snprint(buf, 8, S), 0);
  EXPECT_STREQ(buf, "S");
  EXPECT_GT(snprint(buf, 8, E), 0);
  EXPECT_STREQ(buf, "E");
  EXPECT_GT(snprint(buf, 8, W), 0);
  EXPECT_STREQ(buf, "W");
  // conversions for pairs
  EXPECT_GT(snprint(buf, 8, NE), 0);
  EXPECT_STREQ(buf, "NE");
  EXPECT_GT(snprint(buf, 8, NW), 0);
  EXPECT_STREQ(buf, "NW");
  EXPECT_GT(snprint(buf, 8, SE), 0);
  EXPECT_STREQ(buf, "SE");
  EXPECT_GT(snprint(buf, 8, SW), 0);
  EXPECT_STREQ(buf, "SW");
  // conversions of special-values
  EXPECT_GT(snprint(buf, 8, NONE), 0);
  EXPECT_STREQ(buf, "none");
  EXPECT_GT(snprint(buf, 8, ALL4), 0);
  EXPECT_STREQ(buf, "NSEW");
  EXPECT_GT(snprint(buf, 8, ALL6), 0);
  EXPECT_STREQ(buf, "NSEWUD");
  // error for undersized buffer
  EXPECT_LT(snprint(buf, 6, NONE), 0);
  // failure for out-of-range value
  EXPECT_EQ(snprint(buf, 8, MAX), 0);
}

/*
Check that conversions of strings to ordinals results in the
correct values.
*/
TEST(BoolOrds, OrdsFromString)
{
  using namespace bool_ords_bits;
  // conversions for simple directions
  EXPECT_EQ(from_string("N"), N);
  EXPECT_EQ(from_string("S"), S);
  EXPECT_EQ(from_string("E"), E);
  EXPECT_EQ(from_string("W"), W);
  EXPECT_EQ(from_string("U"), U);
  EXPECT_EQ(from_string("D"), D);
  // case insensitivity
  EXPECT_EQ(from_string("n"), N);
  EXPECT_EQ(from_string("s"), S);
  EXPECT_EQ(from_string("e"), E);
  EXPECT_EQ(from_string("w"), W);
  EXPECT_EQ(from_string("u"), U);
  EXPECT_EQ(from_string("d"), D);
  // conversions for pairs
  EXPECT_EQ(from_string("NE"), NE);
  EXPECT_EQ(from_string("NW"), NW);
  EXPECT_EQ(from_string("SE"), SE);
  EXPECT_EQ(from_string("SW"), SW);
  // order is ignored
  EXPECT_EQ(from_string("EN"), NE);
  EXPECT_EQ(from_string("WN"), NW);
  EXPECT_EQ(from_string("WE"), EW);
  EXPECT_EQ(from_string("WS"), SW);
  // conversions of special-values
  EXPECT_EQ(from_string("none"), NONE);
  EXPECT_EQ(from_string("NSEW"), ALL4);
  EXPECT_EQ(from_string("NSEWUD"), ALL6);
  // trim tail of search
  EXPECT_EQ(from_string("NNNNE", 4), N);
  EXPECT_EQ(from_string("NNNNE", 5), NE);
  // failure for unrecognized values
  EXPECT_EQ(from_string("zoo"), MAX);
  EXPECT_EQ(from_string("???"), MAX);
  EXPECT_EQ(from_string(" N"), MAX);
  EXPECT_EQ(from_string("N "), MAX);
}

/*
Show that name conversions are complete and self-consistent.
*/
TEST(BoolOrds, StringBounce)
{
  using namespace bool_ords_bits;
  char buf[8];
  for(bool_6o_t idx = 0; idx < MAX; ++idx)
  {
    // forward-conversion successful
    EXPECT_GE(snprint(buf, 8, idx), 0);
    // inverse-conversion returns to original value
    EXPECT_EQ(from_string(buf), idx);
  }
}

/*
Check that ordinals can be flipped in all directions.
*/
TEST(BoolOrds, OrdsFlip)
{
  using namespace bool_ords_bits;
  // single values
  EXPECT_EQ(flip(N), S);
  EXPECT_EQ(flip(S), N);
  EXPECT_EQ(flip(E), W);
  EXPECT_EQ(flip(W), E);
  EXPECT_EQ(flip(U), D);
  EXPECT_EQ(flip(D), U);
  // corner-values
  EXPECT_EQ(flip(NE), SW);
  EXPECT_EQ(flip(NW), SE);
  EXPECT_EQ(flip(SE), NW);
  EXPECT_EQ(flip(SW), NE);
  // axial-values are a noop
  EXPECT_EQ(flip(NS), NS);
  EXPECT_EQ(flip(EW), EW);
  EXPECT_EQ(flip(UD), UD);
  // flipping none/all is a noop
  EXPECT_EQ(flip(NONE), NONE);
  EXPECT_EQ(flip(ALL4), ALL4);
  EXPECT_EQ(flip(ALL6), ALL6);
}

/*
Demonstrate that ordinals can be flipped in specific directions.
*/
TEST(BoolOrds, OrdsFlipDir)
{
  using namespace bool_ords_bits;
  // flipping none/all is a noop
  EXPECT_EQ(flip(NONE, bool_3d::X), NONE);
  EXPECT_EQ(flip(ALL4, bool_3d::X), ALL4);
  EXPECT_EQ(flip(ALL6, bool_3d::X), ALL6);
  // no-flip is a noop
  EXPECT_EQ(flip(N, bool_3d::NONE), N);
  EXPECT_EQ(flip(S, bool_3d::NONE), S);
  EXPECT_EQ(flip(E, bool_3d::NONE), E);
  EXPECT_EQ(flip(W, bool_3d::NONE), W);
  EXPECT_EQ(flip(U, bool_3d::NONE), U);
  EXPECT_EQ(flip(D, bool_3d::NONE), D);
  // flipping N/S in Y-direction swaps them
  EXPECT_EQ(flip(N, bool_3d::Y), S);
  EXPECT_EQ(flip(S, bool_3d::Y), N);
  // flipping N/S in other directions is a noop
  EXPECT_EQ(flip(N, bool_3d::X), N);
  EXPECT_EQ(flip(N, bool_3d::Z), N);
  // flipping E/W in X-direction swaps them
  EXPECT_EQ(flip(E, bool_3d::X), W);
  EXPECT_EQ(flip(W, bool_3d::X), E);
  // flipping E/W in other directions is a noop
  EXPECT_EQ(flip(E, bool_3d::Y), E);
  EXPECT_EQ(flip(E, bool_3d::Z), E);
  // flipping U/D in Z-direction swaps them
  EXPECT_EQ(flip(U, bool_3d::Z), D);
  EXPECT_EQ(flip(D, bool_3d::Z), U);
  // flipping U/D in other directions is a noop
  EXPECT_EQ(flip(U, bool_3d::X), U);
  EXPECT_EQ(flip(U, bool_3d::Y), U);
}

/*
Demonstrate conversion from ordinals to a matching direction.
*/
TEST(BoolOrds, ToAxes)
{
  using namespace bool_ords_bits;
  // single values
  EXPECT_EQ(to_axes(N), bool_3d::Y);
  EXPECT_EQ(to_axes(S), bool_3d::Y);
  EXPECT_EQ(to_axes(E), bool_3d::X);
  EXPECT_EQ(to_axes(W), bool_3d::X);
  EXPECT_EQ(to_axes(U), bool_3d::Z);
  EXPECT_EQ(to_axes(D), bool_3d::Z);
  // axial-values
  EXPECT_EQ(to_axes(NS), bool_3d::Y);
  EXPECT_EQ(to_axes(EW), bool_3d::X);
  EXPECT_EQ(to_axes(UD), bool_3d::Z);
  // corner-values
  EXPECT_EQ(to_axes(NE), bool_3d::XY);
  EXPECT_EQ(to_axes(NW), bool_3d::XY);
  EXPECT_EQ(to_axes(SE), bool_3d::XY);
  EXPECT_EQ(to_axes(SW), bool_3d::XY);
  // special values
  EXPECT_EQ(to_axes(NONE), bool_3d::NONE);
  EXPECT_EQ(to_axes(ALL4), bool_3d::ALL2);
  EXPECT_EQ(to_axes(ALL6), bool_3d::ALL3);
  // bad
  EXPECT_EQ(to_axes(MAX), bool_3d::NONE);
}

/*
Demonstrate conversion from directions to the matching axial pairs.
*/
TEST(BoolOrds, FromAxes)
{
  using namespace bool_ords_bits;
  // axial-values
  EXPECT_EQ(from_axes(bool_3d::Y), NS);
  EXPECT_EQ(from_axes(bool_3d::X), EW);
  EXPECT_EQ(from_axes(bool_3d::Z), UD);
  // special values
  EXPECT_EQ(from_axes(bool_3d::NONE), NONE);
  EXPECT_EQ(from_axes(bool_3d::ALL2), ALL4);
  EXPECT_EQ(from_axes(bool_3d::ALL3), ALL6);
  // bad
  EXPECT_EQ(from_axes(bool_3d::MAX), NONE);
}
