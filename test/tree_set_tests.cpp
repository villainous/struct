
#include <utility>
#include "tree_set.inl"

// check this compiles BEFORE gtest leaks symbols
template class viln::TreeSet<double>;

#include <gtest/gtest.h>
#include <math.h>

using namespace viln;

// Debug subclass of sorted set that allows access to the root element.
class TreeSetDebug : public TreeSet<int>
{
public:
  const TreeNode<int>* root(void) const
  {
    return TreeSet<int>::root();
  }
  TreeNode<int>* root(void)
  {
    return TreeSet<int>::root();
  }
};

// Allow dumping of nodes.
static void
dump(std::ostream& os, const TreeNode<int>& nod, int depth)
{
  if (nod.leftChild())
  {
    dump( os, *(nod.leftChild()), depth + 1);
  }
  os << std::setw(depth) << "";
  os << "h=" << depth;
  os << " d=" << nod.data();
  os << std::endl;
  if (nod.rightChild())
  {
    dump(os, *(nod.rightChild()), depth + 1);
  }
}

// Allow dumping of internal structure of the set.
static std::ostream&
operator<<(std::ostream& os, const TreeSetDebug& si)
{
  if (si.root())
  {
    dump(os, *(si.root()), 0);
  }
  os << std::endl;
  return os;
}

/**
Demonstrate that a set can be constructed, and that a single-element set obeys
the add/remove contracts.
*/
TEST(TreeSet, SimpleAddRemove)
{
  // create a set
  TreeSetDebug si;
  // set should initially be empty
  ASSERT_EQ(si.size(), 0);
  // add an element
  ASSERT_TRUE(si.add(999));
  // the set should now contain one element
  ASSERT_EQ(si.size(), 1);
  ASSERT_TRUE(si.contains(999));
  // attempt to add an existing element does nothing
  ASSERT_FALSE(si.add(999));
  ASSERT_EQ(si.size(), 1);
  // removing an element that is not a member does nothing
  ASSERT_FALSE(si.remove(888));
  ASSERT_EQ(si.size(), 1);
  // removing the element results in an empty set
  ASSERT_TRUE(si.remove(999));
  ASSERT_EQ(si.size(), 0);
  ASSERT_FALSE(si.contains(999));
}

/**
Demonstrate that iteration over elements added to a set are order.
*/
TEST(TreeSet, OrderIteration)
{
  TreeSetDebug si;
  // add a bunch of elements in an out-of-order way
  for (int ii = 0; ii < 5; ++ii)
  {
    for (int jj = 0; jj < 5; ++jj)
    {
      ASSERT_TRUE(si.add(10 + (10*ii) - (jj)));
    }
  }
  ASSERT_EQ(si.size(), 5*5);
  // check that iterating over the elements proceeds in order
  int prev = 0;
  int count = 0;
  for (int next : si)
  {
    EXPECT_GT(next, prev);
    prev = next;
    ++count;
  }
  // check that we visited every element of the set
  EXPECT_EQ(count, si.size());
}

/**
Demonstrate that even when adding elements in the worst-case order, the depth
increases with the log of the size of the set, not linearly.
*/
TEST(TreeSet, DepthOptimization)
{
  TreeSetDebug si;
  // add a bunch of elements in forwards-order
  for (int ii = 0; ii < 1000; ++ii)
  {
    ASSERT_TRUE(si.add(ii));
    double height = (double)si.root()->height();
    float optimalDepth = log2f((float)si.size());
    ASSERT_LE(height, 2.0 * optimalDepth) << si;
  }
  // add a bunch of elements in reverse order
  for (int ii = 1; ii < 1000; ++ii)
  {
    ASSERT_TRUE(si.add(-ii));
    float height = (float)si.root()->height();
    float optimalDepth = log2f((float)si.size());
    ASSERT_LE(height, 2.0 * optimalDepth) << si;
  }
  // simple check that the loops above actually ran
  ASSERT_EQ(si.size(), 1999);
}

/**
Demonstrate that the equality operator returns true only if the sets contain
the same elements.
*/
TEST(TreeSet, EqualityOperator)
{
  // self-equality
  TreeSet<int> set1A;
  set1A.add(1);
  set1A.add(2);
  set1A.add(3);
  EXPECT_TRUE(set1A == set1A);
  EXPECT_FALSE(set1A != set1A);
  // sets with the same elements are equal
  TreeSet<int> set1B;
  set1B.add(3);
  set1B.add(2);
  set1B.add(1);
  EXPECT_TRUE(set1A == set1B);
  EXPECT_FALSE(set1A != set1B);
  EXPECT_TRUE(set1B == set1A);
  EXPECT_FALSE(set1B != set1A);
  // sets with different elements are not equal
  TreeSet<int> set2;
  set2.add(9);
  set2.add(8);
  set2.add(7);
  EXPECT_FALSE(set1A == set2);
  EXPECT_TRUE(set1A != set2);
  EXPECT_FALSE(set2 == set1A);
  EXPECT_TRUE(set2 != set1A);
  // sets with a different number of elements are not equal
  TreeSet<int> set3;
  set3.add(1);
  set3.add(2);
  set3.add(3);
  set3.add(4);
  EXPECT_FALSE(set1A == set3);
  EXPECT_TRUE(set1A != set3);
  EXPECT_FALSE(set3 == set1A);
  EXPECT_TRUE(set3 != set1A);
}

/**
Set should be copyable, resulting in copies of all elements.
*/
TEST(TreeSet, CopyConstructor)
{
  // create two sets containing different elements
  TreeSet<int> set1;
  ASSERT_TRUE(set1.add(1));
  ASSERT_TRUE(set1.add(2));
  {
    // copy the set
    TreeSet<int> set2(set1);
    ASSERT_EQ(set1, set2);
  }
  // make sure the other set falling out of scope didnt delete our nodes
  ASSERT_TRUE(set1.contains(1));
}

/**
Demonstrate that the assignment operator makes the LHS set contain the same
elements as the RHS.
*/
TEST(TreeSet, AssignmentOperator)
{
  // create two sets containing different elements
  TreeSet<int> set1;
  ASSERT_TRUE(set1.add(1));
  ASSERT_TRUE(set1.add(2));
  TreeSet<int> set2;
  ASSERT_TRUE(set2.add(3));
  ASSERT_TRUE(set2.add(4));
  ASSERT_TRUE(set2.add(5));
  // the sets are not the same
  ASSERT_NE(set1, set2);
  // perform assetgnment
  set2 = set1;
  // the sets are now the same
  ASSERT_EQ(set1, set2);
}

/*
Demonstrate the move-assign operator via set-swap.
*/
TEST(TreeSet, MoveSwap)
{
  TreeSet<int> set1;
  set1.add(1);
  set1.add(2);
  const TreeSet<int> set1c(set1);
  TreeSet<int> set2;
  set2.add(1);
  set2.add(2);
  set2.add(3);
  const TreeSet<int> set2c(set2);
  // the sets match initial values
  ASSERT_NE(set1c, set2c);
  ASSERT_EQ(set1, set1c);
  ASSERT_EQ(set2, set2c);
  // perform swap
  std::swap(set1, set2);
  // the sets are now swapped
  EXPECT_EQ(set1.size(), 3);
  EXPECT_EQ(set2.size(), 2);
  EXPECT_EQ(set2, set1c);
  EXPECT_EQ(set1, set2c);
}

/**
Remove elements deep in the tree and show that the ordering is preserved.
*/
TEST(TreeSet, DeepRemove)
{
  TreeSetDebug si;
  // add a bunch of elements
  for (int ii = 0; ii < 100; ++ii)
  {
    ASSERT_TRUE(si.add(ii));
  }
  ASSERT_EQ(si.size(), 100);
  // remove the first 10 elements
  for (int ii = 0; ii < 10; ++ii)
  {
    ASSERT_TRUE(si.remove(ii));
  }
  ASSERT_EQ(si.size(), 90);
  // remove every third element
  for (int ii = 10; ii < 100; ii += 3)
  {
    ASSERT_TRUE(si.remove(ii));
  }
  ASSERT_EQ(si.size(), 60);
  // verify ordering
  int prev = 10;
  for (int ii : si)
  {
    EXPECT_GT(ii, prev);
    prev = ii;
  }
  // check that the elements we didn't remove are all present
  for (int ii = 10; ii < 100 - 3; ii += 3)
  {
    EXPECT_TRUE(si.contains(ii + 1));
    EXPECT_TRUE(si.contains(ii + 2));
  }
}

/**
Demonstrate adding elements in an alternating order as a counterpoint to
"linear" add example.
*/
TEST(TreeSet, ZigZagDepthOptimization)
{
  TreeSetDebug si;
  // add elements alternating positive and negative
  for (int ii = 1; ii <= 1000; ++ii)
  {
    ASSERT_TRUE(si.add(ii));
    ASSERT_TRUE(si.add(-ii));
    // check that optimization is working as we add elements
    double height = (double)si.root()->height();
    float optimalDepth = log2f((float)si.size());
    ASSERT_LE(height, 2.0 * optimalDepth) << si;
  }
  // check that all the added elements are present
  ASSERT_EQ(si.size(), 2000);
  for (int ii = 1; ii <= 1000; ++ii)
  {
    EXPECT_TRUE(si.contains(ii));
    EXPECT_TRUE(si.contains(-ii));
  }
  // remove elements alternating positive and negative
  for (int ii = 1; ii <= 1000; ++ii)
  {
    // check that optimization is working as we add elements
    double height = (double)si.root()->height();
    float optimalDepth = log2f((float)si.size());
    ASSERT_LE(height, 2.0 * optimalDepth) << si;
    // remove elements AFTER because we aren't interested when size == 0
    EXPECT_TRUE(si.remove(ii));
    EXPECT_TRUE(si.remove(-ii));
  }
  ASSERT_EQ(si.size(), 0);
}

/**
Demonstrate that the first element of a sorted set can be accessed
and match the least value in the set.
*/
TEST(TreeSet, First)
{
  TreeSetDebug si;
  for (int ii = 10; ii <= 20; ++ii)
  {
    // add some elements on the extreme ends
    ASSERT_TRUE(si.add(-ii));
    // those elements should now be the first and last elements
    ASSERT_EQ(si.first(), -ii) << si;
  }
  for (int ii = 0; ii < 10; ++ii)
  {
    // add some elements in the middle
    ASSERT_TRUE(si.add(-ii));
    // first and last should still be on the extreme ends
    ASSERT_EQ(si.first(), -20) << si;
  }
}

/**
Demonstrate that the last element of a sorted set can be accessed
and match the greatest value in the set.
*/
TEST(TreeSet, Last)
{
  TreeSetDebug si;
  for (int ii = 10; ii <= 20; ++ii)
  {
    // add some elements on the extreme ends
    ASSERT_TRUE(si.add(ii));
    // those elements should now be the first and last elements
    ASSERT_EQ(si.last(), ii) << si;
  }
  for (int ii = 0; ii < 10; ++ii)
  {
    // add some elements in the middle
    ASSERT_TRUE(si.add(ii));
    // first and last should still be on the extreme ends
    ASSERT_EQ(si.last(), 20) << si;
  }
}

/**
Demonstrate that the predecessor element to some specified element can be found
in the set. Show that if there is no predecessor, the return value is the
first element in the sorted set.
*/
TEST(TreeSet, Predecessor)
{
  TreeSetDebug si;
  // add multiples of 10 up to 100 to the set
  for (int ii = 0; ii <= 100; ii += 10) ASSERT_TRUE(si.add(ii));
  // elements before set return first element
  ASSERT_EQ(si.predecessor(-1000), 0) << si;
  ASSERT_EQ(si.predecessor(-1), 0) << si;
  // first element returns first element
  ASSERT_EQ(si.predecessor(0), 0) << si;
  // elements in the set return previous element, not self
  ASSERT_EQ(si.predecessor(10), 0) << si;
  ASSERT_EQ(si.predecessor(20), 10) << si;
  // elements between set elements return the previous element
  ASSERT_EQ(si.predecessor(21), 20) << si;
  ASSERT_EQ(si.predecessor(29), 20) << si;
  // elements past the end return last element
  ASSERT_EQ(si.predecessor(101), 100) << si;
  ASSERT_EQ(si.predecessor(1000), 100) << si;
}

/**
Demonstrate that the successor element to some specified element can be found
in the set. Show that if there is no successor, the return value is the
last element in the sorted set.
*/
TEST(TreeSet, Successor)
{
  TreeSetDebug si;
  // add multiples of 10 up to 100 to the set
  for (int ii = 0; ii <= 100; ii += 10) ASSERT_TRUE(si.add(ii));
  // elements before set return first element
  ASSERT_EQ(si.successor(-1000), 0) << si;
  ASSERT_EQ(si.successor(-1), 0) << si;
  // elements in the set return next element, not self
  ASSERT_EQ(si.successor(0), 10) << si;
  ASSERT_EQ(si.successor(10), 20) << si;
  // elements between set elements return the next element
  ASSERT_EQ(si.successor(21), 30) << si;
  ASSERT_EQ(si.successor(29), 30) << si;
  // last element returns last element
  ASSERT_EQ(si.successor(100), 100) << si;
  // elements past the end return last element
  ASSERT_EQ(si.successor(101), 100) << si;
  ASSERT_EQ(si.successor(1000), 100) << si;
}
