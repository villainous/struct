
#include "detect_equal.inl"

// check this compiles BEFORE gtest leaks symbols
template class viln::DetectEqual<double>;

#include <string>
#include <gtest/gtest.h>

using namespace viln;

/**
Demonstrate default signed-integer "==".
*/
TEST(DetectEqual, DefaultIntEqual)
{
  // signed integers
  DetectEqual<int> de;
  // equal values are equal
  ASSERT_TRUE(de.equal(1, 1));
  // non-equal values have different values
  ASSERT_FALSE(de.equal(-1, 1));
  ASSERT_FALSE(de.equal(1, -1));
}

/**
Demonstrate that std::string (and thereby other compliant equals
implementations from stl) behave as expected.
*/
TEST(DetectEqual, StdString)
{
  DetectEqual<std::string> des;
  // string equals to other string with same content
  ASSERT_TRUE(des.equal("hello", "hello"));
  // string not equal to other string without same content
  ASSERT_FALSE(des.equal("hello", "goodbye"));
}

struct misc_t
{
  int data;
  char distraction;
};

class DetectEqualOverride : DetectEqual<misc_t>
{
public:
  virtual bool equal(const misc_t& o1, const misc_t& o2) const
  {
    return o1.data == o2.data;
  }
};

/**
Demonstrate that we can successfully override the equals function detection.
*/
TEST(DetectEqual, EqualOverride)
{
  DetectEqualOverride de;
  misc_t de1{ 1, 'a' };
  misc_t de2{ 1, 'b' };
  misc_t de3{ 2, 'c' };
  // the equality we implemented ignores the distraction character
  ASSERT_TRUE(de.equal(de1, de2));
  // different data should lead to non-equal values
  ASSERT_FALSE(de.equal(de1, de3));
}

struct eq_op_t
{
  int data;
  char distraction;
  bool operator==(const eq_op_t& o) const { return data == o.data; }
};

struct eq_free_t
{
  int data;
  char distraction;
};
bool operator==(const eq_free_t& o1, const eq_free_t& o2)
{
  return o1.data == o2.data;
}

struct equals_t
{
  int data;
  char distraction;
};
bool equal(const equals_t& o1, const equals_t& o2)
{
  return o1.data == o2.data;
}

using vector_typelist = testing::Types<
  eq_op_t,
  eq_free_t,
  equals_t>;
template<class> struct DetectEqual_sources : testing::Test {};
TYPED_TEST_SUITE(DetectEqual_sources, vector_typelist);

/*
Demonstrate detection of equality via each of the available source-types.
*/
TYPED_TEST(DetectEqual_sources, detection)
{
  DetectEqual<TypeParam> de;
  TypeParam me1{ 233, 'a' };
  TypeParam me2{ 233, 'b' };
  TypeParam me3{ 234, 'c' };
  // the equality we implemented ignores the distraction character
  ASSERT_TRUE(de.equal(me1, me2));
  // different data should lead to non-equal values
  ASSERT_FALSE(de.equal(me1, me3));
}
