#include <gtest/gtest.h>
#include "canary.hpp"
#include "array_list.inl"

using namespace viln;

// provide an override of the default "equals" detection
class CanaryArrayList : public ArrayList<Canary>
{
public:
  CanaryArrayList(size_t cap = 1) : ArrayList<Canary>(cap) {}

protected:
  bool equal(const Canary& c1, const Canary& c2) const override
  {
    return c1.data == c2.data;
  }
};

/**
Check that element construction and destruction is correctly being invoked by
members of a list. Specifically, that the move-constructor is being correctly
invoked when adding elements to the list.
*/
TEST(ArrayListObjects, MoveConstructor)
{
  {
    CanaryArrayList coalmine(3);
    // RESET!
    Canary::reset();
    // add some elements to the list
    ASSERT_TRUE(coalmine.add(Canary(1)));
    ASSERT_TRUE(coalmine.add(Canary(2)));
    ASSERT_TRUE(coalmine.add(Canary(3)));
    // check that the elements actually got put in correctly
    EXPECT_EQ(coalmine[0].data, 1);
    EXPECT_EQ(coalmine[1].data, 2);
    EXPECT_EQ(coalmine[2].data, 3);
    // the actual constructor should be called only once per element
    EXPECT_EQ(Canary::constructor, 3);
    // since we started with capacity 3, only 3 moves should be needed
    EXPECT_EQ(Canary::move, 3);
    // move was called, NOT copy
    EXPECT_EQ(Canary::copy, 0);
    // elements should not be deleted until the list falls out of scope
    EXPECT_EQ(Canary::destructor, 0);
  }
  // once the list falls out of scope, the elements should be deleted
  EXPECT_EQ(Canary::destructor, 3);
}

/**
Check that element construction and destruction is correctly being invoked by
members of a list. Specifically, that the copy-constructor is being correctly
invoked when adding elements to the list.
*/
TEST(ArrayListObjects, CopyConstructor)
{
  Canary c1 = Canary(1);
  Canary c2 = Canary(2);
  Canary c3 = Canary(3);
  {
    CanaryArrayList coalmine(3);
    // RESET!
    Canary::reset();
    // copy some elements into the list
    ASSERT_TRUE(coalmine.add(c1));
    ASSERT_TRUE(coalmine.add(c2));
    ASSERT_TRUE(coalmine.add(c3));
    // check that the elements actually got put in correctly
    EXPECT_EQ(coalmine[0].data, 1);
    EXPECT_EQ(coalmine[1].data, 2);
    EXPECT_EQ(coalmine[2].data, 3);
    // the elements were originally created prior to the reset
    EXPECT_EQ(Canary::constructor, 0);
    // since we started with capacity 3, only 3 copies should be needed
    EXPECT_EQ(Canary::copy, 3);
    // used copy, NOT move
    EXPECT_EQ(Canary::move, 0);
    // elements should not be deleted until the list falls out of scope
    EXPECT_EQ(Canary::destructor, 0);
  }
  // once the list falls out of scope, its elements are deleted
  EXPECT_EQ(Canary::destructor, 3);
}

/**
Check that growing the capacity moves the elements rather than copying and
deleting them.
*/
TEST(ArrayListObjects, ObjectsGrowCapacity)
{
  Canary c3 = Canary(3);
  {
    // create an array of a known size
    ArrayList<Canary> coalmine(2);
    ASSERT_TRUE(coalmine.add(Canary(1)));
    ASSERT_TRUE(coalmine.add(Canary(2)));
    // RESET!
    Canary::reset();
    // adding one more element to the list should trigger a capacity increase
    ASSERT_TRUE(coalmine.add(c3));
    // check that the elements actually got put in correctly
    EXPECT_EQ(coalmine[0].data, 1);
    EXPECT_EQ(coalmine[1].data, 2);
    EXPECT_EQ(coalmine[2].data, 3);
    // no new elements were created since the reset
    EXPECT_EQ(Canary::constructor, 0);
    // the existing elements had to be moved
    EXPECT_EQ(Canary::move, 2);
    // the new element was copied from the stack
    EXPECT_EQ(Canary::copy, 1);
    // nothing should be destroyed until it falls out of scope
    EXPECT_EQ(Canary::destructor, 0);
  }
  // once the list falls out of scope, its elements are deleted
  EXPECT_EQ(Canary::destructor, 3);
}

/**
Check that removing elements correctly optimizes the remianing elements when
they are objects.
*/
TEST(ArrayListObjects, ObjectsOptimizeCapacity)
{
  {
    // create an array of a known size
    ArrayList<Canary> coalmine(3);
    ASSERT_TRUE(coalmine.add(Canary(1)));
    ASSERT_TRUE(coalmine.add(Canary(2)));
    ASSERT_TRUE(coalmine.add(Canary(3)));
    // RESET!
    Canary::reset();
    // remove the element in the middle of the list
    ASSERT_TRUE(coalmine.erase(1));
    // check that the elements are arranged correctly after the removal
    EXPECT_EQ(coalmine[0].data, 1);
    EXPECT_EQ(coalmine[1].data, 3);
    // no new elements were created
    EXPECT_EQ(Canary::constructor, 0);
    // the element at 2 got moved to 1
    EXPECT_EQ(Canary::move, 1);
    // nothing should need to be copied
    EXPECT_EQ(Canary::copy, 0);
    // the removed element should have been destroyed
    EXPECT_EQ(Canary::destructor, 1);
  }
  // once the list falls out of scope, the remaining elements should be deleted
  EXPECT_EQ(Canary::destructor, 3);
}

/**
Demonstrate that containment works on objects, and uses the equality detection.
*/
TEST(ArrayListObjects, Contains)
{
  // create an array with some values
  CanaryArrayList coalmine;
  ASSERT_TRUE(coalmine.add(Canary(1)));
  ASSERT_TRUE(coalmine.add(Canary(992)));
  ASSERT_TRUE(coalmine.add(Canary(3)));
  // should contain a value in the list
  EXPECT_TRUE(coalmine.contains(Canary(992)));
  // should not contain a value not in the list
  EXPECT_FALSE(coalmine.contains(Canary(991)));
}
