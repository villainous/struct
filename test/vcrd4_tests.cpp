
#include <gtest/gtest.h>
#include "vcrd.inl"

using namespace viln;

/*
Demonstrate that equality operators behave as expected.
*/
TEST(VCrd4, Equality)
{
  // self-equality
  vcrd_4i crd1A{ 1, 2, 3, 4 };
  EXPECT_TRUE(crd1A == crd1A);
  EXPECT_FALSE(crd1A != crd1A);
  // equals
  vcrd_4i crd1B{ 1, 2, 3, 4 };
  EXPECT_TRUE(crd1A == crd1B);
  EXPECT_FALSE(crd1A != crd1B);
  // not equals
  vcrd_4i crd2{ 3, 4, 5, 6 };
  EXPECT_FALSE(crd1A == crd2);
  EXPECT_TRUE(crd1A != crd2);
  // element-equality
  vcrd_4i crd3{ 9, 9, 9, 9 };
  EXPECT_TRUE(crd3 == 9);
  EXPECT_FALSE(crd3 != 9);
  vcrd_4i crd4{ 9, 9, 9, 8 };
  EXPECT_FALSE(crd4 == 9);
  EXPECT_TRUE(crd4 != 9);
}

/*
Demonstrate assignment to vcrd[4] from various types.
*/
TEST(VCrd4, Convert)
{
  vcrd_4i crd = {};
  EXPECT_EQ(crd, (vcrd_4i{ 0, 0, 0, 0 }));
  // vcrd-assign
  const vcrd_4i crdA = { 11, 111, 1, 1 };
  EXPECT_EQ((crd = crdA), (vcrd_4i{ 11, 111, 1, 1 }));
  EXPECT_EQ(crd, (vcrd_4i{ 11, 111, 1, 1 }));
  // ucrd-assign
  EXPECT_EQ(crd = ucrd::Z, (vcrd_4i{ 0, 0, 1, 0 }));
  EXPECT_EQ(crd, (vcrd_4i{ 0, 0, 1, 0 }));
  // element-assign
  EXPECT_EQ(crd = 234, (vcrd_4i{ 234, 234, 234, 234 }));
  EXPECT_EQ(crd, (vcrd_4i{ 234, 234, 234, 234 }));
  // vcrd<-assign
  const vcrd<int,1> crd1 = { 11 };
  EXPECT_EQ(crd = crd1, (vcrd_4i{ 11, 0, 0, 0 }));
  EXPECT_EQ(crd, (vcrd_4i{ 11, 0, 0 }));
  // vcrd>-assign
  const vcrd<int,5> crd5 = { 5, 4, 3, 2, 1 };
  EXPECT_EQ(crd = crd5, (vcrd_4i{ 5, 4, 3, 2 }));
  EXPECT_EQ(crd, (vcrd_4i{ 5, 4, 3, 2 }));
}

/*
Verify that the `+` `+=` `++` operators behave as expected.
*/
TEST(VCrd4, Plus)
{
  const vcrd_4i crd1{ 1, 2, 3, 4 };
  const vcrd_4i crd2{ 3, 4, 5, 6 };
  vcrd_4i crd = {};
  // plus
  EXPECT_EQ((crd1 + crd2), (vcrd_4i{ 4, 6, 8, 10 }));
  // ucrd plus
  EXPECT_EQ((crd1 + ucrd::X), (vcrd_4i{ 2, 2, 3, 4 }));
  // element plus
  EXPECT_EQ((crd1 + 5), (vcrd_4i{ 6, 7, 8, 9 }));
  // assign-plus
  crd = crd1;
  EXPECT_EQ((crd += crd2), (vcrd_4i{ 4, 6, 8, 10 }));
  EXPECT_EQ(crd, (vcrd_4i{ 4, 6, 8, 10 }));
  // ucrd assign-plus
  crd = crd1;
  EXPECT_EQ((crd += ucrd::W), (vcrd_4i{ 1, 2, 3, 5 }));
  EXPECT_EQ(crd, (vcrd_4i{ 1, 2, 3, 5 }));
  // element assign-plus
  crd = crd1;
  EXPECT_EQ((crd += 5), (vcrd_4i{ 6, 7, 8, 9 }));
  EXPECT_EQ(crd, (vcrd_4i{ 6, 7, 8, 9 }));
  // left-increment
  crd = crd1;
  EXPECT_EQ(++crd, (vcrd_4i{ 2, 3, 4, 5 }));
  EXPECT_EQ(crd, (vcrd_4i{ 2, 3, 4, 5 }));
}

/*
Verify that the `-` `-=` `--` operators behave as expected.
*/
TEST(VCrd4, Minus)
{
  const vcrd_4i crd1{ 1, 2, 3, 4 };
  const vcrd_4i crd2{ 3, 4, 5, 6 };
  vcrd_4i crd = {};
  // negate
  EXPECT_EQ(-crd1, (vcrd_4i{ -1, -2, -3, -4 }));
  // minus
  EXPECT_EQ((crd1 - crd2), (vcrd_4i{ -2, -2, -2, -2 }));
  // ucrd minus
  EXPECT_EQ((crd1 - ucrd::X), (vcrd_4i{ 0, 2, 3, 4 }));
  // element minus
  EXPECT_EQ((crd1 - 5), (vcrd_4i{ -4, -3, -2, -1 }));
  // assign-minus
  crd = crd1;
  EXPECT_EQ((crd -= crd2), (vcrd_4i{ -2, -2, -2, -2 }));
  EXPECT_EQ(crd, (vcrd_4i{ -2, -2, -2, -2 }));
  // ucrd assign-minus
  crd = crd1;
  EXPECT_EQ((crd -= ucrd::W), (vcrd_4i{ 1, 2, 3, 3 }));
  EXPECT_EQ(crd, (vcrd_4i{ 1, 2, 3, 3 }));
  // element assign-minus
  crd = crd1;
  EXPECT_EQ((crd -= 5), (vcrd_4i{ -4, -3, -2, -1 }));
  EXPECT_EQ(crd, (vcrd_4i{ -4, -3, -2, -1 }));
  // left-decrement
  crd = crd1;
  EXPECT_EQ(--crd, (vcrd_4i{ 0, 1, 2, 3 }));
  EXPECT_EQ(crd, (vcrd_4i{ 0, 1, 2, 3 }));
}

/*
Verify that the `*` `*=` operators behave as expected.
*/
TEST(VCrd4, Multiply)
{
  const vcrd_4i crd1{ 1, 2, 3, 4 };
  const vcrd_4i crd2{ 3, -1, 0, 1 };
  const int fac = 12;
  vcrd_4i crd = {};
  // multiply
  EXPECT_EQ((crd1 * crd2), (vcrd_4i{ 3, -2, 0, 4 }));
  // element multiply
  EXPECT_EQ((crd1 * fac), (vcrd_4i{ 12, 24, 36, 48 }));
  // assign-multiply
  crd = crd1;
  EXPECT_EQ((crd *= crd2), (vcrd_4i{ 3, -2, 0, 4 }));
  EXPECT_EQ(crd, (vcrd_4i{ 3, -2, 0, 4 }));
  // element assign-multiply
  crd = crd1;
  EXPECT_EQ((crd *= fac), (vcrd_4i{ 12, 24, 36, 48 }));
  EXPECT_EQ(crd, (vcrd_4i{ 12, 24, 36, 48 }));
}

/*
Verify that the `/` `/=` operators behave as expected.
*/
TEST(VCrd4, Divide)
{
  const vcrd_4i crd1{ 12, 24, 36, 48 };
  const vcrd_4i crd2{ 11, -11, 1, 2 };
  const int fac = 11;
  vcrd_4i crd = {};
  // div
  EXPECT_EQ((crd1 / crd2), (vcrd_4i{ 1, -2, 36, 24 }));
  // element div
  EXPECT_EQ((crd1 / fac), (vcrd_4i{ 1, 2, 3, 4 }));
  // div-assign
  crd = crd1;
  EXPECT_EQ((crd /= crd2), (vcrd_4i{ 1, -2, 36, 24 }));
  EXPECT_EQ(crd, (vcrd_4i{ 1, -2, 36, 24 }));
  // element div-assign
  crd = crd1;
  EXPECT_EQ((crd /= fac), (vcrd_4i{ 1, 2, 3, 4 }));
  EXPECT_EQ(crd, (vcrd_4i{ 1, 2, 3, 4 }));
}

/*
Verify that the `%` `%=` operators behave as expected.
*/
TEST(VCrd4, Mod)
{
  const vcrd_4i crd1{ 12, 24, 36, 48 };
  const vcrd_4i crd2{ 11, -11, 1, 2 };
  const int fac = 11;
  vcrd_4i crd = {};
  // mod
  EXPECT_EQ((crd1 % crd2), (vcrd_4i{ 1, 2, 0, 0 }));
  // element mod
  EXPECT_EQ((crd1 % fac), (vcrd_4i{ 1, 2, 3, 4 }));
  // mod-assign
  crd = crd1;
  EXPECT_EQ((crd %= crd2), (vcrd_4i{ 1, 2, 0, 0 }));
  EXPECT_EQ(crd, (vcrd_4i{ 1, 2, 0, 0 }));
  // element mod-assign
  crd = crd1;
  EXPECT_EQ((crd %= fac), (vcrd_4i{ 1, 2, 3, 4 }));
  EXPECT_EQ(crd, (vcrd_4i{ 1, 2, 3, 4 }));
}

/*
Verify that the div_floor` and `div_ceil` functions behave as expected.
*/
TEST(VCrd4, Divide_Special)
{
  const vcrd_4i crd1{ 12, 24, 36, 48 };
  const vcrd_4i crd2{ 11, -11, 1, 2 };
  const int fac = 11;
  vcrd_4i crd = {};
  // div_floor
  EXPECT_EQ(div_floor(crd1, crd2), (vcrd_4i{ 1, -3, 36, 24 }));
  // element div_floor
  EXPECT_EQ(div_floor(crd1, fac), (vcrd_4i{ 1, 2, 3, 4 }));
  // div_ceil
  EXPECT_EQ(div_ceil(crd1, crd2), (vcrd_4i{ 2, -2, 36, 24 }));
  // element div_ceil
  EXPECT_EQ(div_ceil(crd1, fac), (vcrd_4i{ 2, 3, 4, 5 }));
}

/*
Check that the SSE/vector div-floor matches the scalar version.
*/
TEST(VCrd4, Divide_Floor_mm)
{
  const vcrd_4i num = { 1, 2, -3, -4 };
  const vcrd_4i den = { -1, 1, -1, 1 };

  for (int ii = -1000; ii < 1000; ii += 10)
  {
    const vcrd_4i iinum = num * ii;
    for (int jj = -77; jj <= 77; jj += 2)
    {
      const vcrd_4i jjden = den * jj;
      vcrd_4i div = div_floor(iinum, jjden);
      EXPECT_EQ(div.x(), div_floor(iinum.x(), jjden.x()));
      EXPECT_EQ(div.y(), div_floor(iinum.y(), jjden.y()));
      EXPECT_EQ(div.z(), div_floor(iinum.z(), jjden.z()));
      EXPECT_EQ(div.w(), div_floor(iinum.w(), jjden.w()));
    }
  }
}

/*
Verify that the `mod_floor` and `mod_ceil` functions behave as expected.
*/
TEST(VCrd4, Mod_Special)
{
  const vcrd_4i crd1{ 12, 24, 36, 48 };
  const vcrd_4i crd2{ 11, -11, 1, 2 };
  const int fac = 11;
  vcrd_4i crd = {};
  // mod_floor
  EXPECT_EQ(mod_floor(crd1, crd2), (vcrd_4i{ 1, -9, 0, 0 }));
  EXPECT_EQ((div_floor(crd1, crd2) * crd2) + mod_floor(crd1, crd2), crd1);
  // element mod_floor
  EXPECT_EQ(mod_floor(crd1, fac), (vcrd_4i{ 1, 2, 3, 4 }));
  EXPECT_EQ((div_floor(crd1, fac) * fac) + mod_floor(crd1, fac), crd1);
  // mod_ceil
  EXPECT_EQ(mod_ceil(crd1, crd2), (vcrd_4i{ -10, 2, 0, 0 }));
  EXPECT_EQ((div_ceil(crd1, crd2) * crd2) + mod_ceil(crd1, crd2), crd1);
  // element mod_ceil
  EXPECT_EQ(mod_ceil(crd1, fac), (vcrd_4i{ -10, -9, -8, -7 }));
  EXPECT_EQ((div_ceil(crd1, fac) * fac) + mod_ceil(crd1, fac), crd1);
}
/*
Check that the SSE/vector div-floor matches the scalar version.
*/
TEST(VCrd4, Mod_Floor_mm)
{
  const vcrd_4i num = { 1, 2, -3, -4 };
  const vcrd_4i den = { -1, 1, -1, 1 };

  for (int ii = -1000; ii < 1000; ii += 10)
  {
    const vcrd_4i iinum = num * ii;
    for (int jj = -77; jj <= 77; jj += 2)
    {
      const vcrd_4i jjden = den * jj;
      vcrd_4i div = mod_floor(iinum, jjden);
      EXPECT_EQ(div.x(), mod_floor(iinum.x(), jjden.x()));
      EXPECT_EQ(div.y(), mod_floor(iinum.y(), jjden.y()));
      EXPECT_EQ(div.z(), mod_floor(iinum.z(), jjden.z()));
      EXPECT_EQ(div.w(), mod_floor(iinum.w(), jjden.w()));
    }
  }
}

/*
Demonstrate math operations.
*/
TEST(VCrd4, Math)
{
  const vcrd_4i crd1{ 12, -24, 36, -48 };
  const vcrd_4i crd2{ 40, -40, 40, -40 };
  // abs
  EXPECT_EQ(abs(crd1), (vcrd_4i{ 12, 24, 36, 48 }));
  EXPECT_EQ(abs(-crd1), (vcrd_4i{ 12, 24, 36, 48 }));
  // sgn
  EXPECT_EQ(sgn(crd1), (vcrd_4i{ 1, -1, 1, -1 }));
  EXPECT_EQ(sgn(-crd1), (vcrd_4i{ -1, 1, -1, 1 }));
  // max1
  EXPECT_EQ(max(crd1), 36);
  // max2
  EXPECT_EQ(max(crd1, crd2), (vcrd_4i{ 40, -24, 40, -40 }));
  EXPECT_EQ(max(crd1, 11), (vcrd_4i{ 12, 11, 36, 11 }));
  // min1
  EXPECT_EQ(min(crd1), -48);
  // min2
  EXPECT_EQ(min(crd1, crd2), (vcrd_4i{ 12, -40, 36, -48 }));
  EXPECT_EQ(min(crd1, 11), (vcrd_4i{ 11, -24, 11, -48 }));
}

/*
Demonstrate vector-math operations.
*/
TEST(VCrd4, Vector)
{
  const vcrd_4i crd1{ 12, -24, 1, 1 };
  const vcrd_4i crd2{ 3, 10, 0, 0 };
  // volume
  EXPECT_EQ(volume(crd1), 288);
  EXPECT_EQ(volume(-crd1), 288);
  // dot
  EXPECT_EQ(dot(crd1, crd2), -204);
  // lengths
  EXPECT_EQ(length_max(crd1), 24);
  EXPECT_EQ(length1(crd1), 38);
  EXPECT_EQ(length(crd1), 26);
  EXPECT_GE(length_oct(crd1), length_max(crd1));
  EXPECT_LE(length_oct(crd1), length1(crd1));
}

/*
Demonstrate 2d vector creation and access.
*/
TEST(VCrd4, 4x4)
{
  vcrd<vcrd<int, 4>, 4> crd4x4{
    vcrd_4i{  1,  2,  3,  4 },
    vcrd_4i{  5,  6,  7,  8 },
    vcrd_4i{  9, 10, 11, 12 },
    vcrd_4i{ 13, 14, 15, 16} };
  // spot-check the values are accessible
  EXPECT_EQ(crd4x4[0][0], 1);
  EXPECT_EQ(crd4x4[0][1], 2);
  EXPECT_EQ(crd4x4[1][0], 5);
  EXPECT_EQ(crd4x4[1][1], 6);
  EXPECT_EQ(crd4x4[2][2], 11);
  EXPECT_EQ(crd4x4[3][3], 16);
}
