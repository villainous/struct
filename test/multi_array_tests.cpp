
#include "multi_array.inl"

// show we can compile the full type before
template class viln::MultiArray<char, 1>;

#include <gtest/gtest.h>

using namespace viln;

/*
Show that the size matches the requested object.
*/
TEST(MultiArray, Size)
{
  MultiArray<double, 5> mbd5({ 5, 4, 3, 2, 1 });
  ASSERT_EQ(mbd5.capacity(), 120);
  ASSERT_EQ(mbd5.size(), (vcrd<int,5>{ 5, 4, 3, 2, 1 }));
}

/*
Show that we can create an array initialized to a specific value.
*/
TEST(MultiArray, ZroInit)
{
  MultiArray<double, 2> mbd({ 2, 10 }, 22.2);
  ASSERT_EQ(mbd.capacity(), 20);
  ASSERT_EQ(mbd.size(), (vcrd_2i{ 2, 10 }));
  for(double e : mbd) EXPECT_EQ(e, 22.2);
}

/*
Demonstrate wrapping a pre-constructed C-array.
*/
TEST(MultiArray, PtrInit)
{
  int* arr6{ new int[6] { 1, 2, 3, 912000, 5, 0 } };
  MultiArray<int, 2> mbd(arr6, { 3, 2 });
  ASSERT_EQ(mbd.capacity(), 6);
  ASSERT_EQ(mbd.size(), (vcrd_2i{ 3, 2 }));
  EXPECT_EQ((mbd[{ 0, 0 }]), 1);
  EXPECT_EQ((mbd[{ 1, 0 }]), 2);
  EXPECT_EQ((mbd[{ 2, 0 }]), 3);
  EXPECT_EQ((mbd[{ 0, 1 }]), 912000);
  EXPECT_EQ((mbd[{ 1, 1 }]), 5);
  EXPECT_EQ((mbd[{ 2, 1 }]), 0);
}

/*
Demonstrate copy constructon.
*/
TEST(MultiArray, CpyInit)
{
  // set up an array
  MultiArray<int, 2> arrd33A({ 3, 3 });
  int accA = 0;
  for(int& e : arrd33A) e = ++accA;
  // copy the array
  MultiArray<int, 2> arrd33B(arrd33A);
  // check that the copy matches the original
  ASSERT_EQ(arrd33B.capacity(), 9);
  ASSERT_EQ(arrd33B.size(), (vcrd_2i{ 3, 3 }));
  int accB = 0;
  for(int& e : arrd33B) EXPECT_EQ(e, ++accB);
}

/*
Fill in the box with a set of unique values, then verify that some of the
values match what we set there.
*/
TEST(MultiArray, At)
{
  MultiArray<int32_t, 3> mbi3({ 4, 4, 4 });
  const MultiArray<int32_t, 3>& cmbi3 = mbi3;
  int32_t temp = 0;
  for (int32_t i = 0; i < 4; ++i)
  {
    for (int32_t j = 0; j < 4; ++j)
    {
      for (int32_t k = 0; k < 4; ++k)
      {
        // mutable-at
        mbi3[{ i, j, k }] = temp++;
      }
    }
  }
  // mutable-at
  EXPECT_EQ((mbi3[{ 0, 0, 0 }]), 0);
  EXPECT_EQ((mbi3[{ 3, 0, 0 }]), 48);
  EXPECT_EQ((mbi3[{ 0, 2, 0 }]), 8);
  EXPECT_EQ((mbi3[{ 0, 0, 1 }]), 1);
  EXPECT_EQ((mbi3[{ 3, 3, 3 }]), 63);
  // const-at
  EXPECT_EQ((cmbi3[{ 0, 0, 0 }]), 0);
  EXPECT_EQ((cmbi3[{ 3, 0, 0 }]), 48);
  EXPECT_EQ((cmbi3[{ 0, 2, 0 }]), 8);
  EXPECT_EQ((cmbi3[{ 0, 0, 1 }]), 1);
  EXPECT_EQ((cmbi3[{ 3, 3, 3 }]), 63);
}

class TestMultiArray_int8_3d : public MultiArray<int8_t, 3>
{
public:
  TestMultiArray_int8_3d(const vvcrd& siz) : MultiArray<int8_t, 3>(siz) {}
  size_t pindex(const vvcrd& idx) const
  {
    return this->index(idx);
  }
};
/*
Verify that the indices increase in the same manner as standard
multidimensional C MultiArrays.
*/
TEST(MultiArray, Index)
{
  TestMultiArray_int8_3d mbc3({ 2, 2, 2 });
  int32_t temp = 0;
  for (int32_t i = 0; i < 2; ++i)
  {
    for (int32_t j = 0; j < 2; ++j)
    {
      for (int32_t k = 0; k < 2; ++k)
      {
        EXPECT_EQ(mbc3.pindex({ k, j, i }), temp++);
      }
    }
  }
}

/*
Demonstrate that multidimensional arrays can be iterated-over.
*/
TEST(MultiArray, Iterable)
{
  MultiArray<int32_t, 2> mbi2({ 2, 2 });
  int32_t temp = 0;
  for (int32_t i = 0; i < 2; ++i)
  {
    for (int32_t j = 0; j < 2; ++j)
    {
      mbi2.at({ i, j }) = temp++;
    }
  }
  bool found[4] = {};
  const MultiArray<int32_t, 2>& mbi2r = mbi2;
  for (int32_t val : mbi2r)
  {
    ASSERT_GE(val, 0);
    ASSERT_LT(val, 4);
    found[val] = true;
  }
  for (int i = 0; i < 4; ++i) EXPECT_TRUE(found[i]);
}

/*
Demonstrate that multidimensional arrays can be modified by their iterators.
*/
TEST(MultiArray, Muterable)
{
  MultiArray<int32_t, 2> mbi2({ 2, 2 });
  int32_t temp = 0;
  for (int32_t& val : mbi2) val = temp++;
  bool found[4] = {};
  for (int32_t val : mbi2)
  {
    ASSERT_GE(val, 0);
    ASSERT_LT(val, 4);
    found[val] = true;
  }
  for (int i = 0; i < 4; ++i) EXPECT_TRUE(found[i]);
}

/*
Demonstrate assignment of an array to a value.
*/
TEST(MultiArray, Assign)
{
  // copy the array
  MultiArray<int, 2> arrd33B({ 3, 3 });
  ASSERT_EQ(arrd33B.capacity(), 9);
  ASSERT_EQ(arrd33B.size(), (vcrd_2i{ 3, 3 }));
  // set the array values
  arrd33B = -1;
  // check that the copy is now filled with the value
  ASSERT_EQ(arrd33B.capacity(), 9);
  ASSERT_EQ(arrd33B.size(), (vcrd_2i{ 3, 3 }));
  for(int& e : arrd33B) EXPECT_EQ(e, -1);
}

/*
Show direct data-access to the elements.
*/
TEST(MultiArray, DataAccess)
{
  int* arr0{ new int[4] { 1, 2, 3, 912000 } };
  MultiArray<int32_t, 1> arri4(arr0, { 4 });
  const MultiArray<int32_t, 1>& arri4r = arri4;
  int32_t* data = arri4.data();
  ASSERT_NE(data, nullptr);
  EXPECT_EQ(data[0], 1);
  EXPECT_EQ(data[1], 2);
  EXPECT_EQ(data[2], 3);
  EXPECT_EQ(data[3], 912000);
  const int32_t* cdata = arri4r.data();
  ASSERT_NE(cdata, nullptr);
  EXPECT_EQ(cdata[0], 1);
  EXPECT_EQ(cdata[1], 2);
  EXPECT_EQ(cdata[2], 3);
  EXPECT_EQ(cdata[3], 912000);
}
