
#include "array.inl"

// show we can compile the full type before
template class viln::Array<char>;

#include <gtest/gtest.h>

using namespace viln;

/*
Show that the size matches the requested object.
*/
TEST(Array, Size)
{
  Array<double> arrd7(7);
  EXPECT_EQ(arrd7.size(), 7);
}

/*
Show that we can create an array initialized to a specific value.
*/
TEST(Array, ZroInit)
{
  Array<int> arrd7(7, 777);
  ASSERT_EQ(arrd7.size(), 7);
  for(int e : arrd7) EXPECT_EQ(e, 777);
}

/*
Show that we can initialize an array to some set of specific values.
*/
TEST(Array, ListInit)
{
  Array<int> arrd4 = { 99, 233, -1, -1 };
  ASSERT_EQ(arrd4.size(), 4);
  EXPECT_EQ(arrd4[0], 99);
  EXPECT_EQ(arrd4[1], 233);
  EXPECT_EQ(arrd4[2], -1);
  EXPECT_EQ(arrd4[3], -1);
}

/*
Demonstrate wrapping a pre-constructed C-array.
*/
TEST(Array, PtrInit)
{
  int* arr5{ new int[5] { 1, 2, 3, 912000, 5 } };
  Array<int> arrd5(arr5, 5);
  ASSERT_EQ(arrd5.size(), 5);
  EXPECT_EQ(arrd5[0], 1);
  EXPECT_EQ(arrd5[1], 2);
  EXPECT_EQ(arrd5[2], 3);
  EXPECT_EQ(arrd5[3], 912000);
  EXPECT_EQ(arrd5[4], 5);
}

/*
Demonstrate copy constructon.
*/
TEST(Array, CpyInit)
{
  Array<int> arrd4a = { 99, 233, -1, -1 };
  Array<int> arrd4b(arrd4a);
  ASSERT_EQ(arrd4b.size(), 4);
  EXPECT_EQ(arrd4b[0], 99);
  EXPECT_EQ(arrd4b[1], 233);
  EXPECT_EQ(arrd4b[2], -1);
  EXPECT_EQ(arrd4b[3], -1);
}

/*
Fill in the box with a set of unique values, then verify that some of the
values match what we set there.
*/
TEST(Array, At)
{
  Array<int32_t> arri3(3, 0);
  const Array<int32_t>& carri3 = arri3;
  // mutable-at
  EXPECT_EQ(arri3[0], 0);
  EXPECT_EQ(arri3[1], 0);
  EXPECT_EQ(arri3[2], 0);
  arri3[0] = 1;
  arri3[1] = 999;
  arri3[2] = -512;
  EXPECT_EQ(arri3[0], 1);
  EXPECT_EQ(arri3[1], 999);
  EXPECT_EQ(arri3[2], -512);
  // const-at
  EXPECT_EQ(carri3[0], 1);
  EXPECT_EQ(carri3[1], 999);
  EXPECT_EQ(carri3[2], -512);
}

/*
Demonstrate that arrays can be iterated-over.
*/
TEST(Array, Iterable)
{
  Array<int32_t> arri4({ 0, 1, 2, 3 });
  ASSERT_EQ(arri4.size(), 4);
  bool found[4] = {};
  const Array<int32_t>& arri4r = arri4;
  for (int32_t val : arri4r)
  {
    ASSERT_GE(val, 0);
    ASSERT_LT(val, 4);
    found[val] = true;
  }
  for (int i = 0; i < 4; ++i) EXPECT_TRUE(found[i]);
}

/*
Demonstrate that arrays can be modified by their iterators.
*/
TEST(Array, Muterable)
{
  Array<int32_t> arri4(4);
  ASSERT_EQ(arri4.size(), 4);
  int32_t temp = 0;
  for (int32_t& val : arri4) val = temp++;
  bool found[4] = {};
  for (int32_t val : arri4)
  {
    ASSERT_GE(val, 0);
    ASSERT_LT(val, 4);
    found[val] = true;
  }
  for (int i = 0; i < 4; ++i) EXPECT_TRUE(found[i]);
}

/*
Demonstrate access of the first and last elements of an array.
*/
TEST(Array, FirstLast)
{
  Array<int32_t> arri4({ 1, 22, 333, 4444 });
  const Array<int32_t>& arri4r = arri4;
  EXPECT_EQ(arri4.first(), 1);
  EXPECT_EQ(arri4r.first(), 1);
  EXPECT_EQ(arri4.last(), 4444);
  EXPECT_EQ(arri4r.last(), 4444);
}

/*
Demonstrate assignment of an array to a value.
*/
TEST(Array, ElementAssign)
{
  // create an array
  Array<int32_t> arrib(4);
  ASSERT_EQ(arrib.size(), 4);
  // assign the whole array to the same value
  arrib = -1;
  // check that the size is unchanged but the values are as desired
  ASSERT_EQ(arrib.size(), 4);
  EXPECT_EQ(arrib[0], -1);
  EXPECT_EQ(arrib[1], -1);
  EXPECT_EQ(arrib[2], -1);
  EXPECT_EQ(arrib[3], -1);
}

/*
Show direct data-access to the elements.
*/
TEST(Array, DataAccess)
{
  Array<int32_t> arri4({ 1, 22, 333, 4444 });
  const Array<int32_t>& arri4r = arri4;
  int32_t* arr4 = arri4.data();
  ASSERT_NE(arr4, nullptr);
  EXPECT_EQ(arr4[0], 1);
  EXPECT_EQ(arr4[1], 22);
  EXPECT_EQ(arr4[2], 333);
  EXPECT_EQ(arr4[3], 4444);
  const int32_t* arr4r = arri4r.data();
  ASSERT_NE(arr4r, nullptr);
  EXPECT_EQ(arr4r[0], 1);
  EXPECT_EQ(arr4r[1], 22);
  EXPECT_EQ(arr4r[2], 333);
  EXPECT_EQ(arr4r[3], 4444);
}
