#include "cterator.inl"

// check this compiles BEFORE gtest leaks symbols
template class viln::Cterator<double>;

#include <gtest/gtest.h>
#include "array_iterator.inl"

using namespace viln;

TEST(Cterator, LoopEnd)
{
  int arr5[] = { 1, 2, 3, 4, 5 };
  int idx = 0;
  Cterator<int> wrcit(new ArrayIterator<int>(arr5, 5, 0));
  EXPECT_TRUE(wrcit != Cterator<int>::after());
  while (wrcit != Cterator<int>::after())
  {
    EXPECT_EQ(*wrcit, arr5[idx]);
    ++wrcit, ++idx;
  }
  EXPECT_EQ(idx, 5);
}
