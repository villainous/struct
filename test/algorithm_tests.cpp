#include <gtest/gtest.h>
#include "algorithm.hpp"

using namespace viln;

/*
Demonstrate that the DeBrujin implemenation of Log2(x) behaves as expected.
*/
TEST(Algorithm, Log2_DeBrujin)
{
  uint64_t mask1 = 1 << 0;
  EXPECT_EQ(log2i_debrujin(mask1), 0);
  uint64_t mask5 = 1 << 5;
  EXPECT_EQ(log2i_debrujin(mask5), 5);
  uint64_t mask30 = 1 << 30;
  mask30 += 1 << 29;
  EXPECT_EQ(log2i_debrujin(mask30), 30);
  uint64_t mask60 = 1ULL << 60;
  mask60 += 1ULL << 59;
  EXPECT_EQ(log2i_debrujin(mask60), 60);
}

/*
Show that the string and byte-based versions of the parity-hash produce the
same results.
*/
TEST(Algorithm, HashFNV1A_consistent)
{
  const char* data = R"V0G0N(
The following components and template are based on the input of many who
contributed to the discussion "Create a README.txt template". For the sake
of having a fleshed-out example, we're using some sample text. Most of it
comes from the Administration menu module's README.md.

It is recommended to take advantage of the newer Markdown format and
create a README.md files (instead of README.txt). If you do so, you may
want to use more Markdown-features than those used in the example snippets
in this template. For instance, Markdown let you create hyperlinks that
can be clicked when the README.md is rendered in GitLab or by a compatible
program (e.g. Advanced Help).
  )V0G0N";
  EXPECT_EQ(hash64_fnv1a(data, strlen(data)), hash64_fnv1a(data));
}

/*
Demonstrate that the FNV1A hashing algorithm does a good job spreading lumpy
data evenly into bins.
*/
TEST(Algorithm, HashFNV1A_bins)
{
  // we will keep track of which bin our values fall into
  constexpr uint64_t NBINS = 32;
  int bins[NBINS] = {};
  // skip over ~1M numbers at a time to make the data lumpy
  int64_t acc = 0;
  for(int idx = 0; idx < 10000; ++idx)
  {
    for(int jdx = 0; jdx < 10; ++jdx)
    {
      uint64_t bin = hash64_fnv1a(&acc, sizeof(acc)) % NBINS;
      ++bins[bin];
      ++acc;
    }
    acc += 1000000;
  }
  // check the filling-ratio of each bin
  int32_t lo = INT32_MAX;
  int32_t hi = 0;
  for(int bin = 0; bin < NBINS; ++bin)
  {
    int32_t fill = bins[bin];
    if (fill > hi) hi = fill;
    if (fill < lo) lo = fill;
  }
  // must fill at least some bins
  ASSERT_GE(hi, 0);
  ASSERT_GE(lo, 0);
  // expect a ratio of 4 between most and least filled bin
  EXPECT_LE((hi/lo), 4);
}

/*
Demonstrate that the parity-based hash correctly identifies and
aggregates bytes. This is important since the algorithm operates
both on 64-bit blocks and 8-bit blocks depending on the size of
the intput-data.
*/
TEST(Algorithm, HashParity_bytes)
{
  // start with an array of bytes that are easy to recognize
  const uint8_t data[16] = {
    0x01, 0x02, 0x03, 0x04,
    0x15, 0x16, 0x17, 0x18,
    0x29, 0x2A, 0x2B, 0x2C,
    0x3D, 0x3E, 0x3F, 0x30
  };
  // 0 is a noop
  EXPECT_EQ(hash64_parity(data, 0), 0ull);
  // up to 7-bytes are processed individually
  EXPECT_EQ(hash64_parity(data, 1), 0x01ull);
  EXPECT_EQ(hash64_parity(data, 2), 0x0201ull);
  EXPECT_EQ(hash64_parity(data, 3), 0x030201ull);
  EXPECT_EQ(hash64_parity(data, 4), 0x04030201ull);
  EXPECT_EQ(hash64_parity(data, 5), 0x1504030201ull);
  EXPECT_EQ(hash64_parity(data, 6), 0x161504030201ull);
  EXPECT_EQ(hash64_parity(data, 7), 0x17161504030201ull);
  // 8 bytes are processed as a single block
  EXPECT_EQ(hash64_parity(data, 8), 0x1817161504030201ull);
  const uint64_t b0 = hash64_parity(data, 8ull);
  // the next 7 bytes are processed individually again
  EXPECT_EQ(hash64_parity(data,  9), b0 ^ 0x29ull);
  EXPECT_EQ(hash64_parity(data, 10), b0 ^ 0x2A29ull);
  EXPECT_EQ(hash64_parity(data, 11), b0 ^ 0x2B2A29ull);
  EXPECT_EQ(hash64_parity(data, 12), b0 ^ 0x2C2B2A29ull);
  EXPECT_EQ(hash64_parity(data, 13), b0 ^ 0x3D2C2B2A29ull);
  EXPECT_EQ(hash64_parity(data, 14), b0 ^ 0x3E3D2C2B2A29ull);
  EXPECT_EQ(hash64_parity(data, 15), b0 ^ 0x3F3E3D2C2B2A29ull);
  // 16 bytes are processed as 2 complete blocks
  EXPECT_EQ(hash64_parity(data, 16), b0 ^ 0x303F3E3D2C2B2A29ull);
}

/*
Demonstrate computing the parity-values for strings.
*/ 
TEST(Algorithm, HashParity_string)
{
  // if the string is 8 characters or less it just gets packed into the int
  const char dataA[17] = "question";
  const uint64_t* dataA64 = (const uint64_t*) dataA;
  EXPECT_EQ(hash64_parity(dataA), *dataA64);
  // if the string is more than 8 characters then we start xor-ing them
  const char dataB[17] = "question" "anything";
  const uint64_t* dataB64 = (const uint64_t*) dataB;
  EXPECT_EQ(hash64_parity(dataB), dataB64[0] ^ dataB64[1]);
}

/*
Show that the string and byte-based versions of the parity-hash produce the
same results.
*/
TEST(Algorithm, HashParity_consistent)
{
  const char* data = R"V0G0N(
             O freddled gruntbuggly thy micturations are to me
                 As plured gabbleblochits on a lurgid bee.
              Groop, I implore thee my foonting turlingdromes.   
           And hooptiously drangle me with crinkly bindlewurdles,
Or I will rend thee in the gobberwarts with my blurlecruncheon, see if I don't.

                (by Prostetnic Vogon Jeltz; see p. 56/57)
  )V0G0N";
  EXPECT_EQ(hash64_parity(data, strlen(data)), hash64_parity(data));
}

/*
Demonstrate that the parity hashing algorithm does a good job spreading lumpy
data evenly into bins.
*/
TEST(Algorithm, HashParity_bins)
{
  // we will keep track of which bin our values fall into
  constexpr uint64_t NBINS = 32;
  int bins[NBINS] = {};
  // skip over ~1M numbers at a time to make the data lumpy
  int64_t acc = 0;
  for(int idx = 0; idx < 10000; ++idx)
  {
    for(int jdx = 0; jdx < 10; ++jdx)
    {
      uint64_t bin = hash64_parity(&acc, sizeof(acc)) % NBINS;
      ++(bins[bin]);
      ++acc;
    }
    acc += 1000000;
  }
  // check the filling-ratio of each bin
  int32_t lo = INT32_MAX;
  int32_t hi = 0;
  for(int bin = 0; bin < NBINS; ++bin)
  {
    int32_t fill = bins[bin];
    if (fill > hi) hi = fill;
    if (fill < lo) lo = fill;
  }
  // must fill at least some bins
  ASSERT_GE(hi, 0);
  ASSERT_GE(lo, 0);
  // expect a ratio of 4 between most and least filled bin
  EXPECT_LE((hi/lo), 4);
}

/*
Demonstrate that the xorshift re-hashing algorithm does a good job spreading lumpy
data evenly into bins.
*/
TEST(Algorithm, ReHash64XORShift_bins)
{
  // we will keep track of which bin our values fall into
  constexpr uint64_t NBINS = 32;
  int bins[NBINS] = {};
  // skip over ~1M numbers at a time to make the data lumpy
  int64_t acc = 0;
  for(int idx = 0; idx < 10000; ++idx)
  {
    for(int jdx = 0; jdx < 10; ++jdx)
    {
      uint64_t bin = rehash64_xorshift(acc) % NBINS;
      ++(bins[bin]);
      ++acc;
    }
    acc += 1000000;
  }
  // check the filling-ratio of each bin
  int32_t lo = INT32_MAX;
  int32_t hi = 0;
  for(int bin = 0; bin < NBINS; ++bin)
  {
    int32_t fill = bins[bin];
    if (fill > hi) hi = fill;
    if (fill < lo) lo = fill;
  }
  // must fill at least some bins
  ASSERT_GE(hi, 0);
  ASSERT_GE(lo, 0);
  // expect a ratio of 4 between most and least filled bin
  EXPECT_LE((hi/lo), 4);
}

/*
Demonstrate that the xorshift re-hashing algorithm does a good job spreading lumpy
data evenly into bins.
*/
TEST(Algorithm, ReHash32XORShift_bins)
{
  // we will keep track of which bin our values fall into
  constexpr uint32_t NBINS = 32;
  int bins[NBINS] = {};
  // skip over ~1k numbers at a time to make the data lumpy
  int32_t acc = 0;
  for(int idx = 0; idx < 10000; ++idx)
  {
    for(int jdx = 0; jdx < 10; ++jdx)
    {
      uint32_t bin = rehash32_xorshift(acc) % NBINS;
      ++(bins[bin]);
      ++acc;
    }
    acc += 1000;
  }
  // check the filling-ratio of each bin
  int32_t lo = INT32_MAX;
  int32_t hi = 0;
  for(int bin = 0; bin < NBINS; ++bin)
  {
    int32_t fill = bins[bin];
    if (fill > hi) hi = fill;
    if (fill < lo) lo = fill;
  }
  // must fill at least some bins
  ASSERT_GE(hi, 0);
  ASSERT_GE(lo, 0);
  // expect a ratio of 4 between most and least filled bin
  EXPECT_LE((hi/lo), 4);
}
