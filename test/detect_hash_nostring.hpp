#ifndef VILN_DETECT_HASH_NOSTRING_HPP
#define VILN_DETECT_HASH_NOSTRING_HPP

// a test method that must not have <string> defined
bool detect_hash_nostring(void);

#endif
