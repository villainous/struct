
#include <gtest/gtest.h>
#include "bool_dirs.hpp"

using namespace viln;

/*
Check that conversions of the directional combinations results in the correct
resulting string values.
*/
TEST(BoolDirs, DirsToString)
{
  using namespace bool_dirs_enum;
  EXPECT_STREQ(to_string(bool_4d::NONE), "none");
  EXPECT_STREQ(to_string(bool_4d::X), "x");
  EXPECT_STREQ(to_string(bool_4d::Y), "y");
  EXPECT_STREQ(to_string(bool_4d::Z), "z");
  EXPECT_STREQ(to_string(bool_4d::W), "w");
  EXPECT_STREQ(to_string(bool_4d::XY), "xy");
  EXPECT_STREQ(to_string(bool_4d::XZ), "xz");
  EXPECT_STREQ(to_string(bool_4d::XW), "xw");
  EXPECT_STREQ(to_string(bool_4d::YZ), "yz");
  EXPECT_STREQ(to_string(bool_4d::YW), "yw");
  EXPECT_STREQ(to_string(bool_4d::ZW), "zw");
  EXPECT_STREQ(to_string(bool_4d::XYZ), "xyz");
  EXPECT_STREQ(to_string(bool_4d::XYW), "xyw");
  EXPECT_STREQ(to_string(bool_4d::XZW), "xzw");
  EXPECT_STREQ(to_string(bool_4d::YZW), "yzw");
  EXPECT_STREQ(to_string(bool_4d::XYZW), "xyzw");
  EXPECT_EQ(to_string(bool_4d::MAX), nullptr);
}

/*
Show that name conversions are complete and self-consistent.
*/
TEST(BoolDirs, StringBounce)
{
  using namespace bool_dirs_enum;
  for(uint8_t idx = 0; idx < bool_4d::MAX; ++idx)
  {
    const char* str = to_string(bool_4d(idx));
    EXPECT_EQ(from_string(str), idx);
  }
  EXPECT_EQ(from_string("bad"), bool_4d::MAX);
}
