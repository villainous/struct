#include "iterator_sequence.inl"

// check this compiles BEFORE gtest leaks symbols
template class viln::IteratorSequence<double>;

#include <gtest/gtest.h>
#include "linked_list.inl"

using namespace viln;

/**
Check that we can create an empty iterator-sequence and then add iterators
to it in order to construct a combined iteration path.
*/
TEST(IteratorSequence, Add)
{
  // lists to iterate over
  LinkedList<int> l1;
  LinkedList<int> l2 = { 1, 2, 3 };
  LinkedList<int> l3 = { 4, 5 };
  // create an empty iterator
  IteratorSequence<int> iseq;
  // iterator should be empty
  ASSERT_FALSE(iseq.hasNext());
  // adding an iterator without elements changes nothing
  ASSERT_FALSE(iseq.add(l1.iterator()));
  ASSERT_FALSE(iseq.hasNext());
  // adding an iterator with elements makes the iterator non-empty
  ASSERT_TRUE(iseq.add(l2.iterator()));
  ASSERT_TRUE(iseq.hasNext());
  // add another iterator
  ASSERT_TRUE(iseq.add(l3.iterator()));
  // check elements are as expected
  int acc = 0;
  while (iseq.hasNext())
  {
    EXPECT_EQ(iseq.next(), ++acc);
  }
  // check we iterated over the right number of elements
  EXPECT_EQ(acc, 5);
  // not permitted to modify after iteration begins
  ASSERT_FALSE(iseq.add(l1.iterator()));
}

/**
Demonstrate wrapping a single iterator using the sequence.
*/
TEST(IteratorSequence, Wrap)
{
  // a list to iterate over
  LinkedList<int> l1 = { 1, 2, 3 };
  // wrap an iterator over the list
  IteratorSequence<int> iseq(l1.iterator());
  // iterator should be ready to use
  ASSERT_TRUE(iseq.hasNext());
  // check elements are as expected
  int acc = 0;
  while (iseq.hasNext())
  {
    EXPECT_EQ(iseq.next(), ++acc);
  }
  // check we iterated over the right number of elements
  EXPECT_EQ(acc, 3);
}

/**
Demonstrate initializing an iterator-sequence using an initializer-list of
iterators.
*/
TEST(IteratorSequence, InitList)
{
  // lists to iterate over
  LinkedList<int> l1;
  LinkedList<int> l2 = { 1, 2, 3 };
  LinkedList<int> l3 = { 4, 5 };
  // create an iterator over all three lists
  IteratorSequence<int> iseq = {
    l1.iterator(),
    l2.iterator(),
    l3.iterator() };
  // iterator should have elements
  ASSERT_TRUE(iseq.hasNext());
  // check elements are as expected
  int acc = 0;
  while (iseq.hasNext())
  {
    EXPECT_EQ(iseq.next(), ++acc);
  }
  // check we iterated over the right number of elements
  EXPECT_EQ(acc, 5);
  // not permitted to modify after iteration begins
  ASSERT_FALSE(iseq.add(l1.iterator()));
}
