
#include <gtest/gtest.h>
#include "box.inl"

using namespace viln;

/*
Check that the various constructors all function properly.
*/
TEST(Box, Construction)
{
  // default constructor
  Box2D b0;
  EXPECT_EQ(b0.position(), (vcrd_2i{ 0, 0 }));
  EXPECT_EQ(b0.size(), (vcrd_2i{ 0, 0 }));
  // point constructor
  Box2D bP(vcrd_2i{ 5, 5 });
  EXPECT_EQ(bP.position(), (vcrd_2i{ 5, 5 }));
  EXPECT_EQ(bP.size(), (vcrd_2i{ 0, 0 }));
  // rectangle constructor
  Box2D bR(vcrd_2i{ 1, 2 }, vcrd_2i{ 3, 4 });
  EXPECT_EQ(bR.position(), (vcrd_2i{ 1, 2 }));
  EXPECT_EQ(bR.size(), (vcrd_2i{ 3, 4 }));
  // copy-constructor
  Box2D bC(bR);
  EXPECT_EQ(bC.position(), bR.position());
  EXPECT_EQ(bC.size(), bR.size());
}

/*
Test conversion between different types of box.
*/
TEST(Box, Conversion)
{
  Box2D tmp;
  // narrowing conversion
  Box<int, 3> b3i({ 1, 2, 3 }, { 4, 5, 6 });
  tmp = b3i;
  EXPECT_EQ(tmp.position(), (vcrd_2i{ 1, 2 }));
  EXPECT_EQ(tmp.size(), (vcrd_2i{ 4, 5 }));
  // widening conversion
  Box<int, 1> b1i({ 1 }, { 4 });
  tmp = b1i;
  EXPECT_EQ(tmp.position(), (vcrd_2i{ 1, 0 }));
  EXPECT_EQ(tmp.size(), (vcrd_2i{ 4, 1 }));
  // type conversion
  Box<char, 2> b2c({ '\7', '\6' }, { '\5', '\4' });
  tmp = b2c;
  EXPECT_EQ(tmp.position(), (vcrd_2i{ 7, 6 }));
  EXPECT_EQ(tmp.size(), (vcrd_2i{ 5, 4 }));
}

/*
Verify the accessors can correctly identify the elements.
*/
TEST(Box, SimpleAccess)
{
  vcrd_2i pos = { 1,-1 };
  vcrd_2i siz = { 5, 4 };
  Box2D sq(pos, siz);
  // check the default accessors
  EXPECT_EQ(sq.min(), (vcrd_2i{ 1, -1 }));
  EXPECT_EQ(sq.max(), (vcrd_2i{ 6, 3 }));
  // check that we can recover the construction parameters
  EXPECT_EQ(sq.position(), pos);
  EXPECT_EQ(sq.size(), siz);
}

/*
Demonstrate the `==` and `!=` comparison operations.
*/
TEST(Box, Compare)
{
  const Box2D bA({ 1, 2 }, { 3, 4 });
  const Box2D bB({ 1, 3 }, { 3, 4 });
  const Box2D bC({ 1, 2 }, { 4, 4 });
  Box2D b = bA;
  // self-equality
  EXPECT_TRUE(b == b);
  EXPECT_FALSE(b != b);
  // same data
  EXPECT_TRUE(b == bA);
  EXPECT_FALSE(b != bA);
  EXPECT_TRUE(bA == b);
  EXPECT_FALSE(bA != b);
  // different position
  EXPECT_FALSE(b == bB);
  EXPECT_TRUE(b != bB);
  EXPECT_FALSE(bB == b);
  EXPECT_TRUE(bB != b);
  // different size
  EXPECT_FALSE(b == bC);
  EXPECT_TRUE(b != bC);
  EXPECT_FALSE(bC == b);
  EXPECT_TRUE(bC != b);
}

/*
Demonstrate the behavior of the various mutators.
*/
TEST(Box, SimpleMutation)
{
  Box2D b1({ 1, -1 }, { 3, 5 });
  // check the initial state upon creation
  EXPECT_EQ(b1.position(), (vcrd_2i{ 1, -1 }));
  EXPECT_EQ(b1.size(), (vcrd_2i{ 3, 5 }));
  EXPECT_EQ(b1.min(), (vcrd_2i{ 1, -1 }));
  EXPECT_EQ(b1.max(), (vcrd_2i{ 4, 4 }));
  // change the position but not size
  b1.setPosition({ -1, 1 });
  EXPECT_EQ(b1.position(), (vcrd_2i{ -1, 1 }));
  EXPECT_EQ(b1.size(), (vcrd_2i{ 3, 5 }));
  EXPECT_EQ(b1.min(), (vcrd_2i{ -1, 1 }));
  EXPECT_EQ(b1.max(), (vcrd_2i{ 2, 6 }));
  // change the size but not position
  b1.setSize({ 7, 7 });
  EXPECT_EQ(b1.position(), (vcrd_2i{ -1, 1 }));
  EXPECT_EQ(b1.size(), (vcrd_2i{ 7, 7 }));
  EXPECT_EQ(b1.min(), (vcrd_2i{ -1, 1 }));
  EXPECT_EQ(b1.max(), (vcrd_2i{ 6, 8 }));
  // change the min but not max
  b1.setMin({ -2, -2 });
  EXPECT_EQ(b1.min(), (vcrd_2i{ -2, -2 }));
  EXPECT_EQ(b1.max(), (vcrd_2i{ 6, 8 }));
  EXPECT_EQ(b1.position(), (vcrd_2i{ -2, -2 }));
  EXPECT_EQ(b1.size(), (vcrd_2i{ 8, 10 }));
  // change the max but not min
  b1.setMax({ 2, 2 });
  EXPECT_EQ(b1.min(), (vcrd_2i{ -2, -2 }));
  EXPECT_EQ(b1.max(), (vcrd_2i{ 2, 2 }));
  EXPECT_EQ(b1.position(), (vcrd_2i{ -2, -2 }));
  EXPECT_EQ(b1.size(), (vcrd_2i{ 4, 4 }));
  // move the box by an amount
  b1.moveBy({ 1, -1 });
  EXPECT_EQ(b1.min(), (vcrd_2i{ -1, -3 }));
  EXPECT_EQ(b1.max(), (vcrd_2i{ 3, 1 }));
  EXPECT_EQ(b1.position(), (vcrd_2i{ -1, -3 }));
  EXPECT_EQ(b1.size(), (vcrd_2i{ 4, 4 }));
}

/*
Demonstrate moving the box by changing the midpoint position.
*/
TEST(Box, SetMidpoint)
{
  Box2D b1({ 1, -1 }, { 3, 5 });
  // check the initial state upon creation
  EXPECT_EQ(b1.midpoint(), (vcrd_2i{ 2, 1 }));
  EXPECT_EQ(b1.position(), (vcrd_2i{ 1, -1 }));
  EXPECT_EQ(b1.size(), (vcrd_2i{ 3, 5 }));
  EXPECT_EQ(b1.min(), (vcrd_2i{ 1, -1 }));
  EXPECT_EQ(b1.max(), (vcrd_2i{ 4, 4 }));
  // move the box by setting the center
  b1.setMidpoint((vcrd_2i{ 10, 10 }));
  EXPECT_EQ(b1.midpoint(), (vcrd_2i{ 10, 10 }));
  EXPECT_EQ(b1.position(), (vcrd_2i{ 9, 8 }));
  EXPECT_EQ(b1.size(), (vcrd_2i{ 3, 5 }));
  EXPECT_EQ(b1.min(), (vcrd_2i{ 9, 8 }));
  EXPECT_EQ(b1.max(), (vcrd_2i{ 12, 13 }));
}

/*
Test that we can determine if two bodies overlap.
*/
TEST(Box, BoxOverlaps)
{
  // self-overlap
  Box2D b1({ 0, 0 }, { 10, 10 });
  ASSERT_TRUE(b1.overlaps(b1));
  // partial overlap
  Box2D b2({ 5, 0 }, { 5, 5 });
  ASSERT_TRUE(b1.overlaps(b2));
  ASSERT_TRUE(b2.overlaps(b1));
  // total overlap
  b2.setPosition({ 4, 1 });
  ASSERT_TRUE(b1.overlaps(b2));
  ASSERT_TRUE(b2.overlaps(b1));
  // touching
  b2.setPosition({ 10, 0 });
  ASSERT_FALSE(b1.overlaps(b2));
  ASSERT_FALSE(b2.overlaps(b1));
  // disjoint
  b2.setPosition({ 20000, 0 });
  ASSERT_FALSE(b1.overlaps(b2));
  ASSERT_FALSE(b2.overlaps(b1));
}

/*
Demonstrate that box and point containment can be correctly identified.
*/
TEST(Box, BoxContains)
{
  // self-containment
  Box2D b1({ 0, 0 }, { 10, 10 });
  EXPECT_TRUE(b1.contains(b1));
  // points inside
  EXPECT_TRUE(b1.contains(vcrd_2i{ 0, 0 }));
  EXPECT_TRUE(b1.contains(vcrd_2i{ 5, 5 }));
  EXPECT_TRUE(b1.contains(vcrd_2i{ 9, 9 }));
  // points outside
  EXPECT_FALSE(b1.contains(vcrd_2i{ -1, -1 }));
  EXPECT_FALSE(b1.contains(vcrd_2i{ 10, 10 }));
  // box inside
  Box2D b2({ 0, 0 }, { 5, 5 });
  EXPECT_TRUE(b1.contains(b2));
  Box2D b3({ 5, 5 }, { 5, 5 });
  EXPECT_TRUE(b1.contains(b3));
  // intersection is not contained
  Box2D b4({ -1, 0 }, { 5, 5 });
  EXPECT_FALSE(b1.contains(b4));
  // disjoint is not contained
  Box2D b5({ 10, 0 }, { 10, 10 });
  EXPECT_FALSE(b1.contains(b5));
}

/*
Test that we can determine if two bodies touch or overlap.
*/
TEST(Box, BoxTouches)
{
  // self-overlap
  Box2D b1({ 0, 0 }, { 10, 10 });
  ASSERT_TRUE(b1.touches(b1));
  // partial overlap
  Box2D b2({ 5, 0 }, { 5, 5 });
  ASSERT_TRUE(b1.touches(b2));
  ASSERT_TRUE(b2.touches(b1));
  // total overlap
  b2.setPosition({ 4, 1 });
  ASSERT_TRUE(b1.touches(b2));
  ASSERT_TRUE(b2.touches(b1));
  // touching
  b2.setPosition({ 10, 0 });
  ASSERT_TRUE(b1.touches(b2));
  ASSERT_TRUE(b2.touches(b1));
  // disjoint
  b2.setPosition({ 20000, 0 });
  ASSERT_FALSE(b1.touches(b2));
  ASSERT_FALSE(b2.touches(b1));
}

/*
Test that we can determine if two bodies overlap.
*/
TEST(Box, EdgeOverlaps)
{
  // self-overlap
  Box2D b1({ 0, 0 }, { 10, 10 });
  ASSERT_TRUE(b1.overlaps(b1));
  // total overlap
  Box2D b00({ 5, 5 }, { 0, 0 });
  ASSERT_TRUE(b1.overlaps(b00));
  ASSERT_TRUE(b00.overlaps(b1));
  // touching
  b00.setPosition({ 10, 5 });
  ASSERT_FALSE(b1.overlaps(b00));
  ASSERT_FALSE(b00.overlaps(b1));
  // disjoint
  b00.setPosition({ 20000, 0 });
  ASSERT_FALSE(b1.overlaps(b00));
  ASSERT_FALSE(b00.overlaps(b1));
}

/*
Test that we can determine if two bodies touch or overlap.
*/
TEST(Box, EdgeTouches)
{
  // self-overlap
  Box2D b1({ 0, 0 }, { 10, 10 });
  ASSERT_TRUE(b1.touches(b1));
  // total overlap
  Box2D b00({ 5, 5 }, { 0, 0 });
  ASSERT_TRUE(b1.touches(b00));
  ASSERT_TRUE(b00.touches(b1));
  // touching
  b00.setPosition({ 10, 5 });
  ASSERT_TRUE(b1.touches(b00));
  ASSERT_TRUE(b00.touches(b1));
  // disjoint
  b00.setPosition({ 20000, 0 });
  ASSERT_FALSE(b1.touches(b00));
  ASSERT_FALSE(b00.touches(b1));
}

/*
Test that we can determine if a point overlaps a body.
*/
TEST(Box, PointContains)
{
  // self-overlap
  Box2D b1({ 0, 0 }, { 10, 10 });
  ASSERT_TRUE(b1.contains(b1));
  // total overlap
  EXPECT_TRUE(b1.contains(vcrd_2i{ 5, 5 }));
  // touching edges
  EXPECT_TRUE(b1.contains(vcrd_2i{ 0, 5 }));
  EXPECT_FALSE(b1.contains(vcrd_2i{ 10, 5 }));
  EXPECT_TRUE(b1.contains(vcrd_2i{ 5, 0 }));
  EXPECT_FALSE(b1.contains(vcrd_2i{ 5, 10 }));
  // touching corners
  EXPECT_TRUE(b1.contains(vcrd_2i{ 0, 0 }));
  EXPECT_FALSE(b1.contains(vcrd_2i{ 0, 10 }));
  EXPECT_FALSE(b1.contains(vcrd_2i{ 10, 0 }));
  EXPECT_FALSE(b1.contains(vcrd_2i{ 10, 10 }));
  // disjoint
  EXPECT_FALSE(b1.contains(vcrd_2i{ 780, -12 }));
}

/*
Demonstrate the ability to iterate over the elements of a box.
*/
TEST(Box, Iterable)
{
  Box2D b1({ 0, -1 }, { 3, 5 });
  vcrd_2i min = b1.min();
  vcrd_2i max = b1.max();
  int acc = 0;
  for (const vcrd_2i& idx : b1)
  {
    // check that each element returned is inside the box
    ASSERT_GE(idx.x(), min.x());
    ASSERT_GE(idx.y(), min.y());
    ASSERT_LT(idx.x(), max.x());
    ASSERT_LT(idx.y(), max.y());
    ++acc;
  }
  // check that we iterated over the correct number of elements
  EXPECT_EQ(acc, b1.volume());
}

/*
Demonstrate that iteration over a box of 0 volume does nothing.
*/
TEST(Box, IterableNoVolume)
{
  Box3D b1({ 0, 0, 0 }, { 2, 2, 0 });
  int acc = 0;
  Iterator<const vcrd_3i>* itr = b1.iterator();
  while (itr->hasNext())
  {
    ++acc;
    itr->next();
  }
  delete itr;
  EXPECT_EQ(acc, 0);
}

/*
Demonstrate that we can clamp a vector to the size of a box.
*/
TEST(Box, Clamp)
{
  Box2D b1({ 0, 0 }, { 10, 1000 });
  // < min
  EXPECT_EQ(b1.clamp({ -1, -1000 }), (vcrd_2i{ 0, 0 }));
  // = min
  EXPECT_EQ(b1.clamp({ 0, 0 }), (vcrd_2i{ 0, 0 }));
  // middle
  EXPECT_EQ(b1.clamp({ 5, 500 }), (vcrd_2i{ 5, 500 }));
  // = max
  EXPECT_EQ(b1.clamp({ 10, 1000 }), (vcrd_2i{ 9, 999 }));
  // > max
  EXPECT_EQ(b1.clamp({ 100, 10000 }), (vcrd_2i{ 9, 999 }));
}

/*
Demonstrate creating bounding-boxes of boxes.
*/
TEST(Box, Bounds)
{
  // self bounds
  Box2D bA({ 1, 2 }, { 3, 4 });
  EXPECT_EQ((bA | bA), bA);
  // equal bounds
  Box2D bB1({ 4, 5 }, { 6, 7 });
  Box2D bB2({ 4, 5 }, { 6, 7 });
  EXPECT_EQ((bB1 | bB2), bB1);
  EXPECT_EQ((bB2 | bB1), bB1);
  // box inside box
  Box2D bC1({ -5, -5 }, { 10, 10 });
  Box2D bC2({ -2, -2 }, { 4, 4 });
  EXPECT_EQ((bC1 | bC2), bC1);
  EXPECT_EQ((bC2 | bC1), bC1);
  // disjoint
  Box2D bE1({ -5, -5 }, { 5, 5 });
  Box2D bE2({ 0, 0 }, { 5, 5 });
  Box2D bEE({ -5, -5 },{ 10, 10 });
  EXPECT_EQ((bE1 | bE2), bEE);
  EXPECT_EQ((bE2 | bE1), bEE);
  // intersecting
  Box2D bD1({ -5, -5 }, { 10, 10 });
  Box2D bD2({ 0, 0 }, { 10, 10 });
  Box2D bDD({ -5, -5 }, { 15, 15 });
  EXPECT_EQ((bD1 | bD2), bDD);
  EXPECT_EQ((bD2 | bD1), bDD);
  // method-call
  EXPECT_EQ(bD1.bounding(bD2), bDD);
  EXPECT_EQ(bD2.bounding(bD1), bDD);
}

/*
Demonstrate box intersections
*/
TEST(Box, Intersect)
{
  // self bounds
  Box2D bA({ 1, 2 }, { 3, 4 });
  EXPECT_EQ((bA & bA), bA);
  // equal bounds
  Box2D bB1({ 4, 5 }, { 6, 7 });
  Box2D bB2({ 4, 5 }, { 6, 7 });
  EXPECT_EQ((bB1 & bB2), bB1);
  EXPECT_EQ((bB2 & bB1), bB1);
  // box inside box
  Box2D bC1({ -5, -5 }, { 10, 10 });
  Box2D bC2({ -2, -2 }, { 4, 4 });
  EXPECT_EQ((bC1 & bC2), bC2);
  EXPECT_EQ((bC2 & bC1), bC2);
  // disjoint
  Box2D bE1({ -5, -5 }, { 5, 5 });
  Box2D bE2({ 0, 0 }, { 5, 5 });
  Box2D bEE = Box2D();
  EXPECT_EQ((bE1 & bE2), bEE);
  EXPECT_EQ((bE2 & bE1), bEE);
  // intersecting
  Box2D bD1({ -5, -5 }, { 10, 10 });
  Box2D bD2({ 0, 0 }, { 10, 10 });
  Box2D bDD({ 0, 0 },{ 5, 5 });
  EXPECT_EQ((bD1 & bD2), bDD);
  EXPECT_EQ((bD2 & bD1), bDD);
  EXPECT_EQ(bD1.intersection(bD2), bDD);
  EXPECT_EQ(bD2.intersection(bD1), bDD);
}

/*
Test coordinate-wrapping into a box area.
*/
TEST(Box, Wrap)
{
  Box2D bA({ 5, 10 }, { 10, 100 });
  // inside
  EXPECT_EQ(bA.wrap({ 6, 11 }), (vcrd_2i{ 6, 11 }));
  EXPECT_EQ(bA.wrap({ 14, 109 }), (vcrd_2i{ 14, 109 }));
  EXPECT_EQ(bA.wrap({ 10, 55 }), (vcrd_2i{ 10, 55 }));
  // lower edge
  EXPECT_EQ(bA.wrap({ 5, 10 }), (vcrd_2i{ 5, 10 }));
  EXPECT_EQ(bA.wrap({ 5, 11 }), (vcrd_2i{ 5, 11 }));
  EXPECT_EQ(bA.wrap({ 6, 10 }), (vcrd_2i{ 6, 10 }));
  // upper edge
  EXPECT_EQ(bA.wrap({ 15, 110 }), (vcrd_2i{ 5, 10 }));
  EXPECT_EQ(bA.wrap({ 15, 11 }), (vcrd_2i{ 5, 11 }));
  EXPECT_EQ(bA.wrap({ 6, 110 }), (vcrd_2i{ 6, 10 }));
  // outside
  EXPECT_EQ(bA.wrap({ -4, 111 }), (vcrd_2i{ 6, 11 }));
  EXPECT_EQ(bA.wrap({ 4, 209 }), (vcrd_2i{ 14, 109 }));
  EXPECT_EQ(bA.wrap({ 0, 155 }), (vcrd_2i{ 10, 55 }));
}

/*
Check that the distance between boxed can be calculated.
*/
TEST(Box, Distance)
{
  Box2D bA({ -5, -5 }, { 10, 10 });
  // self
  EXPECT_EQ(bA.distance(bA), (vcrd_2i{ 0, 0 }));
  // equal
  Box2D bA2 = bA;
  EXPECT_EQ(bA.distance(bA2), (vcrd_2i{ 0, 0 }));
  EXPECT_EQ(bA2.distance(bA), (vcrd_2i{ 0, 0 }));
  // inside
  Box2D bB1({ -2, -2 }, { 4, 4 });
  EXPECT_EQ(bA.distance(bB1), (vcrd_2i{ 0, 0 }));
  EXPECT_EQ(bB1.distance(bA), (vcrd_2i{ 0, 0 }));
  Box2D bB2({ 0, 0 });
  EXPECT_EQ(bA.distance(bB2), (vcrd_2i{ 0, 0 }));
  EXPECT_EQ(bB2.distance(bA), (vcrd_2i{ 0, 0 }));
  vcrd_2i vB3 = { 0, 0 };
  EXPECT_EQ(bA.distance(vB3), (vcrd_2i{ 0, 0 }));
  // edge
  Box2D bC1({ 5, -5 }, { 10, 10 });
  EXPECT_EQ(bA.distance(bC1), (vcrd_2i{ 0, 0 }));
  EXPECT_EQ(bC1.distance(bA), (vcrd_2i{ 0, 0 }));
  Box2D bC2({ -5, 5 }, { 10, 10 });
  EXPECT_EQ(bA.distance(bC2), (vcrd_2i{ 0, 0 }));
  EXPECT_EQ(bC2.distance(bA), (vcrd_2i{ 0, 0 }));
  vcrd_2i vC3 = { 5, 0 };
  EXPECT_EQ(bA.distance(vC3), (vcrd_2i{ 0, 0 }));
  vcrd_2i vC4 = { -5, 0 };
  EXPECT_EQ(bA.distance(vC4), (vcrd_2i{ 0, 0 }));
  // disjoint
  Box2D bD1({ 6, -5 }, { 10, 10 });
  EXPECT_EQ(bA.distance(bD1), (vcrd_2i{ 1, 0 }));
  EXPECT_EQ(bD1.distance(bA), (vcrd_2i{ -1, 0 }));
  Box2D bD2({ -5, 6 }, { 10, 10 });
  EXPECT_EQ(bA.distance(bD2), (vcrd_2i{ 0, 1 }));
  EXPECT_EQ(bD2.distance(bA), (vcrd_2i{ 0, -1 }));
  Box2D bD3({ 20, 20 }, { 10, 10 });
  EXPECT_EQ(bA.distance(bD3), (vcrd_2i{ 15, 15 }));
  EXPECT_EQ(bD3.distance(bA), (vcrd_2i{ -15, -15 }));
  vcrd_2i vD4 = { 20, 20 };
  EXPECT_EQ(bA.distance(vD4), (vcrd_2i{ 15, 15 }));
  vcrd_2i vD5 = { -20, -20 };
  EXPECT_EQ(bA.distance(vD5), (vcrd_2i{ -15, -15 }));
}

/*
Demonstrate that box dilation can alter the size of a box on all sides
while maintaining the midpoint.
*/
TEST(Box, Dilate)
{
  const Box2D bA({ 100, 1000 }, { 50, 500 });
  Box2D box;
  // grow
  box = bA;
  box.dilate({ 5, 7 });
  EXPECT_EQ(box.midpoint(), bA.midpoint());
  EXPECT_EQ(box.min(), (vcrd_2i{ 95, 993 }));
  EXPECT_EQ(box.size(), (vcrd_2i{ 60, 514 }));
  // element-grow
  box = bA;
  box.dilate(1);
  EXPECT_EQ(box.midpoint(), bA.midpoint());
  EXPECT_EQ(box.min(), (vcrd_2i{ 99, 999 }));
  EXPECT_EQ(box.size(), (vcrd_2i{ 52, 502 }));
  // shrink
  box = bA;
  box.dilate({ -10, -5 });
  EXPECT_EQ(box.midpoint(), bA.midpoint());
  EXPECT_EQ(box.min(), (vcrd_2i{ 110, 1005 }));
  EXPECT_EQ(box.size(), (vcrd_2i{ 30, 490 }));
}

TEST(Box, Validate)
{
  // validating a valid box does nothing
  Box2D bA({ 5, 5 }, { 5, 5 });
  Box2D bAA = bA;
  bAA.validate();
  EXPECT_EQ(bAA, bA);
  EXPECT_NE(bAA, Box2D());
  // validating a box with a negative size resets
  Box2D bB({ 5, 5 }, { 5, -5 });
  Box2D bBB = bB;
  bBB.validate();
  EXPECT_NE(bBB, bB);
  EXPECT_EQ(bBB, Box2D());
}