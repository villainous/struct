
#include <gtest/gtest.h>
#include "vcrd.inl"

using namespace viln;

/**
Dimensionality-access for coordinates.
*/
TEST(VCrd, Size)
{
  int8_t s2 = vcrd_2i::SIZE;
  EXPECT_EQ(s2, 2);
  int8_t s3 = vcrd_3i::SIZE;
  EXPECT_EQ(s3, 3);
  int8_t s5 = vcrd<float, 5>::SIZE;
  EXPECT_EQ(s5, 5);
}

/**
Verify the index operator behaves as expected.
*/
TEST(VCrd, Index)
{
  // non-const access
  vcrd_2i crd{ 1, 100 };
  EXPECT_EQ(crd[0], 1);
  EXPECT_EQ(crd[1], 100);
  // can modify
  crd[0] = 2;
  EXPECT_EQ(crd[0], 2);
  // const version is consistent with direct access
  const vcrd_2i& ccrd = crd;
  EXPECT_EQ(ccrd[0], crd.x());
  EXPECT_EQ(ccrd[1], crd.y());
}

/*
Demonstrate that the named-element access behaves as expected.
*/
TEST(VCrd, Access)
{
  vcrd_4i crd = { 11, 22, 33, 44};
  const vcrd_4i& ccrd = crd;
  // access
  EXPECT_EQ(crd.x(), 11);
  EXPECT_EQ(crd.y(), 22);
  EXPECT_EQ(crd.z(), 33);
  EXPECT_EQ(crd.w(), 44);
  // const-access
  EXPECT_EQ(ccrd.x(), 11);
  EXPECT_EQ(ccrd.y(), 22);
  EXPECT_EQ(ccrd.z(), 33);
  EXPECT_EQ(ccrd.w(), 44);
  // modification
  EXPECT_EQ(++crd.x(), 12);
  EXPECT_EQ(crd.x(), 12);
  EXPECT_EQ(++crd.y(), 23);
  EXPECT_EQ(crd.y(), 23);
  EXPECT_EQ(++crd.z(), 34);
  EXPECT_EQ(crd.z(), 34);
  EXPECT_EQ(++crd.w(), 45);
  EXPECT_EQ(crd.w(), 45);
}

/**
Verify that implicit type conversions between array types work.
*/
TEST(VCrd, Conversion)
{
  vcrd<int, 3> tmp;
  // expansion
  vcrd<int, 2> a = { 1, 2 };
  tmp = a;
  // indices present in a are the same
  EXPECT_EQ(tmp[0], 1);
  EXPECT_EQ(tmp[1], 2);
  // remaining indices are 0-filled
  EXPECT_EQ(tmp[2], 0);

  // contraction
  vcrd<int, 5> c = { 11, 22, 33, 44, 55 };
  tmp = c;
  // indices present are the same
  EXPECT_EQ(tmp[0], 11);
  EXPECT_EQ(tmp[1], 22);

  // transformation
  vcrd<char, 3> e = { '\1', '\2', '\3' };
  tmp = e;
  // values are the same
  EXPECT_EQ(tmp[0], 1);
  EXPECT_EQ(tmp[1], 2);
  EXPECT_EQ(tmp[2], 3);
}

/**
Demonstrate increment/decrement by unit vectors.
*/
TEST(VCrd, UnitCrd)
{
  vcrd_3i crd = {};
  EXPECT_EQ(crd, (vcrd_3i{ 0, 0, 0 }));
  // +=
  crd += ucrd::X;
  EXPECT_EQ(crd, (vcrd_3i{ 1, 0, 0 }));
  crd += ucrd::Y;
  EXPECT_EQ(crd, (vcrd_3i{ 1, 1, 0 }));
  crd += ucrd::Z;
  EXPECT_EQ(crd, (vcrd_3i{ 1, 1, 1 }));
  // -=
  crd -= ucrd::X;
  EXPECT_EQ(crd, (vcrd_3i{ 0, 1, 1 }));
  crd -= ucrd::Y;
  EXPECT_EQ(crd, (vcrd_3i{ 0, 0, 1 }));
  crd -= ucrd::Z;
  EXPECT_EQ(crd, (vcrd_3i{ 0, 0, 0 }));

  const vcrd_3i crd1 = { 1, 1, 1 };
  vcrd_3i xrd = {};
  EXPECT_EQ(xrd, (vcrd_3i{ 0, 0, 0 }));
  // =
  xrd = crd1 + ucrd::X;
  EXPECT_EQ(xrd, (vcrd_3i{ 2, 1, 1 }));
  xrd = crd1 + ucrd::Y;
  EXPECT_EQ(xrd, (vcrd_3i{ 1, 2, 1 }));
  xrd = crd1 + ucrd::Z;
  EXPECT_EQ(xrd, (vcrd_3i{ 1, 1, 2 }));
  // +
  xrd = crd1 + ucrd::X;
  EXPECT_EQ(xrd, (vcrd_3i{ 2, 1, 1 }));
  xrd = crd1 + ucrd::Y;
  EXPECT_EQ(xrd, (vcrd_3i{ 1, 2, 1 }));
  xrd = crd1 + ucrd::Z;
  EXPECT_EQ(xrd, (vcrd_3i{ 1, 1, 2 }));
  // -
  xrd = crd1 - ucrd::X;
  EXPECT_EQ(xrd, (vcrd_3i{ 0, 1, 1 }));
  xrd = crd1 - ucrd::Y;
  EXPECT_EQ(xrd, (vcrd_3i{ 1, 0, 1 }));
  xrd = crd1 - ucrd::Z;
  EXPECT_EQ(xrd, (vcrd_3i{ 1, 1, 0 }));
}

/**
Demonstrate that addition of elements of a crd to that crd adds that element
to each memeber of the target.
*/
TEST(VCrd, AddSubElem)
{
  vcrd_3i crd1 = { 0, 1, 2 };
  // addition
  EXPECT_EQ(crd1 + 5, (vcrd_3i{ 5, 6, 7 }));
  // subtraction
  EXPECT_EQ(crd1 - 5, (vcrd_3i{ -5, -4, -3 }));
  // addition-equals
  crd1 += 3;
  EXPECT_EQ(crd1, (vcrd_3i{ 3, 4, 5 }));
  // subtract-equals
  crd1 -= 200;
  EXPECT_EQ(crd1, (vcrd_3i{ -197, -196, -195 }));
}

/*
Demonstrate const-access via structured-bindings of the coordinates.
*/
TEST(VCrd, structured_bindings_const)
{
  vcrd_3i crd1 = { 1, 2, 3 };
  const vcrd_3i& ccrd1 = crd1;
  // explicit get
  EXPECT_EQ(get<0>(ccrd1), 1);
  EXPECT_EQ(get<1>(ccrd1), 2);
  EXPECT_EQ(get<2>(ccrd1), 3);
  // const-refs contain values
  const auto& [xc, yc, zc] = ccrd1;
  EXPECT_EQ(xc, 1);
  EXPECT_EQ(yc, 2);
  EXPECT_EQ(zc, 3);
  // changes to coord reflected in values
  crd1 += 1;
  EXPECT_EQ(xc, 2);
  EXPECT_EQ(yc, 3);
  EXPECT_EQ(zc, 4);
}

/*
Demonstrate reference-access via structured-bindings of the coordinates.
*/
TEST(VCrd, structured_bindings)
{
  vcrd_3i crd1 = { 1, 2, 3 };
  // explicit get
  EXPECT_EQ(get<0>(crd1), 1);
  EXPECT_EQ(get<1>(crd1), 2);
  EXPECT_EQ(get<2>(crd1), 3);
  // refs contain values
  auto& [xx, yy, zz] = crd1;
  EXPECT_EQ(--xx, 0);
  EXPECT_EQ(--yy, 1);
  EXPECT_EQ(++zz, 4);
  // changes reflected in original coord
  EXPECT_EQ(crd1.x(), 0);
  EXPECT_EQ(crd1.y(), 1);
  EXPECT_EQ(crd1.z(), 4);
}
