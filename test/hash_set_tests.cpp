
#include "utility"
#include "hash_set.inl"

// check this compiles BEFORE gtest leaks symbols
template class viln::HashSet<double>;

#include <gtest/gtest.h>
#include "algorithm.hpp"

using namespace viln;

/**
Demonstrate that a set can be constructed, and that a single-element set obeys
the add/remove contracts.
*/
TEST(HashSet, SimpleAddRemove)
{
  // create a set
  HashSet<int> hs;
  // set should initially be empty
  ASSERT_EQ(hs.size(), 0);
  // add an element
  ASSERT_TRUE(hs.add(999));
  // the set should now contain one element
  ASSERT_EQ(hs.size(), 1);
  // attempt to add an existing element does nothing
  ASSERT_FALSE(hs.add(999));
  ASSERT_EQ(hs.size(), 1);
  // removing an element that is not a member does nothing
  ASSERT_FALSE(hs.remove(888));
  ASSERT_EQ(hs.size(), 1);
  // removing the element results in an empty set
  ASSERT_TRUE(hs.remove(999));
  ASSERT_EQ(hs.size(), 0);
}

/**
Demonstrate that all the elements added to a `HashSet` can then be visited by
an iterator.
*/
TEST(HashSet, IteratorVisitsAll)
{
  HashSet<int> hs;
  // add a bunch of contiguous elements
  for (int ii = 0; ii < 25; ++ii)
  {
    ASSERT_TRUE(hs.add(ii));
  }
  ASSERT_EQ(hs.size(), 25);
  // keep track of which elements we visited as we iterate over the set
  Iterator<const int>* itr = hs.iterator();
  bool visited[25] = { false };
  for (int ii : hs)
  {
    // check that we never re-visit a value
    ASSERT_FALSE(visited[ii]);
    // we visited!
    visited[ii] = true;
  }
  delete itr;
  // check that we visited every element of the set
  for (int ii = 0; ii < 25; ++ii)
  {
    ASSERT_TRUE(visited[ii]);
  }
}

class BadHashSet : public HashSet<int>
{
protected:
  // intentionally bad hash that maps all to 0
  virtual uint64_t hash(const int& i) const override
  {
    return 0;
  }
};

/**
Demonstrate that the set remains functional even if the chosen hash function
is really bad. While performance may be poor, it should still function.
*/
TEST(HashSet, PoorHashFunction)
{
  HashSet<int> hs;
  // add a bunch of elements, with specific values for the first and last
  ASSERT_TRUE(hs.add(100));
  for (int ii = 0; ii < 25; ++ii)
  {
    ASSERT_TRUE(hs.add(ii));
  }
  ASSERT_TRUE(hs.add(200));
  ASSERT_EQ(hs.size(), 27);
  // all the elements we added should be present
  for (int ii = 0; ii < 25; ++ii)
  {
    EXPECT_TRUE(hs.contains(ii));
  }
  ASSERT_TRUE(hs.contains(100));
  ASSERT_TRUE(hs.contains(200));
  // removing some elements should result in those elements not being present
  ASSERT_TRUE(hs.remove(100));
  ASSERT_FALSE(hs.contains(100));
  ASSERT_TRUE(hs.remove(200));
  ASSERT_FALSE(hs.contains(200));
  ASSERT_EQ(hs.size(), 25);
}

/**
Demonstrate that the equality operator returns true only if the sets contain
the same elements.
*/
TEST(HashSet, EqualityOperator)
{
  // self-equality
  HashSet<int> hs1A;
  hs1A.add(1);
  hs1A.add(2);
  hs1A.add(3);
  EXPECT_TRUE(hs1A == hs1A);
  EXPECT_FALSE(hs1A != hs1A);
  // sets with the same elements are equal
  HashSet<int> hs1B;
  hs1B.add(3);
  hs1B.add(2);
  hs1B.add(1);
  EXPECT_TRUE(hs1A == hs1B);
  EXPECT_FALSE(hs1A != hs1B);
  EXPECT_TRUE(hs1B == hs1A);
  EXPECT_FALSE(hs1B != hs1A);
  // sets with different elements are not equal
  HashSet<int> hs2;
  hs2.add(9);
  hs2.add(8);
  hs2.add(7);
  EXPECT_FALSE(hs1A == hs2);
  EXPECT_TRUE(hs1A != hs2);
  EXPECT_FALSE(hs2 == hs1A);
  EXPECT_TRUE(hs2 != hs1A);
  // sets with a different number of elements are not equal
  HashSet<int> hs3;
  hs3.add(1);
  hs3.add(2);
  hs3.add(3);
  hs3.add(4);
  EXPECT_FALSE(hs1A == hs3);
  EXPECT_TRUE(hs1A != hs3);
  EXPECT_FALSE(hs3 == hs1A);
  EXPECT_TRUE(hs3 != hs1A);
}

/**
Set should be copyable, resulting in copies of all elements.
*/
TEST(HashSet, CopyConstructor)
{
  // create two sets containing different elements
  HashSet<int> set1;
  ASSERT_TRUE(set1.add(1));
  ASSERT_TRUE(set1.add(2));
  {
    // copy the set
    HashSet<int> set2(set1);
    ASSERT_EQ(set1, set2);
  }
  // make sure the other set falling out of scope didnt delete our nodes
  ASSERT_TRUE(set1.contains(1));
}

/**
Demonstrate that the assignment operator makes the LHS set contain the same
elements as the RHS.
*/
TEST(HashSet, AssignmentOperator)
{
  HashSet<int> hs1;
  hs1.add(1);
  hs1.add(2);
  HashSet<int> hs2;
  hs2.add(1);
  hs2.add(2);
  hs2.add(3);
  // the lists are not the same
  ASSERT_NE(hs1, hs2);
  // perform assignment
  hs2 = hs1;
  // the lists are now the same
  EXPECT_EQ(hs1, hs2);
}

/*
Demonstrate the move-assign operator via set-swap.
*/
TEST(HashSet, MoveSwap)
{
  HashSet<int> set1;
  set1.add(1);
  set1.add(2);
  const HashSet<int> set1c(set1);
  HashSet<int> set2;
  set2.add(1);
  set2.add(2);
  set2.add(3);
  const HashSet<int> set2c(set2);
  // the sets match initial values
  ASSERT_NE(set1c, set2c);
  ASSERT_EQ(set1, set1c);
  ASSERT_EQ(set2, set2c);
  // perform swap
  std::swap(set1, set2);
  // the sets are now swapped
  EXPECT_EQ(set1.size(), 3);
  EXPECT_EQ(set2.size(), 2);
  EXPECT_EQ(set2, set1c);
  EXPECT_EQ(set1, set2c);
}

/**
Demonstrate that a set with 0 initial capacity behaves as expected.
*/
TEST(HashSet, ZeroCap)
{
  // create a set with 0 requested capacity
  HashSet<int> blarg(0);
  ASSERT_EQ(blarg.size(), 0);
  ASSERT_FALSE(blarg.contains(999));
  // add an element to the empty set
  ASSERT_TRUE(blarg.add(999));
  ASSERT_EQ(blarg.size(), 1);
  ASSERT_TRUE(blarg.contains(999));
}
