#include "list_node.inl"

// check this compiles BEFORE gtest leaks symbols
template class viln::ListNode<double>;

#include <gtest/gtest.h>

using namespace viln;

/**
Check that the constructor creates an isolated `ListNode` as expected.
*/
TEST(ListNode, Creation)
{
  ListNode<int> node(new int(22));
  // data is as provided
  EXPECT_EQ(node.data(), 22);
  // next and prev are null upon construction
  EXPECT_EQ(node.nextNode(), nullptr);
  EXPECT_EQ(node.previousNode(), nullptr);
}

/**
Show that linking nodes results in self-consistent ordering.
*/
TEST(ListNode, AppendPrepend)
{
  ListNode<int>* node1 = new ListNode<int>(new int(1));
  ListNode<int>* node2 = new ListNode<int>(new int(22));
  ListNode<int>* node3 = new ListNode<int>(new int(333));
  // set up linkage like -node1--node2--node3-
  node3->prepend(node1);
  node1->append(node2);
  // look at node1 to check it is correctly linked
  EXPECT_EQ(node1->previousNode(), nullptr);
  EXPECT_EQ(node1->nextNode(), node2);
  // look at node2 to check it is correctly linked
  EXPECT_EQ(node2->previousNode(), node1);
  EXPECT_EQ(node2->nextNode(), node3);
  // look at node3 to check it is correctly linked
  EXPECT_EQ(node3->previousNode(), node2);
  EXPECT_EQ(node3->nextNode(), nullptr);
  // quit early if the links are null so the test doesn't segfault
  EXPECT_NE(node2->previousNode(), nullptr);
  EXPECT_NE(node2->nextNode(), nullptr);
  if (node2->previousNode() && node2->nextNode())
  {
    // show that the data for our list looks right
    EXPECT_EQ(node2->previousNode()->data(), 1);
    EXPECT_EQ(node2->data(), 22);
    EXPECT_EQ(node2->nextNode()->data(), 333);
  }
  // deletion in reverse-order is a little bit safer if things go off the rails
  delete node3;
  delete node2;
  delete node1;
}

/**
Demonstrate appending mulitple elements.
*/
TEST(ListNode, AppendMulti)
{
  ListNode<int>* node11 = new ListNode<int>(new int(11));
  ListNode<int>* node12 = new ListNode<int>(new int(12));
  ListNode<int>* node21 = new ListNode<int>(new int(21));
  ListNode<int>* node22 = new ListNode<int>(new int(22));
  // set up linkage like -node11--node12- -node21--node22-
  node11->append(node12);
  node21->append(node22);
  // sanity check linkages
  EXPECT_EQ(node11->previousNode(), nullptr);
  EXPECT_EQ(node11->nextNode(), node12);
  EXPECT_EQ(node12->previousNode(), node11);
  EXPECT_EQ(node12->nextNode(), nullptr);
  EXPECT_EQ(node21->previousNode(), nullptr);
  EXPECT_EQ(node21->nextNode(), node22);
  EXPECT_EQ(node22->previousNode(), node21);
  EXPECT_EQ(node22->nextNode(), nullptr);
  // append the chain containing node22 between node11 and node12
  node11->append(node22);
  // re-check linkages -node11--node21--node22--node12-
  EXPECT_EQ(node11->previousNode(), nullptr);
  EXPECT_EQ(node11->nextNode(), node21);
  EXPECT_EQ(node21->previousNode(), node11);
  EXPECT_EQ(node21->nextNode(), node22);
  EXPECT_EQ(node22->previousNode(), node21);
  EXPECT_EQ(node22->nextNode(), node12);
  EXPECT_EQ(node12->previousNode(), node22);
  EXPECT_EQ(node12->nextNode(), nullptr);
  // deletion in reverse-order is a little bit safer if things go off the rails
  delete node12;
  delete node22;
  delete node21;
  delete node11;
}

/**
Demonstrate detaching the middle node from the list.
*/
TEST(ListNode, DetachMiddle)
{
  ListNode<int>* node1 = new ListNode<int>(new int(1));
  ListNode<int>* node2 = new ListNode<int>(new int(22));
  ListNode<int>* node3 = new ListNode<int>(new int(333));
  // set up linkage like -node1--node2--node3-
  node2->append(node3);
  node1->append(node2);
  // sanity check linkages
  EXPECT_EQ(node1->previousNode(), nullptr);
  EXPECT_EQ(node1->nextNode(), node2);
  EXPECT_EQ(node2->previousNode(), node1);
  EXPECT_EQ(node2->nextNode(), node3);
  EXPECT_EQ(node3->previousNode(), node2);
  EXPECT_EQ(node3->nextNode(), nullptr);
  // detach the middle node
  node2->detach();
  // re-check linkages  -node1--node3-  -node2-
  EXPECT_EQ(node1->previousNode(), nullptr);
  EXPECT_EQ(node1->nextNode(), node3);
  EXPECT_EQ(node2->previousNode(), nullptr);
  EXPECT_EQ(node2->nextNode(), nullptr);
  EXPECT_EQ(node3->previousNode(), node1);
  EXPECT_EQ(node3->nextNode(), nullptr);
  // deletion in reverse-order is a little bit safer if things go off the rails
  delete node3;
  delete node2;
  delete node1;
}

/**
Demonstrating detaching a node from the end of the list.
*/
TEST(ListNode, DetachFirst)
{
  ListNode<int>* node1 = new ListNode<int>(new int(1));
  ListNode<int>* node2 = new ListNode<int>(new int(22));
  ListNode<int>* node3 = new ListNode<int>(new int(333));
  // set up linkage like -node1--node2--node3-
  node2->append(node3);
  node1->append(node2);
  // sanity check linkages
  EXPECT_EQ(node1->previousNode(), nullptr);
  EXPECT_EQ(node1->nextNode(), node2);
  EXPECT_EQ(node2->previousNode(), node1);
  EXPECT_EQ(node2->nextNode(), node3);
  EXPECT_EQ(node3->previousNode(), node2);
  EXPECT_EQ(node3->nextNode(), nullptr);
  // detach the first node
  node1->detach();
  // re-check linkages -node1-  -node2--node3-
  EXPECT_EQ(node1->previousNode(), nullptr);
  EXPECT_EQ(node1->nextNode(), nullptr);
  EXPECT_EQ(node2->previousNode(), nullptr);
  EXPECT_EQ(node2->nextNode(), node3);
  EXPECT_EQ(node3->previousNode(), node2);
  EXPECT_EQ(node3->nextNode(), nullptr);
  // deletion in reverse-order is a little bit safer if things go off the rails
  delete node3;
  delete node2;
  delete node1;
}

/**
Demonstrate splitting the list in half.
*/
TEST(ListNode, Split)
{
  ListNode<int>* node1 = new ListNode<int>(new int(1));
  ListNode<int>* node2 = new ListNode<int>(new int(22));
  ListNode<int>* node3 = new ListNode<int>(new int(333));
  ListNode<int>* node4 = new ListNode<int>(new int(4444));
  // set up linkage like -node1--node2--node3--node4-
  node3->append(node4);
  node2->append(node3);
  node1->append(node2);
  // sanity check linkages
  EXPECT_EQ(node1->previousNode(), nullptr);
  EXPECT_EQ(node1->nextNode(), node2);
  EXPECT_EQ(node2->previousNode(), node1);
  EXPECT_EQ(node2->nextNode(), node3);
  EXPECT_EQ(node3->previousNode(), node2);
  EXPECT_EQ(node3->nextNode(), node4);
  EXPECT_EQ(node4->previousNode(), node3);
  EXPECT_EQ(node4->nextNode(), nullptr);
  // splat at the middle node
  node3->split();
  // re-check linkages -node1--node2-  -node3--node4-
  EXPECT_EQ(node1->previousNode(), nullptr);
  EXPECT_EQ(node1->nextNode(), node2);
  EXPECT_EQ(node2->previousNode(), node1);
  EXPECT_EQ(node2->nextNode(), nullptr);
  EXPECT_EQ(node3->previousNode(), nullptr);
  EXPECT_EQ(node3->nextNode(), node4);
  EXPECT_EQ(node4->previousNode(), node3);
  EXPECT_EQ(node4->nextNode(), nullptr);
  // deletion in reverse-order is a little bit safer if things go off the rails
  delete node4;
  delete node3;
  delete node2;
  delete node1;
}
