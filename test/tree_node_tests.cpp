#include "tree_node.inl"

// check this compiles BEFORE gtest leaks symbols
template class viln::TreeNode<double>;

#include <gtest/gtest.h>

using namespace viln;

/**
Check that the constructor creates an isolated `TreeNode` as expected.
*/
TEST(TreeNode, Creation)
{
  TreeNode<int> node(new int(22));
  // data is as provided
  EXPECT_EQ(node.data(), 22);
  // no children
  EXPECT_EQ(node.rightChild(), nullptr);
  EXPECT_EQ(node.leftChild(), nullptr);
  // no parent
  EXPECT_EQ(node.parent(), nullptr);
  // data is contained
  EXPECT_EQ(node.data(), 22);
}

/**
Demonstrate the left child node connection can be created and destroyed.
*/
TEST(TreeNode, SetDetachLeft)
{
  TreeNode<int>* node1 = new TreeNode<int>(new int(1));
  TreeNode<int>* node2 = new TreeNode<int>(new int(2));
  // nodes are not initially attached
  EXPECT_EQ(node1->leftChild(), nullptr);
  EXPECT_EQ(node2->parent(), nullptr);
  EXPECT_EQ(node1->height(), 0);
  EXPECT_EQ(node1->calcHeight(), 0);
  EXPECT_EQ(node1->size(), 1);
  EXPECT_EQ(node1->calcSize(), 1);
  // attach the nodes on the left
  node1->setLeft(node2);
  // nodes are now attached
  EXPECT_EQ(node1->leftChild(), node2);
  EXPECT_EQ(node2->parent(), node1);
  EXPECT_EQ(node1->height(), 1);
  EXPECT_EQ(node1->calcHeight(), 1);
  EXPECT_EQ(node1->size(), 2);
  EXPECT_EQ(node1->calcSize(), 2);
  // detach the nodes
  node1->detachLeft();
  // nodes are no longer attached
  EXPECT_EQ(node1->leftChild(), nullptr);
  EXPECT_EQ(node2->parent(), nullptr);
  EXPECT_EQ(node1->height(), 0);
  EXPECT_EQ(node1->calcHeight(), 0);
  EXPECT_EQ(node1->size(), 1);
  EXPECT_EQ(node1->calcSize(), 1);
  // clean up
  delete node2;
  delete node1;
}

/**
Demonstrate the right child node connection can be created and destroyed.
*/
TEST(TreeNode, SetDetachRight)
{
  TreeNode<int>* node1 = new TreeNode<int>(new int(1));
  TreeNode<int>* node2 = new TreeNode<int>(new int(2));
  // nodes are not initially attached
  EXPECT_EQ(node1->rightChild(), nullptr);
  EXPECT_EQ(node2->parent(), nullptr);
  EXPECT_EQ(node1->height(), 0);
  EXPECT_EQ(node1->calcHeight(), 0);
  EXPECT_EQ(node1->size(), 1);
  EXPECT_EQ(node1->calcSize(), 1);
  // attach the nodes on the Right
  node1->setRight(node2);
  // nodes are now attached
  EXPECT_EQ(node1->rightChild(), node2);
  EXPECT_EQ(node2->parent(), node1);
  EXPECT_EQ(node1->height(), 1);
  EXPECT_EQ(node1->calcHeight(), 1);
  EXPECT_EQ(node1->size(), 2);
  EXPECT_EQ(node1->calcSize(), 2);
  // detach the nodes
  node1->detachRight();
  // nodes are no longer attached
  EXPECT_EQ(node1->rightChild(), nullptr);
  EXPECT_EQ(node2->parent(), nullptr);
  EXPECT_EQ(node1->height(), 0);
  EXPECT_EQ(node1->calcHeight(), 0);
  EXPECT_EQ(node1->size(), 1);
  EXPECT_EQ(node1->calcSize(), 1);
  // clean up
  delete node2;
  delete node1;
}

/**
Demonstrate that the node can be detached by the child.
*/
TEST(TreeNode, Detach)
{
  TreeNode<int>* node1 = new TreeNode<int>(new int(1));
  TreeNode<int>* node2 = new TreeNode<int>(new int(2));
  // nodes are not initially attached
  EXPECT_EQ(node1->rightChild(), nullptr);
  EXPECT_EQ(node2->parent(), nullptr);
  EXPECT_EQ(node1->height(), 0);
  EXPECT_EQ(node1->calcHeight(), 0);
  EXPECT_EQ(node1->size(), 1);
  EXPECT_EQ(node1->calcSize(), 1);
  // attach the nodes on the Right
  node1->setRight(node2);
  // nodes are now attached
  EXPECT_EQ(node1->rightChild(), node2);
  EXPECT_EQ(node2->parent(), node1);
  EXPECT_EQ(node1->height(), 1);
  EXPECT_EQ(node1->calcHeight(), 1);
  EXPECT_EQ(node1->size(), 2);
  EXPECT_EQ(node1->calcSize(), 2);
  // detach the nodes
  node2->detach();
  // nodes are no longer attached
  EXPECT_EQ(node1->rightChild(), nullptr);
  EXPECT_EQ(node2->parent(), nullptr);
  EXPECT_EQ(node1->height(), 0);
  EXPECT_EQ(node1->calcHeight(), 0);
  EXPECT_EQ(node1->size(), 1);
  EXPECT_EQ(node1->calcSize(), 1);
  // clean up
  delete node2;
  delete node1;
}
