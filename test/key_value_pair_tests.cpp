#include "key_value_pair.inl"

template class viln::KeyValuePair<double, double>;

#include <utility>
#include <gtest/gtest.h>
#include "canary.hpp"

using namespace viln;

/*
Demonstrate the simple case of creating a pair.
*/
TEST(KeyValuePair, create)
{
  Canary::reset();
  {
    // create
    KeyValuePair<int, Canary> kvp(21, new Canary(21));
    // key/value match creation
    EXPECT_EQ(kvp.key(), 21);
    EXPECT_EQ(kvp.value().data, 21);
  }
  // 1 element used
  EXPECT_EQ(Canary::constructor, 1);
  EXPECT_EQ(Canary::destructor, 1);
}

/*
Demonstrate moving a pair is possible and disticnt from copy.
*/
TEST(KeyValuePair, move)
{
  Canary::reset();
  {
    // create
    KeyValuePair<int, Canary> kvp(1, new Canary(1000));
    // move
    KeyValuePair<int, Canary> kvmov(std::move(kvp));
    // moved key/value match creation
    EXPECT_EQ(kvmov.key(), 1);
    EXPECT_EQ(kvmov.value().data, 1000);
  }
  // 1 element used
  EXPECT_EQ(Canary::constructor, 1);
  EXPECT_EQ(Canary::destructor, 1);
  // pointer moved, not copied
  EXPECT_LE(Canary::move, 1);
  EXPECT_EQ(Canary::copy, 0);
}

/*
Demonstrate that the KVP functions as a smart-pointer to the
value-type.
*/
TEST(KeyValuePair, pointer)
{
  KeyValuePair<int, Canary> kvp(1, new Canary(99));
  // pointer-like access
  EXPECT_EQ((*kvp).data, 99);
  EXPECT_EQ(kvp->data, 99);
  // const-pointer access
  const auto& ckvp = kvp;
  EXPECT_EQ((*ckvp).data, 99);
  EXPECT_EQ(ckvp->data, 99);
  // modifiable access
  (*kvp).data = 199;
  EXPECT_EQ((*kvp).data, 199);
  kvp->data = 299;
  EXPECT_EQ((*kvp).data, 299);
}

/*
Demonstrate access via structured-bindings of the key/value pair.
*/
TEST(KeyValuePair, structured_bindings)
{
  KeyVal<int, float> kvp(-2, new float(99.9f));
  EXPECT_EQ(viln::get<0>(kvp), -2);
  EXPECT_EQ(viln::get<1>(kvp), 99.9f);
  const auto& [x, y] = kvp;
  EXPECT_EQ(x, -2);
  EXPECT_EQ(y, 99.9f);
}
