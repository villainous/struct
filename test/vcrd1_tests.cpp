
#include <gtest/gtest.h>
#include "vcrd.inl"

using namespace viln;
typedef vcrd<int, 1> vcrd_1i;

/*
Demonstrate that equality operators behave as expected.
*/
TEST(VCrd1, Equality)
{
  // self-equality
  vcrd_1i crd1A{ 1 };
  EXPECT_TRUE(crd1A == crd1A);
  EXPECT_FALSE(crd1A != crd1A);
  // equals
  vcrd_1i crd1B{ 1 };
  EXPECT_TRUE(crd1A == crd1B);
  EXPECT_FALSE(crd1A != crd1B);
  // not equals
  vcrd_1i crd2{ 3 };
  EXPECT_FALSE(crd1A == crd2);
  EXPECT_TRUE(crd1A != crd2);
  // element-equality
  vcrd_1i crd3{ 9 };
  EXPECT_TRUE(crd3 == 9);
  EXPECT_FALSE(crd3 != 9);
  vcrd_1i crd4{ 8 };
  EXPECT_FALSE(crd4 == 9);
  EXPECT_TRUE(crd4 != 9);
}

/*
Demonstrate assignment to vcrd[2] from various types.
*/
TEST(VCrd1, Convert)
{
  vcrd_1i crd = {};
  EXPECT_EQ(crd, (vcrd_1i{ 0 }));
  // vcrd-assign
  const vcrd_1i crdA = { 11 };
  EXPECT_EQ((crd = crdA), (vcrd_1i{ 11 }));
  EXPECT_EQ(crd, (vcrd_1i{ 11 }));
  // ucrd-assign
  EXPECT_EQ(crd = ucrd::Y, (vcrd_1i{ 0 }));
  EXPECT_EQ(crd, (vcrd_1i{ 0 }));
  // element-assign
  EXPECT_EQ(crd = 234, (vcrd_1i{ 234 }));
  EXPECT_EQ(crd, (vcrd_1i{ 234 }));
  // vcrd>-assign
  const vcrd<int,5> crd5 = { 5, 4, 3, 2, 1 };
  EXPECT_EQ(crd = crd5, (vcrd_1i{ 5 }));
  EXPECT_EQ(crd, (vcrd_1i{ 5 }));
}

/*
Verify that the `+` `+=` `++` operators behave as expected.
*/
TEST(VCrd1, Plus)
{
  const vcrd_1i crd1{ 1 };
  const vcrd_1i crd2{ 3 };
  vcrd_1i crd = {};
  // plus
  EXPECT_EQ((crd1 + crd2), (vcrd_1i{ 4 }));
  // ucrd plus
  EXPECT_EQ((crd1 + ucrd::X), (vcrd_1i{ 2 }));
  // element plus
  EXPECT_EQ((crd1 + 5), (vcrd_1i{ 6 }));
  // assign-plus
  crd = crd1;
  EXPECT_EQ((crd += crd2), (vcrd_1i{ 4 }));
  EXPECT_EQ(crd, (vcrd_1i{ 4 }));
  // ucrd assign-plus
  crd = crd1;
  EXPECT_EQ((crd += ucrd::Y), (vcrd_1i{ 1 }));
  EXPECT_EQ(crd, (vcrd_1i{ 1 }));
  // element assign-plus
  crd = crd1;
  EXPECT_EQ((crd += 5), (vcrd_1i{ 6 }));
  EXPECT_EQ(crd, (vcrd_1i{ 6 }));
  // left-increment
  crd = crd1;
  EXPECT_EQ(++crd, (vcrd_1i{ 2 }));
  EXPECT_EQ(crd, (vcrd_1i{ 2 }));
}

/*
Verify that the `-` `-=` `--` operators behave as expected.
*/
TEST(VCrd1, Minus)
{
  const vcrd_1i crd1{ 1 };
  const vcrd_1i crd2{ 3 };
  vcrd_1i crd = {};
  // negate
  EXPECT_EQ(-crd1, (vcrd_1i{ -1 }));
  // minus
  EXPECT_EQ((crd1 - crd2), (vcrd_1i{ -2 }));
  // ucrd minus
  EXPECT_EQ((crd1 - ucrd::X), (vcrd_1i{ 0 }));
  // element minus
  EXPECT_EQ((crd1 - 5), (vcrd_1i{ -4 }));
  // assign-minus
  crd = crd1;
  EXPECT_EQ((crd -= crd2), (vcrd_1i{ -2 }));
  EXPECT_EQ(crd, (vcrd_1i{ -2 }));
  // ucrd assign-minus
  crd = crd1;
  EXPECT_EQ((crd -= ucrd::Y), (vcrd_1i{ 1 }));
  EXPECT_EQ(crd, (vcrd_1i{ 1 }));
  // element assign-minus
  crd = crd1;
  EXPECT_EQ((crd -= 5), (vcrd_1i{ -4 }));
  EXPECT_EQ(crd, (vcrd_1i{ -4 }));
  // left-decrement
  crd = crd1;
  EXPECT_EQ(--crd, (vcrd_1i{ 0 }));
  EXPECT_EQ(crd, (vcrd_1i{ 0 }));
}

/*
Verify that the `*` `*=` operators behave as expected.
*/
TEST(VCrd1, Multiply)
{
  const vcrd_1i crd1{ 1 };
  const vcrd_1i crd2{ 3 };
  const int fac = 12;
  vcrd_1i crd = {};
  // multiply
  EXPECT_EQ((crd1 * crd2), (vcrd_1i{ 3 }));
  // element multiply
  EXPECT_EQ((crd1 * fac), (vcrd_1i{ 12 }));
  // assign-multiply
  crd = crd1;
  EXPECT_EQ((crd *= crd2), (vcrd_1i{ 3 }));
  EXPECT_EQ(crd, (vcrd_1i{ 3 }));
  // element assign-multiply
  crd = crd1;
  EXPECT_EQ((crd *= fac), (vcrd_1i{ 12 }));
  EXPECT_EQ(crd, (vcrd_1i{ 12 }));
}

/*
Verify that the `/` `/=` operators and `div_floor` behave as expected.
*/
TEST(VCrd1, Divide)
{
  const vcrd_1i crd1{ 12 };
  const vcrd_1i crd2{ 11 };
  const int fac = 11;
  vcrd_1i crd = {};
  // div
  EXPECT_EQ((crd1 / crd2), (vcrd_1i{ 1 }));
  // element div
  EXPECT_EQ((crd1 / fac), (vcrd_1i{ 1 }));
  // div-assign
  crd = crd1;
  EXPECT_EQ((crd /= crd2), (vcrd_1i{ 1 }));
  EXPECT_EQ(crd, (vcrd_1i{ 1 }));
  // element div-assign
  crd = crd1;
  EXPECT_EQ((crd /= fac), (vcrd_1i{ 1 }));
  EXPECT_EQ(crd, (vcrd_1i{ 1 }));
  // div_floor
  EXPECT_EQ(div_floor(crd1, crd2), (vcrd_1i{ 1 }));
  // element div_floor
  EXPECT_EQ(div_floor(crd1, fac), (vcrd_1i{ 1 }));
}

/*
Verify that the `%` `%=` operators and `mod_floor` behave as expected.
*/
TEST(VCrd1, Mod)
{
  const vcrd_1i crd1{ 12 };
  const vcrd_1i crd2{ 11 };
  const int fac = 11;
  vcrd_1i crd = {};
  // mod
  EXPECT_EQ((crd1 % crd2), (vcrd_1i{ 1 }));
  // element mod
  EXPECT_EQ((crd1 % fac), (vcrd_1i{ 1 }));
  // mod-assign
  crd = crd1;
  EXPECT_EQ((crd %= crd2), (vcrd_1i{ 1 }));
  EXPECT_EQ(crd, (vcrd_1i{ 1 }));
  // element mod-assign
  crd = crd1;
  EXPECT_EQ((crd %= fac), (vcrd_1i{ 1 }));
  EXPECT_EQ(crd, (vcrd_1i{ 1 }));
  // mod_floor
  EXPECT_EQ(mod_floor(crd1, crd2), (vcrd_1i{ 1 }));
  // element mod_floor
  EXPECT_EQ(mod_floor(crd1, fac), (vcrd_1i{ 1 }));
}

/*
Demonstrate math operations.
*/
TEST(VCrd1, Math)
{
  const vcrd_1i crd1{ 12 };
  const vcrd_1i crd2{ 40 };
  // abs
  EXPECT_EQ(abs(crd1), (vcrd_1i{ 12 }));
  EXPECT_EQ(abs(-crd1), (vcrd_1i{ 12 }));
  // sgn
  EXPECT_EQ(sgn(crd1), (vcrd_1i{ 1 }));
  EXPECT_EQ(sgn(-crd1), (vcrd_1i{ -1 }));
  // max1
  EXPECT_EQ(max(crd1), 12);
  // max2
  EXPECT_EQ(max(crd1, crd2), (vcrd_1i{ 40 }));
  EXPECT_EQ(max(crd1, 11), (vcrd_1i{ 12 }));
  // min1
  EXPECT_EQ(min(crd1), 12);
  // min2
  EXPECT_EQ(min(crd1, crd2), (vcrd_1i{ 12 }));
  EXPECT_EQ(min(crd1, 11), (vcrd_1i{ 11 }));
}

/*
Demonstrate vector-math operations.
*/
TEST(VCrd1, Vector)
{
  const vcrd_1i crd1{ 12 };
  const vcrd_1i crd2{ 3 };
  // volume
  EXPECT_EQ(volume(crd1), 12);
  EXPECT_EQ(volume(-crd1), 12);
  // dot
  EXPECT_EQ(dot(crd1, crd2), 36);
  // lengths
  EXPECT_EQ(length_max(crd1), 12);
  EXPECT_EQ(length1(crd1), 12);
  EXPECT_EQ(length(crd1), 12);
  EXPECT_EQ(length_oct(crd1), 12);
}

/*
Demonstrate 2d vector creation and access.
*/
TEST(VCrd1, 1x1)
{
  vcrd<vcrd<int, 1>, 1> crd1x1{
    vcrd_2i{ 1 } };
  // spot-check the values are accessible
  EXPECT_EQ(crd1x1[0][0], 1);
}

/*
Demonstrate single-line construction of a coord from a value.
*/
TEST(VCrd1, make_vcrd)
{
  vcrd<int, 1> crd1 = make_vcrd(2344);
  EXPECT_EQ(crd1[0], 2344);
}
