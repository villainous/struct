
# VilnLibs

[![code documentation][doc badge]][documentation]
[![gitlab-ci status][build badge]][pipeline status]
[![test coverage report][codecov badge]][coverage report]

[![discord server][chat badge]][chat server]

# Introduction

This focus of this project is the creation and maintenance of a set of C++
libraries providing desirable "core functionality" similar to the C++ STL and
Boost. The style and methodology at work, however, is inspired by the Java SE 6
JDK core libraries.

## Motivation

The main motivation of the creation of these libraries is to improve the
accessibility of C++ development. If one compares the ease-of-use of the
C++ STL and Boost to the Java core library, the Java libraries are the
clear winner.

C++ has significant performance advantages over most other languages,
and native support for many C libraries. This makes C++ desirable, even
with the accessibility issues. Addressing the accessibility issues would
allow more individuals to unlock those benefits.

## Goals

The intention of this project is to provide a set of loosely-coupled C++
libraries providing similar capabilities to JDK 6. These libraries should
be performant, reliable, and secure. The libraries should be well-documented
and the provided headers should be plain-text readable.

The libraries should be platform-independent where possible, and widely
distributed to multiple platforms.

# Getting Started

Currently, the libraires are obtained by building from source. It is planned
to provide prebuilt binaries for a number of systems at a future date.

It is recommended to use the [Villainous vcpkg][] fork in order to build
the project for your chosen platform. Nominally, the process is:

1. Install [Git][] for version-control.
2. Use `git` to clone a copy of [Villainous vcpkg][].

    ```bash
    git clone https://gitlab.com/villainous/vcpkg
    ```

3. Navigate to the **vcpkg/** install directory and run **bootstrap-vcpkg.sh**.

    ```bash
    cd vcpkg
    ./bootstrap-vcpkg.sh
    ```

4. Install VilnLibs using vcpkg.

    ```bash
    vcpkg install vilnlibs
    ```

If you are using [CMake][] as your project's build tool, then you can add the
`VILN` targets to your project:

5. Your CMake project can now depend on the project via the supplied targets.

    ```cmake
    # CMakeLists.txt

    # add a dependency
    find_package( VilnLibs "<desired version>" REQUIRED )

    # link to the libraries
    add_executable( MyProject "" )
    target_sources( MyProject PRIVATE "my_main.cpp" )
    target_link_libraries( MyProject PRIVATE VILN::Struct )
    ```

6. Use the vcpkg toolchain file when configuring the CMake project.

    ```bash
    cmake -DCMAKE_TOOLCHAIN_FILE="<vcpkg abs path>/scripts/buildsystems/vcpkg.cmake" <...>
    ```

# Components

The capabilities provided are split into several components, each providing
a separate library.

## Structures

This library provides similar capabilities to the Java Collections Framework,
or the STL **map** **vector** and **set**. See the
[collections documentation](src/VilnStruct.md) for more details.

## More?

Additional capabilities are under consideration, however priority is given
to capabilities that are ubiquidously useful, and have a confusing or
difficult to use implementation provided by the STL.

# Conventions

## Naming Conventions

Naming conventions follow the rule that object-oriented elements use Java
conventions, and functional elements use C conventions.

| Element          | Convention                                | Example      |
|------------------|-------------------------------------------|--------------|
| **Functional** |||
| struct           | all lowercase, underscore separated       | `my_struct`  |
| function         | all lowercase, no separators              | `myfunc`     |
| local variables  | camel-case, first letter lowercase        | `myVar`      |
| **Object-Oriented** |||
| class            | camel-case, first letter capital          | `MyClass`    |
| method           | camel-case, first letter lowercase        | `myMethod()` |
| template type    | single uppercase letter                   | `<T>`        |
| member variables | same as locals, but trailing underscore   | `myMember_`  |

## Data Ownership

These libraries do exchange memory via pointers when appropriate. Avoiding
memory leaks is achieved via a strict data ownership model. Every pointer has
an owner context or object, and it is the responsibility of the owner to
free the memory when it is no longer needed.

Functions or methods (in these libraries) which exchange pointer ownership
will do so with an unqualified pointer. Const-qualified pointers indicate
that the pointer ownership is not being exchanged.

| Element       | Meaning                                                     |
|---------------|-------------------------------------------------------------|
| **Return** ||
| `Type*`       | caller *becomes the owner* of the data                      |
| `const Type*` | caller has read-only access, but does not assume ownership  |
| `Type* const` | caller has read-write access, but does not assume ownership |
| **Argument**  ||
| `Type*`       | caller *gives up ownership* of the data to the callee       |
| `const Type*` | callee has read-only access, but does not assume ownership  |
| `Type* const` | callee has read-write access, but does not assume ownership |

Furthermore, functions or methods that involve data exchange will explicitly
state the exchange in their documentation, in order to avoid confusion.

## Documentation

Online [documentation][] is provided for these libraries, but said
documentation is generated from the code itself. The provided headers should
be human-readable, so it is up to preference how to view this information.

[documentation]: https://villainous.gitlab.io/struct/index.html
[pipeline status]: https://gitlab.com/villainous/struct/pipelines/dev/latest
[coverage report]: https://gitlab.com/villainous/struct/builds/artifacts/dev/file/coverage/index.html?job=test.linux
[chat server]: https://discord.com/channels/725789656293179482/957020146331971584

[doc badge]: https://img.shields.io/badge/docs-gitlab.io-informational
[build badge]: https://gitlab.com/villainous/struct/badges/dev/pipeline.svg
[codecov badge]: https://gitlab.com/villainous/struct/badges/dev/coverage.svg
[chat badge]: https://img.shields.io/discord/725789656293179482

[Git]: https://git-scm.com/downloads
[CMake]: https://cmake.org/download/
[Villainous vcpkg]: https://gitlab.com/villainous/vcpkg
