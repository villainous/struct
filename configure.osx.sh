#!/bin/bash

export VCPKG_DEFAULT_TRIPLET=x64-osx
CMAKE_BUILD_TYPE=${1:-Debug}

# Kills the process in case of unrecoverable errors.
function die {
  case "$-" in
  *i*) read -p "$1" ;;
  *)   echo "$1" ;;
  esac
  exit 1
}

# List the dependencies of the specified binary-executable.
function depends {
  for f in $(objdump -p "${1?}" | grep NEEDED | awk '{print $2}')
  do dpkg -S $f | awk '{split($0,a,":"); print a[1]}'
  done | sort | uniq
}

# --- MAIN

if [[ ! -x vcpkg/vcpkg ]]
then vcpkg/bootstrap-vcpkg.sh
fi
vcpkg/vcpkg version
vcpkg/vcpkg install gtest \
  || die "ERROR: vcpkg dependency installation failure"
vcpkg/vcpkg update

cmake --version
cmake \
  -B _build \
  -G Xcode \
  -DBUILD_SHARED_LIBS=ON \
  -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE} \
  -DCMAKE_INSTALL_PREFIX=_install \
  -DCMAKE_TOOLCHAIN_FILE="vcpkg/scripts/buildsystems/vcpkg.cmake" \
  . \
  || die "ERROR: cmake build failure"
