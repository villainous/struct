
# config
cmake_minimum_required( VERSION 3.14 )
cmake_policy( SET CMP0076 NEW )
set( PROJECT_VENDOR "Villainous" )
set( PROJECT_SVENDOR "viln" )
set( PROJECT_SNAME "libs" )
project( VilnLibs VERSION "0.4.18.10" )
set( CMAKE_CXX_STANDARD 20 )
set( CMAKE_CXX_STANDARD_REQUIRED ON )
set( CMAKE_EXPORT_COMPILE_COMMANDS ON )
set( CMAKE_WINDOWS_EXPORT_ALL_SYMBOLS ON )
set( CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}/cmake/" )
message(STATUS "version: ${PROJECT_VERSION}")
message(STATUS "toolchain: ${CMAKE_TOOLCHAIN_FILE}")
include( GNUInstallDirs )

# dependencies
set( GTEST_VER "1.8" )
set( DOXYGEN_VER "1.8" )
find_package( GTest ${GTEST_VER} REQUIRED )
find_package( Doxygen ${DOXYGEN_VER} )

# collections
add_library( VilnStruct "" )
add_library( VILN::Struct ALIAS VilnStruct )
set_target_properties( VilnStruct PROPERTIES OUTPUT_NAME viln_struct )
set_target_properties( VilnStruct PROPERTIES VERSION ${PROJECT_VERSION} )

# tests
enable_testing()
add_executable( VilnTests "" )
target_link_libraries(
  VilnTests
  PRIVATE
    VilnStruct GTest::GTest GTest::Main )


# documentation
if( MSVC )
  set( DOCS_TARGET DOCS )
else()
  set( DOCS_TARGET docs )
endif()
if( DOXYGEN_FOUND )
  add_custom_target(
    ${DOCS_TARGET}
    COMMAND "${DOXYGEN_EXECUTABLE}" "etc/Doxyfile"
    WORKING_DIRECTORY "${CMAKE_BINARY_DIR}"
    COMMENT "Generating documentation using Doxygen."
    VERBATIM )
endif()


# subdirectories
add_subdirectory( "etc" )
add_subdirectory( "src" )
file( GLOB_RECURSE ALL_SOURCES "src/*.cpp" "src/*.hpp" "src/*.inl" )
source_group(TREE "${CMAKE_SOURCE_DIR}/src" FILES ${ALL_SOURCES} )
add_subdirectory( "test" )
file( GLOB_RECURSE ALL_TESTS "test/*.cpp" "test/*.hpp" "test/*.inl" )
source_group(TREE "${CMAKE_SOURCE_DIR}/test" FILES ${ALL_TESTS} )


# install
install(
  TARGETS VilnStruct
  EXPORT VilnStructTargets
  LIBRARY
    DESTINATION "${CMAKE_INSTALL_LIBDIR}"
    COMPONENT runtime
  ARCHIVE
    DESTINATION "${CMAKE_INSTALL_LIBDIR}"
    COMPONENT devel
  RUNTIME
    DESTINATION "${CMAKE_INSTALL_BINDIR}"
    COMPONENT runtime
  PUBLIC_HEADER
    DESTINATION "${CMAKE_INSTALL_INCLUDEDIR}/viln"
    COMPONENT devel )
install(
  EXPORT VilnStructTargets
    NAMESPACE VILN::
    DESTINATION "${CMAKE_INSTALL_DATADIR}/vilnlibs"
    COMPONENT devel )
install(
  DIRECTORY "${CMAKE_BINARY_DIR}/etc/"
  DESTINATION "${CMAKE_INSTALL_DATADIR}/vilnlibs"
  COMPONENT devel
  FILES_MATCHING PATTERN "*onfig*.cmake" )
install(
  FILES "src/VilnStruct.md"
  TYPE DOC
  COMPONENT devel )
install(
  DIRECTORY "${CMAKE_BINARY_DIR}/html"
  TYPE DOC
  COMPONENT devel
  OPTIONAL )
install(
  FILES "LICENSE.md" "README.md"
  TYPE DOC
  COMPONENT runtime )


# package
if( NOT DEFINED CPACK_GENERATOR )
  set( CPACK_GENERATOR "TGZ" )
endif()
set( CPACK_GENERATOR ${CPACK_GENERATOR} CACHE STRING "project packaging type" )
set( CPACK_PACKAGE_NAME ${PROJECT_NAME} )
set( CPACK_PACKAGE_VERSION ${PROJECT_VERSION} )
set( CPACK_PACKAGE_CONTACT "Timothy Aitken" )
set( CPACK_PACKAGE_VENDOR ${PROJECT_VENDOR} )
set( CPACK_PACKAGE_HOMEPAGE_URL "https://gitlab.com/villainous/struct" )
set( CPACK_RESOURCE_FILE_LICENSE "${PROJECT_SOURCE_DIR}/LICENSE.md" )
set( CPACK_NSIS_MENU_LINKS "bin/${PROJECT_SNAME}.exe" "VilnLibs" )
set( CPACK_DEBIAN_PACKAGE_SECTION "libs" )
set( CPACK_DEBIAN_DEVEL_PACKAGE_SECTION "devel" )
set(
  CPACK_PACKAGE_FILE_NAME
  "${PROJECT_NAME}-${PROJECT_VERSION}-${CMAKE_SYSTEM_PROCESSOR}" )
  set(
    CPACK_PACKAGE_DEVEL_FILE_NAME
    "${PROJECT_NAME}-devel-${PROJECT_VERSION}-${CMAKE_SYSTEM_PROCESSOR}" )
set( CPACK_RPM_COMPONENT_INSTALL ON )
set( CPACK_RPM_RUNTIME_FILE_NAME "${CPACK_PACKAGE_FILE_NAME}.rpm" )
set( CPACK_RPM_DEVEL_FILE_NAME "${CPACK_PACKAGE_DEVEL_FILE_NAME}.rpm" )
set( CPACK_DEB_COMPONENT_INSTALL ON )
set( CPACK_DEB_RUNTIME_FILE_NAME "${CPACK_PACKAGE_FILE_NAME}.deb" )
set( CPACK_DEB_DEVEL_FILE_NAME "${CPACK_PACKAGE_DEVEL_FILE_NAME}.deb" )
set( CPACK_ARCHIVE_COMPONENT_INSTALL ON )
set( CPACK_PACKAGE_INSTALL_DIRECTORY "${PROJECT_SNAME}" )
include( CPack )
