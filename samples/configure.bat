@echo off
cls

set VCPKG_DEFAULT_TRIPLET=x64-windows
vcpkg version
vcpkg install vilnlibs
if errorlevel 1 (
  echo ERROR: vcpkg dependency installation failure
  pause
  exit
)
for %%g in ("vcpkg.exe") do @set "VCPKG_PATH=%%~$PATH:g"
for %%A in ("%VCPKG_PATH%") do set VCPKG_DIR=%%~dpA

cmake --version
cmake -B _build -DCMAKE_INSTALL_PREFIX=_install -DCMAKE_TOOLCHAIN_FILE="%VCPKG_DIR%\scripts\buildsystems\vcpkg.cmake" . -Ax64
if errorlevel 1 (
  echo ERROR: cmake build failure
  pause
  exit
) else (
  cd _build
  cmake-gui ..
)
