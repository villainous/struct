#!/bin/bash

# TYPE=RELEASE CC=icx CXX=icpx CXXFLAGS="-msse4.2 -fp-model=precise" ./configure.deb.sh

: ${CC:=/usr/bin/gcc-10}
: ${CXX:=/usr/bin/g++-10}
: ${CXXFLAGS:="-msse4.2"}
: ${TRIPLET:="x64-linux"}
: ${TYPE:=DEBUG}}
export CC
export CXX
export CXXFLAGS
export VCPKG_DEFAULT_TRIPLET=${TRIPLET}
export CMAKE_BUILD_TYPE=${TYPE}

# Kills the process in case of unrecoverable errors.
function die {
  case "$-" in
  *i*) read -p "$1" ;;
  *)   echo "$1" ;;
  esac
  exit 1
}

# List the dependencies of the specified binary-executable.
function depends {
  for f in $(objdump -p "${1?}" | grep NEEDED | awk '{print $2}')
  do dpkg -S $f | awk '{split($0,a,":"); print a[1]}'
  done | sort | uniq
}

# --- MAIN

if [[ ! -x vcpkg/vcpkg ]]
then vcpkg/bootstrap-vcpkg.sh
fi
vcpkg/vcpkg version
vcpkg/vcpkg install gtest \
  || die "ERROR: vcpkg dependency installation failure"
vcpkg/vcpkg update

DEB_DEPENDS="libgcc-s1 libstdc++6 libc6"
dpkg-query --status ${DEB_DEPENDS} 1>/dev/null \
  || die "ERROR: missing system dependencies!"

cmake --version
cmake \
  -B _build \
  -G "Unix Makefiles" \
  -DBUILD_SHARED_LIBS=ON \
  -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE} \
  -DCMAKE_INSTALL_PREFIX=_install \
  -DCPACK_GENERATOR=DEB \
  -DCPACK_DEBIAN_PACKAGE_ARCHITECTURE=$(dpkg --print-architecture) \
  -DCPACK_DEBIAN_PACKAGE_DEPENDS="${DEB_DEPENDS}" \
  -DVCPKG_TARGET_TRIPLET=${VCPKG_DEFAULT_TRIPLET} \
  -DCMAKE_TOOLCHAIN_FILE="vcpkg/scripts/buildsystems/vcpkg.cmake" \
  -DCMAKE_CXX_FLAGS_DEBUG_INIT="-g -O0" \
  . \
  || die "ERROR: cmake build failure"
#  -DCMAKE_CXX_FLAGS_DEBUG_INIT="--coverage" \
