#include <utility>
#include <gtest/gtest.h>

/// default implementation is abstract
template <typename U, typename = void>
class DetectHash
{
public:
    virtual uint64_t hash(const U& u) const = 0;
};

template<
    typename T,
    typename = decltype(std::declval<T>().hash())>
using detect_hash_method = T;
/// specialized for U::hash(void) method
template<typename U>
class DetectHash<detect_hash_method<U>>
{
public:
    virtual uint64_t hash(const U& u) const
    {
        return u.hash();
    }
};

// comment this block and it compiles without issue
template<typename T, typename = void>
struct has_hash_method { typedef T notype; };
template<typename T>
struct has_hash_method<detect_hash_method<T>> {};
template<
    typename T,
    typename = typename has_hash_method<T>::notype,
    typename = decltype(hash(std::declval<T>()))>
using detect_hash_free = T;
/// specialized for ::hash(U) free-function
template<typename U>
class DetectHash<detect_hash_free<U>>
{
public:
    virtual uint64_t hash(const U& u) const
    {
        return hash(u);
    }
}; //MSVC compiler error here!

/// an example type that provides a hash method
class Bar
{
public:
    uint64_t hash(void) const
    {
        return 1;
    }
};

/// try to compile a hash-detector
TEST(Templates, Hashable)
{
    DetectHash<Bar> dhb;
}