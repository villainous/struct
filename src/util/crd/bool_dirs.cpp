
#include <string.h>
#include "bool_dirs.hpp"

using namespace viln;

// ENUM bool_4d

static const char* DIRECTION_NAMES[bool_4d::MAX + 1] =
{
  "none", // 0000
  "x",    // 0001
  "y",    // 0010
  "xy",   // 0011
  "z",    // 0100
  "xz",   // 0101
  "yz",   // 0110
  "xyz",  // 0111
  "w",    // 1000
  "xw",   // 1001
  "yw",   // 1010
  "xyw",  // 1011
  "zw",   // 1100
  "xzw",  // 1101
  "yzw",  // 1110
  "xyzw", // 1111
  nullptr // MAX
};

/**
Transforms the provided direction to a string. The string-values are the
lowercase-form of the corresponding enumeration, so `XYZ` becomes
the string "xyz". If the provided value is `MAX` or an invalid enum-value,
then the return will be `NULL`.

@param val  the direction to convert
@return  the corresponding string-representation
*/
const char* bool_dirs_enum::to_string(bool_4d val)
{
  return DIRECTION_NAMES[val];
}

/**
@see bool_dirs_enum::to_string
*/
bool_4d bool_dirs_enum::from_string(const char* name)
{
  if (!name) return bool_4d::MAX;
  for (uint8_t idx = 0; idx < bool_4d::MAX; ++idx)
  {
    if (!strcmp(name, DIRECTION_NAMES[idx])) return bool_4d(idx);
  }
  return bool_4d::MAX;
}
