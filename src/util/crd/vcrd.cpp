
#include "vcrd.inl"

using namespace viln;

template struct viln::vcrd<int64_t, 1>;
template struct viln::vcrd<int64_t, 2>;
template struct viln::vcrd<int64_t, 3>;
template struct viln::vcrd<int64_t, 4>;

template struct viln::vcrd<uint64_t, 1>;
template struct viln::vcrd<uint64_t, 2>;
template struct viln::vcrd<uint64_t, 3>;
template struct viln::vcrd<uint64_t, 4>;

template struct viln::vcrd<int32_t, 1>;
template struct viln::vcrd<int32_t, 2>;
template struct viln::vcrd<int32_t, 3>;
template struct viln::vcrd<int32_t, 4>;

template struct viln::vcrd<uint32_t, 1>;
template struct viln::vcrd<uint32_t, 2>;
template struct viln::vcrd<uint32_t, 3>;
template struct viln::vcrd<uint32_t, 4>;

template struct viln::vcrd<int16_t, 1>;
template struct viln::vcrd<int16_t, 2>;
template struct viln::vcrd<int16_t, 3>;
template struct viln::vcrd<int16_t, 4>;

template struct viln::vcrd<uint16_t, 1>;
template struct viln::vcrd<uint16_t, 2>;
template struct viln::vcrd<uint16_t, 3>;
template struct viln::vcrd<uint16_t, 4>;

template struct viln::vcrd<int8_t, 1>;
template struct viln::vcrd<int8_t, 2>;
template struct viln::vcrd<int8_t, 3>;
template struct viln::vcrd<int8_t, 4>;

template struct viln::vcrd<uint8_t, 1>;
template struct viln::vcrd<uint8_t, 2>;
template struct viln::vcrd<uint8_t, 3>;
template struct viln::vcrd<uint8_t, 4>;

template struct viln::vcrd<float, 1>;
template struct viln::vcrd<float, 2>;
template struct viln::vcrd<float, 3>;
template struct viln::vcrd<float, 4>;

template struct viln::vcrd<double, 1>;
template struct viln::vcrd<double, 2>;
template struct viln::vcrd<double, 3>;
template struct viln::vcrd<double, 4>;
