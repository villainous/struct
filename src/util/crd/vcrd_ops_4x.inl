#ifndef VILN_VCRD_OPS_4X_INL
#define VILN_VCRD_OPS_4X_INL

#include <stdint.h>
#include <stdlib.h>
#include "vcrd.hpp"
#include "vcrd_ops_4x.hpp"

#include "vmath.inl"

namespace viln
{
  // --- FUNCTIONS

  // COMPARISON

  template<typename E>
  inline bool equal(const vcrd<E, 4>& me, const vcrd<E, 4>& other)
  {
    return
      me[0] == other[0] &&
      me[1] == other[1] &&
      me[2] == other[2] &&
      me[3] == other[3];
  }

  template<typename E>
  inline bool equal(const vcrd<E, 4>& me, const E& base)
  {
    return
      me[0] == base &&
      me[1] == base &&
      me[2] == base &&
      me[3] == base;
  }

  // INCREMENT

  template<typename E>
  inline void inc(vcrd<E, 4>& me)
  {
    ++(me[0]);
    ++(me[1]);
    ++(me[2]);
    ++(me[3]);
  }

  template<typename E>
  inline void dec(vcrd<E, 4>& me)
  {
    --(me[0]);
    --(me[1]);
    --(me[2]);
    --(me[3]);
  }

  // ASSIGNMENT

  template<typename E>
  inline void assign(vcrd<E, 4>& me, const vcrd<E, 4>& other)
  {
    me[0] = other[0];
    me[1] = other[1];
    me[2] = other[2];
    me[3] = other[3];
  }

  template<typename E>
  inline void assign(vcrd<E, 4>& me, const E& base)
  {
    me[0] = base;
    me[1] = base;
    me[2] = base;
    me[3] = base;
  }

  template<typename E>
  inline void assign(vcrd<E, 4>& me, ucrd unit)
  {
    me[0] = ((unit >> 0) & 1);
    me[1] = ((unit >> 1) & 1);
    me[2] = ((unit >> 2) & 1);
    me[3] = ((unit >> 3) & 1);
  }

  template<typename E>
  inline void add_assign(vcrd<E, 4>& me, const vcrd<E, 4>& other)
  {
    me[0] += other[0];
    me[1] += other[1];
    me[2] += other[2];
    me[3] += other[3];
  }

  template<typename E>
  inline void sub_assign(vcrd<E, 4>& me, const vcrd<E, 4>& other)
  {
    me[0] -= other[0];
    me[1] -= other[1];
    me[2] -= other[2];
    me[3] -= other[3];
  }

  template<typename E>
  inline void mul_assign(vcrd<E, 4>& me, const vcrd<E, 4>& other)
  {
    me[0] *= other[0];
    me[1] *= other[1];
    me[2] *= other[2];
    me[3] *= other[3];
  }

  template<typename E>
  inline void div_assign(vcrd<E, 4>& me, const vcrd<E, 4>& other)
  {
    me[0] /= other[0];
    me[1] /= other[1];
    me[2] /= other[2];
    me[3] /= other[3];
  }

  template<typename E>
  inline void mod_assign(vcrd<E, 4>& me, const vcrd<E, 4>& other)
  {
    me[0] = mod(me[0], other[0]);
    me[1] = mod(me[1], other[1]);
    me[2] = mod(me[2], other[2]);
    me[3] = mod(me[3], other[3]);
  }

  template<typename E>
  inline void add_assign(vcrd<E, 4>& me, const E& base)
  {
    me[0] += base;
    me[1] += base;
    me[2] += base;
    me[3] += base;
  }

  template<typename E>
  inline void sub_assign(vcrd<E, 4>& me, const E& base)
  {
    me[0] -= base;
    me[1] -= base;
    me[2] -= base;
    me[3] -= base;
  }

  template<typename E>
  inline void mul_assign(vcrd<E, 4>& me, const E& factor)
  {
    me[0] *= factor;
    me[1] *= factor;
    me[2] *= factor;
    me[3] *= factor;
  }

  template<typename E>
  inline void div_assign(vcrd<E, 4>& me, const E& factor)
  {
    me[0] /= factor;
    me[1] /= factor;
    me[2] /= factor;
    me[3] /= factor;
  }

  template<typename E>
  inline void mod_assign(vcrd<E, 4>& me, const E& factor)
  {
    me[0] = mod(me[0], factor);
    me[1] = mod(me[1], factor);
    me[2] = mod(me[2], factor);
    me[3] = mod(me[3], factor);
  }

  template<typename E>
  void add_assign(vcrd<E, 4>& me, ucrd unit)
  {
    me[0] += (unit >> 0) & 1;
    me[1] += (unit >> 1) & 1;
    me[2] += (unit >> 2) & 1;
    me[3] += (unit >> 3) & 1;
  }

  template<typename E>
  void sub_assign(vcrd<E, 4>& me, ucrd unit)
  {
    me[0] -= (unit >> 0) & 1;
    me[1] -= (unit >> 1) & 1;
    me[2] -= (unit >> 2) & 1;
    me[3] -= (unit >> 3) & 1;
  }

  // ARITHMETIC

  template<typename E>
  inline vcrd<E, 4> negate(const vcrd<E, 4>& me)
  {
    vcrd<E, 4> ret;
    ret[0] = -(me[0]);
    ret[1] = -(me[1]);
    ret[2] = -(me[2]);
    ret[3] = -(me[3]);
    return ret;
  }

  template<typename E>
  inline vcrd<E, 4> add(const vcrd<E, 4>& me, const vcrd<E, 4>& other)
  {
    vcrd<E, 4> ret;
    ret[0] = me[0] + other[0];
    ret[1] = me[1] + other[1];
    ret[2] = me[2] + other[2];
    ret[3] = me[3] + other[3];
    return ret;
  }

  template<typename E>
  inline vcrd<E, 4> sub(const vcrd<E, 4>& me, const vcrd<E, 4>& other)
  {
    vcrd<E, 4> ret;
    ret[0] = me[0] - other[0];
    ret[1] = me[1] - other[1];
    ret[2] = me[2] - other[2];
    ret[3] = me[3] - other[3];
    return ret;
  }

  template<typename E>
  inline vcrd<E, 4> mul(const vcrd<E, 4>& me, const vcrd<E, 4>& other)
  {
    vcrd<E, 4> ret;
    ret[0] = me[0] * other[0];
    ret[1] = me[1] * other[1];
    ret[2] = me[2] * other[2];
    ret[3] = me[3] * other[3];
    return ret;
  }

  template<typename E>
  inline vcrd<E, 4> div(const vcrd<E, 4>& me, const vcrd<E, 4>& other)
  {
    vcrd<E, 4> ret;
    ret[0] = me[0] / other[0];
    ret[1] = me[1] / other[1];
    ret[2] = me[2] / other[2];
    ret[3] = me[3] / other[3];
    return ret;
  }

  template<typename E>
  inline vcrd<E, 4> mod(const vcrd<E, 4>& me, const vcrd<E, 4>& other)
  {
    vcrd<E, 4> ret;
    ret[0] = mod(me[0], other[0]);
    ret[1] = mod(me[1], other[1]);
    ret[2] = mod(me[2], other[2]);
    ret[3] = mod(me[3], other[3]);
    return ret;
  }

  template<typename E>
  inline vcrd<E, 4> add(const vcrd<E, 4>& me, const E& base)
  {
    vcrd<E, 4> ret;
    ret[0] = me[0] + base;
    ret[1] = me[1] + base;
    ret[2] = me[2] + base;
    ret[3] = me[3] + base;
    return ret;
  }

  template<typename E>
  inline vcrd<E, 4> sub(const vcrd<E, 4>& me, const E& base)
  {
    vcrd<E, 4> ret;
    ret[0] = me[0] - base;
    ret[1] = me[1] - base;
    ret[2] = me[2] - base;
    ret[3] = me[3] - base;
    return ret;
  }

  template<typename E>
  inline vcrd<E, 4> mul(const vcrd<E, 4>& me, const E& factor)
  {
    vcrd<E, 4> ret;
    ret[0] = me[0] * factor;
    ret[1] = me[1] * factor;
    ret[2] = me[2] * factor;
    ret[3] = me[3] * factor;
    return ret;
  }

  template<typename E>
  inline vcrd<E, 4> div(const vcrd<E, 4>& me, const E& factor)
  {
    vcrd<E, 4> ret;
    ret[0] = me[0] / factor;
    ret[1] = me[1] / factor;
    ret[2] = me[2] / factor;
    ret[3] = me[3] / factor;
    return ret;
  }

  template<typename E>
  inline vcrd<E, 4> mod(const vcrd<E, 4>& me, const E& factor)
  {
    vcrd<E, 4> ret;
    ret[0] = mod(me[0], factor);
    ret[1] = mod(me[1], factor);
    ret[2] = mod(me[2], factor);
    ret[3] = mod(me[3], factor);
    return ret;
  }

  template<typename E>
  vcrd<E, 4> add(const vcrd<E, 4>& me, ucrd unit)
  {
    vcrd<E, 4> ret;
    ret[0] = me[0] + ((unit >> 0) & 1);
    ret[1] = me[1] + ((unit >> 1) & 1);
    ret[2] = me[2] + ((unit >> 2) & 1);
    ret[3] = me[3] + ((unit >> 3) & 1);
    return ret;
  }

  template<typename E>
  vcrd<E, 4> sub(const vcrd<E, 4>& me, ucrd unit)
  {
    vcrd<E, 4> ret;
    ret[0] = me[0] - ((unit >> 0) & 1);
    ret[1] = me[1] - ((unit >> 1) & 1);
    ret[2] = me[2] - ((unit >> 2) & 1);
    ret[3] = me[3] - ((unit >> 3) & 1);
    return ret;
  }

  // FLOOR-DIV

  template<typename E>
  inline vcrd<E, 4> div_floor(const vcrd<E, 4>& me, const vcrd<E, 4>& other)
  {
    vcrd<E, 4> ret;
    ret[0] = div_floor(me[0], other[0]);
    ret[1] = div_floor(me[1], other[1]);
    ret[2] = div_floor(me[2], other[2]);
    ret[3] = div_floor(me[3], other[3]);
    return ret;
  }

  template<typename E>
  inline vcrd<E, 4> mod_floor(const vcrd<E, 4>& me, const vcrd<E, 4>& other)
  {
    vcrd<E, 4> ret;
    ret[0] = mod_floor(me[0], other[0]);
    ret[1] = mod_floor(me[1], other[1]);
    ret[2] = mod_floor(me[2], other[2]);
    ret[3] = mod_floor(me[3], other[3]);
    return ret;
  }

  template<typename E>
  inline vcrd<E, 4> div_floor(const vcrd<E, 4>& me, const E& factor)
  {
    vcrd<E, 4> ret;
    ret[0] = div_floor(me[0], factor);
    ret[1] = div_floor(me[1], factor);
    ret[2] = div_floor(me[2], factor);
    ret[3] = div_floor(me[3], factor);
    return ret;
  }

  template<typename E>
  inline vcrd<E, 4> mod_floor(const vcrd<E, 4>& me, const E& factor)
  {
    vcrd<E, 4> ret;
    ret[0] = mod_floor(me[0], factor);
    ret[1] = mod_floor(me[1], factor);
    ret[2] = mod_floor(me[2], factor);
    ret[3] = mod_floor(me[3], factor);
    return ret;
  }

  // UTILITY

  template<typename E>
  inline vcrd<E, 4> abs(const vcrd<E, 4>& crd)
  {
    vcrd<E, 4> ret;
    ret[0] = abs(crd[0]);
    ret[1] = abs(crd[1]);
    ret[2] = abs(crd[2]);
    ret[3] = abs(crd[3]);
    return ret;
  }

  template<typename E>
  inline vcrd<E, 4> sgn(const vcrd<E, 4>& crd)
  {
    vcrd<E, 4> ret;
    ret[0] = sgn(crd[0]);
    ret[1] = sgn(crd[1]);
    ret[2] = sgn(crd[2]);
    ret[3] = sgn(crd[3]);
    return ret;
  }

  template<typename E>
  inline vcrd<E, 4> min(const vcrd<E, 4>& crd1, const vcrd<E, 4>& crd2)
  {
    vcrd<E, 4> ret;
    ret[0] = min(crd1[0], crd2[0]);
    ret[1] = min(crd1[1], crd2[1]);
    ret[2] = min(crd1[2], crd2[2]);
    ret[3] = min(crd1[3], crd2[3]);
    return ret;
  }

  template<typename E>
  inline vcrd<E, 4> max(const vcrd<E, 4>& crd1, const vcrd<E, 4>& crd2)
  {
    vcrd<E, 4> ret;
    ret[0] = max(crd1[0], crd2[0]);
    ret[1] = max(crd1[1], crd2[1]);
    ret[2] = max(crd1[2], crd2[2]);
    ret[3] = max(crd1[3], crd2[3]);
    return ret;
  }

  template<typename E>
  inline vcrd<E, 4> min(const vcrd<E, 4>& crd, const E& elem)
  {
    vcrd<E, 4> ret;
    ret[0] = min(crd[0], elem);
    ret[1] = min(crd[1], elem);
    ret[2] = min(crd[2], elem);
    ret[3] = min(crd[3], elem);
    return ret;
  }

  template<typename E>
  inline vcrd<E, 4> max(const vcrd<E, 4>& crd, const E& elem)
  {
    vcrd<E, 4> ret;
    ret[0] = max(crd[0], elem);
    ret[1] = max(crd[1], elem);
    ret[2] = max(crd[2], elem);
    ret[3] = max(crd[3], elem);
    return ret;
  }

  // VECTOR

  /// The area/volume/hypervolume of the coordinate.
  template<typename E>
  inline E volume(const vcrd<E, 4>& crd)
  {
    return abs(crd[0] * crd[1] * crd[2] * crd[3]);
  }

  template<typename E>
  inline E dot(const vcrd<E, 4>& a, const vcrd<E, 4>& b)
  {
    return
      (a[0] * b[0]) +
      (a[1] * b[1]) +
      (a[2] * b[2]) +
      (a[3] * b[3]);
  }

  template<typename E>
  inline E length_max(const vcrd<E, 4>& crd)
  {
    vcrd<E, 4> absv = abs(crd);
    return max(max(absv[0], absv[1]), max(absv[2], absv[3]));
  }

  /// The (Manhattan) 1-norm for this coordinate.
  template<typename E>
  inline E length1(const vcrd<E, 4>& crd)
  {
    vcrd<E, 4> absv = abs(crd);
    return absv[0] + absv[1] + absv[2] + absv[3];
  }

  template<typename E>
  E min(const vcrd<E, 4>& me)
  {
    return min(
      min(me[0], me[1]),
      min(me[2], me[3]));
  }

  template<typename E>
  E max(const vcrd<E, 4>& me)
  {
    return max(
      max(me[0], me[1]),
      max(me[2], me[3]));
  }

  // MISC

  /// Generic hash-code for integer-coordinate types.
  template<typename E>
  inline uint64_t hash(const vcrd<E, 4>& crd)
  {
    constexpr int HVSHIFT = 64 / (4 + 1);
    uint64_t hh = hash(crd[0]);
    hh <<= HVSHIFT;
    hh ^= hash(crd[1]);
    hh <<= HVSHIFT;
    hh ^= hash(crd[2]);
    hh <<= HVSHIFT;
    hh ^= hash(crd[3]);
    return hh;
  }
}

#endif
