#ifndef VILN_VCRD_OPS_HPP
#define VILN_VCRD_OPS_HPP

#include "bool_dirs.hpp"

namespace viln
{
  // forward-declare
  template <typename E, int8_t D> struct vcrd;
  template <typename E> struct vcrd_init;
  typedef bool_4d ucrd;

  // --- FUNCTIONS

  // COMPILE
  template <typename E, int8_t D>
  constexpr size_t vcrd_align(void) { return alignof(E); };
  
  // CREATION
  /// Make a coord with elements set to the given value.
  template<typename E, int8_t D>
  inline vcrd<E, D> make_vcrd(const E& e);
  /// Make a coord of unspecified length with elements set to the given value.
  template<typename E>
  inline vcrd_init<E> make_vcrd(const E& e);

  // COMPARISON
  /// True iff every element of the two coords are equal.
  template<typename E, int8_t D>
  bool equal(const vcrd<E, D>& me, const vcrd<E, D>& other);
  /// True iff every element of the coord matches the given element.
  template<typename E, int8_t D>
  bool equal(const vcrd<E, D>& me, const E& base);

  // INCREMENT
  /// Increment each element of the input.
  template<typename E, int8_t D>
  void inc(vcrd<E, D>& me);
  /// Decrement each element of the input.
  template<typename E, int8_t D>
  void dec(vcrd<E, D>& me);

  // ASSIGNMENT
  /// Set each element of the coord to match another.
  template<typename E, int8_t D>
  void assign(vcrd<E, D>& me, const vcrd<E, D>& other);
  /// Set each element of the coord to match the given element.
  template<typename E, int8_t D>
  void assign(vcrd<E, D>& me, const E& base);
  /// Set the coord to match the specified unit-coord.
  template<typename E, int8_t D>
  void assign(vcrd<E, D>& me, ucrd unit);
  /// Per-element sum of two coords.
  template<typename E, int8_t D>
  void add_assign(vcrd<E, D>& me, const vcrd<E, D>& other);
  /// Per-element subtraction of the second element from the first.
  template<typename E, int8_t D>
  void sub_assign(vcrd<E, D>& me, const vcrd<E, D>& other);
  /// Per-element multiplication of two coords.
  template<typename E, int8_t D>
  void mul_assign(vcrd<E, D>& me, const vcrd<E, D>& other);
  /// Per-element division of the first input by the second.
  template<typename E, int8_t D>
  void div_assign(vcrd<E, D>& me, const vcrd<E, D>& other);
  /// Per-element modulus of the first input by the second.
  template<typename E, int8_t D>
  void mod_assign(vcrd<E, D>& me, const vcrd<E, D>& other);
  /// Add a scalar to the input coord.
  template<typename E, int8_t D>
  void add_assign(vcrd<E, D>& me, const E& base);
  /// Subtract a scalar from the input coord.
  template<typename E, int8_t D>
  void sub_assign(vcrd<E, D>& me, const E& base);
  /// Multiply the input coord by a scalar.
  template<typename E, int8_t D>
  void mul_assign(vcrd<E, D>& me, const E& factor);
  /// Divide the input coord by a scalar.
  template<typename E, int8_t D>
  void div_assign(vcrd<E, D>& me, const E& factor);
  /// Modulate the input coord by a scalar.
  template<typename E, int8_t D>
  void mod_assign(vcrd<E, D>& me, const E& factor);
  /// Add the provided unit-coord(s) from the given coord.
  template<typename E, int8_t D>
  void add_assign(vcrd<E, D>& me, ucrd unit);
  /// Subtract the provided unit-coord(s) from the given coord.
  template<typename E, int8_t D>
  void sub_assign(vcrd<E, D>& me, ucrd unit);

  // ARITHMETIC
  /// The negative-value of each element.
  template<typename E, int8_t D>
  vcrd<E, D> negate(const vcrd<E, D>& me);
  /// Per-element sum of two coords.
  template<typename E, int8_t D>
  vcrd<E, D> add(const vcrd<E, D>& me, const vcrd<E, D>& other);
  /// Per-element subtraction of the second element from the first.
  template<typename E, int8_t D>
  vcrd<E, D> sub(const vcrd<E, D>& me, const vcrd<E, D>& other);
  /// Per-element multiplication of two coords.
  template<typename E, int8_t D>
  vcrd<E, D> mul(const vcrd<E, D>& me, const vcrd<E, D>& other);
  /// Per-element division of the first input by the second.
  template<typename E, int8_t D>
  vcrd<E, D> div(const vcrd<E, D>& me, const vcrd<E, D>& other);
  /// Per-element modulus of the first input by the second.
  template<typename E, int8_t D>
  vcrd<E, D> mod(const vcrd<E, D>& me, const vcrd<E, D>& other);
  /// Add a scalar to the input coord.
  template<typename E, int8_t D>
  vcrd<E, D> add(const vcrd<E, D>& me, const E& base);
  /// Subtract a scalar from the input coord.
  template<typename E, int8_t D>
  vcrd<E, D> sub(const vcrd<E, D>& me, const E& base);
  /// Multiply the input coord by a scalar.
  template<typename E, int8_t D>
  vcrd<E, D> mul(const vcrd<E, D>& me, const E& factor);
  /// Divide the input coord by a scalar.
  template<typename E, int8_t D>
  vcrd<E, D> div(const vcrd<E, D>& me, const E& factor);
  /// Modulate the input coord by a scalar.
  template<typename E, int8_t D>
  vcrd<E, D> mod(const vcrd<E, D>& me, const E& factor);
  /// Add the provided unit-coord(s) from the given coord.
  template<typename E, int8_t D>
  vcrd<E, D> add(const vcrd<E, D>& me, ucrd unit);
  /// Subtract the provided unit-coord(s) from the given coord.
  template<typename E, int8_t D>
  vcrd<E, D> sub(const vcrd<E, D>& me, ucrd unit);

  // FLOOR-DIV
  /// Per-element division of two coord, with rounding to -infinity.
  template<typename E, int8_t D>
  vcrd<E, D> div_floor(const vcrd<E, D>& me, const vcrd<E, D>& other);
  /// Per-element modulus of two coord, with rounding to -infinity.
  template<typename E, int8_t D>
  vcrd<E, D> mod_floor(const vcrd<E, D>& me, const vcrd<E, D>& other);
  /// Divide each element of a coord by a value, rounding to -infinity.
  template<typename E, int8_t D>
  vcrd<E, D> div_floor(const vcrd<E, D>& me, const E& factor);
  /// Modulate each element of a coord by a value, rounding to -infinity.
  template<typename E, int8_t D>
  vcrd<E, D> mod_floor(const vcrd<E, D>& me, const E& factor);

  // CEILING-DIV
  /// Per-element division of two coord, with rounding to +infinity.
  template<typename E, int8_t D>
  vcrd<E, D> div_ceil(const vcrd<E, D>& me, const vcrd<E, D>& other);
  /// Per-element modulus of two coord, with rounding to +infinity.
  template<typename E, int8_t D>
  vcrd<E, D> mod_ceil(const vcrd<E, D>& me, const vcrd<E, D>& other);
  /// Divide each element of a coord by a value, rounding to +infinity.
  template<typename E, int8_t D>
  vcrd<E, D> div_ceil(const vcrd<E, D>& me, const E& factor);
  /// Modulate each element of a coord by a value, rounding to +infinity.
  template<typename E, int8_t D>
  vcrd<E, D> mod_ceil(const vcrd<E, D>& me, const E& factor);

  // UTILITY
  /// The absolute-value of each element of a coord.
  template<typename E, int8_t D>
  vcrd<E, D> abs(const vcrd<E, D>& crd);
  /// The per-element sign (-1,+1,0) of the inputs.
  template<typename E, int8_t D>
  vcrd<E, D> sgn(const vcrd<E, D>& crd);
  /// Take the per-element minimum of two coords.
  template<typename E, int8_t D>
  vcrd<E, D> min(const vcrd<E, D>& crd1, const vcrd<E, D>& crd2);
  /// Take the per-element maximum of two coords.
  template<typename E, int8_t D>
  vcrd<E, D> max(const vcrd<E, D>& crd1, const vcrd<E, D>& crd2);
  /// Take the per-element minimum of this coord and some element.
  template<typename E, int8_t D>
  vcrd<E, D> min(const vcrd<E, D>& crd, const E& elem);
  /// Take the per-element maximum of two coords.
  template<typename E, int8_t D>
  vcrd<E, D> max(const vcrd<E, D>& crd, const E& elem);

  // VECTOR
  /// The area/volume/hypervolume of a rectangle of the given size.
  template<typename E, int8_t D>
  E volume(const vcrd<E, D>& crd);
  /// The vector dot-product of the two coords.
  template<typename E, int8_t D>
  E dot(const vcrd<E, D>& a, const vcrd<E, D>& b);
  /// The L-infinity vector-norm (maximum absolute value of elements).
  template<typename E, int8_t D>
  E length_max(const vcrd<E, D>& crd);
  /// The L1 vector-norm (Manhattan distance).
  template<typename E, int8_t D>
  E length1(const vcrd<E, D>& crd);
  /// An approximation to the L2 norm using L1 and L-infinity.
  template<typename E, int8_t D>
  E length_oct(const vcrd<E, D>& crd);
  /// The standard L2 vector-norm (Euclidian).
  template<typename E, int8_t D>
  E length(const vcrd<E, D>& crd);
  /// The minimum value amongst the elements.
  template<typename E, int8_t D>
  E min(const vcrd<E, D>& me);
  /// The maximum value amongst the elements.
  template<typename E, int8_t D>
  E max(const vcrd<E, D>& me);

  // MISC
  /// Compute a simple hash-code for the provided coord.
  template<typename E, int8_t D>
  uint64_t hash(const vcrd<E, D>& crd);
  /// Lexographic ordering of the coord elements.
  template <typename E, int8_t D>
  bool order(const vcrd<E,D>& a, const vcrd<E,D>& b);
  /// Structured-binding "get" operation.
  template<size_t N, typename E, int8_t D>
  E& get(viln::vcrd<E, D>& crd);
  /// Structured-binding "get" operation.
  template<size_t N, typename E, int8_t D>
  const E& get(const viln::vcrd<E, D>& crd);
}

#endif
