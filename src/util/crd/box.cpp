#include "box.inl"

using namespace viln;

template class viln::Box<int32_t, 1>;
template class viln::Box<int32_t, 2>;
template class viln::Box<int32_t, 3>;
