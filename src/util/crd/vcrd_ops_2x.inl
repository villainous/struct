#ifndef VILN_VCRD_OPS_2X_INL
#define VILN_VCRD_OPS_2X_INL

#include <stdint.h>
#include <stdlib.h>
#include "vcrd.hpp"
#include "vcrd_ops_2x.hpp"

#include "vmath.inl"

namespace viln
{
  // --- FUNCTIONS

  // COMPARISON

  template<typename E>
  inline bool equal(const vcrd<E, 2>& me, const vcrd<E, 2>& other)
  {
    return
      me[0] == other[0] &&
      me[1] == other[1];
  }

  template<typename E>
  inline bool equal(const vcrd<E, 2>& me, const E& base)
  {
    return
      me[0] == base &&
      me[1] == base;
  }

  // INCREMENT

  template<typename E>
  inline void inc(vcrd<E, 2>& me)
  {
    ++(me[0]);
    ++(me[1]);
  }

  template<typename E>
  inline void dec(vcrd<E, 2>& me)
  {
    --(me[0]);
    --(me[1]);
  }

  // ASSIGNMENT

  template<typename E>
  inline void assign(vcrd<E, 2>& me, const vcrd<E, 2>& other)
  {
    me[0] = other[0];
    me[1] = other[1];
  }

  template<typename E>
  inline void assign(vcrd<E, 2>& me, const E& base)
  {
    me[0] = base;
    me[1] = base;
  }

  template<typename E>
  inline void assign(vcrd<E, 2>& me, ucrd unit)
  {
    me[0] = ((unit >> 0) & 1);
    me[1] = ((unit >> 1) & 1);
  }

  template<typename E>
  inline void add_assign(vcrd<E, 2>& me, const vcrd<E, 2>& other)
  {
    me[0] += other[0];
    me[1] += other[1];
  }

  template<typename E>
  inline void sub_assign(vcrd<E, 2>& me, const vcrd<E, 2>& other)
  {
    me[0] -= other[0];
    me[1] -= other[1];
  }

  template<typename E>
  inline void mul_assign(vcrd<E, 2>& me, const vcrd<E, 2>& other)
  {
    me[0] *= other[0];
    me[1] *= other[1];
  }

  template<typename E>
  inline void div_assign(vcrd<E, 2>& me, const vcrd<E, 2>& other)
  {
    me[0] /= other[0];
    me[1] /= other[1];
  }

  template<typename E>
  inline void mod_assign(vcrd<E, 2>& me, const vcrd<E, 2>& other)
  {
    me[0] = mod(me[0], other[0]);
    me[1] = mod(me[1], other[1]);
  }

  template<typename E>
  inline void add_assign(vcrd<E, 2>& me, const E& base)
  {
    me[0] += base;
    me[1] += base;
  }

  template<typename E>
  inline void sub_assign(vcrd<E, 2>& me, const E& base)
  {
    me[0] -= base;
    me[1] -= base;
  }

  template<typename E>
  inline void mul_assign(vcrd<E, 2>& me, const E& factor)
  {
    me[0] *= factor;
    me[1] *= factor;
  }

  template<typename E>
  inline void div_assign(vcrd<E, 2>& me, const E& factor)
  {
    me[0] /= factor;
    me[1] /= factor;
  }

  template<typename E>
  inline void mod_assign(vcrd<E, 2>& me, const E& factor)
  {
    me[0] = mod(me[0], factor);
    me[1] = mod(me[1], factor);
  }

  template<typename E>
  void add_assign(vcrd<E, 2>& me, ucrd unit)
  {
    me[0] += (unit >> 0) & 1;
    me[1] += (unit >> 1) & 1;
  }

  template<typename E>
  void sub_assign(vcrd<E, 2>& me, ucrd unit)
  {
    me[0] -= (unit >> 0) & 1;
    me[1] -= (unit >> 1) & 1;
  }

  // ARITHMETIC

  template<typename E>
  inline vcrd<E, 2> negate(const vcrd<E, 2>& me)
  {
    vcrd<E, 2> ret;
    ret[0] = -(me[0]);
    ret[1] = -(me[1]);
    return ret;
  }

  template<typename E>
  inline vcrd<E, 2> add(const vcrd<E, 2>& me, const vcrd<E, 2>& other)
  {
    vcrd<E, 2> ret;
    ret[0] = me[0] + other[0];
    ret[1] = me[1] + other[1];
    return ret;
  }

  template<typename E>
  inline vcrd<E, 2> sub(const vcrd<E, 2>& me, const vcrd<E, 2>& other)
  {
    vcrd<E, 2> ret;
    ret[0] = me[0] - other[0];
    ret[1] = me[1] - other[1];
    return ret;
  }

  template<typename E>
  inline vcrd<E, 2> mul(const vcrd<E, 2>& me, const vcrd<E, 2>& other)
  {
    vcrd<E, 2> ret;
    ret[0] = me[0] * other[0];
    ret[1] = me[1] * other[1];
    return ret;
  }

  template<typename E>
  inline vcrd<E, 2> div(const vcrd<E, 2>& me, const vcrd<E, 2>& other)
  {
    vcrd<E, 2> ret;
    ret[0] = me[0] / other[0];
    ret[1] = me[1] / other[1];
    return ret;
  }

  template<typename E>
  inline vcrd<E, 2> mod(const vcrd<E, 2>& me, const vcrd<E, 2>& other)
  {
    vcrd<E, 2> ret;
    ret[0] = mod(me[0], other[0]);
    ret[1] = mod(me[1], other[1]);
    return ret;
  }

  template<typename E>
  inline vcrd<E, 2> add(const vcrd<E, 2>& me, const E& base)
  {
    vcrd<E, 2> ret;
    ret[0] = me[0] + base;
    ret[1] = me[1] + base;
    return ret;
  }

  template<typename E>
  inline vcrd<E, 2> sub(const vcrd<E, 2>& me, const E& base)
  {
    vcrd<E, 2> ret;
    ret[0] = me[0] - base;
    ret[1] = me[1] - base;
    return ret;
  }

  template<typename E>
  inline vcrd<E, 2> mul(const vcrd<E, 2>& me, const E& factor)
  {
    vcrd<E, 2> ret;
    ret[0] = me[0] * factor;
    ret[1] = me[1] * factor;
    return ret;
  }

  template<typename E>
  inline vcrd<E, 2> div(const vcrd<E, 2>& me, const E& factor)
  {
    vcrd<E, 2> ret;
    ret[0] = me[0] / factor;
    ret[1] = me[1] / factor;
    return ret;
  }

  template<typename E>
  inline vcrd<E, 2> mod(const vcrd<E, 2>& me, const E& factor)
  {
    vcrd<E, 2> ret;
    ret[0] = mod(me[0], factor);
    ret[1] = mod(me[1], factor);
    return ret;
  }

  template<typename E>
  vcrd<E, 2> add(const vcrd<E, 2>& me, ucrd unit)
  {
    vcrd<E, 2> ret;
    ret[0] = me[0] + ((unit >> 0) & 1);
    ret[1] = me[1] + ((unit >> 1) & 1);
    return ret;
  }

  template<typename E>
  vcrd<E, 2> sub(const vcrd<E, 2>& me, ucrd unit)
  {
    vcrd<E, 2> ret;
    ret[0] = me[0] - ((unit >> 0) & 1);
    ret[1] = me[1] - ((unit >> 1) & 1);
    return ret;
  }

  // FLOOR-DIV

  template<typename E>
  inline vcrd<E, 2> div_floor(const vcrd<E, 2>& me, const vcrd<E, 2>& other)
  {
    vcrd<E, 2> ret;
    ret[0] = div_floor(me[0], other[0]);
    ret[1] = div_floor(me[1], other[1]);
    return ret;
  }

  template<typename E>
  inline vcrd<E, 2> mod_floor(const vcrd<E, 2>& me, const vcrd<E, 2>& other)
  {
    vcrd<E, 2> ret;
    ret[0] = mod_floor(me[0], other[0]);
    ret[1] = mod_floor(me[1], other[1]);
    return ret;
  }

  template<typename E>
  inline vcrd<E, 2> div_floor(const vcrd<E, 2>& me, const E& factor)
  {
    vcrd<E, 2> ret;
    ret[0] = div_floor(me[0], factor);
    ret[1] = div_floor(me[1], factor);
    return ret;
  }

  template<typename E>
  inline vcrd<E, 2> mod_floor(const vcrd<E, 2>& me, const E& factor)
  {
    vcrd<E, 2> ret;
    ret[0] = mod_floor(me[0], factor);
    ret[1] = mod_floor(me[1], factor);
    return ret;
  }

  // UTILITY

  template<typename E>
  inline vcrd<E, 2> abs(const vcrd<E, 2>& crd)
  {
    vcrd<E, 2> ret;
    ret[0] = abs(crd[0]);
    ret[1] = abs(crd[1]);
    return ret;
  }

  template<typename E>
  inline vcrd<E, 2> sgn(const vcrd<E, 2>& crd)
  {
    vcrd<E, 2> ret;
    ret[0] = sgn(crd[0]);
    ret[1] = sgn(crd[1]);
    return ret;
  }

  template<typename E>
  inline vcrd<E, 2> min(const vcrd<E, 2>& crd1, const vcrd<E, 2>& crd2)
  {
    vcrd<E, 2> ret;
    ret[0] = min(crd1[0], crd2[0]);
    ret[1] = min(crd1[1], crd2[1]);
    return ret;
  }

  template<typename E>
  inline vcrd<E, 2> max(const vcrd<E, 2>& crd1, const vcrd<E, 2>& crd2)
  {
    vcrd<E, 2> ret;
    ret[0] = max(crd1[0], crd2[0]);
    ret[1] = max(crd1[1], crd2[1]);
    return ret;
  }

  template<typename E>
  inline vcrd<E, 2> min(const vcrd<E, 2>& crd, const E& elem)
  {
    vcrd<E, 2> ret;
    ret[0] = min(crd[0], elem);
    ret[1] = min(crd[1], elem);
    return ret;
  }

  template<typename E>
  inline vcrd<E, 2> max(const vcrd<E, 2>& crd, const E& elem)
  {
    vcrd<E, 2> ret;
    ret[0] = max(crd[0], elem);
    ret[1] = max(crd[1], elem);
    return ret;
  }

  // VECTOR

  /// The area/volume/hypervolume of the coordinate.
  template<typename E>
  inline E volume(const vcrd<E, 2>& crd)
  {
    return abs(crd[0] * crd[1]);
  }

  template<typename E>
  inline E dot(const vcrd<E, 2>& a, const vcrd<E, 2>& b)
  {
    return (a[0] * b[0]) + (a[1] * b[1]);
  }

  template<typename E>
  inline E length_max(const vcrd<E, 2>& crd)
  {
    return max(abs(crd[0]), abs(crd[1]));
  }

  /// The (Manhattan) 1-norm for this coordinate.
  template<typename E>
  inline E length1(const vcrd<E, 2>& crd)
  {
    return abs(crd[0]) + abs(crd[1]);
  }

  template<typename E>
  E min(const vcrd<E, 2>& me)
  {
    return min(me[0], me[1]);
  }

  template<typename E>
  E max(const vcrd<E, 2>& me)
  {
    return max(me[0], me[1]);
  }

  // MISC

  /// Generic hash-code for integer-coordinate types.
  template<typename E>
  inline uint64_t hash(const vcrd<E, 2>& crd)
  {
    constexpr int HVSHIFT = 64 / (2 + 1);
    uint64_t hh = hash(crd[0]);
    hh <<= HVSHIFT;
    hh ^= hash(crd[1]);
    return hh;
  }
}

#endif
