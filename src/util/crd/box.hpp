#ifndef VILN_BOX_HPP
#define VILN_BOX_HPP

#include "iterable.hpp"
#include "vcrd.hpp"

namespace viln
{
  // --- TYPES

  /// An N-dimensional range (generalized rectangle).
  /**
  A `Box` describes a generalized rectangular of coordinates. It is described
  by a position and size, and contains all points within that range. The range
  is `[min,max)` to provide standard clopen behavior.

  One of the main uses of this class is for iteration over an N-dimensional
  range. This can also be used in range-based iteration:
  ```
  for(vcrd v : Box(start, size))
  ```
  This greatly improves readability for the loop, and also allows for
  complex manipulations of the space being iterated over.

  Most operations complete in **O(1)** time, but iteration over the box will
  visit every element in the volume, and is thus necessarily **O(n^D)**.

  @tparam E  the numeric-type used for the ranges
  @tparam D  the dimensionality of the box
  */
  template <typename E, int8_t D>
  class Box : public ConstIterable<vcrd<E, D>>
  {
  public:
    typedef vcrd<E, D> vvcrd;

    /// Default constructor creates a box of 0 size at the origin.
    Box(void);
    /// Construct a pointlike rectangle of size 0 at the specified position.
    Box(const vvcrd& pos);
    /// Construct a rectangle with the specified position and size.
    Box(const vvcrd& pos, const vvcrd& siz);
    /// Copy constructor.
    Box(const Box& o);
    /// Convert types.
    template<typename EE>
    Box(const Box<EE, D>& o);
    /// Convert dimensions.
    template<int8_t DD>
    Box(const Box<E, DD>& o);
    /// Destructor.
    ~Box(void);

    /// Assignment operator.
    Box& operator=(const Box& o);
    /// Bounding-box enclosing this box and another.
    Box& operator|=(const Box& o);
    /// Intersection of this box and another.
    Box& operator&=(const Box& o);

    /// Find a bounding-box enclosing this box and another.
    Box operator|(const Box& o) const;
    /// Find the intersection of this box and another.
    Box operator&(const Box& o) const;

    /// equality operator
    bool operator==(const Box& o) const;
    /// inequality operator
    bool operator!=(const Box& o) const;

    /// Construct a new iterator over the elements of this object.
    virtual Iterator<const vvcrd>* iterator(void) const override;

    /// Compute the area/volume/hypervolume of the box.
    E volume(void) const;
    /// Get a point in the middle of the box.
    vvcrd midpoint(void) const;
    /// Clamp the given coordinate to fall within the box.
    vvcrd clamp(const vvcrd& crd) const;
    /// Treat the box interior as wrapped and find the coordinate within.
    vvcrd wrap(const vvcrd& crd) const;
    /// What is the distance between this box and another?
    vvcrd distance(const Box& o) const;
    /// Find a bounding-box enclosing this box and another.
    Box bounding(const Box& o) const;
    /// Find the intersection of this box and another.
    Box intersection(const Box& o) const;

    /// Determine if this box overlaps another.
    bool overlaps(const Box& o) const;
    /// Determine if this box overlaps or touches another.
    bool touches(const Box& o) const;
    /// Determine if this box completely contains another.
    bool contains(const Box& o) const;
    /// Determine if the specified point is contained in this box.
    bool contains(const vvcrd& pos) const;

    // setters
    /// Set the position of the box (size fixed).
    void setPosition(const vvcrd& pos);
    /// Move the position of the midpoint (size fixed).
    void setMidpoint(const vvcrd& pos);
    /// Set the size of the box (position fixed).
    void setSize(const vvcrd& siz);
    /// Set the position of the min-coord (max fixed).
    void setMin(const vvcrd& minn);
    /// Set the position of the max-coord (min fixed).
    void setMax(const vvcrd& maxx);
    /// Adjust the position by the given amount (size fixed).
    void moveBy(const vvcrd& amt);
    /// Grow/shrink the box in each coordinate (midpoint fixed).
    void dilate(const vvcrd& amt);
    /// Grow/shrink the box in all directions (midpoint fixed).
    void dilate(const E& amt);
    /// Reset the box if it has a negative size.
    void validate(void);

    // accessors
    /// The position of the least coordinate of the box.
    const vvcrd& position(void) const;
    /// The size of the box.
    const vvcrd& size(void) const;
    /// The minimum is equivalent to the box position.
    vvcrd min(void) const;
    /// The maximum is the position plus the box size.
    vvcrd max(void) const;

  private:
    // bounding box
    vvcrd min_;
    vvcrd size_;
  };

  typedef Box<int32_t, 1> Box1D;
  typedef Box<int32_t, 2> Box2D;
  typedef Box<int32_t, 3> Box3D;
}

#endif
