#ifndef VILN_VCRD_OPS_INL
#define VILN_VCRD_OPS_INL

#include <tuple>
#include "vcrd.hpp"
#include "vcrd_ops.hpp"

#include "vmath.inl"

namespace viln
{
  // --- FUNCTIONS

  // CREATION

  template<typename E, int8_t D>
  inline vcrd<E, D> make_vcrd(const E& e)
  {
    vcrd<E, D> ret;
    viln::assign(ret, e);
    return ret;
  }

  /// @see make_vcrd
  template<typename E>
  struct vcrd_init
  {
    E data;
    
    template<int8_t D>
    operator vcrd<E, D>(void) const
    {
      return viln::make_vcrd<E, D>(data);
    };
  };

  template<typename E>
  inline vcrd_init<E> make_vcrd(const E& e)
  {
    return vcrd_init{ e };
  }

  // COMPARISON

  template<typename E, int8_t D>
  inline bool equal(const vcrd<E, D>& me, const vcrd<E, D>& other)
  {
    for (int8_t idx = 0; idx < D; ++idx)
    {
      if (me[idx] != other[idx]) return false;
    }
    return true;
  }

  template<typename E, int8_t D>
  inline bool equal(const vcrd<E, D>& me, const E& base)
  {
    for (int8_t idx = 0; idx < D; ++idx)
    {
      if (me[idx] != base) return false;
    }
    return true;
  }

  // INCREMENT

  template<typename E, int8_t D>
  inline void inc(vcrd<E, D>& me)
  {
    for (int8_t idx = 0; idx < D; ++idx) ++(me[idx]);
  }

  template<typename E, int8_t D>
  inline void dec(vcrd<E, D>& me)
  {
    for (int8_t idx = 0; idx < D; ++idx) --(me[idx]);
  }

  // ASSIGNMENT

  template<typename E, int8_t D>
  inline void assign(vcrd<E, D>& me, const vcrd<E, D>& other)
  {
    for (int8_t idx = 0; idx < D; ++idx) me[idx] = other[idx];
  }

  template<typename E, int8_t D>
  inline void assign(vcrd<E, D>& me, const E(&other)[D])
  {
    for (int8_t idx = 0; idx < D; ++idx) me[idx] = other[idx];
  }

  template<typename E, int8_t D>
  inline void assign(vcrd<E, D>& me, const E& base)
  {
    for (int8_t idx = 0; idx < D; ++idx) me[idx] = base;
  }

  template<typename E, int8_t D>
  inline void assign(vcrd<E, D>& me, ucrd unit)
  {
    for (int8_t idx = 0; idx < D; ++idx)
    {
      me[idx] = (unit >> idx) & 1;
    }
  }

  template<typename E, int8_t D>
  inline void add_assign(vcrd<E, D>& me, const vcrd<E, D>& other)
  {
    for (int8_t idx = 0; idx < D; ++idx) me[idx] += other[idx];
  }

  template<typename E, int8_t D>
  inline void sub_assign(vcrd<E, D>& me, const vcrd<E, D>& other)
  {
    for (int8_t idx = 0; idx < D; ++idx) me[idx] -= other[idx];
  }

  template<typename E, int8_t D>
  inline void mul_assign(vcrd<E, D>& me, const vcrd<E, D>& other)
  {
    for (int8_t idx = 0; idx < D; ++idx) me[idx] *= other[idx];
  }

  template<typename E, int8_t D>
  inline void div_assign(vcrd<E, D>& me, const vcrd<E, D>& other)
  {
    for (int8_t idx = 0; idx < D; ++idx) me[idx] /= other[idx];
  }

  template<typename E, int8_t D>
  inline void mod_assign(vcrd<E, D>& me, const vcrd<E, D>& other)
  {
    for (int8_t idx = 0; idx < D; ++idx)
    {
      me[idx] = viln::mod(me[idx], other[idx]);
    }
  }

  template<typename E, int8_t D>
  inline void add_assign(vcrd<E, D>& me, const E& base)
  {
    for (int8_t idx = 0; idx < D; ++idx) me[idx] += base;
  }

  template<typename E, int8_t D>
  inline void sub_assign(vcrd<E, D>& me, const E& base)
  {
    for (int8_t idx = 0; idx < D; ++idx) me[idx] -= base;
  }

  template<typename E, int8_t D>
  inline void mul_assign(vcrd<E, D>& me, const E& factor)
  {
    for (int8_t idx = 0; idx < D; ++idx) me[idx] *= factor;
  }

  template<typename E, int8_t D>
  inline void div_assign(vcrd<E, D>& me, const E& factor)
  {
    for (int8_t idx = 0; idx < D; ++idx) me[idx] /= factor;
  }

  template<typename E, int8_t D>
  inline void mod_assign(vcrd<E, D>& me, const E& factor)
  {
    for (int8_t idx = 0; idx < D; ++idx)
    {
      me[idx] = viln::mod(me[idx], factor);
    }
  }

  template<typename E, int8_t D>
  inline void add_assign(vcrd<E, D>& me, ucrd unit)
  {
    for (int8_t idx = 0; idx < D; ++idx)
    {
      me[idx] += (unit >> idx) & 1;
    }
  }

  template<typename E, int8_t D>
  inline void sub_assign(vcrd<E, D>& me, ucrd unit)
  {
    for (int8_t idx = 0; idx < D; ++idx)
    {
      me[idx] -= (unit >> idx) & 1;
    }
  }

  // ARITHMETIC

  template<typename E, int8_t D>
  inline vcrd<E, D> negate(const vcrd<E, D>& me)
  {
    vcrd<E, D> ret;
    for (int8_t idx = 0; idx < D; ++idx) ret[idx] = -(me[idx]);
    return ret;
  }

  template<typename E, int8_t D>
  inline vcrd<E, D> add(const vcrd<E, D>& me, const vcrd<E, D>& other)
  {
    vcrd<E, D> ret;
    for (int8_t idx = 0; idx < D; ++idx) ret[idx] = me[idx] + other[idx];
    return ret;
  }

  template<typename E, int8_t D>
  inline vcrd<E, D> sub(const vcrd<E, D>& me, const vcrd<E, D>& other)
  {
    vcrd<E, D> ret;
    for (int8_t idx = 0; idx < D; ++idx) ret[idx] = me[idx] - other[idx];
    return ret;
  }

  template<typename E, int8_t D>
  inline vcrd<E, D> mul(const vcrd<E, D>& me, const vcrd<E, D>& other)
  {
    vcrd<E, D> ret;
    for (int8_t idx = 0; idx < D; ++idx) ret[idx] = me[idx] * other[idx];
    return ret;
  }

  template<typename E, int8_t D>
  inline vcrd<E, D> div(const vcrd<E, D>& me, const vcrd<E, D>& other)
  {
    vcrd<E, D> ret;
    for (int8_t idx = 0; idx < D; ++idx) ret[idx] = me[idx] / other[idx];
    return ret;
  }

  template<typename E, int8_t D>
  inline vcrd<E, D> mod(const vcrd<E, D>& me, const vcrd<E, D>& other)
  {
    vcrd<E, D> ret;
    for (int8_t idx = 0; idx < D; ++idx)
    {
      ret[idx] = viln::mod(me[idx], other[idx]);
    }
    return ret;
  }

  template<typename E, int8_t D>
  inline vcrd<E, D> add(const vcrd<E, D>& me, const E& base)
  {
    vcrd<E, D> ret;
    for (int8_t idx = 0; idx < D; ++idx) ret[idx] = me[idx] + base;
    return ret;
  }

  template<typename E, int8_t D>
  inline vcrd<E, D> sub(const vcrd<E, D>& me, const E& base)
  {
    vcrd<E, D> ret;
    for (int8_t idx = 0; idx < D; ++idx) ret[idx] = me[idx] - base;
    return ret;
  }

  template<typename E, int8_t D>
  inline vcrd<E, D> mul(const vcrd<E, D>& me, const E& factor)
  {
    vcrd<E, D> ret;
    for (int8_t idx = 0; idx < D; ++idx) ret[idx] = me[idx] * factor;
    return ret;
  }

  template<typename E, int8_t D>
  inline vcrd<E, D> div(const vcrd<E, D>& me, const E& factor)
  {
    vcrd<E, D> ret;
    for (int8_t idx = 0; idx < D; ++idx) ret[idx] = me[idx] / factor;
    return ret;
  }

  template<typename E, int8_t D>
  inline vcrd<E, D> mod(const vcrd<E, D>& me, const E& factor)
  {
    vcrd<E, D> ret;
    for (int8_t idx = 0; idx < D; ++idx)
    {
      ret[idx] = viln::mod(me[idx], factor);
    }
    return ret;
  }

  template<typename E, int8_t D>
  inline vcrd<E, D> add(const vcrd<E, D>& me, ucrd unit)
  {
    vcrd<E, D> ret;
    for (int8_t idx = 0; idx < D; ++idx)
    {
      uint8_t tmp = (unit >> idx) & 1;
      ret[idx] = me[idx] + tmp;
    }
    return ret;
  }

  template<typename E, int8_t D>
  inline vcrd<E, D> sub(const vcrd<E, D>& me, ucrd unit)
  {
    vcrd<E, D> ret;
    for (int8_t idx = 0; idx < D; ++idx)
    {
      uint8_t tmp = (unit >> idx) & 1;
      ret[idx] = me[idx] - tmp;
    }
    return ret;
  }

  // FLOOR-DIV

  template<typename E, int8_t D>
  inline vcrd<E, D> div_floor(const vcrd<E, D>& me, const vcrd<E, D>& other)
  {
    vcrd<E, D> ret;
    for (int8_t idx = 0; idx < D; ++idx)
    {
      ret[idx] = div_floor(me[idx], other[idx]);
    }
    return ret;
  }

  template<typename E, int8_t D>
  inline vcrd<E, D> mod_floor(const vcrd<E, D>& me, const vcrd<E, D>& other)
  {
    vcrd<E, D> ret;
    for (int8_t idx = 0; idx < D; ++idx)
    {
      ret[idx] = mod_floor(me[idx], other[idx]);
    }
    return ret;
  }

  template<typename E, int8_t D>
  inline vcrd<E, D> div_floor(const vcrd<E, D>& me, const E& factor)
  {
    vcrd<E, D> ret;
    for (int8_t idx = 0; idx < D; ++idx)
    {
      ret[idx] = div_floor(me[idx], factor);
    }
    return ret;
  }

  template<typename E, int8_t D>
  inline vcrd<E, D> mod_floor(const vcrd<E, D>& me, const E& factor)
  {
    vcrd<E, D> ret;
    for (int8_t idx = 0; idx < D; ++idx)
    {
      ret[idx] = mod_floor(me[idx], factor);
    }
    return ret;
  }

  // CEILING-DIV

  template<typename E, int8_t D>
  inline vcrd<E, D> div_ceil(const vcrd<E, D>& me, const vcrd<E, D>& other)
  {
    return -div_floor(-me, other);
  }

  template<typename E, int8_t D>
  inline vcrd<E, D> mod_ceil(const vcrd<E, D>& me, const vcrd<E, D>& other)
  {
    return -mod_floor(-me, other);
  }
  
  template<typename E, int8_t D>
  inline vcrd<E, D> div_ceil(const vcrd<E, D>& me, const E& factor)
  {
    return -div_floor(-me, factor);
  }

  template<typename E, int8_t D>
  inline vcrd<E, D> mod_ceil(const vcrd<E, D>& me, const E& factor)
  {
    return -mod_floor(-me, factor);
  }
  
  // UTILITY

  template<typename E, int8_t D>
  inline vcrd<E, D> abs(const vcrd<E, D>& crd)
  {
    vcrd<E, D> ret;
    for (int8_t idx = 0; idx < D; ++idx) ret[idx] = abs(crd[idx]);
    return ret;
  }

  template<typename E, int8_t D>
  inline vcrd<E, D> sgn(const vcrd<E, D>& crd)
  {
    vcrd<E, D> ret;
    for (int8_t idx = 0; idx < D; ++idx) ret[idx] = sgn(crd[idx]);
    return ret;
  }

  template<typename E, int8_t D>
  inline vcrd<E, D> min(const vcrd<E, D>& crd1, const vcrd<E, D>& crd2)
  {
    vcrd<E, D> ret;
    for (int8_t idx = 0; idx < D; ++idx) ret[idx] = min(crd1[idx], crd2[idx]);
    return ret;
  }

  template<typename E, int8_t D>
  inline vcrd<E, D> max(const vcrd<E, D>& crd1, const vcrd<E, D>& crd2)
  {
    vcrd<E, D> ret;
    for (int8_t idx = 0; idx < D; ++idx) ret[idx] = max(crd1[idx], crd2[idx]);
    return ret;
  }

  template<typename E, int8_t D>
  inline vcrd<E, D> min(const vcrd<E, D>& crd, const E& elem)
  {
    vcrd<E, D> ret;
    for (int8_t idx = 0; idx < D; ++idx) ret[idx] = min(crd[idx], elem);
    return ret;
  }

  template<typename E, int8_t D>
  inline vcrd<E, D> max(const vcrd<E, D>& crd, const E& elem)
  {
    vcrd<E, D> ret;
    for (int8_t idx = 0; idx < D; ++idx) ret[idx] = max(crd[idx], elem);
    return ret;
  }

  template<typename E, int8_t D>
  inline E min(const vcrd<E, D>& me)
  {
    E ret = me[0];
    for (int8_t idx = 1; idx < D; ++idx) ret = min(ret, me[idx]);
    return ret;
  }

  template<typename E, int8_t D>
  inline E max(const vcrd<E, D>& me)
  {
    E ret = me[0];
    for (int8_t idx = 1; idx < D; ++idx) ret = max(ret, me[idx]);
    return ret;
  }

  // VECTOR

  /// The area/volume/hypervolume of the coordinate.
  template<typename E, int8_t D>
  inline E volume(const vcrd<E, D>& crd)
  {
    E ret;
    ret = 1;
    for (int8_t idx = 0; idx < D; ++idx) ret *= abs(crd[idx]);
    return ret;
  }

  template<typename E, int8_t D>
  inline E dot(const vcrd<E, D>& a, const vcrd<E, D>& b)
  {
    E ret = E();
    for (int8_t idx = 0; idx < D; ++idx) ret += a[idx] * b[idx];
    return ret;
  }

  template<typename E, int8_t D>
  inline E length_max(const vcrd<E, D>& crd)
  {
    E ret = E();
    for (int8_t idx = 0; idx < D; ++idx) ret = max(ret, abs(crd[idx]));
    return ret;
  }

  /// The (Manhattan) 1-norm for this coordinate.
  template<typename E, int8_t D>
  inline E length1(const vcrd<E, D>& crd)
  {
    E ret = E();
    for (int8_t idx = 0; idx < D; ++idx) ret += abs(crd[idx]);
    return ret;
  }

  /// An octagonal approximation to the 2-norm.
  /**
  Generally speaking, the 1- and max-norms are much faster to calculate than
  the actual 2-norm, which requires a square-root. The following calculation
  combines the two norms together, using an approximation of sqrt(2) in order
  to get a close match to the 2-norm near the corners of the octagon.

  @see https://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.172.5622&rep=rep1&type=pdf
  */
  template<typename E, int8_t D>
  inline E length_oct(const vcrd<E, D>& crd)
  {
    // approximating sqrt(2) as 7/5
    const int32_t a = 7;
    const int32_t b = 5;
    E len1 = length1(crd);
    E lenM = length_max(crd);
    return ((b * b * 2 * lenM) + (a * b * len1)) / ((a * a) + (a * b));
  }

  /// True Euclidian norm.
  template<typename E, int8_t D>
  inline E length(const vcrd<E, D>& crd)
  {
    return E(sqrt(double(dot(crd, crd))));
  }

  // MISC

  /// Generic hash-code for integer-coordinate types.
  template<typename E, int8_t D>
  inline uint64_t hash(const vcrd<E, D>& crd)
  {
    constexpr int HVSHIFT = 64 / (D + 1);
    uint64_t hh = hash(crd[0]);
    for (int8_t idx = 1; idx < D; ++idx)
    {
      hh <<= HVSHIFT;
      hh ^= hash(crd[idx]);
    }
    return hh;
  }
  
  template <typename E, int8_t D>
  inline bool order(const vcrd<E,D>& a, const vcrd<E,D>& b)
  {
    for (int8_t idx = 0; idx < D; ++idx)
    {
      if (a[idx] < b[idx]) return true;
      if (b[idx] < a[idx]) return false;
    }
    return false;
  }

  // c++17 structured bindings
  /**
  Access the elements of the pair via structured-bindings.

  @see https://en.cppreference.com/w/cpp/language/structured_binding
  @tparam N  the index to access
  @param crd  the coordinate to access
  @return  the value at the specified index
  */
  template<size_t N, typename E, int8_t D>
  E& get(viln::vcrd<E, D>& crd)
  {
    static_assert(N < D);
    return crd[N];
  }

  /// @see get(viln::vcrd<E,D>& crd)
  template<size_t N, typename E, int8_t D>
  const E& get(const viln::vcrd<E, D>& crd)
  {
    static_assert(N < D);
    return crd[N];
  }
}

#endif
