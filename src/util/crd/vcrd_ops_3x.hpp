#ifndef VILN_VCRD_OPS_3X_HPP
#define VILN_VCRD_OPS_3X_HPP

#include "vcrd_ops.hpp"

namespace viln
{
  // --- FUNCTIONS

  // COMPARISON
  template<typename E>
  bool equal(const vcrd<E, 3>& me, const vcrd<E, 3>& other);
  template<typename E>
  bool equal(const vcrd<E, 3>& me, const E& base);

  // INCREMENT
  template<typename E>
  void inc(vcrd<E, 3>& me);
  template<typename E>
  void dec(vcrd<E, 3>& me);

  // ASSIGNMENT
  template<typename E>
  void assign(vcrd<E, 3>& me, const vcrd<E, 3>& other);
  template<typename E>
  void assign(vcrd<E, 3>& me, const E& base);
  template<typename E>
  void assign(vcrd<E, 3>& me, ucrd unit);
  template<typename E>
  void add_assign(vcrd<E, 3>& me, const vcrd<E, 3>& other);
  template<typename E>
  void sub_assign(vcrd<E, 3>& me, const vcrd<E, 3>& other);
  template<typename E>
  void mul_assign(vcrd<E, 3>& me, const vcrd<E, 3>& other);
  template<typename E>
  void div_assign(vcrd<E, 3>& me, const vcrd<E, 3>& other);
  template<typename E>
  void mod_assign(vcrd<E, 3>& me, const vcrd<E, 3>& other);
  template<typename E>
  void add_assign(vcrd<E, 3>& me, const E& base);
  template<typename E>
  void sub_assign(vcrd<E, 3>& me, const E& base);
  template<typename E>
  void mul_assign(vcrd<E, 3>& me, const E& factor);
  template<typename E>
  void div_assign(vcrd<E, 3>& me, const E& factor);
  template<typename E>
  void mod_assign(vcrd<E, 3>& me, const E& factor);
  template<typename E>
  void add_assign(vcrd<E, 3>& me, ucrd unit);
  template<typename E>
  void sub_assign(vcrd<E, 3>& me, ucrd unit);

  // ARITHMETIC
  template<typename E>
  vcrd<E, 3> negate(const vcrd<E, 3>& me);
  template<typename E>
  vcrd<E, 3> add(const vcrd<E, 3>& me, const vcrd<E, 3>& other);
  template<typename E>
  vcrd<E, 3> sub(const vcrd<E, 3>& me, const vcrd<E, 3>& other);
  template<typename E>
  vcrd<E, 3> mul(const vcrd<E, 3>& me, const vcrd<E, 3>& other);
  template<typename E>
  vcrd<E, 3> div(const vcrd<E, 3>& me, const vcrd<E, 3>& other);
  template<typename E>
  vcrd<E, 3> mod(const vcrd<E, 3>& me, const vcrd<E, 3>& other);
  template<typename E>
  vcrd<E, 3> add(const vcrd<E, 3>& me, const E& base);
  template<typename E>
  vcrd<E, 3> sub(const vcrd<E, 3>& me, const E& base);
  template<typename E>
  vcrd<E, 3> mul(const vcrd<E, 3>& me, const E& factor);
  template<typename E>
  vcrd<E, 3> div(const vcrd<E, 3>& me, const E& factor);
  template<typename E>
  vcrd<E, 3> mod(const vcrd<E, 3>& me, const E& factor);
  template<typename E>
  vcrd<E, 3> add(const vcrd<E, 3>& me, ucrd unit);
  template<typename E>
  vcrd<E, 3> sub(const vcrd<E, 3>& me, ucrd unit);

  // FLOOR-DIV
  template<typename E>
  vcrd<E, 3> div_floor(const vcrd<E, 3>& me, const vcrd<E, 3>& other);
  template<typename E>
  vcrd<E, 3> mod_floor(const vcrd<E, 3>& me, const vcrd<E, 3>& other);
  template<typename E>
  vcrd<E, 3> div_floor(const vcrd<E, 3>& me, const E& factor);
  template<typename E>
  vcrd<E, 3> mod_floor(const vcrd<E, 3>& me, const E& factor);

  // UTILITY
  template<typename E>
  vcrd<E, 3> abs(const vcrd<E, 3>& crd);
  template<typename E>
  vcrd<E, 3> sgn(const vcrd<E, 3>& crd);
  template<typename E>
  vcrd<E, 3> min(const vcrd<E, 3>& crd1, const vcrd<E, 3>& crd2);
  template<typename E>
  vcrd<E, 3> max(const vcrd<E, 3>& crd1, const vcrd<E, 3>& crd2);
  template<typename E>
  vcrd<E, 3> min(const vcrd<E, 3>& crd, const E& elem);
  template<typename E>
  vcrd<E, 3> max(const vcrd<E, 3>& crd, const E& elem);

  // VECTOR
  template<typename E>
  E volume(const vcrd<E, 3>& crd);
  template<typename E>
  E dot(const vcrd<E, 3>& a, const vcrd<E, 3>& b);
  template<typename E>
  E length_max(const vcrd<E, 3>& crd);
  template<typename E>
  E length1(const vcrd<E, 3>& crd);
  template<typename E>
  E min(const vcrd<E, 3>& me);
  template<typename E>
  E max(const vcrd<E, 3>& me);

  // MISC
  template<typename E>
  uint64_t hash(const vcrd<E, 3>& crd);
}

#endif
