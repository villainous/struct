#ifndef VILN_VCRD_OPS_4I_SIMD_HPP
#define VILN_VCRD_OPS_4I_SIMD_HPP

#include "vcrd_ops.hpp"

namespace viln
{
  typedef vcrd<int32_t, 4> vcrd_4i;

  // --- FUNCTIONS

  // COMPILE
  template <>
  constexpr size_t vcrd_align<int32_t, 4>(void) { return 16; };

  // COMPARISON
  bool equal(const vcrd_4i& me, const vcrd_4i& other);
  bool equal(const vcrd_4i& me, const int32_t& base);

  // INCREMENT
  void inc(vcrd_4i& me);
  template<typename E>
  void dec(vcrd_4i& me);

  // ASSIGNMENT
  void assign(vcrd_4i& me, const vcrd_4i& other);
  void assign(vcrd_4i& me, const int32_t& base);
  void add_assign(vcrd_4i& me, const vcrd_4i& other);
  void sub_assign(vcrd_4i& me, const vcrd_4i& other);
  void mul_assign(vcrd_4i& me, const vcrd_4i& other);
  void div_assign(vcrd_4i& me, const vcrd_4i& other);
  void mod_assign(vcrd_4i& me, const vcrd_4i& other);
  void add_assign(vcrd_4i& me, const int32_t& base);
  void sub_assign(vcrd_4i& me, const int32_t& base);
  void mul_assign(vcrd_4i& me, const int32_t& factor);
  void div_assign(vcrd_4i& me, const int32_t& factor);
  void mod_assign(vcrd_4i& me, const int32_t& factor);

  // ARITHMETIC
  vcrd_4i negate(const vcrd_4i& me);
  vcrd_4i add(const vcrd_4i& me, const vcrd_4i& other);
  vcrd_4i sub(const vcrd_4i& me, const vcrd_4i& other);
  vcrd_4i mul(const vcrd_4i& me, const vcrd_4i& other);
  vcrd_4i div(const vcrd_4i& me, const vcrd_4i& other);
  vcrd_4i mod(const vcrd_4i& me, const vcrd_4i& other);
  vcrd_4i add(const vcrd_4i& me, const int32_t& base);
  vcrd_4i sub(const vcrd_4i& me, const int32_t& base);
  vcrd_4i mul(const vcrd_4i& me, const int32_t& factor);
  vcrd_4i div(const vcrd_4i& me, const int32_t& factor);
  vcrd_4i mod(const vcrd_4i& me, const int32_t& factor);

  // FLOOR-DIV
  vcrd_4i div_floor(const vcrd_4i& me, const vcrd_4i& other);
  vcrd_4i mod_floor(const vcrd_4i& me, const vcrd_4i& other);
  vcrd_4i div_floor(const vcrd_4i& me, const int32_t& factor);
  vcrd_4i mod_floor(const vcrd_4i& me, const int32_t& factor);

  // UTILITY
  vcrd_4i abs(const vcrd_4i& crd);
  vcrd_4i sgn(const vcrd_4i& crd);
  vcrd_4i min(const vcrd_4i& crd1, const vcrd_4i& crd2);
  vcrd_4i max(const vcrd_4i& crd1, const vcrd_4i& crd2);
}

#endif
