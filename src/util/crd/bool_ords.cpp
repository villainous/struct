
#include <string.h>
#include "bool_ords.hpp"

using namespace viln;


// --- FUNCTIONS bool_card_bits

/**
Get the character for the specified cardinal direction. Converts to the
uppercase-version of the character used in the enumeration itself. The `BAD`
value (and any error-values) are converted as null.

@param val  the value to convert to a character
@return  character representing the value (null on error)
*/
char  viln::bool_card_bits::to_char(bool_6c val)
{
  switch(val)
  {
    case N:
      return 'N';
    case S:
      return 'S';
    case E:
      return 'E';
    case W:
      return 'W';
    case U:
      return 'U';
    case D:
      return 'D';
    case BAD:
      return'\0';
  }
  // unreachable?
  return'\0';
}

/**
Transform the specified character into a cardinal direction. Converts the
characters as written in the enumeration itself. Unrecognized characters
are converted as `BAD`.

*Case-insensitive.*

@param ch  the character to convert to a direction
@return  cardinal direction matching the character (`BAD` on error)
*/
bool_6c viln::bool_card_bits::from_char(char ch)
{
  switch(ch)
  {
    case 'n':
    case 'N':
      return N;
    case 's':
    case 'S':
      return S;
    case 'e':
    case 'E':
      return E;
    case 'w':
    case 'W':
      return W;
    case 'u':
    case 'U':
      return U;
    case 'd':
    case 'D':
      return D;
  }
  // default
  return BAD;
}

bool_6c viln::bool_card_bits::flip(bool_6c val, bool_3d axes)
{
  switch (val)
  {
  case N:
    return (axes & bool_3d::Y) ? S : N;
  case S:
    return (axes & bool_3d::Y) ? N : S;
  case E:
    return (axes & bool_3d::X) ? W : E;
  case W:
    return (axes & bool_3d::X) ? E : W;
  case U:
    return (axes & bool_3d::Z) ? D : U;
  case D:
    return (axes & bool_3d::Z) ? U : D;
  case BAD:
    return BAD;
  }
  // unreachable?
  return BAD;

}


// --- FUNCTIONS bool_ords_bits

/**
Print the ordinal to the specified string-buffer of the given size and
returns the number of characters printed. A terminating null-character is
appended to the end to make a valid C-string.

@param buf  the target string to fill (not `NULL`)
@param len  the size of the buffer (minimum 7)
@param val  the ordinal to convert
@return  the number of non-null characters printed (negative on error)
*/
int viln::bool_ords_bits::snprint(char* buf, size_t len, bool_6o_t val)
{
  if (len < 7) return -1;
  char* ptr = buf;
  if (val == NONE)
  {
    strncpy(buf, "none", len);
    return 4;
  }
  if (val & N) *(ptr++) = 'N';
  if (val & S) *(ptr++) = 'S';
  if (val & E) *(ptr++) = 'E';
  if (val & W) *(ptr++) = 'W';
  if (val & U) *(ptr++) = 'U';
  if (val & D) *(ptr++) = 'D';
  *ptr = '\0';
  return int(ptr - buf);
}

/**
Converts the specified string into an ordinal.

The following special strings are considered:

- "none" -> `NONE`
- "all4" -> `ALL4`
- "all6" -> `ALL6`

Otherwise, strings are converted by converting each character to
a cardinal-direction and combining the result. *This is case-insensitive and
disregards order and repitition*. Some examples:

- "EW" -> `EW`
- "Sn" -> `NS`
- "nnnnNn" -> `N`
- "Q" -> `BAD`
- ...

@see bool_card_bits::to_char(bool_6c)
@param str  the string to inspect
@param len  the maximum number of characters to inspect
@return  an ordinal with the matching directions (`MAX` if failed)
*/
bool_6o viln::bool_ords_bits::from_string(const char* str, size_t len)
{
  if (!str) return MAX;
  if (strncmp(str, "none", 4) == 0) return NONE;
  if (strncmp(str, "all6", 4) == 0) return ALL6;
  if (strncmp(str, "all4", 4) == 0) return ALL4;
  bool_6o_t ret = NONE;
  for (int idx = 0; idx < len; ++idx, ++str)
  {
    if (*str == '\0') break;
    ret |= bool_card_bits::from_char(*str);
  }
  if (ret & bool_6c::BAD) return MAX;
  return bool_6o(ret & ALL6);
}

/**
Extract the set of axes present in the ordinal.

@param ord  the ordinal to examine
@return  the directions present in the ordinal
*/
bool_3d viln::bool_ords_bits::to_axes(bool_6o_t ord)
{
  uint8_t ret = bool_3d::NONE;
  if (ord & EW) ret |= bool_3d::X;
  if (ord & NS) ret |= bool_3d::Y;
  if (ord & UD) ret |= bool_3d::Z;
  return bool_3d(ret);
}

/**
Create an ordinal containing the specified directions.

@param dir  the direction to represent
@return  the ordinal directions that would match the directions
*/
bool_6o viln::bool_ords_bits::from_axes(bool_3d dir)
{
  bool_6o_t ret = NONE;
  if (dir & bool_3d::X) ret |= EW;
  if (dir & bool_3d::Y) ret |= NS;
  if (dir & bool_3d::Z) ret |= UD;
  return bool_6o(ret);
}

/**
Flip the provided ordinal-direction. This exchanges `N/S`, `E/W`, and `U/D`
with one another.

@param val  the ordinal-direction to operate upon
@return  the appropriately flipped ordinal-direction
*/
bool_6o_t viln::bool_ords_bits::flip(bool_6o_t val)
{
  bool_6o_t neu = val & (N | E | U);
  bool_6o_t swd = val & (S | W | D);
  return (neu << 1) | (swd >> 1);
}

/**
Flip the provided ordinal-direction over the specified axes:

- `X`: `E/W`
- `Y`: `N/S`
- `Z`: `U/D`

@param val  the ordinal-direction to operate upon
@param axes  the axes to flip
@return  the appropriately flipped ordinal-direction
*/
bool_6o_t viln::bool_ords_bits::flip(bool_6o_t val, bool_3d axes)
{
  bool_6o_t ival = flip(val);
  bool_6o_t ew = ((axes & bool_3d::X) ? ival : val) & EW;
  bool_6o_t ns = ((axes & bool_3d::Y) ? ival : val) & NS;
  bool_6o_t ud = ((axes & bool_3d::Z) ? ival : val) & UD;
  return (ns | ew | ud);
}

/**
@see bool_ords_bits::flip(bool_6o_t, bool_3d)
*/
bool_6o viln::bool_ords_bits::flip(bool_6o val, bool_3d axes)
{
  bool_6o_t ret = flip(bool_6o_t(val), axes);
  return bool_6o(ret & bool_6o::ALL6);
}
