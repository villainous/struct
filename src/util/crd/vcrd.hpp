#ifndef VILN_VCRD_HPP
#define VILN_VCRD_HPP

#include <stdint.h>

#include "vcrd_ops.hpp"
#include "vcrd_ops_2x.hpp"
#include "vcrd_ops_3x.hpp"
#include "vcrd_ops_4x.hpp"
//#include "vcrd_ops_3i_simd.hpp"
//#include "vcrd_ops_4i_simd.hpp"

namespace viln
{
  // --- CLASSES

  /// Static array of integer types. Used for N-dimensional indexing.
  /**
  The `vcrd` template provides a convenient way to create and interact with
  small fixed arrays of numbers. This problem frequently arises when indexing
  multi-dimensional data. This template provides simple and efficient
  vectorized instructions targeting this use-case.

  Performance of most vector-instructions is **O(D)** unless the SIMD
  implementations are being used, which reduces many operations to **O(1)**.
  Since this is based upon a static array, element-based operations are
  always **O(1)**.

  @tparam E  the numeric-type used for the coordinate entries
  @tparam D  the dimensionality of the coordinate
  */
  template <typename E, int8_t D>
  struct alignas(vcrd_align<E,D>()) vcrd
  {
    /// Storage for elements in the array.
    E data[D];

    // access to first template param (element-type)
    typedef E elem_t;
    // access to second template param (dimension/size)
    static constexpr int8_t SIZE = D;

    // named indices
    E& x(void) requires (D > 0);
    E& y(void) requires (D > 1);
    E& z(void) requires (D > 2);
    E& w(void) requires (D > 3);
    const E& x(void) const requires (D > 0);
    const E& y(void) const requires (D > 1);
    const E& z(void) const requires (D > 2);
    const E& w(void) const requires (D > 3);

    // subscript
    E& operator[](int8_t idx);
    const E& operator[](int8_t idx) const;

    // comparison
    bool operator==(const vcrd& other) const;
    bool operator!=(const vcrd& other) const;
    bool operator==(const E& base) const;
    bool operator!=(const E& base) const;

    // conversion
    template<int8_t DD>
    operator vcrd<E, DD>(void) const;
    template<typename EE>
    operator vcrd<EE, D>(void) const;

    // assignment
    vcrd& operator=(const vcrd& other);
    vcrd& operator=(const E& base);
    vcrd& operator=(ucrd unit);
    vcrd& operator+=(const vcrd& other);
    vcrd& operator-=(const vcrd& other);
    vcrd& operator*=(const vcrd& other);
    vcrd& operator/=(const vcrd& other);
    vcrd& operator%=(const vcrd& other);
    vcrd& operator+=(const E& base);
    vcrd& operator-=(const E& base);
    vcrd& operator*=(const E& factor);
    vcrd& operator/=(const E& factor);
    vcrd& operator%=(const E& factor);
    vcrd& operator+=(ucrd unit);
    vcrd& operator-=(ucrd unit);

    // arithmetic
    vcrd operator-(void) const;
    vcrd operator+(const vcrd& other) const;
    vcrd operator-(const vcrd& other) const;
    vcrd operator*(const vcrd& other) const;
    vcrd operator/(const vcrd& other) const;
    vcrd operator%(const vcrd& other) const;
    vcrd operator+(const E& base) const;
    vcrd operator-(const E& base) const;
    vcrd operator*(const E& factor) const;
    vcrd operator/(const E& factor) const;
    vcrd operator%(const E& factor) const;
    vcrd operator+(ucrd unit) const;
    vcrd operator-(ucrd unit) const;

    // increment
    vcrd& operator++(void);
    vcrd& operator--(void);
  };

  // --- TYPES

  // long coords
  typedef vcrd<int64_t, 1> vcrd_1l;
  typedef vcrd<int64_t, 2> vcrd_2l;
  typedef vcrd<int64_t, 3> vcrd_3l;
  typedef vcrd<int64_t, 4> vcrd_4l;

  // ulong coords
  typedef vcrd<uint64_t, 1> vcrd_1ul;
  typedef vcrd<uint64_t, 2> vcrd_2ul;
  typedef vcrd<uint64_t, 3> vcrd_3ul;
  typedef vcrd<uint64_t, 4> vcrd_4ul;

  // int coords
  typedef vcrd<int32_t, 1> vcrd_1i;
  typedef vcrd<int32_t, 2> vcrd_2i;
  typedef vcrd<int32_t, 3> vcrd_3i;
  typedef vcrd<int32_t, 4> vcrd_4i;

  // uint coords
  typedef vcrd<uint32_t, 1> vcrd_1ui;
  typedef vcrd<uint32_t, 2> vcrd_2ui;
  typedef vcrd<uint32_t, 3> vcrd_3ui;
  typedef vcrd<uint32_t, 4> vcrd_4ui;

  // short coords
  typedef vcrd<int16_t, 1> vcrd_1s;
  typedef vcrd<int16_t, 2> vcrd_2s;
  typedef vcrd<int16_t, 3> vcrd_3s;
  typedef vcrd<int16_t, 4> vcrd_4s;

  // ushort coords
  typedef vcrd<uint16_t, 1> vcrd_1us;
  typedef vcrd<uint16_t, 2> vcrd_2us;
  typedef vcrd<uint16_t, 3> vcrd_3us;
  typedef vcrd<uint16_t, 4> vcrd_4us;

  // byte coords
  typedef vcrd<int8_t, 1> vcrd_1b;
  typedef vcrd<int8_t, 2> vcrd_2b;
  typedef vcrd<int8_t, 3> vcrd_3b;
  typedef vcrd<int8_t, 4> vcrd_4b;

  // ubyte coords
  typedef vcrd<uint8_t, 1> vcrd_1ub;
  typedef vcrd<uint8_t, 2> vcrd_2ub;
  typedef vcrd<uint8_t, 3> vcrd_3ub;
  typedef vcrd<uint8_t, 4> vcrd_4ub;

  // float coords
  typedef vcrd<float, 1> vcrd_1f;
  typedef vcrd<float, 2> vcrd_2f;
  typedef vcrd<float, 3> vcrd_3f;
  typedef vcrd<float, 4> vcrd_4f;

  // double coords
  typedef vcrd<double, 1> vcrd_1d;
  typedef vcrd<double, 2> vcrd_2d;
  typedef vcrd<double, 3> vcrd_3d;
  typedef vcrd<double, 4> vcrd_4d;
}

#endif
