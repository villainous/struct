#ifndef VILN_VCRD_OPS_3I_SIMD_INL
#define VILN_VCRD_OPS_3I_SIMD_INL

#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <immintrin.h>
#include "vcrd.hpp"
#include "vcrd_ops_3i_simd.hpp"

#include "vmath.inl"
#include "simd_div.inl"

namespace viln
{
  // --- FUNCTIONS

  // CONVERSION

  inline void mm_store(vcrd_3i& me, const __m128i& base)
  {
    static_assert((alignof(vcrd_3i) % alignof(__m128i)) == 0);
    static_assert(sizeof(vcrd_3i) >= sizeof(__m128i));
    _mm_store_si128((__m128i*) me.data, base);
  }

  inline __m128i mm_load(const vcrd_3i& base)
  {
    return _mm_load_si128((__m128i*) base.data);
  }


  // ARITHMETIC epi32

  inline __m128i mm_div3_epi32(const __m128i a, const __m128i b)
  {
    const __m128i bb = _mm_insert_epi32(b, 1, 3);
    return mm_div_epi32(a, bb);
  }

  inline __m128i mm_mod3_epi32(const __m128i a, const __m128i b)
  {
    const __m128i bb = _mm_insert_epi32(b, 1, 3);
    return mm_mod_epi32(a, bb);
  }

  /// Component-wise division+floor.
  inline __m128i mm_div3_floor_epi32(const __m128i a, const __m128i b)
  {
    const __m128i bb = _mm_insert_epi32(b, 1, 3);
    return mm_div_floor_epi32(a, bb);
  }

  /// Component-wise modulus+floor.
  inline __m128i mm_mod3_floor_epi32(const __m128i a, const __m128i b)
  {
    const __m128i bb = _mm_insert_epi32(b, 1, 3);
    return mm_mod_floor_epi32(a, bb);
  }


  // COMPARISON

  inline bool equal(const vcrd_3i& me, const vcrd_3i& other)
  {
    __m128i vcmp = _mm_cmpeq_epi32(mm_load(me), mm_load(other));
    uint16_t mask = _mm_movemask_epi8(vcmp);
    return ((mask | 0xf000) == 0xffff);
  }

  inline bool equal(const vcrd_3i& me, const int32_t& base)
  {
    __m128i vcmp = _mm_cmpeq_epi32(mm_load(me), _mm_set1_epi32(base));
    uint16_t mask = _mm_movemask_epi8(vcmp);
    return ((mask | 0xf000) == 0xffff);
  }


  // INCREMENT
  
  inline void inc(vcrd_3i& me)
  {
    mm_store(me, _mm_add_epi32(mm_load(me), _mm_set1_epi32(1)));
  }
  
  inline void dec(vcrd_3i& me)
  {
    mm_store(me, _mm_sub_epi32(mm_load(me), _mm_set1_epi32(1)));
  }


  // ASSIGNMENT

  inline void assign(vcrd_3i& me, const vcrd_3i& other)
  {
    mm_store(me, mm_load(other));
  }

  inline void assign(vcrd_3i& me, const int32_t& base)
  {
    mm_store(me, _mm_set1_epi32(base));
  }

  inline void add_assign(vcrd_3i& me, const vcrd_3i& other)
  {
    mm_store(me, _mm_add_epi32(mm_load(me), mm_load(other)));
  }

  inline void sub_assign(vcrd_3i& me, const vcrd_3i& other)
  {
    mm_store(me, _mm_sub_epi32(mm_load(me), mm_load(other)));
  }

  inline void mul_assign(vcrd_3i& me, const vcrd_3i& other)
  {
    mm_store(me, _mm_mullo_epi32(mm_load(me), mm_load(other)));
  }

  inline void div_assign(vcrd_3i& me, const vcrd_3i& other)
  {
    mm_store(me, mm_div3_epi32(mm_load(me), mm_load(other)));
  }

  inline void mod_assign(vcrd_3i& me, const vcrd_3i& other)
  {
    mm_store(me, mm_mod3_epi32(mm_load(me), mm_load(other)));
  }

  inline void add_assign(vcrd_3i& me, const int32_t& base)
  {
    mm_store(me, _mm_add_epi32(mm_load(me), _mm_set1_epi32(base)));
  }

  inline void sub_assign(vcrd_3i& me, const int32_t& base)
  {
    mm_store(me, _mm_sub_epi32(mm_load(me), _mm_set1_epi32(base)));
  }

  inline void mul_assign(vcrd_3i& me, const int32_t& factor)
  {
    mm_store(me, _mm_mullo_epi32(mm_load(me), _mm_set1_epi32(factor)));
  }

  inline void div_assign(vcrd_3i& me, const int32_t& factor)
  {
    mm_store(me, mm_div3_epi32(mm_load(me), _mm_set1_epi32(factor)));
  }

  inline void mod_assign(vcrd_3i& me, const int32_t& factor)
  {
    mm_store(me, mm_mod3_epi32(mm_load(me), _mm_set1_epi32(factor)));
  }


  // ARITHMETIC

  inline vcrd_3i negate(const vcrd_3i& me)
  {
    vcrd_3i ret;
    const __m128i ZERO = _mm_setzero_si128();
    mm_store(ret, _mm_sub_epi32(ZERO, mm_load(me)));
    return ret;
  }

  inline vcrd_3i add(const vcrd_3i& me, const vcrd_3i& other)
  {
    vcrd_3i ret;
    mm_store(ret, _mm_add_epi32(mm_load(me), mm_load(other)));
    return ret;
  }

  inline vcrd_3i sub(const vcrd_3i& me, const vcrd_3i& other)
  {
    vcrd_3i ret;
    mm_store(ret, _mm_sub_epi32(mm_load(me), mm_load(other)));
    return ret;
  }

  inline vcrd_3i mul(const vcrd_3i& me, const vcrd_3i& other)
  {
    vcrd_3i ret;
    mm_store(ret, _mm_mullo_epi32(mm_load(me), mm_load(other)));
    return ret;
  }

  inline vcrd_3i div(const vcrd_3i& me, const vcrd_3i& other)
  {
    vcrd_3i ret;
    mm_store(ret, mm_div3_epi32(mm_load(me), mm_load(other)));
    return ret;
  }

  inline vcrd_3i mod(const vcrd_3i& me, const vcrd_3i& other)
  {
    vcrd_3i ret;
    mm_store(ret, mm_mod3_epi32(mm_load(me), mm_load(other)));
    return ret;
  }

  inline vcrd_3i add(const vcrd_3i& me, const int32_t& base)
  {
    vcrd_3i ret;
    mm_store(ret, _mm_add_epi32(mm_load(me), _mm_set1_epi32(base)));
    return ret;
  }

  inline vcrd_3i sub(const vcrd_3i& me, const int32_t& base)
  {
    vcrd_3i ret;
    mm_store(ret, _mm_sub_epi32(mm_load(me), _mm_set1_epi32(base)));
    return ret;
  }

  inline vcrd_3i mul(const vcrd_3i& me, const int32_t& factor)
  {
    vcrd_3i ret;
    mm_store(ret, _mm_mullo_epi32(mm_load(me), _mm_set1_epi32(factor)));
    return ret;
  }

  inline vcrd_3i div(const vcrd_3i& me, const int32_t& factor)
  {
    vcrd_3i ret;
    mm_store(ret, mm_div3_epi32(mm_load(me), _mm_set1_epi32(factor)));
    return ret;
  }

  inline vcrd_3i mod(const vcrd_3i& me, const int32_t& factor)
  {
    vcrd_3i ret;
    mm_store(ret, mm_mod3_epi32(mm_load(me), _mm_set1_epi32(factor)));
    return ret;
  }


  // FLOOR-DIV

  inline vcrd_3i div_floor(const vcrd_3i& me, const vcrd_3i& other)
  {
    vcrd_3i ret;
    mm_store(ret, mm_div3_floor_epi32(mm_load(me), mm_load(other)));
    return ret;
  }

  inline vcrd_3i mod_floor(const vcrd_3i& me, const vcrd_3i& other)
  {
    vcrd_3i ret;
    mm_store(ret, mm_mod3_floor_epi32(mm_load(me), mm_load(other)));
    return ret;
  }

  inline vcrd_3i div_floor(const vcrd_3i& me, const int32_t& factor)
  {
    vcrd_3i ret;
    mm_store(ret, mm_div3_floor_epi32(mm_load(me), _mm_set1_epi32(factor)));
    return ret;
  }

  inline vcrd_3i mod_floor(const vcrd_3i& me, const int32_t& factor)
  {
    vcrd_3i ret;
    mm_store(ret, mm_mod3_floor_epi32(mm_load(me), _mm_set1_epi32(factor)));
    return ret;
  }


  // UTILITY

  inline vcrd_3i abs(const vcrd_3i& crd)
  {
    vcrd_3i ret;
    mm_store(ret, _mm_abs_epi32(mm_load(crd)));
    return ret;
  }

  inline vcrd_3i sgn(const vcrd_3i& crd)
  {
    vcrd_3i ret;
    mm_store(ret, _mm_sign_epi32(_mm_set1_epi32(1), mm_load(crd)));
    return ret;
  }

  inline vcrd_3i min(const vcrd_3i& crd1, const vcrd_3i& crd2)
  {
    vcrd_3i ret;
    mm_store(ret, _mm_min_epi32(mm_load(crd1), mm_load(crd2)));
    return ret;
  }

  inline vcrd_3i max(const vcrd_3i& crd1, const vcrd_3i& crd2)
  {
    vcrd_3i ret;
    mm_store(ret, _mm_max_epi32(mm_load(crd1), mm_load(crd2)));
    return ret;
  }
}

#endif
