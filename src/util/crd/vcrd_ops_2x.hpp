#ifndef VILN_VCRD_OPS_2X_HPP
#define VILN_VCRD_OPS_2X_HPP

#include "vcrd_ops.hpp"

namespace viln
{
  // --- FUNCTIONS

  // COMPARISON
  template<typename E>
  bool equal(const vcrd<E, 2>& me, const vcrd<E, 2>& other);
  template<typename E>
  bool equal(const vcrd<E, 2>& me, const E& base);

  // INCREMENT
  template<typename E>
  void inc(vcrd<E, 2>& me);
  template<typename E>
  void dec(vcrd<E, 2>& me);

  // ASSIGNMENT

  template<typename E>
  void assign(vcrd<E, 2>& me, const vcrd<E, 2>& other);
  template<typename E>
  void assign(vcrd<E, 2>& me, const E& base);
  template<typename E>
  void assign(vcrd<E, 2>& me, ucrd unit);
  template<typename E>
  void add_assign(vcrd<E, 2>& me, const vcrd<E, 2>& other);
  template<typename E>
  void sub_assign(vcrd<E, 2>& me, const vcrd<E, 2>& other);
  template<typename E>
  void mul_assign(vcrd<E, 2>& me, const vcrd<E, 2>& other);
  template<typename E>
  void div_assign(vcrd<E, 2>& me, const vcrd<E, 2>& other);
  template<typename E>
  void mod_assign(vcrd<E, 2>& me, const vcrd<E, 2>& other);
  template<typename E>
  void add_assign(vcrd<E, 2>& me, const E& base);
  template<typename E>
  void sub_assign(vcrd<E, 2>& me, const E& base);
  template<typename E>
  void mul_assign(vcrd<E, 2>& me, const E& factor);
  template<typename E>
  void div_assign(vcrd<E, 2>& me, const E& factor);
  template<typename E>
  void mod_assign(vcrd<E, 2>& me, const E& factor);
  template<typename E>
  void add_assign(vcrd<E, 2>& me, ucrd unit);
  template<typename E>
  void sub_assign(vcrd<E, 2>& me, ucrd unit);

  // ARITHMETIC
  template<typename E>
  vcrd<E, 2> negate(const vcrd<E, 2>& me);
  template<typename E>
  vcrd<E, 2> add(const vcrd<E, 2>& me, const vcrd<E, 2>& other);
  template<typename E>
  vcrd<E, 2> sub(const vcrd<E, 2>& me, const vcrd<E, 2>& other);
  template<typename E>
  vcrd<E, 2> mul(const vcrd<E, 2>& me, const vcrd<E, 2>& other);
  template<typename E>
  vcrd<E, 2> div(const vcrd<E, 2>& me, const vcrd<E, 2>& other);
  template<typename E>
  vcrd<E, 2> mod(const vcrd<E, 2>& me, const vcrd<E, 2>& other);
  template<typename E>
  vcrd<E, 2> add(const vcrd<E, 2>& me, const E& base);
  template<typename E>
  vcrd<E, 2> sub(const vcrd<E, 2>& me, const E& base);
  template<typename E>
  vcrd<E, 2> mul(const vcrd<E, 2>& me, const E& factor);
  template<typename E>
  vcrd<E, 2> div(const vcrd<E, 2>& me, const E& factor);
  template<typename E>
  vcrd<E, 2> mod(const vcrd<E, 2>& me, const E& factor);
  template<typename E>
  vcrd<E, 2> add(const vcrd<E, 2>& me, ucrd unit);
  template<typename E>
  vcrd<E, 2> sub(const vcrd<E, 2>& me, ucrd unit);

  // FLOOR-DIV
  template<typename E>
  vcrd<E, 2> div_floor(const vcrd<E, 2>& me, const vcrd<E, 2>& other);
  template<typename E>
  vcrd<E, 2> mod_floor(const vcrd<E, 2>& me, const vcrd<E, 2>& other);
  template<typename E>
  vcrd<E, 2> div_floor(const vcrd<E, 2>& me, const E& factor);
  template<typename E>
  vcrd<E, 2> mod_floor(const vcrd<E, 2>& me, const E& factor);

  // UTILITY
  template<typename E>
  vcrd<E, 2> abs(const vcrd<E, 2>& crd);
  template<typename E>
  vcrd<E, 2> sgn(const vcrd<E, 2>& crd);
  template<typename E>
  vcrd<E, 2> min(const vcrd<E, 2>& crd1, const vcrd<E, 2>& crd2);
  template<typename E>
  vcrd<E, 2> max(const vcrd<E, 2>& crd1, const vcrd<E, 2>& crd2);
  template<typename E>
  vcrd<E, 2> min(const vcrd<E, 2>& crd, const E& elem);
  template<typename E>
  vcrd<E, 2> max(const vcrd<E, 2>& crd, const E& elem);

  // VECTOR
  template<typename E>
  E volume(const vcrd<E, 2>& crd);
  template<typename E>
  E dot(const vcrd<E, 2>& a, const vcrd<E, 2>& b);
  template<typename E>
  E length_max(const vcrd<E, 2>& crd);
  template<typename E>
  E length1(const vcrd<E, 2>& crd);
  template<typename E>
  E min(const vcrd<E, 2>& me);
  template<typename E>
  E max(const vcrd<E, 2>& me);

  // MISC
  template<typename E>
  uint64_t hash(const vcrd<E, 2>& crd);
}

#endif
