#ifndef VILN_VCRD_OPS_3I_SIMD_HPP
#define VILN_VCRD_OPS_3I_SIMD_HPP

#include "vcrd_ops.hpp"

namespace viln
{
  typedef vcrd<int32_t, 3> vcrd_3i;

  // --- FUNCTIONS

  // COMPILE
  template <>
  constexpr size_t vcrd_align<int32_t, 3>(void) { return 16; };

  // COMPARISON
  bool equal(const vcrd_3i& me, const vcrd_3i& other);
  bool equal(const vcrd_3i& me, const int32_t& base);

  // INCREMENT
  void inc(vcrd_3i& me);
  template<typename E>
  void dec(vcrd_3i& me);

  // ASSIGNMENT
  void assign(vcrd_3i& me, const vcrd_3i& other);
  void assign(vcrd_3i& me, const int32_t& base);
  void add_assign(vcrd_3i& me, const vcrd_3i& other);
  void sub_assign(vcrd_3i& me, const vcrd_3i& other);
  void mul_assign(vcrd_3i& me, const vcrd_3i& other);
  void div_assign(vcrd_3i& me, const vcrd_3i& other);
  void mod_assign(vcrd_3i& me, const vcrd_3i& other);
  void add_assign(vcrd_3i& me, const int32_t& base);
  void sub_assign(vcrd_3i& me, const int32_t& base);
  void mul_assign(vcrd_3i& me, const int32_t& factor);
  void div_assign(vcrd_3i& me, const int32_t& factor);
  void mod_assign(vcrd_3i& me, const int32_t& factor);

  // ARITHMETIC
  vcrd_3i negate(const vcrd_3i& me);
  vcrd_3i add(const vcrd_3i& me, const vcrd_3i& other);
  vcrd_3i sub(const vcrd_3i& me, const vcrd_3i& other);
  vcrd_3i mul(const vcrd_3i& me, const vcrd_3i& other);
  vcrd_3i div(const vcrd_3i& me, const vcrd_3i& other);
  vcrd_3i mod(const vcrd_3i& me, const vcrd_3i& other);
  vcrd_3i add(const vcrd_3i& me, const int32_t& base);
  vcrd_3i sub(const vcrd_3i& me, const int32_t& base);
  vcrd_3i mul(const vcrd_3i& me, const int32_t& factor);
  vcrd_3i div(const vcrd_3i& me, const int32_t& factor);
  vcrd_3i mod(const vcrd_3i& me, const int32_t& factor);

  // FLOOR-DIV
  vcrd_3i div_floor(const vcrd_3i& me, const vcrd_3i& other);
  vcrd_3i mod_floor(const vcrd_3i& me, const vcrd_3i& other);
  vcrd_3i div_floor(const vcrd_3i& me, const int32_t& factor);
  vcrd_3i mod_floor(const vcrd_3i& me, const int32_t& factor);

  // UTILITY
  vcrd_3i abs(const vcrd_3i& crd);
  vcrd_3i sgn(const vcrd_3i& crd);
  vcrd_3i min(const vcrd_3i& crd1, const vcrd_3i& crd2);
  vcrd_3i max(const vcrd_3i& crd1, const vcrd_3i& crd2);
}

#endif
