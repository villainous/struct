#ifndef VILN_BOX_INL
#define VILN_BOX_INL

#include "box.hpp"

#include "iterable.inl"
#include "vcrd.inl"
#include "box_iterator.inl"

namespace viln
{
  // --- CLASS Box

  /**
  Default constructor. Creates a box of size 0 at the origin.
  */
  template<typename E, int8_t D>
  inline Box<E, D>::Box(void) :
    min_{},
    size_{}
  {
    // no-op
  }

  /**
  Position constructor. Creates a box of size 0 at the given position.

  @param pos  the position of the box
  */
  template<typename E, int8_t D>
  inline Box<E, D>::Box(const vvcrd& pos) :
    min_(pos),
    size_{}
  {
    // no-op
  }

  /**
  Explicit constructor. Creates a box of a specified position and size.

  @param pos  the position of the box (llc)
  @param siz  the size of the box to create
  */
  template<typename E, int8_t D>
  inline Box<E, D>::Box(const vvcrd& pos, const vvcrd& siz) :
    min_(pos),
    size_(siz)
  {
    // no-op
  }

  template<typename E, int8_t D>
  inline Box<E, D>::Box(const Box& o) :
    min_(o.min_),
    size_(o.size_)
  {
    // no-op
  }

  /**
  Convert a box to another element-type.

  @tparam EE  the type of the box coordinates
  @param o  the box to convert
  */
  template<typename E, int8_t D>
  template<typename EE>
  Box<E, D>::Box(const Box<EE, D>& o) :
    min_(o.min()),
    size_(o.size())
  {
    // noop
  }

  /**
  Convert a box to another dimension-size. If the new dimensions are
  larger, then the extra size and position elements are padded with 0s.

  @tparam DD  the dimensionality of the new box
  @param o  the box to convert
  */
  template<typename E, int8_t D>
  template<int8_t DD>
  Box<E, D>::Box(const Box<E, DD>& o)
  {
    min_ = o.min();
    size_ = o.size();
    // default size is 1 not 0
    for (int8_t idx = DD; idx < D; ++idx) size_[idx] = 1;
  }

  template<typename E, int8_t D>
  inline Box<E, D>::~Box(void)
  {
    // no-op
  }

  template<typename E, int8_t D>
  inline Box<E, D>& Box<E, D>::operator=(const Box& o)
  {
    if (&o == this) return *this;
    min_ = o.min_;
    size_ = o.size_;
    return *this;
  }

  /**
  @see bounding
  */
  template<typename E, int8_t D>
  inline Box<E, D>& Box<E, D>::operator|=(const Box& o)
  {
    vvcrd bmin = viln::min(min(), o.min());
    vvcrd bmax = viln::max(max(), o.max());
    size_ = bmax - bmin;
    min_ = bmin;
    return *this;
  }

  /**
  @see intersection
  */
  template<typename E, int8_t D>
  inline Box<E, D>& Box<E, D>::operator&=(const Box& o)
  {
    vvcrd bmin = viln::max(min(), o.min());
    vvcrd bmax = viln::min(max(), o.max());
    size_ = bmax - bmin;
    min_ = bmin;
    return *this;
  }

  /**
  @see bounding
  */
  template<typename E, int8_t D>
  inline Box<E, D> Box<E, D>::operator|(const Box& o) const
  {
    Box<E, D> ret(*this);
    return (ret |= o);
  }

  /**
  @see intersection
  */
  template<typename E, int8_t D>
  inline Box<E, D> Box<E, D>::operator&(const Box& o) const
  {
    Box<E, D> ret(*this);
    return (ret &= o);
  }

  template<typename E, int8_t D>
  inline bool Box<E, D>::operator==(const Box& o) const
  {
    return &o == this || (min_ == o.min_ && size_ == o.size_);
  }

  template<typename E, int8_t D>
  inline bool Box<E, D>::operator!=(const Box& o) const
  {
    return !(this->operator==(o));
  }

  /**
  Create an iterator over the integer-elements spanned by this box.
  The included range of elements includes `[min,max)` in each dimension.
  The caller takes ownership over the produced iterator.
  
  @return a new iterator over the box inside
  */
  template<typename E, int8_t D>
  inline Iterator<const vcrd<E, D>>* Box<E, D>::iterator(void) const
  {
    return new BoxIterator<E, D>(min_, size_);
  }

  template<typename E, int8_t D>
  inline E Box<E, D>::volume(void) const
  {
    return viln::volume(size_);
  }

  template<typename E, int8_t D>
  inline vcrd<E, D> Box<E, D>::midpoint(void) const
  {
    return min_ + (size_ / 2);
  }

  /**
  Clamp a coordinate between two bounds `[min,max)` (clopen). This clamps
  each coordinate to fall within the range of the box.

  @param crd  the value to clamp
  @return  a clamped coordinate within the box
  */
  template<typename E, int8_t D>
  inline vcrd<E, D> Box<E, D>::clamp(const vvcrd& crd) const
  {
    return viln::clamp(min(), max(), crd);
  }

  /**
  Wrap a coordinate around such that it falls inside the box via a modulus
  operation in each dimension.
  
  @param crd  the value to wrap
  @return  a wrapped coordinate within the box
  */
  template<typename E, int8_t D>
  inline vcrd<E, D> Box<E, D>::wrap(const vvcrd& crd) const
  {
    return mod_floor(crd - min_, size_) + min_;
  }

  /**
  Find the distance from this box to another in each dimension.

  @param o  the target box
  @return  the distance to the other box
  */
  template<typename E, int8_t D>
  inline vcrd<E, D> Box<E, D>::distance(const Box& o) const
  {
    vvcrd hi = o.min() - max();
    vvcrd lo = min() - o.max();
    vvcrd ret = {};
    for(int8_t idx = 0; idx < D; ++idx)
    {
      if (ret[idx] < hi[idx]) ret[idx] = hi[idx];
      if (ret[idx] < lo[idx]) ret[idx] = -lo[idx];
    }
    return ret;
  }

  /**
  Compute a bounding-box enclosing both this and another box.

  @param o  the other box to enclose
  @return  a bounding box for both boxes
  */
  template<typename E, int8_t D>
  inline Box<E, D> Box<E, D>::bounding(const Box& o) const
  {
    return (*this | o);
  }

  /**
  Compute a bounding-box from the intersection of this box and another.

  @param o  the other box to enclose
  @return  the intersection of both boxes
  */
  template<typename E, int8_t D>
  inline Box<E, D> Box<E, D>::intersection(const Box& o) const
  {
    return (*this & o);
  }

  template<typename E, int8_t D>
  inline bool Box<E, D>::overlaps(const Box& o) const
  {
    vvcrd hi = o.min() - max();
    vvcrd lo = min() - o.max();
    return viln::max(viln::max(hi, lo)) < 0;
  }

  template<typename E, int8_t D>
  inline bool Box<E, D>::touches(const Box& o) const
  {
    vvcrd hi = o.min() - max();
    vvcrd lo = min() - o.max();
    return viln::max(viln::max(hi, lo)) <= 0;
  }

  template<typename E, int8_t D>
  inline bool Box<E, D>::contains(const Box& o) const
  {
    vvcrd hi = o.max() - max();
    vvcrd lo = min() - o.min();
    return viln::max(viln::max(hi, lo)) <= 0;
  }

  template<typename E, int8_t D>
  inline bool Box<E, D>::contains(const vvcrd& pos) const
  {
    vvcrd hi = pos - max();
    vvcrd lo = min() - pos;
    return viln::max(hi) < 0 && viln::max(lo) <= 0;
  }

  /**
  Set the position of the box, but do not change the size.

  @param pos  the new position for the box (llc)
  */
  template<typename E, int8_t D>
  inline void Box<E, D>::setPosition(const vvcrd& pos)
  {
    min_ = pos;
  }

  /**
  Set the position of the box such that its midpoint is in the specified
  location, without altering the size of the box.

  @param pos  the new position for the box midpoint
  */
  template<typename E, int8_t D>
  inline void Box<E, D>::setMidpoint(const vvcrd& pos)
  {
    min_ = pos - (size_ / 2);
  }

  /**
  Add the specified vector to the box position, without altering the size
  of the box.

  @param amt  the amount to change the box position
  */
  template<typename E, int8_t D>
  inline void Box<E, D>::moveBy(const vvcrd& amt)
  {
    min_ += amt;
  }

  /**
  Change the size of the box. This operation may change the maximum (urc)
  of the box in order to keep its position (llc) constant.

  @param siz  the new size of the box
  */
  template<typename E, int8_t D>
  inline void Box<E, D>::setSize(const vvcrd& siz)
  {
    size_ = siz;
  }

  /**
  Set the minimum (llc) position of the box. This operation may change the size
  of the box in order to keep its maximum (urc) constant.

  @param minn  the new position (llc) for the box
  */
  template<typename E, int8_t D>
  inline void Box<E, D>::setMin(const vvcrd& minn)
  {
    vvcrd maxx = min_ + size_;
    min_ = minn;
    size_ = maxx - min_;
  }

  /**
  Set the maximum (urc) position of the box. This operation may change the size
  of the box in order to keep its minimum (llc) constant.

  @param maxx  the new position (urc) for the box
  */
  template<typename E, int8_t D>
  inline void Box<E, D>::setMax(const vvcrd& maxx)
  {
    size_ = maxx - min_;
  }

  /**
  Expand (dilate) the box by the specified amount on all 4 sides. This
  operation keeps the midpoint constant, but may alter the position and size.
  This version allows dilation of the *x* and *y* coordinates separately.

  @param amt  the amount of expansion on each side
  */
  template<typename E, int8_t D>
  inline void Box<E, D>::dilate(const vvcrd& amt)
  {
    min_ -= amt;
    size_ += amt * 2;
  }

  /**
  Expand (dilate) the box by the specified amount on all 4 sides. This
  operation keeps the midpoint constant, but may alter the position and size.

  @param amt  the amount of expansion on each side
  */
  template<typename E, int8_t D>
  inline void Box<E, D>::dilate(const E& amt)
  {
    min_ -= amt;
    size_ += amt * 2;
  }

  /**
  Check the box for validity, and reset to default 0-size box if needed.
  A box is considered invalid if any of the size coordinates are negative.
  
  Note that the box does not automatically validate its inputs, as boxes of
  negative size may be useful in certain scenarios. It is left to the
  discretion of the caller to determine if and when a box should be forced
  to be valid.
  */
  template<typename E, int8_t D>
  inline void Box<E, D>::validate(void)
  {
    if (viln::min(size_) < 0)
    {
      min_ = 0;
      size_ = 0;
    }
  }

  template<typename E, int8_t D>
  inline const vcrd<E, D>& Box<E, D>::position(void) const
  {
    return min_;
  }

  template<typename E, int8_t D>
  inline const vcrd<E, D>& Box<E, D>::size(void) const
  {
    return size_;
  }

  template<typename E, int8_t D>
  inline vcrd<E, D> Box<E, D>::min(void) const
  {
    return min_;
  }

  template<typename E, int8_t D>
  inline vcrd<E, D> Box<E, D>::max(void) const
  {
    return min_ + size_;
  }
}

#endif
