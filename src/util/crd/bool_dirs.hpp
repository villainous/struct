#ifndef VILN_BOOL_DIRS_HPP
#define VILN_BOOL_DIRS_HPP

#include <stddef.h>
#include <stdint.h>

namespace viln
{
  /// Type used for bitwise operations on axis-representations.
  typedef uint8_t bool_4d_t;

  namespace bool_dirs_enum
  {
    /// Bitfield containing the 4 directional X/Y/Z/W values.
    enum bool_4d : bool_4d_t
    {
      // none
      NONE = 0,
      // singles
      X = 1 << 0,
      Y = 1 << 1,
      Z = 1 << 2,
      W = 1 << 3,
      // pairs
      XY = (X | Y),
      XZ = (X | Z),
      XW = (X | W),
      YZ = (Y | Z),
      YW = (Y | W),
      ZW = (Z | W),
      // triples
      XYZ = (X | Y | Z),
      XYW = (X | Y | W),
      XZW = (X | Z | W),
      YZW = (Y | Z | W),
      // all
      XYZW = (X | Y | Z | W),
      ALL2 = XY,
      ALL3 = XYZ,
      ALL4 = XYZW,
      // end
      MAX = (ALL4 + 1)
    };

    // string
    /// Convert the direction to a string.
    const char* to_string(bool_4d val);
    /// Convert the string to a direction.
    bool_4d from_string(const char* str);
  };
  typedef bool_dirs_enum::bool_4d bool_2d;
  typedef bool_dirs_enum::bool_4d bool_3d;
  typedef bool_dirs_enum::bool_4d bool_4d;
}

#endif
