#ifndef VILN_VCRD_INL
#define VILN_VCRD_INL

#include <stddef.h>

#include "vcrd.hpp"
#include "vcrd_ops.inl"
#include "vcrd_ops_2x.inl"
#include "vcrd_ops_3x.inl"
#include "vcrd_ops_4x.inl"
// #include "vcrd_ops_3i_simd.inl"
// #include "vcrd_ops_4i_simd.inl"

namespace viln
{
  template<typename E, int8_t D>
  inline E& vcrd<E, D>::x(void)
    requires (D > 0)
  {
    return data[0];
  }

  template<typename E, int8_t D>
  inline E& vcrd<E, D>::y(void)
    requires (D > 1)
  {
    return data[1];
  }

  template<typename E, int8_t D>
  inline E& vcrd<E, D>::z(void)
    requires (D > 2)
  {
    return data[2];
  }

  template<typename E, int8_t D>
  inline E& vcrd<E, D>::w(void)
    requires (D > 3)
  {
    return data[3];
  }

  template<typename E, int8_t D>
  inline const E& vcrd<E, D>::x(void) const
    requires (D > 0)
  {
    return data[0];
  }

  template<typename E, int8_t D>
  inline const E& vcrd<E, D>::y(void) const
    requires (D > 1)
  {
    return data[1];
  }

  template<typename E, int8_t D>
  inline const E& vcrd<E, D>::z(void) const
    requires (D > 2)
  {
    return data[2];
  }

  template<typename E, int8_t D>
  inline const E& vcrd<E, D>::w(void) const
    requires (D > 3)
  {
    return data[3];
  }

  template<typename E, int8_t D>
  inline E& vcrd<E, D>::operator[](int8_t idx)
  {
    return data[idx];
  }

  template<typename E, int8_t D>
  inline const E& vcrd<E, D>::operator[](int8_t idx) const
  {
    return data[idx];
  }

  template<typename E, int8_t D>
  inline bool vcrd<E, D>::operator==(const vcrd& other) const
  {
    return equal(*this, other);
  }

  template<typename E, int8_t D>
  inline bool vcrd<E, D>::operator!=(const vcrd& other) const
  {
    return !equal(*this, other);
  }

  template<typename E, int8_t D>
  inline bool vcrd<E, D>::operator==(const E& base) const
  {
    return equal(*this, base);
  }

  template<typename E, int8_t D>
  inline bool vcrd<E, D>::operator!=(const E& base) const
  {
    return !equal(*this, base);
  }

  template<typename E, int8_t D>
  template<int8_t DD>
  inline vcrd<E, D>::operator vcrd<E, DD>(void) const
  {
    vcrd<E, DD> ret;
    int8_t minD = DD < D ? DD : D;
    for (int8_t idx = 0; idx < minD; ++idx)
    {
      ret[idx] = data[idx];
    }
    for (int8_t idx = minD; idx < DD; ++idx)
    {
      ret[idx] = E();
    }
    return ret;
  }

  template<typename E, int8_t D>
  template<typename EE>
  inline vcrd<E, D>::operator vcrd<EE, D>(void) const
  {
    vcrd<EE, D> ret;
    for (int8_t idx = 0; idx < D; ++idx)
    {
      ret[idx] = EE(data[idx]);
    }
    return ret;
  }

  template<typename E, int8_t D>
  inline vcrd<E, D>& vcrd<E, D>::operator=(const vcrd& other)
  {
    assign(*this, other);
    return *this;
  }

  template<typename E, int8_t D>
  inline vcrd<E, D>& vcrd<E, D>::operator=(const E& base)
  {
    assign(*this, base);
    return *this;
  }

  template<typename E, int8_t D>
  inline vcrd<E, D>& vcrd<E, D>::operator=(ucrd unit)
  {
    assign(*this, unit);
    return *this;
  }

  template<typename E, int8_t D>
  inline vcrd<E, D>& vcrd<E, D>::operator+=(const vcrd& other)
  {
    add_assign(*this, other);
    return *this;
  }

  template<typename E, int8_t D>
  inline vcrd<E, D>& vcrd<E, D>::operator-=(const vcrd& other)
  {
    sub_assign(*this, other);
    return *this;
  }

  /// Component-wise multiplication (NOT dot-product).
  template<typename E, int8_t D>
  inline vcrd<E, D>& vcrd<E, D>::operator*=(const vcrd& other)
  {
    mul_assign(*this, other);
    return *this;
  }

  template<typename E, int8_t D>
  inline vcrd<E, D>& vcrd<E, D>::operator/=(const vcrd& other)
  {
    div_assign(*this, other);
    return *this;
  }

  template<typename E, int8_t D>
  inline vcrd<E, D>& vcrd<E, D>::operator%=(const vcrd& other)
  {
    mod_assign(*this, other);
    return *this;
  }

  template<typename E, int8_t D>
  inline vcrd<E, D>& vcrd<E, D>::operator+=(ucrd unit)
  {
    add_assign(*this, unit);
    return *this;
  }

  template<typename E, int8_t D>
  inline vcrd<E, D>& vcrd<E, D>::operator-=(ucrd unit)
  {
    sub_assign(*this, unit);
    return *this;
  }

  template<typename E, int8_t D>
  inline vcrd<E, D>& vcrd<E, D>::operator+=(const E& base)
  {
    add_assign(*this, base);
    return *this;
  }

  template<typename E, int8_t D>
  inline vcrd<E, D>& vcrd<E, D>::operator-=(const E& base)
  {
    sub_assign(*this, base);
    return *this;
  }

  template<typename E, int8_t D>
  inline vcrd<E, D>& vcrd<E, D>::operator*=(const E& factor)
  {
    mul_assign(*this, factor);
    return *this;
  }

  template<typename E, int8_t D>
  inline vcrd<E, D>& vcrd<E, D>::operator/=(const E& factor)
  {
    div_assign(*this, factor);
    return *this;
  }

  template<typename E, int8_t D>
  inline vcrd<E, D>& vcrd<E, D>::operator%=(const E& factor)
  {
    mod_assign(*this, factor);
    return *this;
  }

  template<typename E, int8_t D>
  inline vcrd<E, D> vcrd<E, D>::operator-(void) const
  {
    return negate(*this);
  }

  template<typename E, int8_t D>
  inline vcrd<E, D> vcrd<E, D>::operator+(const vcrd& other) const
  {
    return add(*this, other);
  }

  template<typename E, int8_t D>
  inline vcrd<E, D> vcrd<E, D>::operator-(const vcrd& other) const
  {
    return sub(*this, other);
  }

  /// Component-wise multiplication (NOT dot-product!)
  template<typename E, int8_t D>
  inline vcrd<E, D> vcrd<E, D>::operator*(const vcrd& other) const
  {
    return mul(*this, other);
  }

  template<typename E, int8_t D>
  inline vcrd<E, D> vcrd<E, D>::operator/(const vcrd& other) const
  {
    return div(*this, other);
  }

  template<typename E, int8_t D>
  inline vcrd<E, D> vcrd<E, D>::operator%(const vcrd& other) const
  {
    return mod(*this, other);
  }

  template<typename E, int8_t D>
  inline vcrd<E, D> vcrd<E, D>::operator+(ucrd unit) const
  {
    return add(*this, unit);
  }

  template<typename E, int8_t D>
  inline vcrd<E, D> vcrd<E, D>::operator-(ucrd unit) const
  {
    return sub(*this, unit);
  }

  template<typename E, int8_t D>
  inline vcrd<E, D> vcrd<E, D>::operator+(const E& base) const
  {
    return add(*this, base);
  }

  template<typename E, int8_t D>
  inline vcrd<E, D> vcrd<E, D>::operator-(const E& base) const
  {
    return sub(*this, base);
  }

  template<typename E, int8_t D>
  inline vcrd<E, D> vcrd<E, D>::operator*(const E& factor) const
  {
    return mul(*this, factor);
  }

  template<typename E, int8_t D>
  inline vcrd<E, D> vcrd<E, D>::operator/(const E& factor) const
  {
    return div(*this, factor);
  }

  template<typename E, int8_t D>
  inline vcrd<E, D> vcrd<E, D>::operator%(const E& factor) const
  {
    return mod(*this, factor);
  }

  template<typename E, int8_t D>
  inline vcrd<E, D>& vcrd<E, D>::operator++(void)
  {
    inc(*this);
    return *this;
  }

  template<typename E, int8_t D>
  inline vcrd<E, D>& vcrd<E, D>::operator--(void)
  {
    dec(*this);
    return *this;
  }
}

namespace std
{
  template<typename E, int8_t D>
  struct tuple_size<viln::vcrd<E,D>>
  {
    static constexpr size_t value = D;
  };

  template<size_t N, typename E, int8_t D>
  struct tuple_element<N, viln::vcrd<E,D>>
  {
    using type = E;
  };
}

#endif
