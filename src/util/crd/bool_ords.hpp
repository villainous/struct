#ifndef VILN_BOOL_ORDS6_HPP
#define VILN_BOOL_ORDS6_HPP

#include "bool_dirs.hpp"

namespace viln
{
  /// Type used to represent cardinal directions.
  typedef uint8_t bool_6o_t;
  typedef uint8_t bool_4o_t;

  namespace bool_card_bits
  {
    /// Bitfield for each of the cardinal directions.
    enum bool_6c : bool_6o_t
    {
      // directions
      N = 1 << 0, // east
      S = 1 << 1, // north
      E = 1 << 2, // west
      W = 1 << 3, // south
      U = 1 << 4, // up
      D = 1 << 5, // down
      // bad
      BAD = 1 << 7
    };
    
    /// Convert the provided cardinal-direction to a character.
    char to_char(bool_6c val);
    /// Convert the provided character to a cardinal-direction (BAD on fail).
    bool_6c from_char(char ch);

    // utility
    /// Flip the ordinal-direction over the X, Y or Z axes.
    bool_6c flip(bool_6c val, bool_3d axes = bool_3d::ALL3);
  }
  typedef bool_card_bits::bool_6c bool_6c;
  typedef bool_card_bits::bool_6c bool_4c;

  namespace bool_ords_bits
  {
    /// Bitfield with the cardinal directions and common mixtures.
    enum bool_6o : bool_6o_t
    {
      // none
      NONE = 0,
      // singles
      N = bool_6c::N,
      S = bool_6c::S,
      E = bool_6c::E,
      W = bool_6c::W,
      U = bool_6c::U,
      D = bool_6c::D,
      // axial pairs
      NS = (N|S),
      EW = (E|W),
      UD = (U|D),
      // cardinal pairs
      NE = (N|E),
      NW = (N|W),
      SE = (S|E),
      SW = (S|W),
      // all
      ALL4 = (N|S|E|W),
      ALL6 = (N|S|E|W|U|D),
      // end
      MAX = (ALL6 + 1)
    };

    // string
    /// Print the ordinal-direction to a string.
    int snprint(char* buf, size_t len, bool_6o_t val);
    /// Convert the string to an ordinal-direction.
    bool_6o from_string(const char* str, size_t len = 6);

    // direction
    /// Convert the cardinal directions to the set of axes included. 
    bool_3d to_axes(bool_6o_t ord);
    /// Convert axes to pairs of cardinal directions.
    bool_6o from_axes(bool_3d dir);

    // utility
    /// Flip the ordinal-direction over the X, Y or Z axes.
    bool_6o flip(bool_6o val, bool_3d axes = bool_3d::ALL3);
    bool_6o_t flip(bool_6o_t val, bool_3d axes);
    bool_6o_t flip(bool_6o_t val);
  };
  typedef bool_ords_bits::bool_6o bool_6o;
  typedef bool_ords_bits::bool_6o bool_4o;
}

#endif
