#ifndef VILN_VCRD_OPS_4X_HPP
#define VILN_VCRD_OPS_4X_HPP

#include "vcrd_ops.hpp"

namespace viln
{
  // --- FUNCTIONS

  // COMPARISON
  template<typename E>
  bool equal(const vcrd<E, 4>& me, const vcrd<E, 4>& other);
  template<typename E>
  bool equal(const vcrd<E, 4>& me, const E& base);

  // INCREMENT
  template<typename E>
  void inc(vcrd<E, 4>& me);
  template<typename E>
  void dec(vcrd<E, 4>& me);

  // ASSIGNMENT
  template<typename E>
  void assign(vcrd<E, 4>& me, const vcrd<E, 4>& other);
  template<typename E>
  void assign(vcrd<E, 4>& me, const E& base);
  template<typename E>
  void assign(vcrd<E, 4>& me, ucrd unit);
  template<typename E>
  void add_assign(vcrd<E, 4>& me, const vcrd<E, 4>& other);
  template<typename E>
  void sub_assign(vcrd<E, 4>& me, const vcrd<E, 4>& other);
  template<typename E>
  void mul_assign(vcrd<E, 4>& me, const vcrd<E, 4>& other);
  template<typename E>
  void div_assign(vcrd<E, 4>& me, const vcrd<E, 4>& other);
  template<typename E>
  void mod_assign(vcrd<E, 4>& me, const vcrd<E, 4>& other);
  template<typename E>
  void add_assign(vcrd<E, 4>& me, const E& base);
  template<typename E>
  void sub_assign(vcrd<E, 4>& me, const E& base);
  template<typename E>
  void mul_assign(vcrd<E, 4>& me, const E& factor);
  template<typename E>
  void div_assign(vcrd<E, 4>& me, const E& factor);
  template<typename E>
  void mod_assign(vcrd<E, 4>& me, const E& factor);
  template<typename E>
  void add_assign(vcrd<E, 4>& me, ucrd unit);
  template<typename E>
  void sub_assign(vcrd<E, 4>& me, ucrd unit);

  // ARITHMETIC
  template<typename E>
  vcrd<E, 4> negate(const vcrd<E, 4>& me);
  template<typename E>
  vcrd<E, 4> add(const vcrd<E, 4>& me, const vcrd<E, 4>& other);
  template<typename E>
  vcrd<E, 4> sub(const vcrd<E, 4>& me, const vcrd<E, 4>& other);
  template<typename E>
  vcrd<E, 4> mul(const vcrd<E, 4>& me, const vcrd<E, 4>& other);
  template<typename E>
  vcrd<E, 4> div(const vcrd<E, 4>& me, const vcrd<E, 4>& other);
  template<typename E>
  vcrd<E, 4> mod(const vcrd<E, 4>& me, const vcrd<E, 4>& other);
  template<typename E>
  vcrd<E, 4> add(const vcrd<E, 4>& me, const E& base);
  template<typename E>
  vcrd<E, 4> sub(const vcrd<E, 4>& me, const E& base);
  template<typename E>
  vcrd<E, 4> mul(const vcrd<E, 4>& me, const E& factor);
  template<typename E>
  vcrd<E, 4> div(const vcrd<E, 4>& me, const E& factor);
  template<typename E>
  vcrd<E, 4> mod(const vcrd<E, 4>& me, const E& factor);
  template<typename E>
  vcrd<E, 4> add(const vcrd<E, 4>& me, ucrd unit);
  template<typename E>
  vcrd<E, 4> sub(const vcrd<E, 4>& me, ucrd unit);

  // FLOOR-DIV
  template<typename E>
  vcrd<E, 4> div_floor(const vcrd<E, 4>& me, const vcrd<E, 4>& other);
  template<typename E>
  vcrd<E, 4> mod_floor(const vcrd<E, 4>& me, const vcrd<E, 4>& other);
  template<typename E>
  vcrd<E, 4> div_floor(const vcrd<E, 4>& me, const E& factor);
  template<typename E>
  vcrd<E, 4> mod_floor(const vcrd<E, 4>& me, const E& factor);

  // UTILITY
  template<typename E>
  vcrd<E, 4> abs(const vcrd<E, 4>& crd);
  template<typename E>
  vcrd<E, 4> sgn(const vcrd<E, 4>& crd);
  template<typename E>
  vcrd<E, 4> min(const vcrd<E, 4>& crd1, const vcrd<E, 4>& crd2);
  template<typename E>
  vcrd<E, 4> max(const vcrd<E, 4>& crd1, const vcrd<E, 4>& crd2);
  template<typename E>
  vcrd<E, 4> min(const vcrd<E, 4>& crd, const E& elem);
  template<typename E>
  vcrd<E, 4> max(const vcrd<E, 4>& crd, const E& elem);

  // VECTOR
  template<typename E>
  E volume(const vcrd<E, 4>& crd);
  template<typename E>
  E dot(const vcrd<E, 4>& a, const vcrd<E, 4>& b);
  template<typename E>
  E length_max(const vcrd<E, 4>& crd);
  template<typename E>
  E length1(const vcrd<E, 4>& crd);
  template<typename E>
  E min(const vcrd<E, 4>& me);
  template<typename E>
  E max(const vcrd<E, 4>& me);

  // MISC
  template<typename E>
  uint64_t hash(const vcrd<E, 4>& crd);
}

#endif
