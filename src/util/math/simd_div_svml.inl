// simd_div*.inl exclusive
#ifndef VILN_SIMD_DIV_INL
#define VILN_SIMD_DIV_INL

#include "simd_div.hpp"

namespace viln
{
  // ARITHMETIC epi32

  inline __m128i mm_div_epi32(const __m128i a, const __m128i b)
  {
    return _mm_div_epi32(a, b);
  }

  inline __m128i mm_mod_epi32(const __m128i a, const __m128i b)
  {
    const __m128i div = mm_div_epi32(a, b);
    const __m128i mul = _mm_mullo_epi32(div, b);
    return _mm_sub_epi32(a, mul);
  }

  /// Component-wise division+floor.
  inline __m128i mm_div_floor_epi32(const __m128i a, const __m128i b)
  {
    const __m128i div = mm_div_epi32(a, b); // a/b
    const __m128i abb = _mm_mullo_epi32(div, b); // (a/b)*b
    const __m128i ZERO = _mm_setzero_si128();
    const __m128i aL0 = _mm_cmplt_epi32(a, ZERO); // a<0
    const __m128i bL0 = _mm_cmplt_epi32(b, ZERO); // b<0
    const __m128i dif = _mm_andnot_si128(
      _mm_cmpeq_epi32(a, abb), // (a/b)*b == a
      _mm_xor_si128(aL0, bL0)); // a<0 == b<0
    const __m128i ret = _mm_add_epi32(div, dif);
    return ret;
    // (d * b == a) ? d : d - ((a < 0) ^ (b < 0));
  }

  /// Component-wise modulus+floor.
  inline __m128i mm_mod_floor_epi32(const __m128i a, const __m128i b)
  {
    const __m128i div = mm_div_epi32(a, b); // a/b
    const __m128i abb = _mm_mullo_epi32(div, b); // (a/b)*b
    const __m128i mod = _mm_sub_epi32(a, abb);
    const __m128i ZERO = _mm_setzero_si128();
    const __m128i aL0 = _mm_cmplt_epi32(a, ZERO); // a<0
    const __m128i bL0 = _mm_cmplt_epi32(b, ZERO); // b<0
    const __m128i dif = _mm_andnot_si128(
      _mm_cmpeq_epi32(a, abb), // (a/b)*b == a
      _mm_xor_si128(aL0, bL0)); // a<0 == b<0
    const __m128i xtr = _mm_and_si128(dif, b);
    const __m128i ret = _mm_add_epi32(mod, xtr);
    return ret;
  }
}

#endif
