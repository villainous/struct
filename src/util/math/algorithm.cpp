
#include <limits.h>
#include <stddef.h>
#include <string.h>
#include "algorithm.hpp"


// --- LOG2

// constants used in the below calculations
static constexpr uint64_t DEBRUJIN_64 = 0x218A392CD3D5DBFull;

/// \cond DO_NOT_DOCUMENT
struct debrujin_hash_64
{
  constexpr debrujin_hash_64(void) : lut()
  {
    for (uint64_t idx = 0; idx < 64; ++idx)
    {
      lut[(DEBRUJIN_64 << idx) >> 58] = (uint8_t) idx;
    }
  }
  // LUT for log2i_debrujin
  uint8_t lut[64];
};
/// \endcond
static constexpr debrujin_hash_64 dbhash64;

/**
Implements efficient log-base-2 of integer values. This operation is equivalent
to finding the index of the highest nonzero bit.

Similar to platform-dependent POSIX `fls` or Microsoft `_BitScanReverse` but
extended to 64-bits. While those implementations may directly use builtin CPU
instructions, the behavior tends to be inconsistent and poorly behaved in
corner-cases. Use of the CPU directly should in theory be more efficient,
however testing shows no appreciable difference between the approaches on
x86, so there is no real motivation for use of the platform-specific versions.

@param pow  number to take the logarithm of
@return  the logarithm base-2 of the param (rounded down)
@see http://graphics.stanford.edu/~seander/bithacks.html#IntegerLogDeBruijn
*/
uint8_t viln::log2i_debrujin(uint64_t pow)
{
  // round up to the nearest power-of-2 minus one
  pow |= pow >> 1;
  pow |= pow >> 2;
  pow |= pow >> 4;
  pow |= pow >> 8;
  pow |= pow >> 16;
  pow |= pow >> 32;
  // round down to the nearest power-of-2 to isolate the top bit
  pow ^= pow >> 1;
  // use our perfect hash function to look up the value
  return dbhash64.lut[(pow * DEBRUJIN_64) >> 58];
}


// --- HASH

static constexpr uint64_t FNV1A_BASE_64 = 0xCBF29CE484222325ull;
static constexpr uint64_t FNV1A_PRIME_64 = 0x100000001B3ull;

/**
Straightforward hash with relatively good performance on arbitrary
arrays of bytes. Uses the Fowler/Noll/Vo 1A algorithm for non-cryptographic
hashing.

@param dat  a pointer to the data to be hashed
@param len  the length (in bytes) of the data to hash
@return  the computed hash value for the data
@see http://www.isthe.com/chongo/tech/comp/fnv/
*/
uint64_t viln::hash64_fnv1a(const void* dat, size_t len)
{
  const uint8_t* data = (const uint8_t*) dat;
  uint64_t h = FNV1A_BASE_64;
  for (size_t idx = 0; idx < len; ++idx)
  {
    h = (h ^ data[idx]) * FNV1A_PRIME_64;
  }
  return h;
}

/**
@see hash64_fnv1a(const void*, size_t)
*/
uint64_t viln::hash64_fnv1a(const char* str)
{
  const uint8_t* data = (const uint8_t*) str;
  uint64_t h = FNV1A_BASE_64;
  for (uint8_t ch = *data; ch != 0; ch = *(++data))
  {
    h = (h ^ ch) * FNV1A_PRIME_64;
  }
  return h;
}

/**
Compute the 64-bit parity value for the provided data. This is not
generally a very robust hashing-mechanism, but it is very simple and FAST.

@param dat  a pointer to the data to be hashed
@param len  the length (in bytes) of the data to hash
@return  the computed hash value for the data
*/
uint64_t viln::hash64_parity(const void* dat, size_t len)
{
  uint64_t acc = 0;
  uint64_t* dat64 = (uint64_t*) dat;
  size_t num64 = len / sizeof(uint64_t);
  for(size_t ii = 0; ii < num64; ++ii) acc ^= *(dat64++);
  uint8_t* acc8 = (uint8_t*) &acc;
  uint8_t* dat8 = (uint8_t*) dat64;
  size_t num8 = len % sizeof(uint64_t);
  for(size_t ii = 0; ii < num8; ++ii) acc8[ii] ^= *(dat8++);
  return acc;
}

/**
@see hash64_parity(const void*, size_t)
*/
uint64_t viln::hash64_parity(const char* str)
{
  uint64_t acc = 0;
  uint8_t* acc8 = (uint8_t*) &acc;
  const uint8_t* data8 = (const uint8_t*) str;
  size_t n8 = 0;
  for (uint8_t ch = *data8; ch != 0; ch = *(++data8))
  {
    acc8[n8++ % sizeof(uint64_t)] ^= ch;
  }
  return acc;
}


// --- REHASH

static constexpr size_t VRAND_NTAB = 4 * 8 * 4;
// a table of integers used as a source of entropy
static constexpr char VRAND_CTAB[VRAND_NTAB + 1] =
{
  "tree" "pace" "taxi" "find" "wire" "desk" "high" "kick"
  "bond" "call" "stir" "sofa" "Mars" "vain" "soft" "bend"
  "sail" "fade" "neck" "seat" "duke" "curl" "slab" "twin"
  "gift" "core" "sail" "rice" "hurl" "hurt" "shop" "sell"
};

/**
This is a seeded random number generator that uses an entropy table
combined with xorshift for random number generation. The entropy-table
splits nearby inputs in order ro remove patterns resulting from sequential
input-values.

@param seed  the input-value being re-hashed
@see https://www.jstatsoft.org/article/view/v008i14
*/
uint64_t viln::rehash64_xorshift(uint64_t seed)
{
  constexpr uint64_t VRAND_NTAB64 = VRAND_NTAB / sizeof(uint64_t);
  const uint64_t* VRAND_TAB64 = (uint64_t*) VRAND_CTAB;

  seed ^= VRAND_TAB64[seed % VRAND_NTAB64];
  seed ^= seed << 13;
  seed ^= seed >> 7;
  seed ^= seed << 17;
  return seed;
}

/**
@see rehash64_xorshift(uint64_t)
*/
uint32_t viln::rehash32_xorshift(uint32_t seed)
{
  constexpr uint32_t VRAND_NTAB32 = VRAND_NTAB / sizeof(uint32_t);
  const uint32_t* VRAND_TAB32 = (uint32_t*) VRAND_CTAB;

  seed ^= VRAND_TAB32[seed % VRAND_NTAB32];
  seed ^= seed << 13;
  seed ^= seed >> 17;
  seed ^= seed << 5;
  return seed;
}
