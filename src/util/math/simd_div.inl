#ifdef __INTEL_COMPILER
#include "mm_div_svml.inl"
#elif defined _MSC_VER
#include "mm_div_svml.inl"
#endif
#ifndef VILN_SIMD_DIV_INL
#define VILN_SIMD_DIV_INL

#include "simd_div.hpp"

namespace viln
{
  // ARITHMETIC epi32

  inline __m128i mm_div3_epi32(const __m128i a, const __m128i b)
  {
    const __m128i bb = _mm_insert_epi32(b, 1, 3);
    const __m128 ma = _mm_cvtepi32_ps(a);
    const __m128 mb = _mm_cvtepi32_ps(bb);
    const __m128 div = _mm_div_ps(ma, mb);
    return _mm_cvttps_epi32(div);
  }

  inline __m128i mm_mod3_epi32(const __m128i a, const __m128i b)
  {
    const __m128i div = mm_div3_epi32(a, b);
    const __m128i mul = _mm_mullo_epi32(div, b);
    return _mm_sub_epi32(a, mul);
  }

  inline __m128i mm_div3_floor_epi32(const __m128i a, const __m128i b)
  {
    const __m128i bb = _mm_insert_epi32(b, 1, 3);
    const __m128 ma = _mm_cvtepi32_ps(a);
    const __m128 mb = _mm_cvtepi32_ps(bb);
    const __m128 div = _mm_div_ps(ma, mb);
    const __m128 flr = _mm_floor_ps(div);
    return _mm_cvttps_epi32(flr);
  }

  inline __m128i mm_mod3_floor_epi32(const __m128i a, const __m128i b)
  {
    const __m128i div = mm_div3_floor_epi32(a, b);
    const __m128i mul = _mm_mullo_epi32(div, b);
    return _mm_sub_epi32(a, mul);
  }
}

#endif
