#ifndef VILN_ALGORITHM_HPP
#define VILN_ALGORITHM_HPP

#include <stddef.h>
#include <stdint.h>

namespace viln
{
  // --- MATH

  /// Logarithm in base-2 of unsigned 64-bit integer.
  uint8_t log2i_debrujin(uint64_t pow);

  // --- HASH (non-cryptographic!)

  /// Fowler/Noll/Vo 1A hashing of arbitrary data.
  uint64_t hash64_fnv1a(const void* dat, size_t len);
  /// Fowler/Noll/Vo 1A hashing of a null-terminated c-string.
  uint64_t hash64_fnv1a(const char* str);

  /// Simple parity-based hashing of arbitrary data.
  uint64_t hash64_parity(const void* dat, size_t len);
  /// Simple parity-based hashing of a null-terminated c-string.
  uint64_t hash64_parity(const char* str);

  // --- REHASH (random numbers)

  /// A random number generator using an entropy-table and xorshift.
  uint64_t rehash64_xorshift(uint64_t seed);
  /// A random number generator using an entropy-table and xorshift.
  uint32_t rehash32_xorshift(uint32_t seed);
}

#endif
