#ifndef VILN_VMATH_INL
#define VILN_VMATH_INL

#include <math.h>

#include "algorithm.hpp"
#include "vmath.hpp"

namespace viln
{
  // --- ARITHMETIC

  template<typename T>
  inline T sgn(T a)
  {
    return T((0 < a) - (a < 0));
  }

  template<typename T>
  inline T abs(T a)
  {
    return a > 0 ? a : -a;
  }

  template<typename T>
  inline T min(T a, T b)
  {
    return a < b ? a : b;
  }

  template<typename T>
  inline T max(T a, T b)
  {
    return a > b ? a : b;
  }

  /**
  Clamp a value between two bounds `[min,max)` (clopen).

  @param minn  the minimum (left) bound
  @param maxx  the maximum (right) bound
  @param val  the value to clamp
  @return  minn, val, or --maxx
  */
  template<typename T>
  inline T clamp(T minn, T maxx, T val)
  {
    return min(max(minn, --maxx), max(minn, val));
  }

  /**
  Clamp a float between two bounds `[min,max)` (clopen).

  @param minn  the minimum (left) bound
  @param maxx  the maximum (right) bound
  @param val  the value to clamp
  @return  minn, val, or limit(max)
  */
  inline float clamp(float minn, float maxx, float val)
  {
    if (val < minn) return minn;
    if (val < maxx) return val;
    return nextafterf(maxx, -HUGE_VALF);
  }

  /**
  Clamp a double between two bounds `[min,max)` (clopen).

  @param minn  the minimum (left) bound
  @param maxx  the maximum (right) bound
  @param val  the value to clamp
  @return  minn, val, or limit(max)
  */
  inline double clamp(double minn, double maxx, double val)
  {
    if (val < minn) return minn;
    if (val < maxx) return val;
    return nextafter(maxx, -HUGE_VAL);
  }

  /**
  The modulus of the provided numerator with respect to the denominator.
   
  @param num  numerator
  @param den  denominator
  @return  the modulus `num % den`
  */
  template<typename T>
  inline T mod(T num, T den)
  {
    return num % den;
  }

  inline float mod(float num, float den)
  {
    return 0;
  }

  inline double mod(double num, double den)
  {
    return 0;
  }

  /**
  Implements integer division with rounding towards negative invinity.
  Note that standard division rounds toward 0 which is problematic
  because we want to use these values for indexing operations sometimes.

  The compiler should be smart enough to combine the `/` and `%`, so this
  only really adds the cost of the conditional. Anecdotally, this shouldn't
  more than double the overall cost of division, which was already slow.

  @param num  numerator
  @param den  denominator
  @return  the floor of the quotiant
  */
  template<typename T>
  inline T div_floor(T num, T den)
  {
    T q = num / den;
    T r = num % den;
    if (r && ((r < 0) != (den < 0))) --q;
    return q;
  }

  inline float div_floor(float num, float den)
  {
    return num / den;
  }

  inline double div_floor(double num, double den)
  {
    return num / den;
  }

  /**
  Since division rounds to negative infinity, we also must adjust the
  remainder operator so that `a * ( x / a ) + ( x % a ) = x` and the two
  remain self-consistent.

  @param num  numerator
  @param den  denominator
  @return  the floor of the quotiant
  @see div_floor
  */
  template<typename T>
  inline T mod_floor(T num, T den)
  {
    T r = viln::mod(num, den);
    if (r && ((r < 0) != (den < 0))) r += den;
    return r;
  }

  inline float mod_floor(float num, float den)
  {
    return 0;
  }

  inline double mod_floor(double num, double den)
  {
    return 0;
  }

  template<typename T>
  inline T div_ceil(T num, T den)
  {
    return -div_floor(-num, den);
  }

  template<typename T>
  inline T mod_ceil(T num, T den)
  {
    return -mod_floor(-num, den);
  }


  // --- HASHING

  template<typename T>
  inline auto hash(T ii)
    ->decltype(uint64_t(ii))
  {
    return uint64_t(ii);
  }

  inline uint64_t hash(float ff)
  {
    if (isnan(ff)) return UINT64_MAX;
    return viln::hash64_fnv1a(&ff, sizeof(ff));
  }

  inline uint64_t hash(double df)
  {
    if (isnan(df)) return UINT64_MAX;
    return viln::hash64_fnv1a(&df, sizeof(df));
  }
}

#endif
