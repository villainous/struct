#ifndef VILN_VMATH_HPP
#define VILN_VMATH_HPP

#include <stdint.h>

namespace viln
{
  /// sign
  template<typename T>
  T sgn(T a);
  /// absolute-value
  template<typename T>
  T abs(T a);

  /// minimum
  template<typename T>
  T min(T a, T b);
  /// maximum
  template<typename T>
  T max(T a, T b);

  /// clamp a value between `[min,max)`
  template<typename T>
  T clamp(T minn, T maxx, T val);
  float clamp(float minn, float maxx, float val);
  double clamp(double minn, double maxx, double val);

  /// modulus operator
  template<typename T>
  T mod(T num, T den);
  float mod(float num, float den);
  double mod(double num, double den);

  /// division with rounding to `-inf`
  template<typename T>
  T div_floor(T num, T den);
  float div_floor(float num, float den);
  double div_floor(double num, double den);
  /// modulus with rounding to `-inf`
  template<typename T>
  T mod_floor(T num, T den);
  
  /// division with rounding to `+inf`
  template<typename T>
  T div_ceil(T num, T den);
  /// modulus with rounding to `+inf`
  template<typename T>
  T mod_ceil(T num, T den);

  /// simple hashing of numeric types
  template<typename T>
  auto hash(T ii)
    ->decltype(uint64_t(ii));
  uint64_t hash(float ff);
  uint64_t hash(double df);
}

#endif
