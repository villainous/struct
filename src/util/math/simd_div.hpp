#ifndef VILN_SIMD_DIV_HPP
#define VILN_SIMD_DIV_HPP

#include <immintrin.h>

namespace viln
{
  // ARITHMETIC epi32
  /// Component-wise division.
  __m128i mm_div_epi32(const __m128i a, const __m128i b);
  /// Component-wise modulus.
  __m128i mm_mod_epi32(const __m128i a, const __m128i b);
  /// Component-wise division+floor.
  __m128i mm_div_floor_epi32(const __m128i a, const __m128i b);
  /// Component-wise modulus+floor.
  __m128i mm_mod_floor_epi32(const __m128i a, const __m128i b);
}

#endif
