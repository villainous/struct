#ifndef VILN_COLLECTION_HPP
#define VILN_COLLECTION_HPP

#include <stddef.h>

#include "iterable.hpp"

namespace viln
{
  /// A generic group of elements.
  /**
  The `Collection` interface is intended to serve as the base inteface for
  the collections framework. The intention of this interface is to provide
  interoperability between instances of different collection implementations
  (such as `List` or `Set`).

  It is suggested that implementations of `Collection` should provide a default
  (no-arg) constructor that creates an empty instance of the collection, as
  well as a copy-constructor from another `const Collection&`.

  Many of the non-const methods are specified to return a `bool`. These return
  values should indicate if the object was modified by the operation. This may
  be considered an error in some cases, but that is up to interpretation by
  the caller.

  Note that invoking any of the mutator methods may invalidate active iterators
  over the collection.
  */
  template<typename E>
  class Collection : virtual public ConstIterable<E>
  {
  public:
    /// Virtual destructor.
    virtual ~Collection(void) {};

    /// Move an element into the collection.
    virtual bool add(E&& e) = 0;
    /// Add an element to the collection.
    virtual bool add(const E& e) = 0;
    /// Add all the elements from another collection to this one.
    virtual bool addAll(const ConstIterable<E>& c);
    /// Add all elements returned by the iterator to this collection.
    virtual bool addAll(Iterator<const E>* itr);
    /// Remove an element from the collection.
    virtual bool remove(const E& e) = 0;
    /// Remove all elements in the provided collection from this one.
    virtual bool removeAll(const ConstIterable<E>& c);
    /// Remove all elements returned by the iterator from this collection.
    virtual bool removeAll(Iterator<const E>* itr);
    /// Remove all elements from this collection.
    virtual void clear(void) = 0;

    /// Determine if the provided element is contained in this collection.
    virtual bool contains(const E& e) const = 0;
    /// Determine if the provided collection is a subset of this one.
    virtual bool containsAll(const ConstIterable<E>& c) const;
    /// Determine if the iterated elements are all elements of this collection.
    virtual bool containsAll(Iterator<const E>* itr) const;
    /// Determine if this collection is empty.
    virtual bool isEmpty(void) const;
    /// Determine the size of this collection.
    virtual size_t size(void) const = 0;
    /// Construct an array of elements equivalent to this collection.
    virtual E* toArray(void) const;
  };
}

#endif
