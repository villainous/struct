#ifndef VILN_ITERABLE_HPP
#define VILN_ITERABLE_HPP

#include "iterator.hpp"
#include "cterator.hpp"

namespace viln
{
  /// An object which provides *immutable* elements to be iterated over.
  /**
  An object implementing the `ConstIterable` interface provides access to elements
  that can be accessed one at a time. While the iterator itself is modifiable,
  the elements returned by it are *not*.
  */
  template<typename E>
  class ConstIterable
  {
  public:
    /// Virtual destructor.
    virtual ~ConstIterable(void) {};

    /// Construct a new const-iterator over the elements of this object.
    virtual Iterator<const E>* iterator(void) const = 0;

    /// Create a `Cterator` pointing at the first element.
    virtual Cterator<const E> begin(void) const;
    /// Create a `Cterator` representing a state after the last element.
    virtual Cterator<const E> end(void) const;
  };

  /// An object which provides elements to be iterated over.
  /**
  An object implementing the `Iterable` interface provides access to elements
  that can be accessed one at a time. These elements can be modified if the
  iterable-implementation is modifiable.
  */
  template<typename E>
  class Iterable : virtual public ConstIterable<E>
  {
  public:
    /// Virtual destructor.
    virtual ~Iterable(void) {};

    /// Construct a new iterator over the elements of this object.
    virtual Iterator<E>* iterator(void) = 0;
    using ConstIterable<E>::iterator;

    /// Create a `Cterator` pointing at the first element.
    virtual Cterator<E> begin(void);
    using ConstIterable<E>::begin;
    /// Create a `Cterator` representing a state after the last element.
    virtual Cterator<E> end(void);
    using ConstIterable<E>::end;
  };
}

#endif
