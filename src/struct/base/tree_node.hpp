#ifndef VILN_TREE_NODE_HPP
#define VILN_TREE_NODE_HPP

#include <stdint.h>

namespace viln
{
  /// A data wrapper element of a binary-tree structure.
  /**
  A `TreeNode` contains a data element and up to two children. The data and
  children are considered to be owned by the node, and will be deleted upon
  deletion of the node.

  Nodes also have a height attribute, which is the maximum number of steps
  to get to a leaf element among all children. The height is recalculated
  on all mutator operations, and updates propagate up through parent nodes.
  This means that adding or removing elements is an **O(depth)** operation, so
  unbalanced trees should be avoided.

  Operations that do not modify the node are all **O(1)**.
  */
  template<typename E>
  class TreeNode
  {
  public:
    /// Construct a node that owns the provided data.
    TreeNode(E* dat);
    /// Destructor.
    ~TreeNode(void);

    // not copyable!
    TreeNode(const TreeNode&) = delete;
    TreeNode(TreeNode<E>&&) = delete;
    TreeNode& operator=(const TreeNode&) = delete;

    /// Detach the left child from this one.
    TreeNode<E>* detachLeft(void);
    /// Detach the right child from this one.
    TreeNode<E>* detachRight(void);
    /// Detach this node from its parent.
    TreeNode<E>* detach(void);

    /// Attach the provided node as the left child.
    void setLeft(TreeNode<E>* lft);
    /// Attach the provided node as the right child.
    void setRight(TreeNode<E>* rgt);

    /// Get the right node.
    TreeNode<E>* const rightChild(void);
    const TreeNode<E>* rightChild(void) const;
    /// Get the left node.
    TreeNode<E>* const leftChild(void);
    const TreeNode<E>* leftChild(void) const;
    /// Get the parent to this node.
    TreeNode<E>* const parent(void);
    const TreeNode<E>* parent(void) const;
    /// Get the root ot the tree containing this node.
    TreeNode<E>* const root(void);
    const TreeNode<E>* root(void) const;
    /// Access the data.
    E& data(void);
    const E& data(void) const;

    /// Get the height of the node above the deepest leaf.
    int32_t calcHeight(void) const;
    int32_t height(void);
    /// Get the size of the subtree rooted at this node.
    int32_t calcSize(void) const;
    int32_t size(void);
    /// The difference in height between the left and right children.
    int32_t calcBalance(void) const;
    int32_t balance(void);

  private:
    // Invalidate cached values for this node and parents.
    void dirty(void);

    // the left and right child nodes
    TreeNode<E>* left_;
    TreeNode<E>* right_;
    // the parent node to this one (NOT owned by this)
    TreeNode<E>* parentptr_;
    // the data being held
    E* const data_;
    // height above the farthest leaf
    int32_t height_;
    // size of the subtree rooted at this node
    int32_t size_;
  };
}

#endif
