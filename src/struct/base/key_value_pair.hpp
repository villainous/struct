#ifndef VILN_KEY_VALUE_PAIR_HPP
#define VILN_KEY_VALUE_PAIR_HPP

namespace viln
{
  /// A type representing a linkage between a key and an associated value.
  /**
  A key/value pair represents the association of some key with a specific
  value. Typically this means that the value can be looked-up using the
  key.

  The key-type is not modifiable after construction, as it is a
  "lookup" so modifying this in-place would cause problems for any
  enclosing dictionary.

  The value-type is passed by pointer so that it can be modified
  easily after construction, and so that it does not require a
  defined copy-constructor.
  */
  template<typename K, typename V>
  class KeyValuePair
  {
  public:
    /// Constructor.
    KeyValuePair(const K& k, V* v);
    /// Move-constructor.
    KeyValuePair(KeyValuePair<K,V>&& o);
    /// Destructor.
    ~KeyValuePair(void);

    // not copyable
    KeyValuePair(const KeyValuePair&) = delete;
    KeyValuePair& operator=(const KeyValuePair&) = delete;

    // operators
    /// Conversion to boolean checks validity.
    operator bool(void) const;
    /// Pointer-like access to the value.
    V& operator*(void);
    /// Pointer-like access to the value.
    V* const operator->(void);
    /// Pointer-like access to the value (const).
    const V& operator*(void) const;
    /// Pointer-like access to the value (const).
    const V* operator->(void) const;

    /// Replace the value being held in this pair (assumes ownership).
    void put(V* v);
    /// Release the value to the caller (exchange ownership).
    V* swap(V* v);

    /// Access the key (const).
    const K& key(void) const;
    /// Access the value (modifiable).
    V& value(void);
    /// Access the value (const).
    const V& value(void) const;

  private:
    const K key_;
    V* value_;
  };
  template<typename K, typename V>
  using KeyVal = KeyValuePair<K,V>;
}

#endif
