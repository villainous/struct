#ifndef VILN_ITERABLE_INL
#define VILN_ITERABLE_INL

#include "iterable.hpp"

#include "cterator.inl"

namespace viln
{
  /**
  Create a `Cterator` pointing at the default "first" element that would be
  returned by a newly constructed `Iterator`.

  @return  an iterator at the "beginning"
  */
  template<typename E>
  inline Cterator<const E> ConstIterable<E>::begin(void) const
  {
    return Cterator<const E>(iterator());
  }

  /**
  Create a `Cterator` representing the state of pointing past the last element
  of the iterable object (or "infinity" if there is no such last element).

  @return  an iterator *marker* at the "end"
  */
  template<typename E>
  inline Cterator<const E> ConstIterable<E>::end(void) const
  {
    return Cterator<const E>::after();
  }

  /// @see ConstIterable<E>::begin
  template<typename E>
  inline Cterator<E> Iterable<E>::begin(void)
  {
    return Cterator<E>(iterator());
  }

  /// @see ConstIterable<E>::end
  template<typename E>
  inline Cterator< E> Iterable<E>::end(void)
  {
    return Cterator<E>::after();
  }
}

#endif
