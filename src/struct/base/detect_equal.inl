#ifndef VILN_DETECT_EQUALS_INL
#define VILN_DETECT_EQUALS_INL

#include <stdint.h>

#include "detect_equal.hpp"

namespace viln
{
  // --- CLASS EqualFunc

  /**
  Construct a equals operator from a function-pointer.

  @param func  the equals function to wrap
  */
  template<typename U>
  inline EqualFunc<U>::EqualFunc(equal_func<U> func) :
    func_(func)
  {
    // no-op
  }

  /**
  An equals method delegating to a function-pointer.

  @param u1  the left-hand element
  @param u2  the right-hand element
  @return  true iff `u1` is "equal to" `u2`
  */
  template<typename U>
  inline bool EqualFunc<U>::equal(const U& u1, const U& u2) const
  {
    return func_(u1, u2);
  }
}

#endif
