#ifndef VILN_DETECT_LESS_THAN_INL
#define VILN_DETECT_LESS_THAN_INL

#include <stdlib.h>
#include <stdint.h>

#include "detect_order.hpp"

namespace viln
{
  // --- CLASS OrderFunc

  /**
  Construct a less-than operator from a function-pointer.

  @param func  the less-than function to wrap
  */
  template<typename U>
  inline OrderFunc<U>::OrderFunc(order_func<U> func) :
    func_(func)
  {
    // no-op
  }

  /**
  A less-than method delegating to a function-pointer.

  @param u1  the left-hand element
  @param u2  the right-hand element
  @return  true iff `u1` is "less than" `u2`
  */
  template<typename U>
  inline bool OrderFunc<U>::order(const U& u1, const U& u2) const
  {
    return func_(u1, u2);
  }
}

#endif
