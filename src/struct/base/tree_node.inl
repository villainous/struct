#ifndef VILN_TREE_NODE_INL
#define VILN_TREE_NODE_INL

#include <stdint.h>

#include "tree_node.hpp"

namespace viln
{
  /**
  Construct a `TreeNode` holding the provided data. The new node takes
  ownership over the provided data. The left and right pointers for the node
  children are initially null. The height of a node with no children is zero.

  @param dat  data that this node will be a wrapper for (takes ownership)
  */
  template<typename E>
  inline TreeNode<E>::TreeNode(E* dat) :
    left_(nullptr),
    right_(nullptr),
    parentptr_(nullptr),
    data_(dat),
    height_(0),
    size_(1)
  {
    // no-op
  }

  /**
  The destructor deletes child nodes recursively, as well as any data. If the
  node has a parent, then the parent's reference to this node is removed.
  */
  template<typename E>
  inline TreeNode<E>::~TreeNode(void)
  {
    detach();
    if (left_)
    {
      delete left_;
    }
    if (right_)
    {
      delete right_;
    }
    delete data_;
  }

  /**
  Data access. Note that the data is location-stable, so maintaining pointers
  to this data is safe so long as the node is not deleted.

  @return  the data held in the node
  */
  template<typename E>
  inline E& TreeNode<E>::data(void)
  {
    return *data_;
  }


  /**
  Constant data access. Note that the data is location-stable, so maintaining
  pointers to this data is safe so long as the node is not deleted.

  @return  the data held in the node
  */
  template<typename E>
  inline const E& TreeNode<E>::data(void) const
  {
    return *data_;
  }

  /**
  Detach the left node from this one. The caller takes ownership over the
  newly detached node.

  @return  the left node (caller takes ownership)
  */
  template<typename E>
  inline TreeNode<E>* TreeNode<E>::detachLeft(void)
  {
    TreeNode<E>* lft = left_;
    if (lft)
    {
      left_ = nullptr;
      lft->parentptr_ = nullptr;
    }
    dirty();
    return lft;
  }

  /**
  Detach the right node from this one. The caller takes ownership over the
  newly detached node.

  @return  the right node (caller takes ownership)
  */
  template<typename E>
  inline TreeNode<E>* TreeNode<E>::detachRight(void)
  {
    TreeNode<E>* rgt = right_;
    if (rgt)
    {
      right_ = nullptr;
      rgt->parentptr_ = nullptr;
    }
    dirty();
    return rgt;
  }

  /**
  Detach this node from its parent. The caller takes ownership over the newly
  detached node.

  @return  this node (caller takes ownership)
  */
  template<typename E>
  inline TreeNode<E>* TreeNode<E>::detach(void)
  {
    // if no parent, then nothing to do
    if (!parentptr_)
    {
      return this;
    }
    // determine which child of the parent is and detach it
    if (parentptr_->left_ == this)
    {
      parentptr_->left_ = nullptr;
    }
    else
    {
      parentptr_->right_ = nullptr;
    }
    // update height of the parent
    parentptr_->dirty();
    parentptr_ = nullptr;
    return this;
  }

  /**
  Set the left node. This node takes ownership over the provided node.

  @param lft  the node to use as the left node (takes ownership) (may be null)
  */
  template<typename E>
  inline void TreeNode<E>::setLeft(TreeNode<E>* lft)
  {
    if (left_) delete left_;
    left_ = lft;
    if (lft) lft->parentptr_ = this;
    dirty();
  }

  /**
  Set the right node. This node takes ownership over the provided node.

  @param rgt  the node to use as the right node (takes ownership) (may be null)
  */
  template<typename E>
  inline void TreeNode<E>::setRight(TreeNode<E>* rgt)
  {
    if (right_) delete right_;
    right_ = rgt;
    if (rgt) rgt->parentptr_ = this;
    dirty();
  }

  template<typename E>
  inline TreeNode<E>* const TreeNode<E>::rightChild(void)
  {
    return right_;
  }

  template<typename E>
  inline const TreeNode<E>* TreeNode<E>::rightChild(void) const
  {
    return right_;
  }

  template<typename E>
  inline TreeNode<E>* const TreeNode<E>::leftChild(void)
  {
    return left_;
  }

  template<typename E>
  inline const TreeNode<E>* TreeNode<E>::leftChild(void) const
  {
    return left_;
  }

  template<typename E>
  inline TreeNode<E>* const TreeNode<E>::parent(void)
  {
    return parentptr_;
  }

  template<typename E>
  inline const TreeNode<E>* TreeNode<E>::parent(void) const
  {
    return parentptr_;
  }

  template<typename E>
  inline TreeNode<E>* const TreeNode<E>::root(void)
  {
    TreeNode<E>* up = this;
    while (up->parentptr_)
    {
      up = up->parentptr_;
    }
    return up;
  }

  template<typename E>
  inline const TreeNode<E>* TreeNode<E>::root(void) const
  {
    const TreeNode<E>* up = this;
    while (up->parentptr_)
    {
      up = up->parentptr_;
    }
    return up;
  }

  /**
  Compute the height of this node above the deepest descendant. If a node has no
  children, it is defined as having a height of 0.

  @note  **O(n)** performance in all cases
  @return  the height of this node
  */
  template<typename E>
  inline int32_t TreeNode<E>::calcHeight(void) const
  {
    int32_t lh = 0;
    if (left_) lh = left_->calcHeight() + 1;
    int32_t rh = 0;
    if (right_) rh = right_->calcHeight() + 1;
    return lh > rh ? lh : rh;
  }

  /**
  The height of this node above the deepest descendant. If a node has no
  children, it is defined as having a height of 0.

  @note  **O(n)** for first call, **O(1)** thereafter until node is changed
  @return  the height of this node
  */
  template<typename E>
  inline int32_t TreeNode<E>::height(void)
  {
    // if we have a cached value then return it
    if (height_ >= 0) return height_;

    // height is maximum of left and right node heights
    int32_t lh = 0;
    if (left_) lh = left_->height() + 1;
    int32_t rh = 0;
    if (right_) rh = right_->height() + 1;
    height_ = lh > rh ? lh : rh;
    return height_;
  }

  /**
  Compute the size is the number of nodes that are direct or indirect children
  of this node, plus one for the node itself.

  @note  **O(n)** performance in all cases
  @return  the size of the subtree
  */
  template<typename E>
  inline int32_t TreeNode<E>::calcSize(void) const
  {
    int32_t siz = 1;
    if (left_) siz += left_->calcSize();
    if (right_) siz += right_->calcSize();
    return siz;
  }

  /**
  The size is the number of nodes that are direct or indirect children
  of this node, plus one for the node itself.

  @note  **O(n)** for first call, **O(1)** thereafter until node is changed
  @return  the size of the subtree
  */
  template<typename E>
  inline int32_t TreeNode<E>::size(void)
  {
    // if we have a cached value then return it
    if (size_ > 0) return size_;

    // size is sum of left and right node sizes, plus one for this node
    int32_t siz = 1;
    if (left_) siz += left_->size();
    if (right_) siz += right_->size();
    size_ = siz;
    return size_;
  }

  /**
  Compute the "balance" of this node, which is the difference between the max
  height on the left and right. The balance is positive if the right height
  is deeper than the left and negative if the left height is deeper. Zero
  height indicates perfect balance.

  @note  **O(n)** performance in all cases
  @return the balance between this node's child heights
  */
  template<typename E>
  inline int32_t TreeNode<E>::calcBalance(void) const
  {
    int32_t ret = 0;
    if (left_) ret -= left_->calcHeight() + 1;
    if (right_) ret += right_->calcHeight() + 1;
    return ret;
  }

  /**
  The "balance" of this node, which is the difference between the max
  height on the left and right. The balance is positive if the right height
  is deeper than the left and negative if the left height is deeper. Zero
  height indicates perfect balance.

  @note  **O(n)** for first call, **O(1)** thereafter until node is changed
  @return the balance between this node's child heights
  */
  template<typename E>
  inline int32_t TreeNode<E>::balance(void)
  {
    int32_t ret = 0;
    if (left_) ret -= left_->height() + 1;
    if (right_) ret += right_->height() + 1;
    return ret;
  }

  /**
  Invalidate the height for this node and any valid ancestors.

  @see height(), size()
  */
  template<typename E>
  inline void TreeNode<E>::dirty(void)
  {
    // invalidate heights
    TreeNode<E>* hnod = this;
    while (hnod && hnod->height_ >= 0)
    {
      hnod->height_ = -1;
      hnod = hnod->parentptr_;
    }
    // invalidate sizes
    TreeNode<E>* snod = this;
    while (snod && snod->size_ > 0)
    {
      snod->size_ = -1;
      snod = snod->parentptr_;
    }
  }

}

#endif
