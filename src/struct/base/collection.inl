#ifndef VILN_COLLECTION_INL
#define VILN_COLLECTION_INL

#include <stddef.h>
#include <stdlib.h>
#include <new>

#include "iterable.inl"

#include "collection.hpp"

/// \cond DO_NOT_DOCUMENT
namespace viln_hidden
{
  /**
  Allocate memory to fit the specified number of elements of a type.

  @param num  the number of elements to allocate
  @return  a pointer to the allocated memory
  @see  vfree
  */
  template<typename E>
  inline E* vmalloc(size_t num = 1)
  {
    return (E*) new char[num * sizeof(E)];
  }

  /**
  Free the block of memory for the specified array pointer.

  @param ptr  the pointer to the memory being freed
  @see  vmalloc
  */
  // Free the memory for the specified array pointer.
  template<typename E>
  inline void vfree(E* ptr)
  {
    delete[] ((char*) ptr);
  }
}
/// \endcond

namespace viln
{
  /**
  Add a copy of each element from the provided collection to this one. This can
  be thought of as a union operation.

  @param c  the collection of elements to copy
  @return  true iff at least one element was added
  */
  template<typename E>
  inline bool Collection<E>::addAll(const ConstIterable<E>& c)
  {
    bool ret = false;
    // Self-add would invalidate the iterator, so need a second copy of data.
    if (this == &c)
    {
      const E* dataCopy = this->toArray();
      const size_t sizeCopy = this->size();
      for (size_t idx = 0; idx < sizeCopy; ++idx)
      {
        ret |= add(dataCopy[idx]);
      }
      delete[] dataCopy;
    }
    // Add each element of the other collection to this one.
    else
    {
      ret = addAll(c.iterator());
    }
    return ret;
  }

  /**
  Add all the elements provided by the given iterator to this collection.
  Note that if the iterator is not finite, then this operation will never
  terminate.

  @warning If the iterator points to this collection, the behavior is undefined.

  @param itr  an iterator over elements to add (takes ownership)
  @return  true iff at least one element was added
  */
  template<typename E>
  inline bool Collection<E>::addAll(Iterator<const E>* itr)
  {
    bool ret = false;
    while (itr->hasNext())
    {
      ret |= add(itr->next());
    }
    delete itr;
    return ret;
  }

  /**
  Remove each element in the provided collection from this one. This can be
  thought of as a subtraction operation.

  @param c  the collection of elements to remove
  @return  true iff at least one element was removed
  */
  template<typename E>
  inline bool Collection<E>::removeAll(const ConstIterable<E>& c)
  {
    bool ret = false;
    // Self-removal is the same as clear.
    if (this == &c)
    {
      ret = !isEmpty();
      clear();
    }
    // Remove each element of the other list.
    else
    {
      ret = removeAll(c.iterator());
    }
    return ret;
  }

  /**
  Remove all the elements provided by the given iterator from this collection.
  Note that if the iterator is not finite, then this operation will never
  terminate.

  @warning If the iterator points to this collection, the behavior is undefined.

  @param itr  an iterator over elements to remove (takes ownership)
  @return  true iff at least one element was removed
  */
  template<typename E>
  inline bool Collection<E>::removeAll(Iterator<const E>* itr)
  {
    bool ret = false;
    while (itr->hasNext()) ret |= remove(itr->next());
    delete itr;
    return ret;
  }

  /**
  Determine if this collection contains every element in the provided
  collection.

  @param c  the collection of elements to search for
  @return  true iff all elements are contained in this collection
  */
  template<typename E>
  inline bool Collection<E>::containsAll(const ConstIterable<E>& c) const
  {
    if (this == &c) return true;
    return containsAll(c.iterator());
  }

  /**
  Determine if this collection contains every element returned by the
  given iterator. This action consumes the iterator. Note that if the
  provided iterator is not finite, then this method will never complete.

  @param itr  an iterator providing elements to search for (takes ownership)
  @return  true iff all elements are contained in this collection
  */
  template<typename E>
  inline bool Collection<E>::containsAll(Iterator<const E>* itr) const
  {
    bool ret = true;
    while (itr->hasNext())
    {
      const E& elem = itr->next();
      if (!contains(elem))
      {
        delete itr;
        return false;
      }
    }
    delete itr;
    return true;
  }

  /**
  Determine if this collection has no elements.

  @return  true iff there are no elements in the list
  */
  template<typename E>
  inline bool Collection<E>::isEmpty(void) const
  {
    return size() == 0;
  }

  /**
  Create an array containing copies of the elements of this collection. The
  caller takes ownership of the resulting array. The number of elements
  is guaranteed to be equal to the value of `size()` at the time this method
  was called.

  @return  an array of copies of elements (caller takes ownership)
  */
  template<typename E>
  inline E* Collection<E>::toArray(void) const
  {
    E* ret = viln_hidden::vmalloc<E>(size());
    const E* after = ret + size();
    Iterator<const E>* itr = this->iterator();
    for (E* ptr = ret; ptr < after && itr->hasNext(); ++ptr)
    {
      new ((void*)ptr) E(itr->next());
    }
    delete(itr);
    return ret;
  }

}

#endif
