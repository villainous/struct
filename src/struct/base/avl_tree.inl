#ifndef VILN_AVL_TREE_INL
#define VILN_AVL_TREE_INL

#include <stdint.h>

#include "tree_node.inl"
#include "tree_node_iterator.inl"
#include "avl_tree.hpp"

namespace viln
{
  template<typename E>
  inline AvlTree<E>::AvlTree(E* e):
    root_(new TreeNode<E>(e))
  {
    // no-op
  }

  template<typename E>
  inline AvlTree<E>::AvlTree(AvlTree<E>&& o) :
    root_(o.root_)
  {
    o.root_ = nullptr;
  }

  template<typename E>
  inline AvlTree<E>::~AvlTree(void)
  {
    if (root_) delete root_;
  }

  /**
  Add a node to the tree at the leftmost position that is right of the
  given node. If the specified node does not have a right child, then this will
  add the element as the right child of this node.

  @warning If the added node has children, this will break the AVL property!

  @note **O(log(n))** worst-case performance
  @param nod  the parent node under which the element will be added
  @param enod  the node to add (ownership transferred to this tree)
  */
  template<typename E>
  void AvlTree<E>::addSuccessor(TreeNode<E>* const nod, TreeNode<E>* enod)
  {
    // if there is already a right node then add this as its leftmost child
    if (nod->rightChild())
    {
      TreeNode<E>* rnod = nod->rightChild();
      while (rnod->leftChild())
      {
        rnod = rnod->leftChild();
      }
      rnod->setLeft(enod);
    }
    // otherwise we can safely add the node directly as the right child
    else
    {
      nod->setRight(enod);
    }
    // look for and repair up to one imbalance in ancestor nodes
    TreeNode<E>* pnod = enod->parent();
    while (pnod)
    {
      pnod = rebalance(pnod) ? nullptr : pnod->parent();
    }
    // rebalance may have rotated the root down
    root_ = root_->root();
  }

  /**
  Add a node to the tree at the rightmost position that is left of the
  given node. If the specified node does not have a left child, then this will
  add the element as the left child of this node.

  @warning If the added node has children, this will break the AVL property!

  @note **O(log(n))** worst-case performance
  @param nod  the parent node under which the element will be added
  @param enod  the node to add (ownership transferred to this tree)
  */
  template<typename E>
  void AvlTree<E>::addPredecessor(TreeNode<E>* const nod, TreeNode<E>* enod)
  {
    // if there is already a left node then add this as its rightmost child
    if (nod->leftChild())
    {
      TreeNode<E>* lnod = nod->leftChild();
      while (lnod->rightChild())
      {
        lnod = lnod->rightChild();
      }
      lnod->setRight(enod);
    }
    // otherwise we can safely add the node directly as the left child
    else
    {
      nod->setLeft(enod);
    }
    // look for and repair up to one imbalance in ancestor nodes
    TreeNode<E>* pnod = enod->parent();
    while (pnod)
    {
      pnod = rebalance(pnod) ? nullptr : pnod->parent();
    }
    // rebalance may have rotated the root down
    root_ = root_->root();
  }

  /**
  Add an element to the tree at the leftmost position that is right of the
  given node. If the specified node does not have a right child, then this will
  add the element as the right child of this node.

  @note **O(log(n))** worst-case performance
  @param nod  the parent node under which the element will be added
  @param e  the element to add (ownership transferred to this tree)
  @return  the node created by this operation
  */
  template<typename E>
  inline TreeNode<E>*
    AvlTree<E>::addSuccessor(TreeNode<E>* const nod, E* e)
  {
    TreeNode<E>* enod = new TreeNode<E>(e);
    addSuccessor(nod, enod);
    return enod;
  }

  /**
  Add an element to the tree at the rightmost position that is left of the
  given node. If the specified node does not have a left child, then this will
  add the element as the left child of this node.

  @note **O(log(n))** worst-case performance
  @param nod  the parent node under which the element will be added
  @param e  the element to add (ownership transferred to this tree)
  @return  the node created by this operation
  */
  template<typename E>
  inline TreeNode<E>*
    AvlTree<E>::addPredecessor(TreeNode<E>* const nod, E* e)
  {
    TreeNode<E>* enod = new TreeNode<E>(e);
    addPredecessor(nod, enod);
    return enod;
  }

  /**
  Remove the specified node from the tree and delete it. The rest of the tree
  is rearranged to repair the missing element.

  @note **O(log(n))** worst-case performance
  @param nod  the node to remove from the tree
  */
  template<typename E>
  inline void AvlTree<E>::remove(TreeNode<E>* const nod)
  {
    // find and detach a replacement for the node to be removed
    TreeNode<E>* rnod = nullptr;
    TreeNode<E>* rpnt = nullptr;
    // if we have both children, then need to locate and promote a leaf
    if (nod->leftChild() && nod->rightChild())
    {
      // use the next node to the left as the replacement
      rnod = nod->leftChild();
      while (rnod->rightChild())
      {
        rnod = rnod->rightChild();
      }
      // if the replacement has children to the left then promote them
      rpnt = rnod->parent();
      if (rpnt->leftChild() == rnod)
      {
        rpnt->detachLeft(); // TAKE OWNERSHIP
        rpnt->setLeft(rnod->detachLeft());
      }
      else // tpnt->right_ == tnod
      {
        rpnt->detachRight(); // TAKE OWNERSHIP
        rpnt->setRight(rnod->detachLeft());
      }
      // give the node's children to the replacement node
      rnod->setLeft(nod->detachLeft());
      rnod->setRight(nod->detachRight());
    }
    // if we have only a left node then directly promote it
    else if (nod->leftChild())
    {
      rnod = nod->detachLeft(); // TAKE OWNERSHIP
    }
    // if we have only a right node then directly promote it
    else if (nod->rightChild())
    {
      rnod = nod->detachRight(); // TAKE OWNERSHIP
    }

    // if removing the root then update the root pointer to the replacement
    if(root_ == nod)
    {
      root_ = rnod;
    }
    // update the removed node's parent to reference the replacement
    else
    {
      TreeNode<E>* pnt = nod->parent();
      if (pnt->leftChild() == nod)
      {
        pnt->detachLeft();
        pnt->setLeft(rnod);
      }
      else // pnt->right_ == nod
      {
        pnt->detachRight();
        pnt->setRight(rnod);
      }
    }
    // rebalance the nodes along the path from the deepest changed node
    TreeNode<E>* deep = rpnt ? rpnt : rnod;
    while (deep)
    {
      rebalance(deep);
      deep = deep->parent();
    }
    // rebalance may have rotated the root down
    if (root_) root_ = root_->root();
    // finally delete the isolated node
    delete nod;
  }

  template<typename E>
  inline TreeNode<E>* AvlTree<E>::root(void)
  {
    return root_;
  }

  template<typename E>
  inline const TreeNode<E>* AvlTree<E>::root(void) const
  {
    return root_;
  }

  /**
  Perform an AVL triple-rotation to reduce the imbalance at the specified
  node by one. If the node is already balanced, then this is a no-op.

  @param nod  the imbalanced node to rotate
  @return  true iff a rotation was performed
  */
  template<typename E>
  inline bool AvlTree<E>::rebalance(TreeNode<E>* const nod)
  {
    int32_t bal = nod->balance();
    bool changed = false;
    // unbalanced such that the right is higher
    if (bal > 1)
    {
      if (nod->rightChild()->balance() < 0)
      {
        rotateRight(nod->rightChild());
      }
      rotateLeft(nod);
      changed = true;
    }
    // unbalanced so the left is higher
    else if (bal < -1)
    {
      if (nod->leftChild()->balance() > 0)
      {
        rotateLeft(nod->leftChild());
      }
      rotateRight(nod);
      changed = true;
    }
    return changed;
  }

  /**
  A "left rotation" moves the given node down one level and to the left.
  The surrounding nodes (including the parent, if it exists) are updated to
  reflect this change. The return value is the right child of this node, which
  is the new root of the subtree. This is the inverse of `rotateRight()`.
  ```
         y                          x
        / \     Right Rotation     / \
       x  T3    - - - - - - ->    T1  y
      / \       <- - - - - - -       / \
     T1 T2      Left Rotation       T2 T3
  ```
  @param nod  the node to rotate
  @return  the node that replaces this one as the root of the subtree
  */
  template<typename E>
  inline void AvlTree<E>::rotateLeft(TreeNode<E>* const nod)
  {
    // name relevant pointers to make this easier to read
    TreeNode<E>* x = nod;
    TreeNode<E>* y = x->detachRight();
    TreeNode<E>* p = x->parent();
    TreeNode<E>* t2 = y->detachLeft();

    // update parent
    if (p)
    {
      bool left = p->leftChild() == x;
      x->detach();
      left ? p->setLeft(y) : p->setRight(y);
    }
    // update root
    else
    {
      root_ = y;
    }

    // rotate elements
    y->setLeft(x);
    x->setRight(t2);
  }

  /**
  A "right rotation" moves the given node down one level and to the right.
  The surrounding nodes (including the parent, if it exists) are updated to
  reflect this change. The return value is the left child of this node, which
  is the new root of the subtree. This is the inverse of `rotateLeft()`.
  ```
         y                          x
        / \     Right Rotation     / \
       x  T3    - - - - - - ->    T1  y
      / \       <- - - - - - -       / \
     T1 T2      Left Rotation       T2 T3
  ```
  @param nod  the node to rotate
  @return  the node that replaces this one as the root of the subtree
  */
  template<typename E>
  inline void AvlTree<E>::rotateRight(TreeNode<E>* const nod)
  {
    // name relevant pointers to make this easier to read
    TreeNode<E>* y = nod;
    TreeNode<E>* x = y->detachLeft();
    TreeNode<E>* p = y->parent();
    TreeNode<E>* t2 = x->detachRight();

    // update parent
    if (p)
    {
      bool left = p->leftChild() == y;
      y->detach();
      left ? p->setLeft(x) : p->setRight(x);
    }
    // update root
    else
    {
      root_ = x;
    }

    // rotate elements
    x->setRight(y);
    y->setLeft(t2);
  }

}

#endif
