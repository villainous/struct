#ifndef VILN_SHARED_REF_HPP
#define VILN_SHARED_REF_HPP

#include <stdint.h>

namespace viln
{
  /// A kind of smart-pointer offering reference-type data-access.
  /**
  The `SharedRef` is a smart-pointer, but unlike the STL `smart_ptr` allows
  automatic type-promotion from a pointer, and access as *reference* rather than
  as *pointer*. These differences make it better for use within the VILN
  Collections library, where it enables:

  ```
  // Collection of items that do not implement a copy-constructor.
  Collection<SharedRef<uncopyable_type>>;
  // Collection of base-classes of some polymorphic type.
  Collection<SharedRef<base_class>>;
  // Map with values that do not implement a copy-constructor.
  Map<key_type, SharedRef<uncopyable_type>>;
  // Map where the value-type is the base-class of a polymorphic type.
  Map<key_type, SharedRef<base_class>>;
  ```

  Note that since the references are being shared, changes to one reference
  will affect the other reference. If a deep-copy is required or desired, this
  is not an appropriate solution.
  */
  template<typename E>
  class SharedRef
  {
  public:
    /// Create a new shared-ref which owns the specified pointer.
    SharedRef(E* dat);
    /// Copy-constructor shares the same element.
    SharedRef(const SharedRef& o);
    /// Destructor.
    ~SharedRef(void);

    /// Conversion-operator to element-ref.
    operator E&(void);
    /// Conversion-operator to element-ref (const).
    operator const E&(void) const;

    /// Comparison-operator against another shared-ref.
    bool operator==(const SharedRef& o) const;
    bool operator!=(const SharedRef& o) const;

    /// Explicitly access the data.
    E& data(void);
    /// Explicitly access the data (const).
    const E& data(void) const;

  private:
    // the shared element
    E* const data_;
    // count shared among references
    int32_t* count_;
  };
}
#endif
