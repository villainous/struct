#ifndef VILN_KEY_VALUE_PAIR_INL
#define VILN_KEY_VALUE_PAIR_INL

#include <stddef.h>
#include <tuple>

#include "key_value_pair.hpp"

namespace viln
{
  /**
  Construct a key/value pair. The key is passed as a reference and copied
  into this object, but the value must be provided as a pointer. This
  places as few restrictions as possible on the value-type.

  @param k  the key
  @param v  a value-pointer (this takes ownership) (not `NULL`)
  */
  template<typename K, typename V>
  inline KeyValuePair<K,V>::KeyValuePair(const K& k,V* v) :
    key_(k),
    value_(v)
  {
    // no-op
  }

  template<typename K, typename V>
  inline KeyValuePair<K,V>::KeyValuePair(KeyValuePair<K,V>&& o) :
    key_(o.key_),
    value_(o.value_)
  {
    o.value_ = nullptr;
  }

  template<typename K, typename V>
  inline KeyValuePair<K,V>::~KeyValuePair(void)
  {
    if (value_) delete value_;
  }

  /**
  Check the validity of this pair.

  @return  true iff this pair has a value
  */
  template<typename K, typename V>
  inline KeyValuePair<K,V>::operator bool(void) const
  {
    return value_;
  }

  template<typename K, typename V>
  V& KeyValuePair<K,V>::operator*(void)
  {
    return *value_;
  }

  template<typename K, typename V>
  V* const KeyValuePair<K,V>::operator->(void)
  {
    return value_;
  }

  template<typename K, typename V>
  const V& KeyValuePair<K,V>::operator*(void) const
  {
    return *value_;
  }

  template<typename K, typename V>
  const V* KeyValuePair<K,V>::operator->(void) const
  {
    return value_;
  }

  /**
  Change the value associated with this key.

  @param v  a value-pointer (assumes ownership) (not `NULL`)
  */
  template<typename K, typename V>
  inline void KeyValuePair<K,V>::put(V* v)
  {
    if (value_) delete value_;
    value_ = v;
  }

  /**
  Release the value-pointer to the caller. Following this call the pair is
  no longer valid, and should be deleted or must have a new value replaced.

  @param v  a value-pointer (assumes ownership) (not `NULL`)
  @return  the value-pointer of this pair (caller owns)
  */
  template<typename K, typename V>
  inline V* KeyValuePair<K,V>::swap(V* v)
  {
    V* ret = value_;
    value_ = v;
    return ret;
  }

  template<typename K, typename V>
  inline const K& KeyValuePair<K,V>::key(void) const
  {
    return key_;
  }

  template<typename K, typename V>
  inline V& KeyValuePair<K,V>::value(void)
  {
    return *value_;
  }

  template<typename K, typename V>
  inline const V& KeyValuePair<K,V>::value(void) const
  {
    return *value_;
  }

  // c++17 structured bindings (const)
  /**
  Access the elements of the pair via structured-bindings.

  @see https://en.cppreference.com/w/cpp/language/structured_binding
  @tparam N  the element to access (key=1, value=2)
  @param kvp  the pair to access
  @return  the *key* or *value* (depending on `N`)
  */
  template<size_t N, typename K, typename V>
  const std::tuple_element_t<N, viln::KeyValuePair<K, V>>&
    get(const viln::KeyValuePair<K, V>& kvp)
  {
    static_assert(N < 2);
    if constexpr (N == 0) return kvp.key();
    if constexpr (N == 1) return kvp.value();
  }
}

namespace std
{
  template<typename K, typename V>
  struct tuple_size<viln::KeyValuePair<K,V>>
  {
    static constexpr size_t value = 2;
  };

  template<typename K, typename V>
  struct tuple_element<0, viln::KeyValuePair<K,V>>
  {
    using type = K;
  };

  template<typename K, typename V>
  struct tuple_element<1, viln::KeyValuePair<K,V>>
  {
    using type = V;
  };
}

#endif
