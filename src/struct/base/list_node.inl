#ifndef VILN_LIST_NODE_INL
#define VILN_LIST_NODE_INL

#include "list_node.hpp"

namespace viln
{
  /**
  Construct a `ListNode` holding the provided data. The new node takes
  ownership over the provided data. The previous and next pointers for the node
  are initially null.

  @param dat  the data to be wrapped by this node (takes ownership)
  */
  template<typename E>
  inline ListNode<E>::ListNode(E* dat):
    prevptr_(nullptr),
    next_(nullptr),
    data_(dat)
  {
    // no-op
  }

  /**
  The destructor deletes this node and all subsequent nodes.
  */
  template<typename E>
  inline ListNode<E>::~ListNode(void)
  {
    if (prevptr_) prevptr_->next_ = nullptr;
    delete data_;
    if (next_)
    {
      prevptr_ = nullptr;
      ListNode<E>* chain = this;
      while (chain->next_) chain = chain->next_;
      while (chain && chain != this)
      {
        chain = chain->prevptr_;
        delete chain->next_;
      }
    }
  }

  /**
  Detach this node from its neighbors. The previous and next nodes, if they
  exist, are made to be neighbors of one another. The caller assumes ownership
  of the newly detached node.

  @return  a pointer to this node (caller assumes ownership)
  */
  template<typename E>
  inline ListNode<E>* ListNode<E>::detach(void)
  {
    if (next_)
    {
      next_->prevptr_ = prevptr_;
    }
    if (prevptr_)
    {
      prevptr_->next_ = next_;
    }
    next_ = nullptr;
    prevptr_ = nullptr;
    return this;
  }

  /**
  Split the node from its previous node. This breaks one chain of nodes into
  two. The caller assumes ownership of the newly detached node.

  @return  a pointer to this node (caller assumes ownership)
  */
  template<typename E>
  inline ListNode<E>* ListNode<E>::split(void)
  {
    if (prevptr_)
    {
      prevptr_->next_ = nullptr;
      prevptr_ = nullptr;
    }
    return this;
  }

  /**
  Append the provided node between this node and the current next node (if it
  exists). If the appended node has neighbors, then **all** neighbors are
  inserted rather than just the appended node itself.

  @param app  the node to be appended (this node takes ownership)
  */
  template<typename E>
  inline void ListNode<E>::append(ListNode<E>* app)
  {
    // walk to the first node to append
    ListNode<E>* app1 = app;
    while (app1->prevptr_)
    {
      app1 = app1->prevptr_;
    }
    // walk to the last node to append
    ListNode<E>* app2 = app;
    while (app2->next_)
    {
      app2 = app2->next_;
    }
    // rewire the attachments
    if (next_)
    {
      next_->prevptr_ = app2;
    }
    app2->next_ = next_;
    next_ = app1;
    app1->prevptr_ = this;
  }

  /**
  Prepend the provided node between this node and the current previous node
  (if it exists). If the appended node has neighbors, then **all** neighbors
  are inserted rather than just the appended node itself.

  @param pre  the node to be prepended (this node is owned by prepended one)
  */
  template<typename E>
  inline void ListNode<E>::prepend(ListNode<E>* pre)
  {
    // walk to the first node to preend
    ListNode<E>* pre1 = pre;
    while (pre1->prevptr_)
    {
      pre1 = pre1->prevptr_;
    }
    // walk to the last node to preend
    ListNode<E>* pre2 = pre;
    while (pre2->next_)
    {
      pre2 = pre2->next_;
    }
    // rewire the attachments
    if (prevptr_)
    {
      prevptr_->next_ = pre1;
    }
    pre1->prevptr_ = prevptr_;
    prevptr_ = pre2;
    pre2->next_ = this;

  }

  template<typename E>
  inline ListNode<E>* const ListNode<E>::nextNode(void)
  {
    return next_;
  }

  template<typename E>
  inline const ListNode<E>* ListNode<E>::nextNode(void) const
  {
    return next_;
  }

  template<typename E>
  inline ListNode<E>* const ListNode<E>::previousNode(void)
  {
    return prevptr_;
  }

  template<typename E>
  inline const ListNode<E>* ListNode<E>::previousNode(void) const
  {
    return prevptr_;
  }

  /**
  Data access. Note that the data is location-stable, so maintaining pointers
  to this data is safe so long as the node is not deleted.

  @return  the data held by this node
  */
  template<typename E>
  inline E& ListNode<E>::data(void)
  {
    return *data_;
  }


  /**
  Constant data access. Note that the data is location-stable, so maintaining
  pointers to this data is safe so long as the node is not deleted.

  @return  the data held by this node
  */
  template<typename E>
  inline const E& ListNode<E>::data(void) const
  {
    return *data_;
  }
}

#endif
