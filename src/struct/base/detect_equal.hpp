#ifndef VILN_DETECT_EQUALS_HPP
#define VILN_DETECT_EQUALS_HPP

#include <stddef.h>
#include <stdint.h>

namespace viln
{
  // --- FUNCTIONS

  /// Convenience function-pointer type matching `operator==`.
  template<typename U>
  using equal_func = bool(*)(const U&, const U&);

  /// A a concept detecting the free-function `equal(U,U)`.
  template <typename T>
  concept has_equal_free = requires (T t1, T t2) { equal(t1, t2); };
  template <typename T>
  concept prefer_equal_free = (has_equal_free<T>);
  /// A pass-through to `equal(U,U)` when available.
  template <prefer_equal_free U>
  inline bool detect_equal(const U& u1, const U& u2)
  {
    return equal(u1, u2);
  }

  /// A concept detecting `operator==`.
  template <typename T>
  concept has_equal_op = requires (T t1, T t2) { t1 == t2; };
  template <typename T>
  concept prefer_equal_op =
    (has_equal_op<T> && !has_equal_free<T>);
  /// A pass-through to `operator==` when available.
  template<prefer_equal_op U>
  inline bool detect_equal(const U& u1, const U& u2)
  {
    return u1 == u2;
  }


  // --- CLASS EqualOp

  /// A equals operation is an abstract base for classes implementing "==".
  /**
  The `EqualOp` class is an abstract specification of the "==" operation. This
  allows for more flexibility than a function-pointer alone, since it allows
  the implementer to add data.
  */
  template <typename U>
  class EqualOp
  {
  public:
    /// Pure virtual destructor.
    virtual ~EqualOp(void) {}

    /// A user-defined equals operator.
    virtual bool equal(const U& u1, const U& u2) const = 0;
  };


  // --- CLASS EqualFunc

  /// A equals operation that wraps a equals function.
  /**
  The `EqualFunc` class allows use of a function-pointer as an operator
  class.
  */
  template <typename U>
  class EqualFunc : public EqualOp<U>
  {
  public:
    /// Construct a equals operator wrapping the specified function.
    EqualFunc(equal_func<U> func);
    /// Pure virtual destructor.
    virtual ~EqualFunc(void) {}

    /// A user-defined equals operator.
    virtual bool equal(const U& u1, const U& u2) const override;

  private:
    // function being wrapped
    equal_func<U> func_;
  };


  // --- CLASS DetectEqual

  /// Detects and commonizes the equals function from the template parameter.
  /**
  This interface provides a virtual equals function that attempts to detect
  existing `==` implementations using template metaprogramming. This allows
  subclasses of this interface to use or override this behavior without
  needing specific handling depending on the template parameter.

  Specializations exist that automatically use the common equality operators:
  ```
  bool U::operator==(U)
  bool operator==(U, U)
  ```
  */
  template <typename U>
  class DetectEqual : public EqualOp<U>
  {
  public:
    /// Pure virtual destructor.
    virtual ~DetectEqual(void) {}

    /// An automatically detected equals operator implementation.
    virtual bool equal(const U& u1, const U& u2) const override
    {
      // if operator== not defined, then use pointer equality
      return &u1 == &u2;
    }
  };

  /// A concept aggregating any of the available `==`-sources.
  template <typename T>
  concept has_equal = requires (T t1, T t2)
  {
    viln::detect_equal(t1, t2);
  };
  /// Specialization of `DetectEqual` for auto-detected ops.
  template<has_equal U>
  class DetectEqual<U> : public EqualOp<U>
  {
  public:
    virtual ~DetectEqual(void) {}

    virtual bool equal(const U& u1, const U& u2) const override
    {
      return viln::detect_equal(u1, u2);
    }
  };
}

#endif
