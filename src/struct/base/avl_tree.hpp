#ifndef VILN_AVL_TREE_HPP
#define VILN_AVL_TREE_HPP

namespace viln
{
  /// \cond DO_NOT_DOCUMENT
  template<typename E> class TreeNode;
  template<typename E> class TreeNodeIterator;
  template<typename E> class TreeList;
  /// \endcond

  /// An auto-balancing binary tree.
  /**
  This class provides mechanisms for interacting with `TreeNode`s which keeps
  the left and right subtrees of each node balanced to within ±1 (the AVL tree
  property). This ensures that the maximum depth of the tree is at
  most **O(log(n))**, while only requiring **O(log(n))** overhead for the
  operations provided.

  Note that direct modification of the nodes in the tree may break the AVL
  property. While the methods provided here would still function, the
  performance guarantees would no longer hold.
  */
  template<typename E>
  class AvlTree
  {
  public:
    /// Construct a tree with the given element as the root.
    AvlTree(E* e);
    /// Move-constructor.
    AvlTree(AvlTree<E>&& o);
    /// Destructor.
    virtual ~AvlTree(void);

    // not copyable!
    AvlTree(const AvlTree&) = delete;
    AvlTree& operator=(const AvlTree&) = delete;

    /// Add a node containing the provided element as the next on the right.
    TreeNode<E>* addSuccessor(TreeNode<E>* const nod, E* e);
    /// Add a node containing the provided element as the next on the left.
    TreeNode<E>* addPredecessor(TreeNode<E>* const nod, E* e);
    /// Remove this node and rearrange the tree to account for it.
    void remove(TreeNode<E>* const nod);

    /// Access the root node of the tree.
    TreeNode<E>* root(void);
    const TreeNode<E>* root(void) const;

  protected:
    /// Rebalance a specific node.
    bool rebalance(TreeNode<E>* const nod);

    /// Rotate the node down one level and to the left.
    void rotateLeft(TreeNode<E>* const nod);
    /// Rotate the node down one level and to the right.
    void rotateRight(TreeNode<E>* const nod);

    /// Add a node containing the provided element as the next on the right.
    void addSuccessor(TreeNode<E>* const nod, TreeNode<E>* enod);
    /// Add a node containing the provided element as the next on the left.
    void addPredecessor(TreeNode<E>* const nod, TreeNode<E>* enod);

    friend class TreeList<E>;

  private:
    // the root of the tree being managed
    TreeNode<E>* root_;
  };
}

#endif
