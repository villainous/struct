#ifndef VILN_LIST_NODE_HPP
#define VILN_LIST_NODE_HPP

namespace viln
{
  /// A data wrapper element of a doubly-linked-list structure.
  /**
   Each node maintains ownership of its next node, and is owned by its
   previous node. `LinkedList` elements are contained in nodes.
  */
  template<typename E>
  class ListNode
  {
  public:
    /// Construct a node that owns the provided data.
    ListNode(E* dat);
    /// Destructor.
    ~ListNode(void);

    // not copyable!
    ListNode(const ListNode&) = delete;
    ListNode(ListNode<E>&&) = delete;
    ListNode& operator=(const ListNode&) = delete;

    /// Remove references from and to this node from its neighbors.
    ListNode<E>* detach(void);
    /// Break the connection between this node and its previous one.
    ListNode<E>* split(void);
    /// Append the provided node as the next node of this one.
    void append(ListNode<E>* nxt);
    /// Prepend the provided node as the previous node of this one.
    void prepend(ListNode<E>* prvs);

    /// Get the next node.
    ListNode<E>* const nextNode(void);
    const ListNode<E>* nextNode(void) const;
    /// Get the previous node.
    ListNode<E>* const previousNode(void);
    const ListNode<E>* previousNode(void) const;
    /// Access the data.
    E& data(void);
    const E& data(void) const;

  private:
    // the previous node (not owned by this)
    ListNode<E>* prevptr_;
    // the next node (owned by this)
    ListNode<E>* next_;
    // the data being held (owned by this)
    E* const data_;
  };
}

#endif
