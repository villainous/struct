#ifndef VILN_DETECT_ORDER_HPP
#define VILN_DETECT_ORDER_HPP

#include <stdint.h>

namespace viln
{
  // --- FUNCTIONS

  /// Convenience function-pointer type matching `operator<`.
  template<typename U>
  using order_func = bool(*)(const U&, const U&);

  /// A a concept detecting the free-function `order(U,U)`.
  template <typename T>
  concept has_order_free = requires (T t1, T t2) { order(t1, t2); };
  template <typename T>
  concept prefer_order_free = (has_order_free<T>);
  /// A pass-through to `order(U,U)` when available.
  template <prefer_order_free U>
  inline bool detect_order(const U& u1, const U& u2)
  {
    return order(u1, u2);
  }
  
  /// A concept detecting `operator<`.
  template <typename T>
  concept has_less_than_op = requires (T t1, T t2) { t1 < t2; };
  template <typename T>
  concept prefer_less_than_op =
    (has_less_than_op<T> && !has_order_free<T>);
  /// A pass-through to `operator<` when available.
  template<prefer_less_than_op U>
  inline bool detect_order(const U& u1, const U& u2)
  {
    return u1 < u2; 
  }
  
  /// A concept detecting `operator>`.
  template <typename T>
  concept has_greater_than_op = requires (T t1, T t2) { t1 > t2; };
  template <typename T>
  concept prefer_greater_than_op =
    (has_greater_than_op<T> && !has_less_than_op<T> && !has_order_free<T>);
  /// A pass-through to `!operator>` when available.
  template<prefer_greater_than_op U>
  inline bool detect_order(const U& u1, const U& u2)
  {
    return u2 > u1; 
  }


  // --- CLASS OrderOp

  /// A less-than operation is an abstract base for classes implementing "<".
  /**
  The `OrderOp` class is an abstract specification of the "<" operation. This
  allows for more flexibility than a function-pointer alone, since it allows
  the implementer to add data. A common example would be for "less-than" to
  mean "closest number to X" with "X" specified at runtime.
  */
  template <typename U>
  class OrderOp
  {
  public:
    /// Pure virtual destructor.
    virtual ~OrderOp(void) {}

    /// A user-defined less-than operator.
    virtual bool order(const U& u1, const U& u2) const = 0;
  };


  // --- CLASS OrderFunc

  /// A less-than operation that wraps a less-than function.
  /**
  The `OrderFunc` class allows use of a function-pointer as an operator
  class.
  */
  template <typename U>
  class OrderFunc : public OrderOp<U>
  {
  public:
    /// Construct a less-than operator wrapping the specified function.
    OrderFunc(order_func<U> func);
    /// Pure virtual destructor.
    virtual ~OrderFunc(void) {}

    /// A user-defined less-than operator.
    virtual bool order(const U& u1, const U& u2) const override;

  private:
    // function being wrapped
    order_func<U> func_;
  };


  // --- CLASS DetectOrder

  /// Detects and commonizes the less-than function from the template parameter.
  /**
  This interface provides a virtual less-than function that attempts to detect
  existing `<` implementations using template metaprogramming. This allows
  subclasses of this interface to use or override this behavior without
  needing specific handling depending on the template parameter.

  Specializations exist that automatically use the common less-than operators:
  ```
  bool U::operator<(U)
  bool operator<(U, U)
  ```
  */
  template <typename U>
  class DetectOrder : public OrderOp<U>
  {
  public:
    /// Pure virtual destructor.
    virtual ~DetectOrder(void) {}

    /// A less-than method must be implemented.
    virtual bool order(const U& u1, const U& u2) const = 0;
  };

  /// A concept aggregating any of the available order-sources.
  template <typename T>
  concept has_order = requires (T t1, T t2)
  {
    viln::detect_order(t1, t2);
  };
  /// Specialization of `DetectOrder` for auto-detected ops.
  template<has_order U>
  class DetectOrder<U> : public OrderOp<U>
  {
  public:
    virtual ~DetectOrder(void) {}

    virtual bool order(const U& u1, const U& u2) const override
    {
      return viln::detect_order(u1, u2);
    }
  };
}

#endif
