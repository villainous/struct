#ifndef VILN_DETECT_HASH_HPP
#define VILN_DETECT_HASH_HPP

#include <stdint.h>

#include "vmath.hpp"
#include <functional>

namespace viln
{

  // --- FUNCTIONS
  
  /// Convenience function-pointer type matching `hash`.
  template<typename U>
  using hash_func = uint64_t(*)(const U&);

  /// A concept detecting the method `U::hash()`.
  template <typename T>
  concept has_hash_method = requires (T t) { t.hash(); };
  template <typename T>
  concept prefer_hash_method = (has_hash_method<T>);
  /// A pass-through to `U::hash()` when available.
  template <prefer_hash_method U>
  inline uint64_t detect_hash(const U& u)
  {
    return u.hash();
  }

  /// A a concept detecting the free-function `hash(U)`.
  template <typename T>
  concept has_hash_free = requires (T t) { hash(t); };
  template <typename T>
  concept prefer_hash_free = (has_hash_free<T> && !has_hash_method<T>);
  /// A pass-through to `hash(U)` when available.
  template <prefer_hash_free U>
  inline uint64_t detect_hash(const U& u)
  {
    return hash(u);
  }
  
  /// A concept detecting STL `std::hash<U>`.
  template <typename T>
  concept has_hash_stl = requires (T t) { std::hash<T>{}(t); };
  template <typename T>
  concept prefer_hash_stl =
    (has_hash_stl<T> && !has_hash_method<T> && !has_hash_free<T>);
  /// A pass-through to STL `std::hash<U>` when available.
  template <prefer_hash_stl U>
  inline uint64_t detect_hash(const U& u)
  {
    return std::hash<U>{}(u);
  }
  

  // --- CLASS HashOp

  /// A hash-op is an abstract base for transforming objects into hashes.
  /**
  The `HashOp` class is an abstract specification of a hash-function. This
  allows for flexible (possible stateful) definitions of hash-functions
  beyond what is possible with function-pointers alone.
  */
  template <typename U>
  class HashOp
  {
  public:
    /// Pure virtual destructor.
    virtual ~HashOp(void) {}
    
    /// A user-defined hash method.
    virtual uint64_t hash(const U& u) const = 0;
  };


  // --- CLASS DetectHash

  /// Detects and commonizes the hash function from the template parameter.
  /**
  This interface provides a virtual hash function that attempts to detect
  existing hash implementations using template metaprogramming. This allows
  subclasses of this interface to use or override this behavior without
  needing specific handling depending on the template parameter.

  Specializations exist that automatically use the common hash-sources
  (in order of precedence):
  ```
  uint64_t U::hash(void);
  uint64_t hash(const U&);
  size_t std::hash<U>{}(const U&);
  ```
  */
  template <typename U>
  class DetectHash : public HashOp<U>
  {
  public:
    /// Pure virtual destructor.
    virtual ~DetectHash(void) {}
    
    /// A hash-method must be implemented.
    virtual uint64_t hash(const U& u) const = 0;
  };

  /// A concept aggregating available `hash`-sources.
  template <typename T>
  concept has_hash = requires (T t) { viln::detect_hash(t); };
  /// A `DetectHash` specialization using auto-detected ops.
  template <has_hash U>
  class DetectHash<U> : public HashOp<U>
  {
  public:
    virtual ~DetectHash(void) {}

    virtual uint64_t hash(const U& u) const override
    {
      return viln::detect_hash(u);
    }
  };

}

#endif
