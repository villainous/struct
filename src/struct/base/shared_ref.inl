#ifndef VILN_SHARED_REF_INL
#define VILN_SHARED_REF_INL

#include "shared_ref.hpp"

namespace viln
{
  /**
  Construct a `SharedRef` holding the provided data. The new node takes
  ownership over the provided data. The previous and next pointers for the node
  are initially null.

  Note that passing a `NULL` data-value has undefined behavior.

  @param dat  the data to be wrapped by this object (takes ownership)
  */
  template<typename E>
  inline SharedRef<E>::SharedRef(E* dat) :
    data_(dat),
    count_(new int32_t(1))
  {
    // no-op
  }

  /**
  The copy-constructor for a shared-ref increases the reference-count, and
  results in *both* objects referencing the *same* element.

  @param o  the ref to copy
  */
  template<typename E>
  inline SharedRef<E>::SharedRef(const SharedRef& o) :
    data_(o.data_),
    count_(o.count_)
  {
    ++(*count_);
  }

  /**
  The destructor decreases the ref-count. If the ref-count reaches zero, then
  it will free the shared element.
  */
  template<typename E>
  inline SharedRef<E>::~SharedRef(void)
  {
    --(*count_);
    if (*count_ <= 0)
    {
      delete data_;
      delete count_;
    }
  }

  /**
  Cast this ref as its element-type.

  @return  the element being managed by this ref
  */
  template<typename E>
  inline SharedRef<E>::operator E&(void)
  {
    return *data_;
  }

  /**
  Cast this ref as its element-type.

  @return  the element being managed by this ref
  */
  template<typename E>
  inline SharedRef<E>::operator const E&(void) const
  {
    return *data_;
  }

  /**
  Compare this ref to another. Two refs are equal if they are part of the
  same reference-counting pool - that is, they were copied from one another.

  @return  true iff the two refs are part of the same pool
  */
  template<typename E>
  inline bool SharedRef<E>::operator==(const SharedRef& o) const
  {
    // pointer-comparison uses count not data, since it cannot be leaked
    return count_ == o.count_;
  }

  /**
  Compare this ref to another. Two refs are equal if they are part of the
  same reference-counting pool - that is, they were copied from one another.

  @return  false iff the two refs are part of the same pool
  */
  template<typename E>
  inline bool SharedRef<E>::operator!=(const SharedRef& o) const
  {
    return !SharedRef::operator==(o);
  }

  /**
  Access the element being wrapped.

  @return  the element being managed by this ref
  */
  template<typename E>
  inline E& SharedRef<E>::data(void)
  {
    return *data_;
  }

  /**
  Access the element being wrapped.

  @return  the element being managed by this ref
  */
  template<typename E>
  inline const E& SharedRef<E>::data(void) const
  {
    return *data_;
  }
}

#endif
