#ifndef VILN_HASH_MAP_HPP
#define VILN_HASH_MAP_HPP

#include <stddef.h>

#include "detect_hash.hpp"
#include "detect_equal.hpp"
#include "map.hpp"

namespace viln
{
  /// \cond DO_NOT_DOCUMENT
  template<typename E> class ListNode;
  /// \endcond

  /// A map implementation that uses a hash-table for element storage.
  /**
  A `HashMap` is a `Map` implementation that uses a hash-table for key-
  lookup and value storage. This allows most single-element operations
  on the map to execute in **O(1)** at the expense of the ordering of
  elements being semi-random.

  This class uses `DetectHash<K>` and `DetectEqual<K>` for the templatedtype,
  so either `operator==` and `hash()` must be defined for the key-type,
  or the `equal(K,K)` and `hash(K)` methods should be overridden.

  This class does not implement thread-safety.

  Assuming a perfect hash-function and that the storage array does not require
  resizing, the performance of hash-map operations would be **O(m)**. The
  storage resizing, if needed, will add overhead, making the
  performance **O(m*log(n))** for operations that modify the map. If the
  provided hash-function is poor, the performance could be as bad as **O(m*n)**
  in the worst case. To ensure the best performance, a good hash-function must
  be provided, and the initial capacity should provide excess space beyond the
  anticipated size of the map.

  Note that modification of the map will invalidate any active iterators over
  the map.
  */
  template<typename K, typename V>
  class HashMap :
    public Map<K,V>,
    protected DetectHash<K>,
    protected DetectEqual<K>
  {
  public:
    /// Default constructor.
    HashMap(size_t cap = 2);
    /// Copy constructor.
    HashMap(const HashMap<K,V>& o);
    /// Move-constructor.
    HashMap(HashMap<K,V>&& o);
    /// Destructor.
    virtual ~HashMap(void);

    /// Assignment operator.
    HashMap<K,V>& operator=(const HashMap<K,V>& o);
    /// Move-assign operator.
    HashMap<K,V>& operator=(HashMap<K,V>&& o);

    /// Obtain an iterator over the key-value pairs in this map.
    virtual Iterator<KeyVal<K, V>>* iterator(void) override;
    /// Obtain an iterator over the key-value pairs in this map.
    virtual Iterator<const KeyVal<K,V>>* iterator(void) const override;

    /// Add the value associated with the given key (takes ownership).
    virtual bool add(const K& key, V* val);
    /// Add the value associated with the given key.
    virtual bool add(const K& key, V&& val) override;
    /// Add the value associated with the given key.
    virtual bool add(const K& key, const V& val) override;
    /// Add or replace the val associated with the given key.
    virtual void put(const K& key, V* val);
    /// Add or replace the val associated with the given key.
    virtual void put(const K& key, V&& val) override;
    /// Add or replace the val associated with the given key.
    virtual void put(const K& key, const V& val) override;
    /// Remove and delete the val associated with the given key.
    virtual bool remove(const K& key) override;
    /// Release the value associated with the given key (caller owns).
    virtual V* release(const K& key);
    /// Swap the values referenced by the specified keys.
    virtual bool swap(const K& k1, const K& k2) override;
    /// Remove all the vals and key-mappings from this map.
    virtual void clear(void) override;

    /// Determine if this map contains a val associated with the given key.
    virtual bool contains(const K& key) const override;
    /// Obtain the val associated with the given key.
    virtual V& at(const K& key) override;
    /// Obtain the value associated with the given key.
    virtual const V& at(const K& key) const override;
    /// Obtain the val associated with the given key if it exists.
    virtual V* const get(const K& key) override;
    /// Obtain the val associated with the given key if it exists.
    virtual const V* get(const K& key) const override;
    /// Get the number of key/val pairs in the map.
    virtual size_t size(void) const override;

  protected:
    /// Obtain the hash table's capacity.
    virtual size_t capacity(void) const;
    /// Update the hash table capacity to the given size.
    void setCapacity(size_t cap);
    /// Determine the table index for the given element.
    size_t index(const K& key) const;

  private:
    // get the key/value pair if it exists
    KeyVal<K,V>* getHelper(const K& key) const;
    // add an element with no checks to see if it is present
    void addHelper(const K& key, V* val);

    // the actual storage for the map
    ListNode<KeyVal<K,V>>** data_;
    // the size of the data array
    size_t capacity_;
    // the number of nodes in the table
    size_t size_;
  };
}

#endif
