#ifndef VILN_MAP_HPP
#define VILN_MAP_HPP

#include <stddef.h>

#include "key_value_pair.hpp"
#include "iterable.hpp"

namespace viln
{
  /// A group of values, indexed by a key-type.
  /**
  A `Map` represents a relationship between keys and values associated
  with those keys. The keys must be unique, but there is no requirement
  placed upon the values.
  */
  template<typename K, typename V>
  class Map :
    public Iterable<KeyVal<K,V>>
  {
  public:
    /// Constructor.
    Map(void);
    /// Virtual destructor.
    virtual ~Map(void) {};

    // not copyable!
    Map(const Map&) = delete;
    Map(Map&&) = delete;
    Map& operator=(const Map&) = delete;

    /// Obtain the value associated with the given key.
    virtual V& operator[](const K& key);
    /// Obtain the value associated with the given key.
    virtual const V& operator[](const K& key) const;

    /// Obtain an iterator over the key-value pairs in this map.
    virtual Iterator<const KeyVal<K,V>>* iterator(void) const = 0;
    /// Obtain an iterator over the key-value pairs in this map.
    virtual Iterator<KeyVal<K,V>>* iterator(void) = 0;
    /// Obtain an iterator over the keys in this map.
    virtual Iterator<const K>* keyIterator(void) const;
    /// Obtain an iterator over the values in this map.
    virtual Iterator<const V>* valueIterator(void) const;
    /// Obtain an iterator over the values in this map.
    virtual Iterator<V>* valueIterator(void);

    /// View the map keys as an iterable object.
    virtual const ConstIterable<K>& keys(void) const;
    /// View the map values as an iterable object.
    virtual Iterable<V>& values(void);
    virtual const Iterable<V>& values(void) const;

    /// Add the provided value with associated with the given key.
    virtual bool add(const K& key, V&& value) = 0;
    /// Add the provided value with associated with the given key.
    virtual bool add(const K& key, const V& value) = 0;
    /// Add all the values from another map to this one with the same keys.
    virtual bool addAll(const ConstIterable<KeyVal<K, V>>& o);
    /// Add or replace the value associated with the given key.
    virtual void put(const K& key, V&& value) = 0;
    /// Add or replace the value associated with the given key.
    virtual void put(const K& key, const V& value) = 0;
    /// Add all the values from another map to this one with the same keys.
    virtual void putAll(const ConstIterable<KeyVal<K, V>>& o);
    /// Remove and delete the value associated with the given key.
    virtual bool remove(const K& key) = 0;
    /// Remove and delete values associated with any of the provided keys.
    virtual bool removeAll(const ConstIterable<K>& keys);
    /// Remove and delete values associated with any of the provided keys.
    virtual bool removeAll(Iterator<const K>* keys);
    /// Swap the values referenced by the specified keys (if they exist).
    virtual bool swap(const K& k1, const K& k2) = 0;
    /// Remove all the values and key-mappings from this map.
    virtual void clear(void) = 0;

    /// Determine if this map contains a value associated with the given key.
    virtual bool contains(const K& key) const = 0;
    /// Determine if this map contains values for all the specified keys.
    virtual bool containsAll(const ConstIterable<K>& keys) const;
    /// Determine if this map contains values for all the specified keys.
    virtual bool containsAll(Iterator<const K>* keys) const;
    /// Obtain the value associated with the given key.
    virtual V& at(const K& key) = 0;
    /// Obtain the value associated with the given key.
    virtual const V& at(const K& key) const = 0;
    /// Obtain the value associated with the given key if it exists.
    virtual V* const get(const K& key) = 0;
    /// Obtain the value associated with the given key if it exists.
    virtual const V* get(const K& key) const = 0;
    /// Determine if this map is empty.
    virtual bool isEmpty(void) const;
    /// Get the number of key/value pairs in the map.
    virtual size_t size(void) const = 0;

  protected:
    /// Inner class providing an iterable interface over the map keys.
    class KeyView : public ConstIterable<K>
    {
    public:
      /// Construct a key-view of the given map.
      KeyView(Map* const map);
      /// Create a new iterator over the map keys (caller owns iterator).
      virtual Iterator<const K>* iterator(void) const override;
    private:
      // back-pointer to map
      const Map* mapPtr_;
    };

    /// Inner class providing an iterable interface over the map values.
    class ValueView : public Iterable<V>
    {
    public:
      /// Construct a value-view of the given map.
      ValueView(Map* const map);
      /// Create a new iterator over the map values (caller owns iterator).
      virtual Iterator<V>* iterator(void) override;
      virtual Iterator<const V>* iterator(void) const override;
    private:
      // back-pointer to map
      Map* const mapPtr_;
    };

  private:
    // the keys presented as iterable
    KeyView keyView_;
    // the values presented as iterable
    ValueView valueView_;
  };
}

#endif
