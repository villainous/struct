#ifndef VILN_TREE_MAP_HPP
#define VILN_TREE_MAP_HPP

#include <stddef.h>

#include "detect_order.hpp"
#include "map.hpp"

namespace viln
{
  /// \cond DO_NOT_DOCUMENT
  template<typename E> class TreeNode;
  template<typename E> class AvlTree;
  /// \endcond

  /// A map implementation that uses a tree sorted by keys for storage.
  /**
  A `TreeMap` is a `Map` implementation which uses an auto-balancing
  binary search tree for element storage. This allows iteration over
  the elements to progress *in ascending key-order*.

  This class uses `DetectOrder<K>` for the templated type, so either
  `operator<` must be defined for the key-type, or the `order(K,K)`
  method should be overridden. Equality is inferred from the less-than
  operation, so there is no requirement for a separate equality operation to
  be defined.

  This class does not implement thread-safety.

  Operations all have worst-case performance of **O(m*log(n))** where **m** is
  the size of the input (if applicable), and **n** is the size of the map.

  Note that modification of the map will invalidate any active iterators over
  the map.
  */
  template<typename K, typename V>
  class TreeMap :
    public Map<K,V>,
    protected DetectOrder<K>
  {
  public:
    /// Default constructor.
    TreeMap(void);
    /// Copy constructor.
    TreeMap(const TreeMap<K,V>& o);
    /// Move-constructor.
    TreeMap(TreeMap<K,V>&& o);
    /// Destructor.
    virtual ~TreeMap(void);

    /// Assignment operator.
    TreeMap<K,V>& operator=(const TreeMap<K,V>& o);
    /// Move-assign operator.
    TreeMap<K,V>& operator=(TreeMap<K,V>&& o);

    /// Obtain an iterator over the key-value pairs in this map.
    virtual Iterator<KeyVal<K,V>>* iterator(void) override;
    /// Obtain an iterator over the key-value pairs in this map.
    virtual Iterator<const KeyVal<K, V>>* iterator(void) const override;

    /// Add the value associated with the given key (takes ownership).
    virtual bool add(const K& key, V* val);
    /// Add the value associated with the given key.
    virtual bool add(const K& key, V&& val) override;
    /// Add the value associated with the given key.
    virtual bool add(const K& key, const V& val) override;
    /// Add or replace the value associated with the given key.
    virtual void put(const K& key, V* val);
    /// Add or replace the value associated with the given key.
    virtual void put(const K& key, V&& val) override;
    /// Add or replace the value associated with the given key.
    virtual void put(const K& key, const V& val) override;
    /// Remove and delete the value associated with the given key.
    virtual bool remove(const K& key) override;
    /// Release the value associated with the given key (caller owns).
    virtual V* release(const K& key);
    /// Swap the values referenced by the specified keys.
    virtual bool swap(const K& k1, const K& k2) override;
    /// Remove all the values and key-mappings from this map.
    virtual void clear(void) override;

    /// Determine if this map contains a value associated with the given key.
    virtual bool contains(const K& key) const override;
    /// Obtain the value associated with the given key.
    virtual V& at(const K& key) override;
    virtual const V& at(const K& key) const override;
    /// Obtain the value associated with the given key if it exists.
    virtual V* const get(const K& key) override;
    virtual const V* get(const K& key) const override;
    /// Get the number of key/value pairs in the map.
    virtual size_t size(void) const override;

    /// Obtain the value with the smallest key.
    virtual KeyVal<K,V>& first(void);
    virtual const KeyVal<K,V>& first(void) const;
    /// Obtain the value with the largest key.
    virtual KeyVal<K,V>& last(void);
    virtual const KeyVal<K,V>& last(void) const;
    /// Find the value with the next key following the one specified.
    virtual KeyVal<K,V>& successor(const K& key);
    virtual const KeyVal<K,V>& successor(const K& key) const;
    /// Find the value with the previous key preceding the one specified.
    virtual KeyVal<K,V>& predecessor(const K& key);
    virtual const KeyVal<K,V>& predecessor(const K& key) const;

  protected:
    /// Access the root element of this map.
    TreeNode<KeyVal<K,V>>* const root(void);
    /// Access the root element of this map.
    const TreeNode<KeyVal<K,V>>* root(void) const;

  private:
    // multiple return states for the search operation
    struct search_results
    {
      // node containing the element or parent
      TreeNode<KeyVal<K,V>>* node;
      // does the specified element exist in the set?
      bool found;
      // if not found, should add element as left child of node?
      bool left;
    };
    // find a tree node containing the specified element
    search_results searchHelper(const K& e) const;
    // helper for the various add methods
    bool addHelper(const K& key, V* val, const search_results& fn);

    // auto-balancing tree storage
    AvlTree<KeyVal<K,V>>* tree_;
    // size of the set
    size_t size_;
  };
}

#endif
