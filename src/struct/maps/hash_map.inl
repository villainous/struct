#ifndef VILN_HASH_MAP_INL
#define VILN_HASH_MAP_INL

#include <stddef.h>

#include "list_node.inl"
#include "hash_table_iterator.inl"
#include "detect_hash.inl"
#include "detect_equal.inl"
#include "map.inl"

#include "const_iterator.hpp"
#include "hash_map.hpp"

namespace viln
{
  /**
  The constructor will create a map with the specified initial capacity,
  with a default value of 2. The capacity of the map will automatically be
  updated once the hash-map becomes too full. Setting the initial capacity
  can reduce re-hashing overhead if the number of elements expected
  is known ahead of time.

  @param cap  initial capacity for the hash table
  */
  template<typename K, typename V>
  HashMap<K,V>::HashMap(size_t cap) :
    capacity_(cap < 2 ? 2 : cap),
    size_(0)
  {
    data_ = new ListNode<KeyVal<K,V>>*[capacity_]();
  }

  template<typename K, typename V>
  HashMap<K,V>::HashMap(const HashMap<K,V>& o) :
    data_(new ListNode<KeyVal<K,V>>*[o.capacity_]()),
    capacity_(o.capacity_),
    size_(0)
  {
    HashMap::addAll(o);
  }

  template<typename K, typename V>
  inline HashMap<K,V>::HashMap(HashMap<K,V>&& o) :
    data_(o.data_),
    capacity_(o.capacity_),
    size_(o.size_)
  {
    o.data_ = nullptr;
  }

  template<typename K, typename V>
  HashMap<K,V>::~HashMap(void)
  {
    if (data_)
    {
      HashMap::clear();
      delete[] data_;
    }
  }

  template<typename K, typename V>
  inline HashMap<K,V>& HashMap<K, V>::operator=(const HashMap<K,V>& o)
  {
    if (this == &o) return *this;
    HashMap::clear();
    HashMap::addAll(o);
    return *this;
  }

  template<typename K, typename V>
  inline HashMap<K,V>& HashMap<K, V>::operator=(HashMap<K,V>&& o)
  {
    if (this == &o) return *this;
    if (data_)
    {
      HashMap::clear();
      delete[] data_;
    }
    capacity_ = o.capacity_;
    size_ = o.size_;
    data_ = o.data_;
    o.data_ = nullptr;
    return *this;
  }

  /**
  Create an iterator over the key/value pairs in this map.

  @return  a new pair iterator (caller takes ownership)
  */
  template<typename K, typename V>
  inline Iterator<const KeyVal<K,V>>* HashMap<K,V>::iterator(void) const
  {
    HashTableIterator<KeyVal<K,V>>* itr =
      new HashTableIterator<KeyVal<K,V>>(data_, capacity_);
    return new ConstIterator<KeyVal<K,V>>(itr);
  }

  /**
  Create an iterator over the key/value pairs in this map.

  @return  a new pair iterator (caller takes ownership)
  */
  template<typename K, typename V>
  inline Iterator<KeyVal<K,V>>* HashMap<K,V>::iterator(void)
  {
    return new HashTableIterator<KeyVal<K,V>>(data_, capacity_);
  }

  /**
  Add the given key/value pair to this map if the key is not already
  a member of this map.

  This version takes ownership over the provided value.

  @note **O(1)** average-case, **O(n)** worst-case performance
  @param key  the key to add to the map
  @param val  the value to associate with the key (take ownership)
  @return  true iff an element was added
  */
  template<typename K, typename V>
  inline bool HashMap<K,V>::add(const K& key, V* val)
  {
    if (contains(key)) return false;
    addHelper(key, val);
    return true;
  }

  /**
  Add the given key/value pair to this map if the key is not already
  a member of this map.

  This version moves the value into the map.

  @note **O(1)** average-case, **O(n)** worst-case performance
  @param key  the key to add to the map
  @param val  the value to associate with the key
  @return  true iff an element was added
  */
  template<typename K, typename V>
  inline bool HashMap<K,V>::add(const K& key, V&& val)
  {
    if (contains(key)) return false;
    addHelper(key, new V((V&&)val));
    return true;
  }

  /**
  Add the given key/value pair to this map if the key is not already
  a member of this map.

  This version copies the provided value.

  @note **O(1)** average-case, **O(n)** worst-case performance
  @param key  the key to add to the map
  @param val  the value to associate with the key
  @return  true iff an element was added
  */
  template<typename K, typename V>
  inline bool HashMap<K,V>::add(const K& key, const V& val)
  {
    if (contains(key)) return false;
    addHelper(key, new V(val));
    return true;
  }

  /**
  Add the given key/value pair to this map, replacing any existing
  value associated with the given key.

  This version takes ownership over the provided value.

  @note **O(1)** average-case, **O(n)** worst-case performance
  @param key  the key to add to the map
  @param val  the value to associate with the key (take ownership)
  */
  template<typename K, typename V>
  inline void HashMap<K,V>::put(const K& key, V* val)
  {
    KeyVal<K,V>* old = getHelper(key);
    if (old) old->put(val);
    else addHelper(key, val);
  }

  /**
  Add the given key/value pair to this map, replacing any existing
  value associated with the given key.

  This version moves the value into the map.

  @note **O(1)** average-case, **O(n)** worst-case performance
  @param key  the key to add to the map
  @param val  the value to associate with the key
  */
  template<typename K, typename V>
  inline void HashMap<K,V>::put(const K& key, V&& val)
  {
    KeyVal<K,V>* old = getHelper(key);
    V* nval = new V((V&&)val);
    if (old) old->put(nval);
    else addHelper(key, nval);
  }

  /**
  Add the given key/value pair to this map, replacing any existing
  value associated with the given key.

  This version copies the provided value.

  @note **O(1)** average-case, **O(n)** worst-case performance
  @param key  the key to add to the map
  @param val  the value to associate with the key
  */
  template<typename K, typename V>
  inline void HashMap<K,V>::put(const K& key, const V& val)
  {
    KeyVal<K,V>* old = getHelper(key);
    V* nval = new V(val);
    if (old) old->put(nval);
    else addHelper(key, nval);
  }

  /**
  Remove the key and the value associated with it from this map if it exists.
  
  @note **O(1)** average-case, **O(n)** worst-case performance
  @param key  the key to remove
  @return  true iff a key/value pair was removed
  */
  template<typename K, typename V>
  inline bool HashMap<K,V>::remove(const K& key)
  {
    size_t idx = index(key);
    ListNode<KeyVal<K,V>>* nod = data_[idx];
    while (nod)
    {
      if (this->equal(nod->data().key(), key))
      {
        // update the start of the chain
        if (data_[idx] == nod)
        {
          data_[idx] = nod->nextNode();
        }
        // delete the found node
        nod->detach();
        delete nod;

        // if less than 1/4 full, then re-hash the table
        --size_;
        if (4 * size_ < capacity_)
        {
          setCapacity(2 * size_);
        }
        return true;
      }
      nod = nod->nextNode();
    }

    return false;
  }

  /**
  Releases the value associated with the specified key from the map. This
  removes the key/value pair from the map and returns the value to the caller.
  The caller assumes ownership of the pointer.

  @note **O(1)** average-case, **O(n)** worst-case performance
  @param key  the key to remove
  @return  the value that was removed (if it existed) (caller owns)
  */
  template<typename K, typename V>
  inline V* HashMap<K,V>::release(const K& key)
  {
    // search
    KeyVal<K,V>* kv = getHelper(key);
    if (!kv) return nullptr;
    V* ret = kv->swap(nullptr);
    this->remove(key);
    return ret;
  }

  /**
  Swap the values referenced by the keys if they exist. On success, subsequent
  accesses to either key will return the value previously returned by the
  opposite. No effect if both keys are equal or missing from the map.

  Swapping is value-*stable* (pointers to values do not move in memory).

  @note **O(1)** average-case, **O(n)** worst-case performance
  @param k1  the first key to swap
  @param k2  the second key to swap
  @return  true iff the values were successfully swapped
  */
  template<typename K, typename V>
  inline bool HashMap<K,V>::swap(const K& k1, const K& k2)
  {
    // search
    KeyVal<K,V>* kv1 = getHelper(k1);
    KeyVal<K,V>* kv2 = getHelper(k2);
    // swap
    if (kv1 && kv2)
    {
      if (kv1 == kv2) return false;
      kv1->swap(kv2->swap(kv1->swap(nullptr)));
      return true;
    }
    // swap 1
    if (kv1)
    {
      add(k2, kv1->swap(nullptr));
      remove(k1);
      return true;
    }
    // swap 2
    if (kv2)
    {
      add(k1, kv2->swap(nullptr));
      remove(k2);
      return true;
    }
    // nothing
    return false;
  }

  template<typename K, typename V>
  inline void HashMap<K,V>::clear(void)
  {
    for (size_t idx = 0; idx < capacity_; ++idx)
    {
      if (data_[idx])
      {
        delete data_[idx];
        data_[idx] = nullptr;
      }
    }
    size_ = 0;
  }

  /**
  Determine if the specified key has an associated value in this map.

  @note **O(1)** average-case, **O(n)** worst-case performance
  @param key  the key to search for in this map
  @return  true iff the given key is an element of this map
  */
  template<typename K, typename V>
  inline bool HashMap<K,V>::contains(const K& key) const
  {
    return getHelper(key);
  }

  /**
  Obtain the value associated with the given key.

  @note **O(1)** average-case, **O(n)** worst-case performance
  @param key  the key to look-up
  @return  the value associated with the given key
  */
  template<typename K, typename V>
  inline V& HashMap<K,V>::at(const K& key)
  {
    return getHelper(key)->value();
  }

  /**
  Obtain the value associated with the given key.

  @note **O(1)** average-case, **O(n)** worst-case performance
  @param key  the key to look-up
  @return  the value associated with the given key
  */
  template<typename K, typename V>
  inline const V& HashMap<K,V>::at(const K& key) const
  {
    return getHelper(key)->value();
  }

  /**
  Get the value associated with the specified key if it exists, or `NULL`
  otherwise.

  @note **O(1)** average-case, **O(n)** worst-case performance
  @param key  the key to search for
  @return  a pointer to the found value
  */
  template<typename K, typename V>
  inline V* const HashMap<K,V>::get(const K& key)
  {
    KeyVal<K,V>* kvp = getHelper(key);
    return kvp ? &(kvp->value()) : nullptr;
  }

  /**
  Get the value associated with the specified key if it exists, or `NULL`
  otherwise.

  @note **O(1)** average-case, **O(n)** worst-case performance
  @param key  the key to search for
  @return  a pointer to the found value
  */
  template<typename K, typename V>
  inline const V* HashMap<K,V>::get(const K& key) const
  {
    KeyVal<K,V>* kvp = getHelper(key);
    return kvp ? &(kvp->value()) : nullptr;
  }

  /**
  Obtain the number of key/value pairs in the map.

  @return  the size of the map
  */
  template<typename K, typename V>
  inline size_t HashMap<K,V>::size(void) const
  {
    return size_;
  }

  /**
  Obtain the current capacity of the hash table. The `add()` and `remove()`
  methods will automatically adjust the capacity to be appropriate.

  @return  the capacity of the hash table
  */
  template<typename K, typename V>
  inline size_t HashMap<K,V>::capacity(void) const
  {
    return capacity_;
  }

  /**
  Set the capacity of the hash table. This will re-distribute the key/value
  pairs into the new table using their hash functions.

  @note **O(n)** average-case, **O(n²)** worst-case performance
  @param cap  the capacity of the hash table
  */
  template<typename K, typename V>
  inline void HashMap<K,V>::setCapacity(size_t cap)
  {
    // re-setting capacity is a no-op
    if (cap == capacity_) return;
    // capacity must always be at least 2
    if (cap < 2) cap = 2;
    // keep the old table while performing the update
    ListNode<KeyVal<K,V>>** oldData = data_;
    size_t oldCap = capacity_;
    // create a new empty table
    data_ = new ListNode<KeyVal<K,V>> * [cap]();
    capacity_ = cap;
    for (size_t idx = 0; idx < oldCap; ++idx)
    {
      // keep track of current and next node in the chain
      ListNode<KeyVal<K,V>>* nod = oldData[idx];
      ListNode<KeyVal<K,V>>* nxt = nullptr;
      while (nod)
      {
        nxt = nod->nextNode();
        // move the node over to the new table
        size_t jdx = index(nod->data().key());
        nod->detach();
        if (data_[jdx])
        {
          nod->append(data_[jdx]);
        }
        data_[jdx] = nod;
        // increment the node to the next in the chain
        nod = nxt;
      }
    }
    delete[] oldData;
  }

  /**
  Compute the index in the hash-table for the specified key using
  the hash function and current table capacity.

  @param key  the key to locate in the hash table
  @return  the appropriate index in the hash table for this key
  */
  template<typename K, typename V>
  inline size_t HashMap<K,V>::index(const K& key) const
  {
    return this->hash(key) % capacity_;
  }

  template<typename K, typename V>
  inline KeyVal<K,V>* HashMap<K,V>::getHelper(const K& key) const
  {
    size_t idx = index(key);
    ListNode<KeyVal<K,V>>* nod = data_[idx];
    while (nod)
    {
      if (this->equal(nod->data().key(), key))
      {
        return &nod->data();
      }
      nod = nod->nextNode();
    }
    return nullptr;
  }

  template<typename K, typename V>
  inline void HashMap<K,V>::addHelper(const K& key, V* val)
  {
    size_t idx = index(key);
    // add the element to the start of the chain
    KeyVal<K,V>* kvp = new KeyVal<K,V>(key, val);
    ListNode<KeyVal<K,V>>* enod = new ListNode<KeyVal<K,V>>(kvp);
    if (data_[idx])
    {
      enod->append(data_[idx]);
    }
    data_[idx] = enod;

    // if more than 3/4 full, then re-hash the table
    ++size_;
    if (4 * size_ > 3 * capacity_)
    {
      setCapacity(2 * size_);
    }
  }
}

#endif
