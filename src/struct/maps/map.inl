#ifndef VILN_MAP_INL
#define VILN_MAP_INL

#include "key_value_pair.inl"
#include "pair_iterators.inl"
#include "iterable.inl"

#include "map.hpp"

namespace viln
{
  template<typename K, typename V>
  inline Map<K, V>::Map(void) :
    keyView_(this),
    valueView_(this)
  {
    // no_op
  }

  /**
  Create an iterator over the keys in this map.

  @return  a new key iterator (caller takes ownership)
  */
  template<typename K, typename V>
  inline Iterator<const K>* Map<K,V>::keyIterator(void) const
  {
    return new ConstKeyIterator<K,V>(this->iterator());
  }

  /**
  Create an iterator over the values in this map.

  @return  a new value iterator (caller takes ownership)
  */
  template<typename K, typename V>
  inline Iterator<const V>* Map<K,V>::valueIterator(void) const
  {
    return new ConstValueIterator<K,V>(this->iterator());
  }

  /**
  Create an iterator over the values in this map, allowing modification.

  @return  a new value iterator (caller takes ownership)
  */
  template<typename K, typename V>
  inline Iterator<V>* Map<K,V>::valueIterator(void)
  {
    return new MutableValueIterator<K,V>(this->iterator());
  }

  /**
  View the map keys as an iterable object.

  @return  an iterable view of the map keys
  */
  template<typename K, typename V>
  inline const ConstIterable<K>& Map<K, V>::keys(void) const
  {
    return keyView_;
  }

  /**
  View the map values as an iterable object.

  @return  an iterable view of the map values
  */
  template<typename K, typename V>
  inline const Iterable<V>& Map<K, V>::values(void) const
  {
    return valueView_;
  }

  /**
  View the map values as an iterable object.

  @return  an iterable view of the map values
  */
  template<typename K, typename V>
  inline Iterable<V>& Map<K, V>::values(void)
  {
    return valueView_;
  }

  /**
  Remove any key/value pairs from this map which have the specified keys.

  @param keys  the keys to remove from this map
  @return  true iff any elements were removed from this map
  */
  template<typename K, typename V>
  inline bool viln::Map<K,V>::removeAll(const ConstIterable<K>& keys)
  {
    return removeAll(keys.iterator());
  }

  /**
  Remove any key/value pairs from this map which have the specified keys.

  @warning If the iterator points to this map, the behavior is undefined.

  @param keys  the keys to remove from this map (takes ownership)
  @return  true iff any elements were removed from this map
  */
  template<typename K, typename V>
  inline bool Map<K, V>::removeAll(Iterator<const K>* keys)
  {
    bool ret = false;
    while (keys->hasNext())
    {
      ret |= remove(keys->next());
    }
    delete keys;
    return ret;
  }

  /**
  Add all the key/value pairs in the specified map to this one if the
  given keys are not present in this one.

  @param o  map to add key/value pairs from
  @return  true iff at least one element was added
  */
  template<typename K, typename V>
  inline bool Map<K,V>::addAll(const ConstIterable<KeyVal<K, V>>& o)
  {
    if (&o == this) return false;
    bool ret = false;
    for (const KeyVal<K,V>& pair : o)
    {
      ret |= add(pair.key(), pair.value());
    }
    return ret;
  }

  /**
  Add all the key/value pairs in the specified map to this one. Any
  keys already in the set will have their values overridden by the
  provided values.

  @param o  map to add key/value pairs from
  */
  template<typename K, typename V>
  inline void Map<K,V>::putAll(const ConstIterable<KeyVal<K, V>>& o)
  {
    if (&o == this) return;
    for (const KeyVal<K,V>& pair : o)
    {
      put(pair.key(), pair.value());
    }
  }

  /**
  Determine if this map contains values for each of the given keys.

  @param keys  a collection of elements to search for
  @return  true iff this map contains all the given keys
  */
  template<typename K, typename V>
  inline bool Map<K,V>::containsAll(const ConstIterable<K>& keys) const
  {
    return containsAll(keys.iterator());
  }

  /**
  Determine if this map contains all the keys returned by the specified
  iterator.

  @param keys  a collection of elements to search for (takes ownership)
  @return  true iff this map contains all the given keys
  */
  template<typename K, typename V>
  inline bool Map<K, V>::containsAll(Iterator<const K>* keys) const
  {
    bool ret = true;
    while (keys->hasNext())
    {
      ret &= contains(keys->next());
    }
    delete keys;
    return ret;
  }

  template<typename K, typename V>
  inline bool Map<K,V>::isEmpty(void) const
  {
    return this->size() == 0;
  }

  /// @copydoc at(const K& key)
  template<typename K, typename V>
  inline V& Map<K,V>::operator[](const K& key)
  {
    return this->at(key);
  }

  /// @copydoc at(const K& key)
  template<typename K, typename V>
  inline const V& Map<K,V>::operator[](const K& key) const
  {
    return this->at(key);
  }

  template<typename K, typename V>
  inline Map<K, V>::KeyView::KeyView(Map* const map) :
    mapPtr_(map)
  {
    // no-op
  }

  template<typename K, typename V>
  inline Iterator<const K>* Map<K, V>::KeyView::iterator(void) const
  {
    return mapPtr_->keyIterator();
  }

  template<typename K, typename V>
  inline Map<K, V>::ValueView::ValueView(Map* const map) :
    mapPtr_(map)
  {
    // no-op
  }

  template<typename K, typename V>
  inline Iterator<V>* Map<K, V>::ValueView::iterator(void)
  {
    return mapPtr_->valueIterator();
  }

  template<typename K, typename V>
  inline Iterator<const V>* Map<K, V>::ValueView::iterator(void) const
  {
    const Map<K, V>* cmap = mapPtr_;
    return cmap->valueIterator();
  }
}

#endif
