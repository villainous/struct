#ifndef VILN_TREE_MAP_INL
#define VILN_TREE_MAP_INL

#include <stddef.h>

#include "tree_node.inl"
#include "avl_tree.inl"
#include "detect_order.inl"
#include "map.inl"

#include "const_iterator.hpp"
#include "tree_map.hpp"

namespace viln
{
  template<typename K, typename V>
  inline TreeMap<K,V>::TreeMap(void) :
    tree_(nullptr),
    size_(0)
  {
    // no-op
  }

  template<typename K, typename V>
  inline TreeMap<K,V>::TreeMap(const TreeMap<K,V>& o) :
    tree_(nullptr),
    size_(0)
  {
    TreeMap::addAll(o);
  }

  template<typename K, typename V>
  inline TreeMap<K,V>::TreeMap(TreeMap<K,V>&& o) :
    tree_(o.tree_),
    size_(o.size_)
  {
    o.tree_ = nullptr;
  }

  template<typename K, typename V>
  inline TreeMap<K,V>::~TreeMap(void)
  {
    if (tree_) delete tree_;
  }

  template<typename K, typename V>
  inline TreeMap<K,V>& TreeMap<K,V>::operator=(const TreeMap<K,V>& o)
  {
    if (this == &o) return *this;
    TreeMap::clear();
    TreeMap::addAll(o);
    return *this;
  }

  template<typename K, typename V>
  inline TreeMap<K, V>& TreeMap<K, V>::operator=(TreeMap<K, V>&& o)
  {
    if (this == &o) return *this;
    if (tree_) delete tree_;
    size_ = o.size_;
    tree_ = o.tree_;
    o.tree_ = nullptr;
    return *this;
  }

  /**
  Create an iterator over the key/value pairs in this map.

  @return  a new pair iterator (caller takes ownership)
  */
  template<typename K, typename V>
  inline Iterator<const KeyVal<K,V>>* TreeMap<K,V>::iterator(void) const
  {
    TreeNode<KeyVal<K,V>>* tnod = tree_ ? tree_->root() : nullptr;
    TreeNodeIterator<KeyVal<K,V>>* itr =
      new TreeNodeIterator<KeyVal<K,V>>(tnod);
    return new ConstIterator<KeyVal<K,V>>(itr);
  }

  /**
  Create an iterator over the key/value pairs in this map.

  @return  a new pair iterator (caller takes ownership)
  */
  template<typename K, typename V>
  inline Iterator<KeyVal<K,V>>* TreeMap<K,V>::iterator(void)
  {
    TreeNode<KeyVal<K,V>>* tnod = tree_ ? tree_->root() : nullptr;
    return new TreeNodeIterator<KeyVal<K,V>>(tnod);
  }

  /**
  Remove the key and the value associated with it from this map if it exists.

  @note **O(log(n))** worst-case performance
  @param key  the key to remove
  @return  true iff a key/value pair was removed
  */
  template<typename K, typename V>
  inline bool TreeMap<K,V>::remove(const K& key)
  {
    search_results fn = searchHelper(key);
    if (fn.found)
    {
      tree_->remove(fn.node);
      --size_;
      return true;
    }
    return false;
  }

  /**
  Releases the value associated with the specified key from the map. This
  removes the key/value pair from the map and returns the value to the caller.
  The caller assumes ownership of the pointer.

  @note **O(log(n))** worst-case performance
  @param key  the key to remove
  @return  the value that was removed (if it existed) (caller owns)
  */
  template<typename K, typename V>
  inline V* TreeMap<K,V>::release(const K& key)
  {
    search_results fn = searchHelper(key);
    V* ret = nullptr;
    if (fn.found)
    {
      ret = fn.node->data().swap(nullptr);
      tree_->remove(fn.node);
      --size_;
    }
    return ret;
  }

  /**
  Swap the values referenced by the keys if they exist. On success, subsequent
  accesses to either key will return the value previously returned by the
  opposite. No effect if both keys are equal or missing from the map.

  Swapping is value-*stable* (pointers to values do not move in memory).

  @note **O(log(n))** worst-case performance
  @param k1  the first key to swap
  @param k2  the second key to swap
  @return  true iff the values were successfully swapped
  */
  template<typename K, typename V>
  inline bool TreeMap<K,V>::swap(const K& k1, const K& k2)
  {
    // search
    search_results fn1 = searchHelper(k1);
    search_results fn2 = searchHelper(k2);
    // swap 1/2
    if (fn1.found && fn2.found)
    {
      if (fn1.node == fn2.node) return false;
      KeyVal<K,V>& kv1 = fn1.node->data();
      KeyVal<K,V>& kv2 = fn2.node->data();
      kv1.swap(kv2.swap(kv1.swap(nullptr)));
      return true;
    }
    // swap 1
    if (fn1.found)
    {
      KeyVal<K,V>& kv1 = fn1.node->data();
      add(k2, kv1.swap(nullptr));
      remove(k1);
      return true;
    }
    // swap 2
    if (fn2.found)
    {
      KeyVal<K,V>& kv2 = fn2.node->data();
      add(k1, kv2.swap(nullptr));
      remove(k2);
      return true;
    }
    // nothing
    return false;
  }

  /**
  Add the given key/value pair to this map if the key is not already
  a member of this map.

  This version takes ownership over the provided value.

  @note **O(log(n))** worst-case performance
  @param key  the key to add to the map
  @param val  the value to associate with the key (take ownership)
  @return  true iff an element was added
  */
  template<typename K, typename V>
  inline bool TreeMap<K,V>::add(const K& key, V* val)
  {
    return addHelper(key, val, searchHelper(key));
  }

  /**
  Add the given key/value pair to this map if the key is not already
  a member of this map.

  This version moves the value into the map.

  @note **O(log(n))** worst-case performance
  @param key  the key to add to the map
  @param val  the value to associate with the key
  @return  true iff an element was added
  */
  template<typename K, typename V>
  inline bool TreeMap<K,V>::add(const K& key, V&& val)
  {
    search_results fn = searchHelper(key);
    return (!fn.found) && addHelper(key, new V((V&&)val), fn);
  }

  /**
  Add the given key/value pair to this map if the key is not already
  a member of this map.

  This version copies the provided value.

  @note **O(log(n))** worst-case performance
  @param key  the key to add to the map
  @param val  the value to associate with the key
  @return  true iff an element was added
  */
  template<typename K, typename V>
  inline bool TreeMap<K,V>::add(const K& key, const V& val)
  {
    search_results fn = searchHelper(key);
    return (!fn.found) && addHelper(key, new V(val), fn);
  }

  /**
  Add the given key/value pair to this map, replacing any existing
  value associated with the given key.

  This version takes ownership over the provided value.

  @note **O(log(n))** worst-case performance
  @param key  the key to add to the map
  @param val  the value to associate with the key (take ownership)
  */
  template<typename K, typename V>
  inline void TreeMap<K,V>::put(const K& key, V* val)
  {
    search_results fn = searchHelper(key);
    if (fn.found) fn.node->data().put(val);
    else addHelper(key, val, fn);
  }

  /**
  Add the given key/value pair to this map, replacing any existing
  value associated with the given key.

  This version moves the value into the map.

  @note **O(log(n))** worst-case performance
  @param key  the key to add to the map
  @param val  the value to associate with the key
  */
  template<typename K, typename V>
  inline void TreeMap<K,V>::put(const K& key, V&& val)
  {
    search_results fn = searchHelper(key);
    V* vval = new V((V&&)val);
    if (fn.found) fn.node->data().put(vval);
    else addHelper(key, vval, fn);
  }

  /**
  Add the given key/value pair to this map, replacing any existing
  value associated with the given key.

  This version copies the provided value.

  @note **O(log(n))** worst-case performance
  @param key  the key to add to the map
  @param val  the value to associate with the key
  */
  template<typename K, typename V>
  inline void TreeMap<K,V>::put(const K& key, const V& val)
  {
    search_results fn = searchHelper(key);
    V* vval = new V((V&&)val);
    if (fn.found) fn.node->data().put(vval);
    else addHelper(key, vval, fn);
  }

  template<typename K, typename V>
  inline void TreeMap<K,V>::clear(void)
  {
    if (tree_) delete tree_;
    tree_ = nullptr;
    size_ = 0;
  }

  /**
  Determine if the specified key has an associated value in this map.

  @note **O(log(n))** worst-case performance
  @param key  the key to search for in this map
  @return  true iff the given key is an element of this map
  */
  template<typename K, typename V>
  inline bool TreeMap<K,V>::contains(const K& key) const
  {
    return searchHelper(key).found;
  }

  /**
  Obtain the value associated with the given key.

  @note **O(log(n))** worst-case performance
  @param key  the key to look-up
  @return  the value associated with the given key
  */
  template<typename K, typename V>
  inline V& TreeMap<K,V>::at(const K& key)
  {
    return searchHelper(key).node->data().value();
  }

  /// @copydoc at(const K& key)
  template<typename K, typename V>
  inline const V& TreeMap<K,V>::at(const K& key) const
  {
    return searchHelper(key).node->data().value();
  }

  /**
  Obtain the value associated with the given key if it exists, or
  `NULL` otherwise.

  @note **O(log(n))** worst-case performance
  @param key  the key to look-up
  @return  the value associated with the given key
  */
  template<typename K, typename V>
  inline V* const TreeMap<K,V>::get(const K& key)
  {
    search_results fn = searchHelper(key);
    return fn.found ? &(fn.node->data().value()) : nullptr;
  }

  /// @copydoc get(const K& key)
  template<typename K, typename V>
  inline const V* TreeMap<K,V>::get(const K& key) const
  {
    search_results fn = searchHelper(key);
    return fn.found ? &(fn.node->data().value()) : nullptr;
  }

  template<typename K, typename V>
  inline size_t TreeMap<K,V>::size(void) const
  {
    return size_;
  }

  /**
  Find the first value in the map. This is the value corresponding to the
  smallest key in the map.

  @note **O(log(n))** worst-case performance
  @return  the first pair in the map
  */
  template<typename K, typename V>
  inline KeyVal<K,V>& TreeMap<K,V>::first(void)
  {
    TreeNode<KeyVal<K,V>>* nod = tree_->root();
    while (nod->leftChild()) nod = nod->leftChild();
    return nod->data();
  }

  /// @copydoc first(void)
  template<typename K, typename V>
  inline const KeyVal<K,V>& TreeMap<K,V>::first(void) const
  {
    TreeNode<KeyVal<K,V>>* nod = tree_->root();
    while (nod->leftChild()) nod = nod->leftChild();
    return nod->data();
  }

  /**
  Find the last value in the map. This is the value corresponding to the
  largest key in the map.

  @note **O(log(n))** worst-case performance
  @return  the last pair in the map
  */
  template<typename K, typename V>
  inline KeyVal<K,V>& TreeMap<K,V>::last(void)
  {
    TreeNode<KeyVal<K,V>>* nod = tree_->root();
    while (nod->rightChild()) nod = nod->rightChild();
    return nod->data();
  }

  /// @copydoc last(void)
  template<typename K, typename V>
  inline const KeyVal<K,V>& TreeMap<K,V>::last(void) const
  {
    const TreeNode<KeyVal<K,V>>* nod = tree_->root();
    while (nod->rightChild()) nod = nod->rightChild();
    return nod->data();
  }

  /**
  Find the value corresponding to the next key following the one specified.
  The specified key may or may not be an element of the key-set of this map.
  The successor for elements after the last key return the last key.

  @note **O(log(n))** worst-case performance
  @param key  a key to search for
  @return  the pair corresponding to the next key
  */
  template<typename K, typename V>
  inline KeyVal<K,V>& TreeMap<K,V>::successor(const K& key)
  {
    search_results result = searchHelper(key);
    // result is not successor, so try to find one
    if (result.found || !result.left)
    {
      TreeNodeIterator<KeyVal<K,V>> itr(tree_->root(), result.node);
      if (itr.hasNext()) return itr.next();
    }
    // otherwise, searched node is the best result
    return result.node->data();
  }

  /// @copydoc successor(const K& key)
  template<typename K, typename V>
  inline const KeyVal<K,V>& TreeMap<K,V>::successor(const K& key) const
  {
    search_results result = searchHelper(key);
    // result is not successor, so try to find one
    if (result.found || !result.left)
    {
      TreeNodeIterator<KeyVal<K,V>> itr(tree_->root(), result.node);
      if (itr.hasNext()) return itr.next();
    }
    // otherwise, searched node is the best result
    return result.node->data();
  }

  /**
  Find the value corresponding to the previous key before the one specified.
  The specified key may or may not be an element of the key-set of this map.
  The predecessor for elements before the first key return the first key.

  @note **O(log(n))** worst-case performance
  @param key  a key to search for
  @return  the pair corresponding to the next key
  */
  template<typename K, typename V>
  inline KeyVal<K,V>& TreeMap<K,V>::predecessor(const K& key)
  {
    search_results result = searchHelper(key);
    // result is not successor, so try to find one
    if (result.found || result.left)
    {
      TreeNodeIterator<KeyVal<K,V>> itr(tree_->root(), result.node);
      itr.previous(); // iterator moves to result.node
      if (itr.hasPrevious()) return itr.previous();
    }
    // otherwise, searched node is the best result
    return result.node->data();
  }

  /// @copydoc predecessor(const K& key)
  template<typename K, typename V>
  inline const KeyVal<K,V>& TreeMap<K,V>::predecessor(const K& key) const
  {
    search_results result = searchHelper(key);
    // result is not successor, so try to find one
    if (result.found || result.left)
    {
      TreeNodeIterator<KeyVal<K,V>> itr(tree_->root(), result.node);
      itr.previous(); // iterator moves to result.node
      if (itr.hasPrevious()) return itr.previous();
    }
    // otherwise, searched node is the best result
    return result.node->data();
  }

  /**
  Get access to the root node of this map if it exists, or `NULL`
  otherwise.

  @return  the root node of this map
  */
  template<typename K, typename V>
  inline TreeNode<KeyVal<K,V>>* const TreeMap<K,V>::root(void)
  {
    return tree_ ? tree_->root() : nullptr;
  }

  /**
  Get access to the root node of this map if it exists, or `NULL`
  otherwise.

  @return  the root node of this map
  */
  template<typename K, typename V>
  inline const TreeNode<KeyVal<K,V>>* TreeMap<K,V>::root(void) const
  {
    return tree_ ? tree_->root() : nullptr;
  }

  template<typename K, typename V>
  inline typename TreeMap<K,V>::search_results
    TreeMap<K,V>::searchHelper(const K& key) const
  {
    TreeNode<KeyVal<K,V>>* nod = tree_ ? tree_->root() : nullptr;
    while (nod)
    {
      const K& nkey = nod->data().key();
      if (this->order(key, nkey))
      {
        if (nod->leftChild())
        {
          nod = nod->leftChild();
        }
        else
        {
          return search_results{ nod, false, true };
        }
      }
      else if (this->order(nkey, key))
      {
        if (nod->rightChild())
        {
          nod = nod->rightChild();
        }
        else
        {
          return search_results{ nod, false, false };
        }
      }
      else
      {
        return search_results{ nod, true };
      }
    }
    return search_results{ nullptr, false };
  }

  template<typename K, typename V>
  inline bool
    TreeMap<K,V>::addHelper(const K& key, V* val, const search_results& fn)
  {
    // if found then nothing to do
    if (fn.found)
    {
      delete val; // took ownership so dispose of it I guess?
      return false;
    }
    KeyVal<K,V>* ppair = new KeyVal<K,V>(key, val);
    // no node found means no root
    if (!fn.node)
    {
      if (tree_) delete tree_;
      tree_ = new AvlTree<KeyVal<K,V>>(ppair);
      size_ = 1;
    }
    else if (fn.left)
    {
      tree_->addPredecessor(fn.node, ppair);
      ++size_;
    }
    else // right
    {
      tree_->addSuccessor(fn.node, ppair);
      ++size_;
    }
    return true;
  }

}

#endif
