#ifndef VILN_ARRAY_HPP
#define VILN_ARRAY_HPP

#include <stddef.h>
#include <stdint.h>
#include <initializer_list>

#include "iterable.hpp"
#include "list_iterator.hpp"

namespace viln
{
  /// An iterable array (not resizable).
  /**
  An `Array` wraps a standard C-style array with some convenience methods
  for interacting with the elements. The `Array` is not resizeable.

  This class does not implement thread-safety, nor does it implement bounds-
  checking for methods that accept an index as a parameter.

  Construction, copy, and assignment operations are **O(n)**, with others
  all being **O(1)**.
  */
  template<typename E>
  class Array : public Iterable<E>
  {
  public:
    /// Initializes container to specified dimensions.
    Array(size_t siz);
    /// Initializes container to specified dimensions and value.
    Array(size_t siz, const E& zro);
    /// Initializer-list brace-construction.
    Array(std::initializer_list<E> li);
    /// Wrap the provided array of elements (takes ownership).
    Array(E* arr, size_t siz);
    /// Copy constructor.
    Array(const Array& o);
    /// Move constructor.
    Array(Array&& o);
    /// Destructor.
    virtual ~Array(void);

    /// Element assignment.
    Array& operator=(const E& val);
    // not assignable
    Array<E>& operator=(const Array<E>& o) = delete;

    /// Construct a new iterator over the elements of this array.
    virtual ListIterator<E>* iterator(void) override;
    virtual ListIterator<const E>* iterator(void) const override;
    using Iterable<E>::begin;
    using Iterable<E>::end;

    /// Construct a new iterator over the array starting at the given index.
    virtual ListIterator<E>* listIterator(size_t idx);
    virtual ListIterator<const E>* listIterator(size_t idx) const;

    /// Index operator.
    virtual E& operator[](size_t idx);
    virtual const E& operator[](size_t idx) const;

    /// Obtain the content of the container at the given index.
    virtual E& at(size_t idx);
    virtual const E& at(size_t idx) const;
    /// Get the first element in the list.
    virtual E& first(void);
    virtual const E& first(void) const;
    /// Get the last element in the list.
    virtual E& last(void);
    virtual const E& last(void) const;

    /// The size of the container.
    virtual int32_t size(void) const;
    /// Direct access the underlying data pointer.
    virtual E* const data(void);
    virtual const E* data(void) const;

  private:
    // size of the box in each dimension
    size_t size_;
    // the data storage being managed
    E* data_;
  };

}

#endif
