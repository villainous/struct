#ifndef VILN_PRIORITY_QUEUE_INL
#define VILN_PRIORITY_QUEUE_INL

#include <stddef.h>

#include "tree_node.inl"
#include "avl_tree.inl"
#include "key_value_pair.inl"
#include "detect_order.inl"
#include "tree_node_iterator.inl"
#include "tree_priority_queue.hpp"

namespace viln
{
  template<typename P, typename V>
  inline TreePriorityQueue<P,V>::TreePriorityQueue(void) :
    tree_(nullptr),
    previous_(nullptr),
    size_(0)
  {
    // no-op
  }

  template<typename P, typename V>
  inline TreePriorityQueue<P,V>::TreePriorityQueue(
    TreePriorityQueue<P,V>&& o) :
    tree_(o.tree_),
    previous_(o.previous_),
    size_(o.size_)
  {
    o.tree_ = nullptr;
  }

  template<typename P, typename V>
  inline TreePriorityQueue<P,V>::~TreePriorityQueue(void)
  {
    if (tree_)
    {
      delete tree_;
    }
  }

  /**
  This operation returns the value associated with the next priority in
  increasing order. The returned value is guaranteed to be held by this
  queue until the subsequent call to `next()`, at which time it will be
  deleted. Each call to `next()` should be preceeded by a call to `hasNext()`
  to determine if a next element exists.

  @note **O(log(n))** worst-case performance
  @return  the next value in order of increasing priority
  */
  template<typename P, typename V>
  inline V& TreePriorityQueue<P,V>::next(void)
  {
    // remove the previously returned next value
    if (previous_)
    {
      tree_->remove(previous_);
      if (!tree_->root()) clear();
    }
    // leftmost node is the next one
    previous_ = tree_->root();
    while (previous_->leftChild())
    {
      previous_ = previous_->leftChild();
    }
    --size_;
    return previous_->data().value();
  }

  /**
  Add an element to the queue and assign it a priority.

  This version takes ownership of the provided value.

  @note **O(log(n))** worst-case performance
  @param priority  the priority at which to add the value
  @param value  the value to add to the queue
  @return  true iff an element was added to the queue (takes ownership)
  */
  template<typename P, typename V>
  inline bool TreePriorityQueue<P,V>::add(const P& priority, V* value)
  {
    KeyVal<P,V>* ppair = new KeyVal<P,V>(priority, value);
    ++size_;
    if (!tree_)
    {
      tree_ = new AvlTree<KeyVal<P,V>>(ppair);
      return true;
    }
    TreeNode<KeyVal<P,V>>* nod = tree_->root();
    while (nod)
    {
      const P& nodp = nod->data().key();
      // priority less than this one
      if (this->order(priority, nodp))
      {
        if (nod->leftChild())
        {
          nod = nod->leftChild();
        }
        else
        {
          tree_->addPredecessor(nod, ppair);
          return true;
        }
      }
      // priority greater than this one
      else if (this->order(nodp, priority))
      {
        if (nod->rightChild())
        {
          nod = nod->rightChild();
        }
        else
        {
          tree_->addSuccessor(nod, ppair);
          return true;
        }
      }
      // equal priority
      else
      {
        tree_->addSuccessor(nod, ppair);
        return true;
      }
    }
    // should be inaccessible
    return false;
  }

  /**
  Add an element to the queue and assign it a priority.

  This version moves the provided value into the queue.

  @note **O(log(n))** worst-case performance
  @param priority  the priority at which to add the value
  @param value  the value to add to the queue
  @return  true iff an element was added to the queue (moved)
  */
  template<typename P, typename V>
  inline bool TreePriorityQueue<P,V>::add(const P& priority, V&& value)
  {
    return add(priority, new V((V&&) value));
  }

  /**
  Add an element to the queue and assign it a priority.

  This version makes a copy of the provided value.

  @note **O(log(n))** worst-case performance
  @param priority  the priority at which to add the value
  @param value  the value to add to the queue
  @return  true iff an element was added to the queue
  */
  template<typename P, typename V>
  inline bool TreePriorityQueue<P,V>::add(const P& priority, const V& value)
  {
    return add(priority, new V(value));
  }

  template<typename P, typename V>
  inline void TreePriorityQueue<P,V>::clear(void)
  {
    if (tree_)
    {
      delete tree_;
      tree_ = nullptr;
      previous_ = nullptr;
    }
    size_ = 0;
  }

  /**
  Determine if there are more elements in the queue. It is necessary
  to check if there is a next element in the queue before accessing said
  element.

  @return  true iff there is a next element in the queue
  */
  template<typename P, typename V>
  inline bool TreePriorityQueue<P,V>::hasNext(void) const
  {
    return size_;
  }

  /**
  Determine the number of remaining elements in the queue. This many calls to
  `next()` should be possible before exhausting the queue.

  @return  the number of elements in the queue
  */
  template<typename P, typename V>
  inline size_t TreePriorityQueue<P,V>::size(void) const
  {
    return size_;
  }
}

#endif
