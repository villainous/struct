#ifndef VILN_ARRAY_INL
#define VILN_ARRAY_INL

#include "array.hpp"

#include "iterable.inl"
#include "array_iterator.inl"

namespace viln
{
  /**
  Create an empty `Array` with the given number of elements. The elements
  will be *default*-initialized, so arrays of primitives may initially
  contain garbage.

  @param siz  the size of the array to create
  */
  template<typename E>
  inline Array<E>::Array(size_t siz) :
    data_(new E[siz]),
    size_(siz)
  {
    // no-op
  }

  /**
  Create an empty `Array` with the given number of elements.  The array
  will be initially populated with copies of the specified element.

  @param siz  the size of the array to create
  @param zro  the initial value of each element
  */
  template<typename E>
  inline Array<E>::Array(size_t siz, const E& zro) :
    data_(new E[siz]),
    size_(siz)
  {
    size_t idx = 0;
    while (idx < siz) data_[idx++] = zro;
  }

  /**
   Create an `Array` populated with the specified elements.
   
   @param li  the list of items to add to place in the array
   */
  template<typename E>
  inline Array<E>::Array(std::initializer_list<E> li) :
    data_(new E[li.size()]),
    size_(li.size())
  {
    size_t idx = 0;
    for (const E& e : li) data_[idx++] = e;
  }

  /**
  Create an `Array` using the given array of elements. Ownership of the
  provided array is transferred to the newly created object.

  @param arr  the array to wrap (takes ownership!)
  @param siz  the number of elements in the array
  */
  template<typename E>
  inline Array<E>::Array(E* arr, size_t siz) :
    data_(arr),
    size_(siz)
  {
    // no-op
  }

  template<typename E>
  inline Array<E>::Array(const Array& o) :
    data_(new E[o.size_]),
    size_(o.size_)
  {
    for (size_t idx = 0; idx < size_; ++idx)
    {
      data_[idx] = o.data_[idx];
    }
  }

  template<typename E>
  inline Array<E>::Array(Array&& o) :
    data_(o.data_),
    size_(o.size_)
  {
    o.data_ = nullptr;
  }

  template<typename E>
  inline Array<E>::~Array(void)
  {
    if (data_) delete[] data_;
  }

  template<typename E>
  inline Array<E>& Array<E>::operator=(const E& val)
  {
    for (size_t idx = 0; idx < size_; ++idx)
    {
      data_[idx] = val;
    }
    return *this;
  }

  /**
  Create an iterator over this array. This allows access to and modification of
  elements being iterated over.

  @return  an iterator over the array elements (caller takes ownership)
  */
  template<typename E>
  inline ListIterator<E>* Array<E>::iterator(void)
  {
    return this->listIterator(0);
  }

  /// @copydoc Array<E>::iterator(void)
  template<typename E>
  inline ListIterator<const E>* Array<E>::iterator(void) const
  {
    return this->listIterator(0);
  }

  /**
  Create an iterator over this array. This allows access to and modification of
  elements being iterated over.

  @param idx  the initial position of the iterator to create
  @return  an iterator over the array elements (caller takes ownership)
  */
  template<typename E>
  inline ListIterator<E>* Array<E>::listIterator(size_t idx)
  {
    return new ArrayIterator<E>(data_, size_, idx);
  }

  /// @copydoc Array<E>::listIterator(size_t)
  template<typename E>
  inline ListIterator<const E>* Array<E>::listIterator(size_t idx) const
  {
    return new ArrayIterator<const E>(data_, size_, idx);
  }

  /**
  Obtain a reference to the element at the specified index in the array.

  @param idx  the index of the element to be accessed
  @return  the element at the specified index
  */
  template<typename E>
  inline E& Array<E>::operator[](size_t idx)
  {
    return this->at(idx);
  }

  /// @copydoc Array<E>::operator[](size_t)
  template<typename E>
  inline const E& Array<E>::operator[](size_t idx) const
  {
    return this->at(idx);
  }

  /**
  Access the element at the specified index.

  @param idx  the index of the element to be accessed
  @return  the element at the specified index
  */
  template<typename E>
  inline E& Array<E>::at(size_t idx)
  {
    return data_[idx];
  }

  /// @copydoc Array<E>::at(size_t)
  template<typename E>
  inline const E& Array<E>::at(size_t idx) const
  {
    return data_[idx];
  }

  /**
  Obtain the size of the array.

  @return  the number of elements in the array
  */
  template<typename E>
  inline int32_t Array<E>::size(void) const
  {
    return int32_t(size_);
  }

  /**
  Obtain the first element in the array.

  @return element `[0]`
  */
  template<typename E>
  inline E& Array<E>::first(void)
  {
    return this->at(0);
  }

  /// @copydoc Array<E>::first(void)
  template<typename E>
  inline const E& Array<E>::first(void) const
  {
    return this->at(0);
  }

  /**
  Obtain the last element in the array.

  @return element `[size - 1]`
  */
  template<typename E>
  inline E& Array<E>::last(void)
  {
    return this->at(this->size() - 1);
  }

  /// @copydoc Array<E>::last(void)
  template<typename E>
  inline const E& Array<E>::last(void) const
  {
    return this->at(this->size() - 1);
  }

  /**
  Direct access to the C-array being wrapped.
  
  @return  the raw data array
  */
  template<typename E>
  inline E* const Array<E>::data(void)
  {
    return data_;
  }

  /// @copydoc Array<E>::data(void)
  template<typename E>
  inline const E* Array<E>::data(void) const
  {
    return data_;
  }
}

#endif
