#ifndef VILN_MULTI_ARRAY_HPP
#define VILN_MULTI_ARRAY_HPP

#include <stddef.h>
#include <stdint.h>

#include "vcrd.hpp"
#include "iterable.hpp"
#include "list_iterator.hpp"

namespace viln
{
  /// A multi-dimensional array implementation.
  /**
  The `MultiArray` template class implements a multi-dimensional array.
  This is implemented as a C-style linear array with indexing by N-dimensional
  coordinates.

  This class does not implement thread-safety, nor does it implement bounds-
  checking for methods that accept an index as a parameter.

  Construction, copy, and assignment operations are **O(n)**, with others
  all being **O(1)**.
  */
  template<typename E, int8_t D>
  class MultiArray : public Iterable<E>
  {
  public:
    typedef vcrd<int32_t, D> vvcrd;

    /// Initializes container to specified dimensions.
    MultiArray(const vvcrd& siz);
    /// Initializes container to specified dimensions and value.
    MultiArray(const vvcrd& siz, const E& zro);
    /// Wrap the provided array of elements (takes ownership).
    MultiArray(E* arr, const vvcrd& siz);
    /// Copy constructor.
    MultiArray(const MultiArray& o);
    /// Move constructor.
    MultiArray(MultiArray&& o);
    /// Destructor.
    virtual ~MultiArray(void);

    /// Assign all elements the same value.
    MultiArray& operator=(const E& val);
    // not assignable
    MultiArray& operator=(const MultiArray& o) = delete;

    /// Construct a new iterator over the elements of this array.
    virtual ListIterator<E>* iterator(void) override;
    virtual ListIterator<const E>* iterator(void) const override;
    using Iterable<E>::begin;
    using Iterable<E>::end;

    /// Index operator.
    virtual E& operator[](const vvcrd& idx);
    virtual const E& operator[](const vvcrd& idx) const;

    /// Obtain the content of the container at the given index.
    virtual E& at(const vvcrd& idx);
    virtual const E& at(const vvcrd& idx) const;

    /// The size of the container.
    virtual const vvcrd& size(void) const;
    /// Give the total number of elements in the box.
    virtual size_t capacity(void) const;
    /// Direct access the underlying data pointer.
    virtual E* const data(void);
    virtual const E* data(void) const;

  protected:
    /// Find the linear offset into the internal storage for the given index.
    virtual size_t index(const vvcrd& idx) const;

  private:
    // size of the box in each dimension
    vvcrd size_;
    // the data storage being managed
    E* data_;
  };

}

#endif
