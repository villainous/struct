
set( PWD "${CMAKE_CURRENT_SOURCE_DIR}" )

file( GLOB_RECURSE CPP_HEADERS "${PWD}" "*.hpp" )
file( GLOB_RECURSE CPP_SOURCES "${PWD}" "*.cpp" )
file( GLOB_RECURSE CPP_INLINES "${PWD}" "*.inl" )

target_sources(
  VilnStruct
  PRIVATE ${CPP_HEADERS} ${CPP_INLINES} ${CPP_SOURCES} )

target_include_directories(
  VilnStruct
  PUBLIC
    $<BUILD_INTERFACE:${PWD}>
    $<INSTALL_INTERFACE:include> )
set_property(
  TARGET VilnStruct
  APPEND PROPERTY
  PUBLIC_HEADER "${CPP_HEADERS}" )
set_property(
  TARGET VilnStruct
  APPEND PROPERTY
  PUBLIC_HEADER "${CPP_INLINES}" )
