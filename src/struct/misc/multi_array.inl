#ifndef VILN_MULTI_ARRAY_INL
#define VILN_MULTI_ARRAY_INL

#include "multi_array.hpp"

#include "vcrd.inl"
#include "iterable.inl"
#include "array_iterator.inl"

/// \cond DO_NOT_DOCUMENT
namespace viln_hidden
{
  /**
  Unfold an index within a specified size to find its lexographic index. It is
  assumed that the index is within-range of the given size.

  @param idx  multi-dimensional index to unfold
  @param siz  the size of the area being indexed
  @return  a lexographic order linear index
  */
  template<typename E, int8_t D>
  inline size_t unfold(const viln::vcrd<E, D>& idx, const viln::vcrd<E, D>& siz)
  {
    // Starting at the last dimension, expand the accumulated offset by the
    // size, and add the current coordinate. This will result in indices such
    // that earlier dimensions get packed closer together in memory, while later
    // ones get spread apart.
    size_t acc = 0;
    for (int8_t dim = D-1; dim >= 0; --dim)
    {
      acc *= siz[dim];
      acc += idx[dim];
    }
    return acc;
  }
  
  template<typename E>
  inline size_t unfold(const viln::vcrd<E, 2>& idx, const viln::vcrd<E, 2>& siz)
  {
    size_t acc = idx[1];
    acc = (acc * siz[0]) + idx[0];
    return acc;
  }

  template<typename E>
  inline size_t unfold(const viln::vcrd<E, 3>& idx, const viln::vcrd<E, 3>& siz)
  {
    size_t acc = idx[2];
    acc = (acc * siz[1]) + idx[1];
    acc = (acc * siz[0]) + idx[0];
    return acc;
  }

  template<typename E>
  inline size_t unfold(const viln::vcrd<E, 4>& idx, const viln::vcrd<E, 4>& siz)
  {
    size_t acc = idx[3];
    acc = (acc * siz[2]) + idx[2];
    acc = (acc * siz[1]) + idx[1];
    acc = (acc * siz[0]) + idx[0];
    return acc;
  }
}
/// \endcond

namespace viln
{
  /**
  Create an empty `MultiArray` with the given number of elements. The
  elements will be *default*-initialized, so arrays of primitives may
  initially contain garbage.

  @param siz  the size of the array to create
  */
  template<typename E, int8_t D>
  MultiArray<E, D>::MultiArray(const vvcrd& siz) :
    data_(new E[volume(siz)]),
    size_(abs(siz))
  {
    // noop
  }

  /**
  Create an empty `MultiArray` with the given number of elements. The array
  will be initially populated with copies of the specified element.

  @param siz  the size of the array to create
  @param zro  the initial value of each element
  */
  template<typename E, int8_t D>
  MultiArray<E, D>::MultiArray(const vvcrd& siz, const E& zro) :
    data_(nullptr),
    size_(abs(siz))
  {
    size_t cap = volume(siz);
    data_ = new E[cap];
    for (size_t idx = 0; idx < cap; ++idx)
    {
      data_[idx] = zro;
    }
  }

  /**
  Create an `MultiArray` using the given array of elements. Ownership of the
  provided array is transferred to the newly created object.

  @param arr  the array to wrap (takes ownership!)
  @param siz  the size of the array
  */
  template<typename E, int8_t D>
  inline MultiArray<E, D>::MultiArray(E* arr, const vvcrd& siz) :
    data_(arr),
    size_(abs(siz))
  {
    // no-op
  }

  template<typename E, int8_t D>
  inline MultiArray<E, D>::MultiArray(const MultiArray& o) :
    size_(o.size_),
    data_(nullptr)
  {
    size_t cap = o.capacity();
    data_ = new E[cap];
    for (size_t idx = 0; idx < cap; ++idx)
    {
      data_[idx] = o.data_[idx];
    }
  }

  template<typename E, int8_t D>
  inline MultiArray<E, D>::MultiArray(MultiArray&& o) :
    size_(o.size_),
    data_(o.data_)
  {
    o.data_ = nullptr;
  }

  template<typename E, int8_t D>
  MultiArray<E, D>::~MultiArray(void)
  {
    if (data_) delete[] data_;
  }

  template<typename E, int8_t D>
  inline MultiArray<E, D>& MultiArray<E, D>::operator=(const E& val)
  {
    size_t cap = MultiArray::capacity();
    for (size_t idx = 0; idx < cap; ++idx)
    {
      data_[idx] = val;
    }
    return *this;
  }

  /**
  Create an iterator over this array. This allows access to and modification of
  elements being iterated over.

  @return  an iterator over the array elements (caller takes ownership)
  */
  template<typename E, int8_t D>
  inline ListIterator<E>* MultiArray<E, D>::iterator(void)
  {
    return new ArrayIterator<E>(data_, capacity());
  }

  /// @copydoc MultiArray<E,D>::iterator(void)
  template<typename E, int8_t D>
  inline ListIterator<const E>* MultiArray<E, D>::iterator(void) const
  {
    return new ArrayIterator<const E>(data_, capacity());
  }

  /**
  Obtain a reference to the element at the specified index in the array.

  @param idx  the index of the element to be accessed
  @return  the element at the specified index
  */
  template<typename E, int8_t D>
  inline E& MultiArray<E, D>::operator[](const vvcrd& idx)
  {
    return this->at(idx);
  }

  /// @copydoc MultiArray<E,D>::operator[](const vvcrd&)
  template<typename E, int8_t D>
  inline const E& MultiArray<E, D>::operator[](const vvcrd& idx) const
  {
    return this->at(idx);
  }

  /**
  Access the element at the specified index.

  @param idx  the index of the element to be accessed
  @return  the element at the specified index
  */
  template<typename E, int8_t D>
  inline E& MultiArray<E, D>::at(const vvcrd& idx)
  {
    return data_[index(idx)];
  }

  /// @copydoc MultiArray<E,D>::at(const vvcrd&)
  template<typename E, int8_t D>
  inline const E& MultiArray<E, D>::at(const vvcrd& idx) const
  {
    return data_[index(idx)];
  }

  /**
  Obtain the dimensions of the array (multi-dimensional).

  @return  the array dimensions
  */
  template<typename E, int8_t D>
  const typename MultiArray<E, D>::vvcrd&
    MultiArray<E, D>::size(void) const
  {
    return size_;
  }

  /**
  Obtain the linear size of the underlying C-array.

  @return  the linear array size
  */
  template<typename E, int8_t D>
  inline size_t MultiArray<E, D>::capacity(void) const
  {
    return viln::volume(size_);
  }

  /**
  Direct access to the C-array being wrapped.
  
  @return  the raw data array
  */
  template<typename E, int8_t D>
  inline E* const MultiArray<E, D>::data(void)
  {
    return data_;
  }

  /// @copydoc MultiArray<E,D>::data(void)
  template<typename E, int8_t D>
  inline const E* MultiArray<E, D>::data(void) const
  {
    return data_;
  }

  /**
  Get the linear-index in the C-array corresponding to the given coord-index
  in the multi-dimensional array.
  
  @param idx  the desired coord-index
  @return  the corresponding linear-index
  */
  template<typename E, int8_t D>
  inline size_t MultiArray<E, D>::index(const vvcrd& idx) const
  {
    return viln_hidden::unfold(idx, size_);
  }
}

#endif
