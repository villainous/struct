#ifndef VILN_TREE_PRIORITY_QUEUE_HPP
#define VILN_TREE_PRIORITY_QUEUE_HPP

#include <stddef.h>

#include "key_value_pair.hpp"
#include "detect_order.hpp"
#include "iterator.hpp"

namespace viln
{
  /// \cond DO_NOT_DOCUMENT
  template<typename E> class TreeNode;
  template<typename E> class AvlTree;
  /// \endcond

  /// An aggregate of elements to be accessed in priority order.
  /**
  A `TreePriorityQueue` is an aggregate of values with a priority access order.
  Iteration over the elements in a queue removes previously accessed elements,
  and as such the queue is itself an `Iterator`.

  Elements are accessed in the order of ascending priority, with ordering
  of priorities determined according to the `DetectOrder` interface.

  This class does not implement thread-safety.

  Operations generally have performance of **O(m*log(n))** where **m** is
  the size of the input (if applicable), and **n** is the size of the queue.
  */
  template <typename P, typename V>
  class TreePriorityQueue :
    public Iterator<V>,
    protected DetectOrder<P>
  {
  public:
    /// Default constructor.
    TreePriorityQueue(void);
    /// Move-constructor.
    TreePriorityQueue(TreePriorityQueue<P,V>&& o);
    /// Destructor.
    virtual ~TreePriorityQueue(void);

    // not copyable
    TreePriorityQueue(const TreePriorityQueue<P, V>& o) = delete;
    TreePriorityQueue<P,V>& operator=(const TreePriorityQueue<P,V>& o) = delete;

    /// Accesses the next element, consuming it.
    virtual V& next(void) override;
    /// Add the provided value with associated with the given priority.
    virtual bool add(const P& priority, V* value);
    /// Add the provided value with associated with the given priority.
    virtual bool add(const P& priority, V&& value);
    /// Add the provided value with associated with the given priority.
    virtual bool add(const P& priority, const V& value);
    /// Remove all the values from this queue.
    virtual void clear(void);

    /// Determine if the queue is empty.
    virtual bool hasNext(void) const override;
    /// Get the number of values pairs in the queue.
    virtual size_t size(void) const;

  private:
    // storage for queued elements
    AvlTree<KeyVal<P,V>>* tree_;
    // remember the previously returned element
    TreeNode<KeyVal<P,V>>* previous_;
    // number of elements queued
    size_t size_;
  };

}

#endif
