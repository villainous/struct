#ifndef VILN_ARRAY_LIST_HPP
#define VILN_ARRAY_LIST_HPP

#include <stddef.h>
#include <initializer_list>

#include "list.hpp"

namespace viln
{
  /// A type of list which uses a contiguous C array for element storage.
  /**
  An `ArrayList` is a `List` implementation which is implemented as a resizable
  C-style array. This means that the benefits associated with arrays (direct
  element indexing and contiguous memory arrangement) with the added
  convenience of the `List` interface.

  This class uses `DetectEqual<E>` for the templated type, so either
  `operator==` must be defined for the element-type, or the `equal(E,E)`
  method should be overridden.

  This class does not implement thread-safety, nor does it implement bounds-
  checking for methods that accept an index as a parameter.

  Generally, the performance of array-list operations is **O(m)**. Inserting
  or removing elements that are not at the end of the list has
  performance **O(m+n)**. If the storage capacity needs updating, then the
  amortized performance is **O(m*log(n))**. Several aggregate operations
  leverage the constant-time indexing in order to provide improved performance
  beyond these general guidelines.

  Note that modification of the list will invalidate any active iterators over
  the list. Modification of list *elements*, on the other hand, is safe.
  */
  template<typename E>
  class ArrayList : public List<E>
  {
  public:
    /// Construct a list with initial capacity as requested (default = 1).
    ArrayList(size_t capacity = 1);
    /// Initializer-list brace-construction.
    ArrayList(std::initializer_list<E> li);
    /// Wrap the provided array of elements in a list.
    ArrayList(E* arr, size_t size);
    /// Copy-constructor.
    ArrayList(const ArrayList<E>& o);
    /// Move-constructor.
    ArrayList(ArrayList<E>&& o);
    /// Destructor.
    virtual ~ArrayList(void);

    /// Assignment operator.
    ArrayList<E>& operator=(const ArrayList<E>& o);
    /// Move-assign operator.
    ArrayList<E>& operator=(ArrayList<E>&& o);

    /// Construct a new iterator over the list starting at the given index.
    virtual ListIterator<E>* listIterator(size_t index) override;
    /// Construct a new iterator over the list starting at the given index.
    virtual ListIterator<const E>* listIterator(size_t index) const override;

    /// Move an element into the list at the given index.
    virtual bool insert(size_t index, E&& e) override;
    /// Insert an additional element at the given index.
    virtual bool insert(size_t index, const E& e) override;
    /// Insert each element from the given collection into this one.
    virtual bool insertAll(size_t index, const ConstIterable<E>& c) override;
    /// Insert each element provided by the iterator into this list.
    virtual bool insertAll(size_t index, Iterator<const E>* itr) override;
    /// Add all the elements from another collection to this list.
    virtual bool addAll(const ConstIterable<E>& c) override;
    /// Add all elements returned by the iterator to this collection.
    virtual bool addAll(Iterator<const E>* itr) override;
    /// Remove the element at the specified index.
    virtual bool erase(size_t index) override;
    /// Remove elements at each of the specified indices.
    virtual bool eraseAll(const ListIndices& indices) override;
    /// Remove an element from the collection.
    virtual bool remove(const E& e) override;
    /// Remove all elements in the provided collection from this one.
    virtual bool removeAll(const ConstIterable<E>& c) override;
    /// Remove all elements returned by the iterator from this collection.
    virtual bool removeAll(Iterator<const E>* itr) override;
    /// Remove all elements from this collection.
    virtual void clear(void) override;
    /// Sort the elements in the list according to the given ordering.
    virtual void sort(const OrderOp<E>& comp) override;
    using List<E>::sort;

    /// Determine the size of this collection.
    virtual size_t size(void) const override;

    /// Indexed access into the list elements.
    virtual E& at(size_t index) override;
    virtual const E& at(size_t index) const override;

  private:
    // Swap positions of two objects in memory using their move constructors.
    void objswap(E* a, E* b);
    // Move objects in memory using their move constructors.
    void objmove(E* dest, E* src, size_t num);
    // Insert an empty range of the given size at the given index.
    void split(size_t index, size_t size);
    // Remove empty elements from the list to make it contiguous.
    void defragment(const ListIndices& empties);
    // Sort the elements between the two indices using quicksort.
    void quicksort(size_t lo, size_t hi, const OrderOp<E>& comp);

    // the current number of valid elements
    size_t size_;
    // the size of the aray, including both valid and invalid elements
    size_t capacity_;
    // underlying storage for the list
    E* array_;
  };
}

#endif
