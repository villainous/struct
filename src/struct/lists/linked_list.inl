#ifndef VILN_LINKED_LIST_INL
#define VILN_LINKED_LIST_INL

#include <stddef.h>

#include "list_node.inl"
#include "list_node_iterator.inl"
#include "list.inl"

#include "const_list_iterator.hpp"
#include "linked_list.hpp"

namespace viln
{
  /**
  Create an empty `LinkedList`.
  */
  template<typename E>
  inline LinkedList<E>::LinkedList(void):
    size_(0),
    first_(nullptr),
    lastptr_(nullptr)
  {
    // no-op
  }

  template<typename E>
  inline LinkedList<E>::LinkedList(std::initializer_list<E> li) :
    size_(0),
    first_(nullptr),
    lastptr_(nullptr)
  {
    for (const E& e : li) LinkedList::add(e);
  }

  template<typename E>
  inline LinkedList<E>::LinkedList(const LinkedList<E>& o):
    size_(0),
    first_(nullptr),
    lastptr_(nullptr)
  {
    LinkedList::addAll(o);
  }

  template<typename E>
  inline LinkedList<E>::LinkedList(LinkedList<E>&& o) :
    size_(o.size_),
    first_(o.first_),
    lastptr_(o.lastptr_)
  {
    o.first_ = nullptr;
  }

  template<typename E>
  inline LinkedList<E>::~LinkedList(void)
  {
    if (first_) delete first_;
  }

  template<typename E>
  inline LinkedList<E>& LinkedList<E>::operator=(const LinkedList<E>& o)
  {
    if (this == &o) return *this;
    LinkedList::clear();
    LinkedList::addAll(o);
    return *this;
  }

  template<typename E>
  inline LinkedList<E>& LinkedList<E>::operator=(LinkedList<E>&& o)
  {
    if (this == &o) return *this;
    if (first_) delete first_;
    size_ = o.size_;
    first_ = o.first_;
    lastptr_ = o.lastptr_;
    o.first_ = nullptr;
    return *this;
  }

  /**
  Create an iterator over the list. The iterator will begin at the specified
  index, which may range from 0 to the size of the list. The returned iterator
  will be pointing between the given indexed element, and its previous element.
  Thus, providing 0 as the argument starts the iterator at the beginning of the
  list, where it has a next element but no previous element, while providing
  the list size will start the iterator at the end of the list where it has
  a previous element but no next one.

  @param index  the starting index for the iterator
  @return  an iterator over the elements of this list
  */
  template<typename E>
  inline ListNodeIterator<E>* LinkedList<E>::listIterator(size_t index)
  {
    ListNodeIterator<E>* itr = nullptr;
    if (index >= size_)
    {
      itr = new ListNodeIterator<E>(lastptr_, nullptr);
    }
    else
    {
      ListNode<E>* nnod = nodeAt(index);
      itr = new ListNodeIterator<E>(nnod->previousNode(), nnod);
    }
    return itr;
  }

  template<typename E>
  inline ListIterator<const E>*
    LinkedList<E>::listIterator(size_t index) const
  {
    ListNodeIterator<E>* itr = nullptr;
    if (index >= size_)
    {
      itr = new ListNodeIterator<E>(lastptr_, nullptr);
    }
    else
    {
      ListNode<E>* nnod = nodeAt(index);
      itr = new ListNodeIterator<E>(nnod->previousNode(), nnod);
    }
    return new ConstListIterator<E>(itr);
  }

  /*
  Insert an element at the current location of the iterator.

  The inserted element will be placed before the next node if one exists, or
  appended to the end of the list otherwise. The iterator's previous and next
  elements will NOT be modified by this operation, so the next call to
  `ListIterator::previous()` or `ListIterator::next()` will skip the added
  element. Repeated calls to this method will add the elements in ascending
  order, as though they were being added to the end of a sub-list.

  This version takes ownership over the provided element pointer.

  @note **O(1)** performance
  @param index  the position in which to insert an element
  @param e  the element to be inserted (list takes ownership)
  @return  true iff the element was added to the list
  */
  template<typename E>
  inline bool
    LinkedList<E>::insert(ListNodeIterator<E>* const index, E* e)
  {
    if (!index->hasNext())
    {
      // equivalent to add if there is no next
      return add(e);
    }
    // insert before the next node
    insertNode(index->peekNextNode(), new ListNode<E>(e));
    return true;
  }

  /**
  Insert an element at the current location of the iterator.

  This version calls the move-constructor to take ownership of the provided
  element. This version of insertion is used when the object being inserted
  is constructed in-line with the insert operation.

  @note **O(1)** performance
  @param index  the position in which to insert an element
  @param e  the element to be inserted (object moved)
  @return  true iff the element was added to the list
  @see LinkedList<E>::insert(ListNodeIterator<E>*, E*)
  */
  template<typename E>
  inline bool
    LinkedList<E>::insert(ListNodeIterator<E>* const index, E&& e)
  {
    E* elem = new E((E&&)e);
    return insert(index, elem);
  }

  /**
  Insert an element at the current location of the iterator.

  This version makes a copy of the provided element reference using its copy-
  constructor.
  
  @note **O(1)** performance
  @param index  the position in which to insert an element
  @param e  the element to be inserted (object copied)
  @return  true iff the element was added to the list
  @see LinkedList<E>::insert(ListNodeIterator<E>*, E*)
  */
  template<typename E>
  inline bool
    LinkedList<E>::insert(ListNodeIterator<E>* const index, const E& e)
  {
    E* elem = new E(e);
    return insert(index, elem);
  }

  /**
  Insert an element at the specified index in the list. Elements at or above
  the specified index will have their index incremented by one.

  This version takes ownership over the provided element pointer.

  @note **O(n)** performance
  @param index  the position in which to insert an element
  @param e  the element to be inserted (list takes ownership)
  @return  true iff the element was added to the list
  */
  template<typename E>
  inline bool LinkedList<E>::insert(size_t index, E* e)
  {
    // insertion at the last index is equivalent to add
    if (index == size_)
    {
      return add(e);
    }
    // insert a node at the given index
    insertNode(nodeAt(index), new ListNode<E>(e));
    return true;
  }

  /**
  Insert an element at the specified index in the list. Elements at or above
  the specified index will have their index incremented by one.

  This version calls the move-constructor to take ownership of the provided
  element. This version of insertion is used when the object being inserted
  is constructed in-line with the insert operation.

  @note **O(n)** performance
  @param index  the position in which to insert an element
  @param e  the element to be inserted (object moved)
  @return  true iff the element was added to the list
  */
  template<typename E>
  inline bool LinkedList<E>::insert(size_t index, E&& e)
  {
    return insert(index, new E((E&&)e));
  }

  /**
  Insert an element at the specified index in the list. Elements at or above
  the specified index will have their index incremented by one.

  This version makes a copy of the provided element reference using its copy-
  constructor.
  
  @note **O(n)** performance
  @param index  the position in which to insert an element
  @param e  the element to be inserted (object copied)
  @return  true iff the element was added to the list
  */
  template<typename E>
  inline bool LinkedList<E>::insert(size_t index, const E& e)
  {
    return insert(index, new E(e));
  }

  /**
  Insert copies of all the elements of the provided collection into this one
  at the location of the provided iterator.
  The inserted elements will be added in the order specified by the provided
  collection's iterator. The elements will be inserted into the list between
  the previous and next elements that would be returned by the iterator. The
  iterator itself will NOT be modified by this operation, so the next call to
  `ListIterator::previous()` or `ListIterator::next()` will skip the added
  elements. Repeated calls to this method will add the elements in ascending
  order, as though they were being added to the end of a sub-list.

  @note **O(m)** performance
  @param index  the position in which to insert elements
  @param c  the collection of elements to insert
  @return  true iff the element was added to the list
  */
  template<typename E>
  inline bool
    LinkedList<E>::insertAll(
      ListNodeIterator<E>* const index,
      const ConstIterable<E>& c)
  {
    // if there is no next element, then this is equivalent to add
    if (!index->hasNext())
    {
      return this->addAll(c);
    }
    // handle self-reference with temporary copy
    if (&c == this)
    {
      LinkedList<E> temp(*this);
      return insertAll(index, temp);
    }
    // insert using iterators
    return insertAll(index, c.iterator());
  }

  /**
  Insert copies of all the elements of the provided iterator into this list
  at the location of the provided iterator. Inserted elements will be placed
  between the previous and next nodes of the index-iterator in the order
  they are returned by the element-iterator.

  @warning If the iterator points to this list, the behavior is undefined.

  @note **O(m)** performance
  @param index  the position in which to insert elements
  @param itr  an iterator over elements to insert (takes ownership)
  @return  true iff the element was added to the list
  */
  template<typename E>
  inline bool LinkedList<E>::insertAll(
    ListNodeIterator<E>* const index,
    Iterator<const E>* itr)
  {
    // if no next index then equivalent to add
    if (!index->hasNext())
    {
      return this->addAll(itr);
    }
    // get the next node so we can insert everything before it
    ListNode<E>* nnod = index->peekNextNode();
    size_t oldSize = size_;
    while (itr->hasNext())
    {
      insertNode(nnod, new ListNode<E>(new E(itr->next())));
    }
    delete itr;
    // if size increased then we added at least one element
    return size_ > oldSize;
  }

  /**
  Insert copies of all the elements of the provided collection into this one
  at the specified index. Elements at or above the specified index will have
  their index incremented by the size of the provided collection.

  @note **O(n+m)** performance
  @param index  the position in which to insert elements
  @param c  the collection of elements to insert
  @return  true iff the element was added to the list
  */
  template<typename E>
  inline bool LinkedList<E>::insertAll(size_t index, const ConstIterable<E>& c)
  {
    // Insertion at the last index is equivalent to add.
    if (index == size_)
    {
      return this->addAll(c);
    }

    // Self-insert might be infinitely recursive, so add as a data copy.
    if (this == &c)
    {
      LinkedList<E> temp(*this);
      return insertAll(index, temp);
    }

    // Insert using iterator.
    return this->insertAll(index, c.iterator());
  }

  /**
  Insert copies of all the elements of the provided iterator into this list
  at the given index in the order they are returned by the provided iterator.

  @warning If the iterator points to this list, the behavior is undefined.

  @note **O(n+m)** performance
  @param index  the position in which to insert elements
  @param itr  an iterator over elements to insert (takes ownership)
  @return  true iff the element was added to the list
  */
  template<typename E>
  inline bool LinkedList<E>::insertAll(size_t index, Iterator<const E>* itr)
  {
    // if no next index then equivalent to add
    if (index == size_)
    {
      return this->addAll(itr);
    }
    // get the node at the index we plan to supplant
    ListNode<E>* nnod = nodeAt(index);
    size_t oldSize = size_;
    while (itr->hasNext())
    {
      insertNode(nnod, new ListNode<E>(new E(itr->next())));
    }
    delete itr;
    // if size increased then we added at least one element
    return size_ > oldSize;
  }

  /**
  Add an element to the end of the list.

  This version takes ownership of the provided element pointer.

  @note **O(1)** performance
  @param e  the element to be added to the list (list takes ownership)
  @return  true iff the element was added to the list
  */
  template<typename E>
  inline bool LinkedList<E>::add(E* e)
  {
    addNode(new ListNode<E>(e));
    return true;
  }

  /**
  Add an element to the end of the list.

  This version calls the move-constructor to take ownership of the provided
  element. This version of insertion is used when the object being inserted
  is constructed in-line with the insert operation.

  @note **O(1)** performance
  @param e  the element to be added to the list (object moved)
  @return  true iff the element was added to the list
  */
  template<typename E>
  inline bool LinkedList<E>::add(E&& e)
  {
    return add(new E((E&&)e));
  }

  /**
  Add an element to the end of the list.

  This version makes a copy of the provided element reference using its copy-
  constructor.

  @note **O(1)** performance
  @param e  the element to be added to the list (object copied)
  @return  true iff the element was added to the list
  */
  template<typename E>
  inline bool LinkedList<E>::add(const E& e)
  {
    return add(new E(e));
  }

  /**
  Delete an element from the list at the location specified by the given
  iterator.

  This version will remove the next element following the iterator position.

  @note **O(1)** performance
  @param index  the position of the element to be erased
  @return  true iff an element was removed from the list
  */
  template<typename E>
  inline bool LinkedList<E>::eraseNext(ListNodeIterator<E>* const index)
  {
    if (!index->hasNext()) return false;
    // if there is a node before this one, snap to it
    if (index->hasPrevious())
    {
      eraseNode(index->peekNextNode());
      index->snapToPrevious();
    }
    // if there is no next node then rewind
    else
    {
      eraseNode(index->nextNode());
      index->snapToNext();
    }
    return true;
  }

  /**
  Delete an element from the list at the location specified by the given
  iterator.

  This version will remove the previous element preceding the iterator
  position.

  @note **O(1)** performance
  @param index  the position of the element to be erased
  @return  true iff an element was removed from the list
  */
  template<typename E>
  inline bool LinkedList<E>::erasePrevious(ListNodeIterator<E>* const index)
  {
    if (!index->hasPrevious()) return false;
    // if there is a node following this one, snap to it
    if (index->hasNext())
    {
      eraseNode(index->peekPreviousNode());
      index->snapToNext();
    }
    // if there is no next node then rewind
    else
    {
      eraseNode(index->previousNode());
      index->snapToPrevious();
    }
    return true;
  }

  /**
  Delete the element at the given index from the list. Elements above the given
  index will have their indices decremented by one.

  @note **O(n)** performance
  @param index  the position of the element to be removed
  @return  true iff the element was removed from the list
  */
  template<typename E>
  inline bool LinkedList<E>::erase(size_t index)
  {
    ListNode<E>* nod = nodeAt(index);
    if (nod)
    {
      eraseNode(nod);
      return true;
    }
    return false;
  }

  /**
  Delete the elements at all the specified indices.

  @note **O(n+m)** performance
  @param indices  the positions of the elements to be removed
  @return  true iff at least one element was removed
  */
  template<typename E>
  inline bool LinkedList<E>::eraseAll(const ListIndices& indices)
  {
    // if there are no indices to remove then this is a no-op
    if (indices.isEmpty() || this->isEmpty())
    {
      return false;
    }
    // otherwise, walk through indices to remove
    size_t idx = 0;
    ListNode<E>* cur = first_;
    bool ret = false;
    for (const size_t& iidx : indices)
    {
      // walk between indices-to-remove in the actual list
      while (idx < iidx && cur)
      {
        cur = cur->nextNode();
        ++idx;
      }
      // if we walk off the end, then stop
      if (!cur)
      {
        break;
      }
      // remove the node we found
      ListNode<E>* found = cur;
      cur = cur->nextNode();
      ++idx;
      eraseNode(found);
      ret = true;
    }
    return ret;
  }

  /**
  Delete elements equal to the provided element from the list. *All* elements
  considered equal are deleted.

  @note **O(n)** performance
  @param e  the element to remove from the list
  @return  true iff at least one element was removed
  */
  template<typename E>
  inline bool LinkedList<E>::remove(const E& e)
  {
    ListNode<E>* cur = first_;
    bool ret = false;
    while (cur)
    {
      ListNode<E>* nxt = cur->nextNode();
      if (this->equal(cur->data(), e))
      {
        eraseNode(cur);
        ret = true;
      }
      cur = nxt;
    }
    return ret;
  }

  template<typename E>
  inline void LinkedList<E>::clear(void)
  {
    if (first_)
    {
      delete first_;
    }
    size_ = 0;
    first_ = nullptr;
    lastptr_ = nullptr;
  }

  /**
  Re-orders the list elements to be sorted according to the provided
  sorting function. This is implemented using merge-sort.

  @note **O(n*log(n))** performance
  @param comp  the comparison function to sue
  */
  template<typename E>
  inline void LinkedList<E>::sort(const OrderOp<E>& comp)
  {
    // exit early if trivial sort
    if (size_ < 2) return;
    // detach all nodes and store in array
    ListNode<E>** nodes = new ListNode<E>*[size_]();
    ListNode<E>* nod = first_;
    for (size_t idx = 0; idx < size_; ++idx)
    {
      nodes[idx] = nod;
      nod = nod->nextNode();
      nodes[idx]->detach();
    }
    // merge sublists of size 1, 2, 4, etc.
    for (size_t nsubs = size_; nsubs > 1; nsubs = (nsubs + 1) / 2)
    {
      // merge each sublist of the current size
      for (size_t idx = 0; idx < nsubs / 2; ++idx)
      {
        nodes[idx] = mergeNodes(nodes[2 * idx], nodes[(2 * idx) + 1], comp);
      }
      // odd number of pairs mean the last sublist gets a pass
      if (nsubs % 2) nodes[nsubs / 2] = nodes[nsubs - 1];
    }
    // re-point the first and last nodes to recover the list
    first_ = nodes[0];
    lastptr_ = first_;
    while (lastptr_->nextNode()) lastptr_ = lastptr_->nextNode();
    delete[] nodes;
  }

  template<typename E>
  inline size_t LinkedList<E>::size(void) const
  {
    return size_;
  }

  /**
  Obtain a reference to the element at the specified index in the list.

  @note **O(n)** performance
  @param index  the index of the element to be accessed
  @return  the element at the specified index
  */
  template<typename E>
  inline E& LinkedList<E>::at(size_t index)
  {
    return nodeAt(index)->data();
  }

  /**
  Obtain a reference to the element at the specified index in the list.

  @note **O(n)** performance
  @param index  the index of the element to be accessed
  @return  the element at the specified index
  */
  template<typename E>
  inline const E& LinkedList<E>::at(size_t index) const
  {
    return nodeAt(index)->data();
  }

  /**
  Obtain the first element in the list.

  @note **O(1)** performance
  @return element `[0]`
  */
  template<typename E>
  inline E& LinkedList<E>::first(void)
  {
    return first_->data();
  }

  /**
  Obtain the first element in the list.

  @note **O(1)** performance
  @return element `[0]`
  */
  template<typename E>
  inline const E& LinkedList<E>::first(void) const
  {
    return first_->data();
  }

  /**
  Obtain the last element in the list.

  @note **O(1)** performance
  @return element `[size - 1]`
  */
  template<typename E>
  inline E& LinkedList<E>::last(void)
  {
    return lastptr_->data();
  }

  /**
  Obtain the last element in the list.

  @note **O(1)** performance
  @return element `[size - 1]`
  */
  template<typename E>
  inline const E& LinkedList<E>::last(void) const
  {
    return lastptr_->data();
  }

  /**
  Insert a node before the given target node. The target position must be an
  element of this list, and the node added must not be an element of this list,
  or the operation will lead to corruption of the list.

  @note **O(1)** performance
  @param target  the position to add the new node (element of this list)
  @param nod  the node to add to the list (not yet an element of this list)
  */
  template<typename E>
  inline void
    LinkedList<E>::insertNode(ListNode<E>* const target, ListNode<E>* nod)
  {
    target->prepend(nod);
    if (target == first_)
    {
      first_ = nod;
    }
    ++size_;
  }

  /**
  Add a node to the end of the list. The added node must not be an element of
  this list or the operation will lead to corruption of the list.

  @note **O(1)** performance
  @param nod  the node to add to the list (not yet an element of this list)
  */
  template<typename E>
  inline void LinkedList<E>::addNode(ListNode<E>* nod)
  {
    if (!first_)
    {
      first_ = nod;
      size_ = 1;
    }
    else
    {
      lastptr_->append(nod);
      ++size_;
    }
    lastptr_ = nod;
  }

  /**
  Erase the specified node from the list. The node to be removed must be an
  element of this list or the operation will lead to corruption of the list.

  @note **O(1)** performance
  @param nod  the position to add the new node (element of this list)
  */
  template<typename E>
  inline void LinkedList<E>::eraseNode(ListNode<E>* nod)
  {
    if (nod == first_)
    {
      first_ = nod->nextNode();
    }
    if (nod == lastptr_)
    {
      lastptr_ = nod->previousNode();
    }
    nod->detach();
    delete nod;
    --size_;
  }

  /**
  Merges the two sub-lists provided according to the provided comparison
  function. If the two provided lists are order, then the resulting
  merged list will also be order.

  @param a  the head of a list to merge (ownership taken)
  @param b  the second of a list to merge (ownership taken)
  @param comp  the comparison function to sue
  @return  the head of a merged list (caller takes ownership)
  */
  template<typename E>
  inline ListNode<E>* LinkedList<E>::mergeNodes(
    ListNode<E>* a,
    ListNode<E>* b,
    const OrderOp<E>& comp)
  {
    // identify the first node to start
    ListNode<E>* temp = nullptr;
    if (comp.order(a->data(), b->data()))
    {
      temp = a;
      a = a->nextNode();
    }
    else
    {
      temp = b;
      b = b->nextNode();
    }
    // this is the head node that we will return
    ListNode<E>* mrg = temp->detach();
    ListNode<E>* ret = mrg;
    // pick the least of each pair of next-nodes
    while (a && b)
    {
      if (comp.order(a->data(), b->data()))
      {
        temp = a;
        a = a->nextNode();
      }
      else
      {
        temp = b;
        b = b->nextNode();
      }
      mrg->append(temp->detach());
      mrg = temp;
    }
    // finish by appending remaining nodes
    if (a) mrg->append(a);
    if (b) mrg->append(b);
    return ret;
  }

  /**
  Get the node located at the specified index. If the index is past the end
  of the list, then `NULL` is returned.

  @note **O(n)** performance
  @param index  the index of the element to access
  @return  the element at the given index
  */
  template<typename E>
  inline ListNode<E>* LinkedList<E>::nodeAt(size_t index) const
  {
    // if the index is not an element, then return null
    if (index >= size_)
    {
      return nullptr;
    }
    ListNode<E>* cur;
    // search from the first if that is closer
    if (index <= size_ / 2)
    {
      cur = first_;
      for (size_t idx = 0; idx < index; ++idx)
      {
        cur = cur->nextNode();
      }
    }
    // search from the last if that is closer
    else
    {
      cur = lastptr_;
      for (size_t idx = size_ - 1; idx > index; --idx)
      {
        cur = cur->previousNode();
      }
    }
    return cur;
  }
}

#endif
