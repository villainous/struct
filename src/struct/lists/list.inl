#ifndef VILN_LIST_INL
#define VILN_LIST_INL

#include <stddef.h>

#include "list_iterator.hpp"
#include "tree_set.hpp"
#include "detect_order.inl"
#include "detect_equal.inl"
#include "collection.inl"

#include "list.hpp"

namespace viln
{
  extern template class TreeSet<size_t>;

  /**
  Two lists are considered equal if they contain the same elements in the same
  order.

  @param o  the target list to compare elements
  @return  true iff the list elements and indices are the same
  */
  template<typename E>
  inline bool List<E>::operator==(const List<E>& o) const
  {
    if (this == &o) return true;
    // exit early if sizes are different
    if (this->size() != o.size()) return false;
    // compare each element in order
    Iterator<const E>* itr1 = this->iterator();
    Iterator<const E>* itr2 = o.iterator();
    bool ret = true;
    while (ret && itr1->hasNext() && itr2->hasNext())
    {
      ret &= this->equal(itr1->next(), itr2->next());
    }
    delete itr1;
    delete itr2;
    return ret;
  }

  /**
  Two lists are not considered equal if they contain different elements
  or if those elements are order differently.

  @param o  the target list to compare elements
  @return  true iff the list elements or indices are not the same
  */
  template<typename E>
  inline bool List<E>::operator!=(const List<E>& o) const
  {
    return !this->List<E>::operator==(o);
  }

  /**
  Obtain a reference to the element at the specified index in the list.

  @param index  the index of the element to be accessed
  @return  the element at the specified index
  */
  template<typename E>
  inline E& List<E>::operator[](size_t index)
  {
    return this->at(index);
  }

  /**
  Obtain a reference to the element at the specified index in the list.

  @param index  the index of the element to be accessed
  @return  the element at the specified index
  */
  template<typename E>
  inline const E& List<E>::operator[](size_t index) const
  {
    return this->at(index);
  }

  /**
  Obtain a `ListIterator` which starts at the beginning of the list. The cursor
  of the iterator begins just before the first element of the list, such that
  the first call to `ListIterator::next()` would return the element at index
  0 (assuming such an element exists).
  */
  template<typename E>
  inline ListIterator<E>* List<E>::iterator(void)
  {
    return this->listIterator(0);
  }

  template<typename E>
  inline ListIterator<const E>* List<E>::iterator(void) const
  {
    return this->listIterator(0);
  }

  /**
  Delete the elements at all the specified indices. The provided indices must
  be an order list with no repitition.

  @param indices  the positions of the elements to be removed
  @return  true iff at least one element was removed
  */
  template<typename E>
  inline bool List<E>::eraseAll(const ListIndices& indices)
  {
    size_t acc = 0;
    for (size_t idx : indices)
    {
      if (erase(idx - acc)) ++acc;
    }
    return acc;
  }

  /**
  Insert each element in the provided `Collection` into this list at the given
  index. Elements inserted will be in the order given by the collection's
  iterator, so if the other collection is another `List`, then the added range
  will be expanded in the same order as that list.

  @param index  the position in the list to insert the elements
  @param c  the elements to insert into this list
  @return  true iff at least one element was added
  */
  template<typename E>
  inline bool List<E>::insertAll(size_t index, const ConstIterable<E>& c)
  {
    // Insertion at the last index is equivalent to add.
    if (index == this->size())
    {
      return this->addAll(c);
    }

    // Self-insert might be infinitely recursive, so add as a data copy.
    if (this == &c)
    {
      const E* dataCopy = this->toArray();
      const size_t sizeCopy = this->size();
      bool ret = false;
      for (size_t cidx = 0; cidx < sizeCopy; ++cidx)
      {
        ret |= this->insert(index + cidx, dataCopy[cidx]);
      }
      delete[] dataCopy;
      return ret;
    }

    // Insert using iterator.
    return this->insertAll(index, c.iterator());
  }

  /**
  Add all the elements provided by the given iterator to this list at the
  specified index. Elements will be added in order as specified by the
  given interator. Note that if the iterator is not finite, then this
  operation will never terminate.

  @warning If the iterator points to this list, the behavior is undefined.

  @param index  the position in the list to insert the elements
  @param itr  an iterator over elements to add (takes ownership)
  @return  true iff at least one element was added
  */
  template<typename E>
  inline bool List<E>::insertAll(size_t index, Iterator<const E>* itr)
  {
    bool ret = false;
    size_t idx = index;
    while (itr->hasNext())
    {
      const E& elem = itr->next();
      ret |= insert(idx++, elem);
    }
    delete itr;
    return ret;
  }

  /**
  Add an element to the end of the list.

  @param e  the element to be added (object moved)
  @return  true iff an element was added to the list
  */
  template<typename E>
  inline bool List<E>::add(E&& e)
  {
    return insert(this->size(), (E&&)e);
  }

  /**
  Add an element to the end of the list.

  @param e  the element to be added (object moved)
  @return  true iff an element was added to the list
  */
  template<typename E>
  inline bool List<E>::add(const E& e)
  {
    return insert(this->size(), e);
  }

  /**
  Sort the list according to the provided less-than function-pointer. Elements
  are sorted in ascending order.

  @see viln::order_func
  */
  template<typename E>
  inline void List<E>::sort(order_func<E> comp)
  {
    OrderFunc<E> op(comp);
    sort(op);
  }

  /**
  Obtain the first element in the list.

  @return element `[0]`
  */
  template<typename E>
  inline E& List<E>::first(void)
  {
    return this->at(0);
  }

  /**
  Obtain the first element in the list.

  @return element `[0]`
  */
  template<typename E>
  inline const E& List<E>::first(void) const
  {
    return this->at(0);
  }

  /**
  Obtain the last element in the list.

  @return element `[size - 1]`
  */
  template<typename E>
  inline E& List<E>::last(void)
  {
    return this->at(this->size() - 1);
  }

  /**
  Obtain the last element in the list.

  @return element `[size - 1]`
  */
  template<typename E>
  inline const E& List<E>::last(void) const
  {
    return this->at(this->size() - 1);
  }

  /**
  Determine if this collection contains an element equal to the specified
  element.

  @param e  the element to search for
  @return  true iff the list contains such an element
  */
  template<typename E>
  inline bool List<E>::contains(const E& e) const
  {
    return firstIndexOf(e) < this->size();
  }

  /**
  Find the first index in this list of an element which is equal to the given
  element. If there is no such element in this list, then the return value will
  be equal to `size()`.

  @param e  the element to search for
  @return  the first index at which the element was found
  */
  template<typename E>
  inline size_t List<E>::firstIndexOf(const E& e) const
  {
    ListIterator<const E>* itr = this->listIterator(0);
    size_t idx;
    for (idx = 0; itr->hasNext(); ++idx)
    {
      if (this->equal(itr->next(), e))
      {
        break;
      }
    }
    delete itr;
    return idx;
  }

  /**
  Find the last index in this list of an element which is equal to the given
  element. If there is no such element in this list, then the return value will
  be equal to `size()`.

  @param e  the element to search for
  @return  the last index at which the element was found
  */
  template<typename E>
  inline size_t List<E>::lastIndexOf(const E& e) const
  {
    const size_t siz = this->size();
    ListIterator<const E>* itr = this->listIterator(siz);
    size_t idx;
    for (idx = 0; itr->hasPrevious(); ++idx)
    {
      if (this->equal(itr->previous(), e))
      {
        break;
      }
    }
    delete itr;
    return idx == siz ? idx : siz - idx - 1;
  }
}

#endif
