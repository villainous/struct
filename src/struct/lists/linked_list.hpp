#ifndef VILN_LINKED_LIST_HPP
#define VILN_LINKED_LIST_HPP

#include <stddef.h>
#include <initializer_list>

#include "list_node_iterator.hpp"
#include "list.hpp"

namespace viln
{
  /// \cond DO_NOT_DOCUMENT
  template<typename E> class ListNode;
  /// \endcond

  /// A type of list using doubly-linked elements.
  /**
  A `LinkedList` is a `List` implementation which uses doubly-linked nodes
  as a container for its elements. This makes insertion/deletion operations
  more efficient at the cost of making index-based access more costly.

  This class uses `DetectEqual<E>` for the templated type, so either
  `operator==` must be defined for the element-type, or the `equal(E,E)`
  method should be overridden.

  This class does not implement thread-safety, nor does it implement bounds-
  checking for methods that accept an index as a parameter.
  
  Index-based operations all have worst-case performance of **O(m*n)**
  where **m** is the size of the input (if applicable), and **n** is the size
  of the list. Non-index-based operations, or those using an iterator as an
  index, have **O(m)** performance. See the documentation for individual
  methods for exceptions to these general rules.

  Note that modification of the list while there are active iterators will
  cause undefined behavior in those iterators. The exception is modification
  of the list via methods that accept the iterator as a parameter, which do
  not invalidate the iterator, but may modify it.
  */
  template<typename E>
  class LinkedList : public List<E>
  {
  public:
    /// Default constructor.
    LinkedList(void);
    /// Initializer-list brace-construction.
    LinkedList(std::initializer_list<E> li);
    /// Copy constructor.
    LinkedList(const LinkedList<E>& o);
    /// Move-constructor.
    LinkedList(LinkedList<E>&& o);
    /// Destructor.
    virtual ~LinkedList(void);

    /// Assignment operator.
    LinkedList<E>& operator=(const LinkedList<E>& o);
    /// Move-assign operator.
    LinkedList<E>& operator=(LinkedList<E>&& o);

    /// Construct a new iterator over the list starting at the given index.
    virtual ListNodeIterator<E>* listIterator(size_t idx) override;
    /// Construct a new iterator over the list starting at the given index.
    virtual ListIterator<const E>* listIterator(size_t idx) const override;

    /// Insert the provided pointer at the previous iterator position.
    virtual bool insert(ListNodeIterator<E>* const index, E* e);
    /// Move an element into the list at the previous iterator position.
    virtual bool insert(ListNodeIterator<E>* const index, E&& e);
    /// Insert an additional element at the previous iterator position.
    virtual bool insert(ListNodeIterator<E>* const index, const E& e);
    /// Take the pointer and insert it into the list at the given index.
    virtual bool insert(size_t index, E* e);
    /// Move an element into the list at the given index.
    virtual bool insert(size_t index, E&& e) override;
    /// Insert an additional element at the given index.
    virtual bool insert(size_t index, const E& e) override;
    /// Insert each element of the provided collection into this one.
    virtual bool
      insertAll(ListNodeIterator<E>* const index, const ConstIterable<E>& c);
    /// Insert each element provided by the iterator into this list.
    virtual bool
      insertAll(ListNodeIterator<E>* const index, Iterator<const E>* itr);
    /// Insert each element from the given collection into this one.
    virtual bool insertAll(size_t index, const ConstIterable<E>& c) override;
    /// Insert each element provided by the iterator into this list.
    virtual bool insertAll(size_t index, Iterator<const E>* itr) override;
    /// Take the provided pointer and add it to the list.
    virtual bool add(E* e);
    /// Move an element into the list at the end.
    virtual bool add(E&& e) override;
    /// Add an element to the list at the end.
    virtual bool add(const E& e) override;
    /// Remove the next element that would be returned by the iterator.
    virtual bool eraseNext(ListNodeIterator<E>* const index);
    /// Remove the previous element returned by the iterator.
    virtual bool erasePrevious(ListNodeIterator<E>* const index);
    /// Remove the element at the specified index.
    virtual bool erase(size_t index) override;
    /// Remove elements at each of the specified indices.
    virtual bool eraseAll(const ListIndices& indices) override;
    /// Remove an element from the collection.
    virtual bool remove(const E& e) override;
    /// Remove all elements from this collection.
    virtual void clear(void) override;
    /// Sort the elements in the list according to the given ordering.
    virtual void sort(const OrderOp<E>& comp) override;
    using List<E>::sort;

    /// Determine the size of this collection.
    virtual size_t size(void) const override;

    /// Indexed access into the list elements.
    virtual E& at(size_t index) override;
    virtual const E& at(size_t index) const override;
    /// Get the first element in the list.
    virtual E& first(void) override;
    virtual const E& first(void) const override;
    /// Get the last element in the list.
    virtual E& last(void) override;
    virtual const E& last(void) const override;

  protected:
    /// Insert a node before the target node.
    void insertNode(ListNode<E>* const target, ListNode<E>* nod);
    /// Add the given node to the end of the list.
    void addNode(ListNode<E>* nod);
    /// Remove the given node, respecting its neighbors.
    void eraseNode(ListNode<E>* nod);
    /// Merge the two provided order sub-lists using the given comparator.
    ListNode<E>*
      mergeNodes(ListNode<E>* a, ListNode<E>* b, const OrderOp<E>& comp);

  private:
    /// Find the node at the specified index.
    ListNode<E>* nodeAt(size_t index) const;

    // the current number of valid elements
    size_t size_;
    // the first node in the list
    ListNode<E>* first_;
    // the last node in the list (ownership is via first not last)
    ListNode<E>* lastptr_;
  };
}

#endif
