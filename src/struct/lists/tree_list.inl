#ifndef VILN_TREE_LIST_INL
#define VILN_TREE_LIST_INL

#include <stddef.h>

#include "avl_tree.inl"
#include "tree_node.inl"
#include "tree_node_iterator.inl"
#include "list.inl"

#include "const_list_iterator.hpp"
#include "tree_list.hpp"

namespace viln
{
  template<typename E>
  TreeList<E>::TreeList(void) :
    tree_(nullptr),
    size_(0)
  {
    // no-op
  }

  template<typename E>
  inline TreeList<E>::TreeList(std::initializer_list<E> li) :
    tree_(nullptr),
    size_(0)
  {
    for (const E& e : li) TreeList::add(e);
  }

  template<typename E>
  TreeList<E>::TreeList(const TreeList<E>& o) :
    tree_(nullptr),
    size_(0)
  {
    TreeList::addAll(o);
  }

  template<typename E>
  inline TreeList<E>::TreeList(TreeList<E>&& o) :
    tree_(o.tree_),
    size_(o.size_)
  {
    o.tree_ = nullptr;
  }

  template<typename E>
  TreeList<E>::~TreeList(void)
  {
    if (tree_) delete tree_;
  }

  template<typename E>
  inline TreeList<E>& TreeList<E>::operator=(const TreeList<E>& o)
  {
    if (this == &o) return *this;
    TreeList::clear();
    TreeList::addAll(o);
    return *this;
  }

  template<typename E>
  inline TreeList<E>& TreeList<E>::operator=(TreeList<E>&& o)
  {
    if (this == &o) return *this;
    if (tree_) delete tree_;
    size_ = o.size_;
    tree_ = o.tree_;
    o.tree_ = nullptr;
    return *this;
  }

  /**
  Create an iterator over the elements in the list. Elements will be iterated
  over in increasing order within the list. Be aware that modification of the
  list while an iterator is active may invalidate the state of the iterator.

  @return  a new iterator over the list elements (caller takes ownership)
  */
  template<typename E>
  inline ListIterator<E>* TreeList<E>::listIterator(size_t idx)
  {
    return this->nodeIterator(idx);
  }

  /**
  Create an iterator over the elements in the list. Elements will be iterated
  over in increasing order within the list. Be aware that modification of the
  list while an iterator is active may invalidate the state of the iterator.

  @return  a new iterator over the list elements (caller takes ownership)
  */
  template<typename E>
  inline ListIterator<const E>* TreeList<E>::listIterator(size_t idx) const
  {
    TreeNode<E>* iroot = tree_ ? tree_->root() : nullptr;
    TreeNode<E>* iprev = idx > 0 ? nodeAt(idx - 1) : nullptr;
    TreeNodeIterator<E>* itr = new TreeNodeIterator<E>(iroot, iprev);
    return new ConstListIterator<E>(itr);
  }

  /**
  Insert an element at the specified index in the list. Elements at or above
  the specified index will have their index incremented by one.

  This version takes ownership over the provided element pointer.

  @note **O(log(n))** worst-case performance, amortized over all add/remove ops
  @param index  the position in which to insert an element
  @param e  the element to be inserted (list takes ownership)
  @return  true iff the element was added to the list
  */
  template<typename E>
  inline bool TreeList<E>::insert(size_t index, E* e)
  {
    if (index >= size_)
    {
      add(e);
    }
    else
    {
      tree_->addPredecessor(this->nodeAt(index), e);
      ++size_;
    }
    return true;
  }

  /**
  Insert an element at the specified index in the list. Elements at or above
  the specified index will have their index incremented by one.

  This version calls the move-constructor to take ownership of the provided
  element. This version of insertion is used when the object being inserted
  is constructed in-line with the insert operation.

  @note **O(log(n))** worst-case performance, amortized over all insert/add
  @param index  the position in which to insert an element
  @param e  the element to be inserted (object moved)
  @return  true iff the element was added to the list
  */
  template<typename E>
  inline bool TreeList<E>::insert(size_t index, E&& e)
  {
    return insert(index, new E((E&&)e));
  }

  /**
  Insert an element at the specified index in the list. Elements at or above
  the specified index will have their index incremented by one.

  This version makes a copy of the provided element reference using its copy-
  constructor.

  @note **O(log(n))** worst-case performance, amortized over all add/remove ops
  @param index  the position in which to insert an element
  @param e  the element to be inserted (object copied)
  @return  true iff the element was added to the list
  */
  template<typename E>
  inline bool TreeList<E>::insert(size_t index, const E& e)
  {
    return insert(index, new E(e));
  }

  /**
  Add an element to the list if the list does not already contain the element.

  This version takes ownership of the provided element pointer.

  @note **O(log(n))** worst-case performance
  @return  true iff the element was added to the list
  */
  template<typename E>
  inline bool TreeList<E>::add(E* e)
  {
    TreeNode<E>* nod = tree_ ? tree_->root() : nullptr;
    // no root so create a new tree
    if (!nod)
    {
      if (tree_) delete tree_;
      tree_ = new AvlTree<E>(e);
      size_ = 1;
    }
    // add a successor to the rightmost node
    else
    {
      while (nod->rightChild())
      {
        nod = nod->rightChild();
      }
      tree_->addSuccessor(nod, e);
      ++size_;
    }
    return true;
  }

  /**
  Add an element to the list if the list does not already contain the element.

  This version calls the move-constructor to take ownership of the provided
  element. This version of insertion is used when the object being inserted
  is constructed in-line with the insert operation.

  @note **O(log(n))** worst-case performance
  @return  true iff the element was added to the list
  */
  template<typename E>
  inline bool TreeList<E>::add(E&& e)
  {
    return add(new E((E&&)e));
  }

  /**
  Add an element to the list if the list does not already contain the element.

  This version makes a copy of the provided element reference using its copy-
  constructor.

  @note **O(log(n))** worst-case performance
  @return  true iff the element was added to the list
  */
  template<typename E>
  inline bool TreeList<E>::add(const E& e)
  {
    return add(new E(e));
  }

  /**
  Delete the element at the given index from the list. Elements above the given
  index will have their indices decremented by one.

  @note **O(log(n))** worst-case performance, amortized over all add/remove ops
  @param index  the position of the element to be removed
  @return  true iff the element was removed from the list
  */
  template<typename E>
  inline bool TreeList<E>::erase(size_t index)
  {
    TreeNode<E>* nod = nodeAt(index);
    if (nod)
    {
      tree_->remove(nod);
      --size_;
      return true;
    }
    return false;
  }

  /**
  Delete elements equal to the provided element from the list. *All* elements
  considered equal are deleted.

  @note **O(log(n))** worst-case performance, amortized over all add/remove ops
  @param e  the element to remove from the list
  @return  true iff at least one element was removed
  */
  template<typename E>
  inline bool TreeList<E>::remove(const E& e)
  {
    ListIndices li;
    size_t idx = 0;
    for (const E& elem : (*this))
    {
      if (this->equal(elem, e))
      {
        li.add(idx);
      }
      ++idx;
    }
    return this->eraseAll(li);
  }

  template<typename E>
  inline void TreeList<E>::clear(void)
  {
    if (tree_) delete tree_;
    tree_ = nullptr;
    size_ = 0;
  }

  /**
  Sort the nodes in this list according to the specified sorting function.
  The sorting implementation is insertion-sort.
  
  @note  **O(n*log(n))** performance
  @param comp  the comparison function to use to sort the nodes
  */
  template<typename E>
  inline void TreeList<E>::sort(const OrderOp<E>& comp)
  {
    // skip if empty
    if (!tree_) return;
    TreeNode<E>* oldroot = tree_->root();
    size_t siz = size();
    // put all the nodes in an array
    TreeNode<E>** nodes = new TreeNode<E>*[siz]();
    TreeNodeIterator<E>* itr = this->nodeIterator(0);
    size_t acc = 0;
    while (itr->hasNext()) nodes[acc++] = itr->nextNode();
    delete itr;
    // dissociate all the nodes (temporarily own all but root)
    for (size_t idx = 0; idx < siz; ++idx)
    {
      nodes[idx]->detachLeft();
      nodes[idx]->detachRight();
    }
    // insert each node in order into the tree
    for (size_t idx = 0; idx < siz; ++idx)
    {
      TreeNode<E>* enod = nodes[idx];
      // old root node is already in the tree - insert each other node
      if (enod != oldroot) insertNode(enod, comp);
    }
    delete[] nodes;
  }

  template<typename E>
  inline size_t TreeList<E>::size(void) const
  {
    return size_;
  }

  /**
  Obtain a reference to the element at the specified index in the list.

  @note **O(log(n))** worst-case performance, amortized over all add/remove ops
  @param index  the index of the element to be accessed
  @return  the element at the specified index
  */
  template<typename E>
  inline E& TreeList<E>::at(size_t index)
  {
    return nodeAt(index)->data();
  }

  /**
  Obtain a reference to the element at the specified index in the list.

  @note **O(log(n))** worst-case performance, amortized over all add/remove ops
  @param index  the index of the element to be accessed
  @return  the element at the specified index
  */
  template<typename E>
  inline const E& TreeList<E>::at(size_t index) const
  {
    return nodeAt(index)->data();
  }

  template<typename E>
  inline TreeNode<E>* const TreeList<E>::root(void)
  {
    return tree_ ? tree_->root() : nullptr;
  }

  template<typename E>
  inline const TreeNode<E>* TreeList<E>::root(void) const
  {
    return tree_ ? tree_->root() : nullptr;
  }

  template<typename E>
  inline TreeNodeIterator<E>* TreeList<E>::nodeIterator(size_t idx)
  {
    TreeNode<E>* iroot = tree_ ? tree_->root() : nullptr;
    TreeNode<E>* iprev = idx > 0 ? nodeAt(idx - 1) : nullptr;
    return new TreeNodeIterator<E>(iroot, iprev);
  }

  /**
  Access the node at the given index if it exists, or `NULL` otherwise.

  @note **O(log(n))** worst-case performance, amortized over all add/remove ops
  @param index  the index of the node to access
  @return  a pointer to the node at the given index
  */
  template<typename E>
  inline TreeNode<E>* TreeList<E>::nodeAt(size_t index) const
  {
    // if the index is not an element, then return null
    if (index >= size_) return nullptr;
    // search on the left
    else if (index <= size_ / 2) return leftNodeAt(index);
    // search on the right
    return rightNodeAt(index);
  }

  template<typename E>
  inline TreeNode<E>* TreeList<E>::rightNodeAt(size_t index) const
  {
    TreeNode<E>* cur = tree_ ? tree_->root() : nullptr;
    if (!cur) return nullptr;
    // getting the last element is very easy
    if (index == size_ - 1)
    {
      while (cur->rightChild())
      {
        cur = cur->rightChild();
      }
      return cur;
    }
    // search elsewhere in the second half of the tree
    size_t subdx = size_ - index - 1;
    while (cur)
    {
      TreeNode<E>* right = cur->rightChild();
      size_t rightsz = right ? right->size() : 0;
      // current node is at the desired index
      if (rightsz == subdx)
      {
        return cur;
      }
      // element is in right subtree
      if (rightsz > subdx)
      {
        cur = right;
      }
      // element is in left subtree
      else
      {
        subdx -= 1 + rightsz;
        cur = cur->leftChild();
      }
    }
    return cur;
  }

  template<typename E>
  inline TreeNode<E>* TreeList<E>::leftNodeAt(size_t index) const
  {
    TreeNode<E>* cur = tree_ ? tree_->root() : nullptr;
    if (!cur) return nullptr;
    // getting the first element is very easy
    if (index == 0)
    {
      while (cur->leftChild())
      {
        cur = cur->leftChild();
      }
      return cur;
    }
    // search elsewhere in the first half of the tree
    size_t subdx = index;
    while (cur)
    {
      TreeNode<E>* left = cur->leftChild();
      size_t leftsz = left ? left->size() : 0;
      // current node is at the desired index
      if (leftsz == subdx)
      {
        return cur;
      }
      // element is in left subtree
      if (leftsz > subdx)
      {
        cur = left;
      }
      // element is in right subtree
      else
      {
        subdx -= 1 + leftsz;
        cur = cur->rightChild();
      }
    }
    return cur;
  }

  /**
  This method is a helper for `sort()` which inserts a specified node at the
  position dictated by the comparison function.

  @note **O(log(n))** worst-case performance
  @param enod  the node to insert (ownership taken by tree)
  @param comp  the list-ordering comparison to use
  */
  template<typename E>
  inline void
    TreeList<E>::insertNode(TreeNode<E>* enod, const OrderOp<E>& comp)
  {
    TreeNode<E>* nod = tree_->root();
    const E& e = enod->data();
    while (nod)
    {
      const E& ndata = nod->data();
      if (comp.order(e, ndata))
      {
        if (nod->leftChild())
        {
          nod = nod->leftChild();
        }
        else
        {
          tree_->addPredecessor(nod, enod);
          break;
        }
      }
      else if (comp.order(ndata, e))
      {
        if (nod->rightChild())
        {
          nod = nod->rightChild();
        }
        else
        {
          tree_->addSuccessor(nod, enod);
          break;
        }
      }
      else
      {
        tree_->addSuccessor(nod, enod);
        break;
      }
    }
  }
}

#endif
