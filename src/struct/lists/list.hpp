#ifndef VILN_LIST_HPP
#define VILN_LIST_HPP

#include <stddef.h>

#include "list_iterator.hpp"
#include "detect_order.hpp"
#include "detect_equal.hpp"
#include "collection.hpp"

namespace viln
{
  /// \cond DO_NOT_DOCUMENT
  template<typename E> class TreeSet;
  /// \endcond

  typedef TreeSet<size_t> ListIndices;

  /// A type of collection with indexed ordering of elements.
  /**
  A `List` is a type of `Collection` providing precise control over the indexed
  order of each element. Elements may be accessed or inserted by index, making
  lists operate similarly to arrays.

  This class uses `DetectEqual<E>` for the templated type, so either
  `operator==` must be defined for the element-type, or the `equal(E,E)`
  method should be overridden.

  Lists provide a special type of `Iterator`, the `ListIterator`, which allows
  traversal of the list in either direction. These iterators can additionally
  be obtained starting at indices other than the beginning, allowing arbitrary
  sub-list iteration.
  */
  template<typename E>
  class List :
    public Collection<E>,
    public Iterable<E>,
    protected DetectEqual<E>
  {
  public:
    /// Virtual destructor.
    virtual ~List(void) {};

    /// Lists are equal if they have the same elements in the same order.
    virtual bool operator==(const List<E>& o) const;
    /// Lists are not  equal if they have differing elements or ordering.
    virtual bool operator!=(const List<E>& o) const;
    /// Indexed access into the list elements.
    virtual E& operator[](size_t index);
    virtual const E& operator[](size_t index) const;

    /// Construct a new iterator over the elements of this object.
    virtual ListIterator<E>* iterator(void) override;
    virtual ListIterator<const E>* iterator(void) const override;
    using Iterable<E>::begin;
    using Iterable<E>::end;

    /// Construct a new iterator over the list starting at the given index.
    virtual ListIterator<E>* listIterator(size_t index) = 0;
    /// Construct a new iterator over the list starting at the given index.
    virtual ListIterator<const E>* listIterator(size_t index) const = 0;

    /// Move an element into the list at the end.
    virtual bool add(E&& e) override;
    /// Add an element to the list at the end.
    virtual bool add(const E& e) override;
    /// Move an element into the list at the given index.
    virtual bool insert(size_t index, E&& e) = 0;
    /// Insert an additional element at the given index.
    virtual bool insert(size_t index, const E& e) = 0;
    /// Insert each element from the given collection into this one.
    virtual bool insertAll(size_t index, const ConstIterable<E>& c);
    /// Insert all elements returned by the iterator into this collection.
    virtual bool insertAll(size_t index, Iterator<const E>* itr);
    /// Remove the element at the specified index.
    virtual bool erase(size_t index) = 0;
    /// Remove elements at each of the specified indices.
    virtual bool eraseAll(const ListIndices& indices);
    /// Sort the elements in the list according to the given ordering.
    virtual void sort(const OrderOp<E>& comp) = 0;
    /// Sort the elements in the list according to the given ordering.
    virtual void sort(order_func<E> comp = viln::detect_order<E>);

    /// Indexed access into the list elements.
    virtual E& at(size_t index) = 0;
    virtual const E& at(size_t index) const = 0;
    /// Get the first element in the list.
    virtual E& first(void);
    virtual const E& first(void) const;
    /// Get the last element in the list.
    virtual E& last(void);
    virtual const E& last(void) const;

    /// Determine if the provided element is contained in this collection.
    virtual bool contains(const E& e) const override;
    /// Find the index of the first occurrence of the given element.
    virtual size_t firstIndexOf(const E& e) const;
    /// Find the index of the last occurrence of the given element.
    virtual size_t lastIndexOf(const E& e) const;
  };
}

#endif
