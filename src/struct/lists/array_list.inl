#ifndef VILN_ARRAY_LIST_INL
#define VILN_ARRAY_LIST_INL

#include <stddef.h>
#include <stdlib.h>
#include <string.h>

#include "array_iterator.inl"
#include "list.inl"

#include "array_list.hpp"

namespace viln
{
  /**
  Create an empty `ArrayList` with the given initial capacity. In situations
  where the number of expected elements is known, it is desirable to set the
  initial capacity in order to minimize the number of re-allocation operations
  needed.

  @param capacity  the initial capacity of the list
  */
  template<typename E>
  inline ArrayList<E>::ArrayList(size_t capacity) :
    size_(0),
    capacity_(capacity),
    array_(viln_hidden::vmalloc<E>(capacity))
  {
    // no-op
  }

  /**
   Create an `ArrayList` initially populated with the specified elements. The
   created list's initial capacity will exactly match the number of elements
   provided.
   
   @param li  the list of items to add to the list
   */
  template<typename E>
  inline ArrayList<E>::ArrayList(std::initializer_list<E> li) :
    size_(0),
    capacity_(li.size()),
    array_(viln_hidden::vmalloc<E>(li.size()))
  {
    for (const E& e : li) ArrayList::add(e);
  }

  /**
  Create an `ArrayList` using the given array of elements. Ownership of the
  provided array is transferred to the newly created object, and any
  modification of the `ArrayList` may result in this array being invalidated.

  @param size  the number of elements in the array
  @param arr  the elements to add to the list (list takes ownership)
  */
  template<typename E>
  inline ArrayList<E>::ArrayList(E* arr, size_t size) :
    size_(size),
    capacity_(size),
    array_(arr)
  {
    // no-op
  }

  template<typename E>
  inline ArrayList<E>::ArrayList(const ArrayList<E>& o) :
    size_(0),
    capacity_(o.size_),
    array_(viln_hidden::vmalloc<E>(o.size_))
  {
    ArrayList::addAll(o);
  }

  template<typename E>
  inline ArrayList<E>::ArrayList(ArrayList<E>&& o) :
    size_(o.size_),
    capacity_(o.capacity_),
    array_(o.array_)
  {
    o.array_ = nullptr;
  }

  template<typename E>
  inline ArrayList<E>::~ArrayList(void)
  {
    if (array_)
    {
      ArrayList::clear();
      viln_hidden::vfree<E>(array_);
    }
  }

  template<typename E>
  inline ArrayList<E>& ArrayList<E>::operator=(const ArrayList<E>& o)
  {
    if (this == &o) return *this;
    ArrayList::clear();
    ArrayList::addAll(o);
    return *this;
  }

  template<typename E>
  inline ArrayList<E>& ArrayList<E>::operator=(ArrayList<E>&& o)
  {
    if (this == &o) return *this;
    if (array_)
    {
      ArrayList::clear();
      viln_hidden::vfree<E>(array_);
    }
    capacity_ = o.capacity_;
    size_ = o.size_;
    array_ = o.array_;
    o.array_ = nullptr;
    return *this;
  }

  /**
  Create an iterator over this list. This allows access to and modification of
  elements being iterated over.

  Note that any modification to the list may invalidate the iterator, and
  lead to unspecified behavior of said iterator.

  @param index  the initial position of the iterator to create
  @return  an iterator over the list elements (caller takes ownership)
  */
  template<typename E>
  inline ListIterator<E>* ArrayList<E>::listIterator(size_t index)
  {
    return new ArrayIterator<E>(array_, size_, index);
  }

  /**
  Create a const-iterator over this list. This allows access to elements being
  iterated over.

  Note that any modification to the list may invalidate the iterator, and
  lead to unspecified behavior of said iterator.

  @param index  the initial position of the iterator to create
  @return  an iterator over the list elements (caller takes ownership)
  */
  template<typename E>
  inline ListIterator<const E>* ArrayList<E>::listIterator(size_t index) const
  {
    return new ArrayIterator<const E>(array_, size_, index);
  }

  /**
  Insert an element at the given index. The elements currently at this index
  or above will have their indicies shifted up by one. This version makes use
  of the move-constructor, if available.

  @note **O(n)** worst-case performance
  @param index  the index at which to insert the element
  @param e  element to insert into the list (element moved)
  @return  true iff the element was added
  */
  template<typename E>
  inline bool ArrayList<E>::insert(size_t index, E&& e)
  {
    split(index, 1);
    new ((void*)(array_ + index)) E((E&&)e);
    ++size_;
    return true;
  }

  /**
  Insert an element at the given index. The elements currently at this index
  or above will have their indicies shifted up by one.

  @note **O(n)** worst-case performance
  @param index  the index at which to insert the element
  @param e  element to insert into the list (element copied)
  @return  true iff the element was added
  */
  template<typename E>
  inline bool ArrayList<E>::insert(size_t index, const E& e)
  {
    if (&e >= array_ && &e < array_ + size_)
    {
      E ecpy(e);
      split(index, 1);
      new ((void*)(array_ + index)) E((E&&)ecpy);
    }
    else
    {
      split(index, 1);
      new ((void*)(array_ + index)) E(e);
    }
    ++size_;
    return true;
  }

  /**
  Insert each element of the provided collection into this list, in the order
  returned by that collection's iterator. The elements currently at this index
  or above will have their indices shifted up by the size of the collection
  added.

  @note **O(n+m)** worst-case performance
  @param index  the index at which to insert the element
  @param c  collection of elements to copy into this list
  @return  true iff at least one element was added
  */
  template<typename E>
  inline bool ArrayList<E>::insertAll(size_t index, const ConstIterable<E>& c)
  {
    // Self-insert might be infinitely recursive, so add as a data copy.
    if (this == &c)
    {
      if (this->isEmpty()) return false;
      split(index, this->size());
      // elements before the fragment
      for (int idx = 0; idx < index; ++idx)
      {
        new ((void*)(array_ + index + idx)) E(array_[idx]);
      }
      // elements after the fragment
      for (int idx = 0; idx < size_ - index; ++idx)
      {
        new((void*)(array_ + (2 * index) + idx)) E(array_[index + size_ + idx]);
      }
      size_ *= 2;
      return true;
    }

    // Insert using iterator.
    return this->insertAll(index, c.iterator());
  }

  /**
  Insert copies of all the elements of the provided iterator into this list
  at the given index in the order they are returned by the provided iterator.

  @warning If the iterator points to this list, the behavior is undefined.

  @note **O(n+m)** worst-case performance
  @param index  the position in which to insert elements
  @param itr  an iterator over elements to insert (takes ownership)
  @return  true iff the element was added to the list
  */
  template<typename E>
  inline bool ArrayList<E>::insertAll(size_t index, Iterator<const E>* itr)
  {
    // exit early if no elements
    if (!itr->hasNext())
    {
      delete itr;
      return false;
    }
    // move all the elements right of the target to a temporary location
    size_t tempCount = size_ - index;
    E* temp = viln_hidden::vmalloc<E>(tempCount);
    objmove(temp, array_ + index, tempCount);
    size_ -= tempCount;
    // add each element one at a time since we don't know how many we will get
    bool ret = addAll(itr);
    // add back the remaining elements
    split(size_, tempCount);
    objmove(array_ + size_, temp, tempCount);
    size_ += tempCount;
    viln_hidden::vfree<E>(temp);
    return ret;
  }

  /**
  Add elements from the provided collection to the end of the list. Elements
  will be added in the order returned by that collection's iterator.

  @note **O(m*log(n))** amortized worst-case performance
  @param c  collection of elements to copy into this list
  @return true iff at least one element was added
  */
  template<typename E>
  inline bool ArrayList<E>::addAll(const ConstIterable<E>& c)
  {
    return insertAll(this->size(), c);
  }

  /**
  Add copies of all the elements of the provided iterator to this list in the
  order they are returned by the provided iterator.

  **If the provided iterator is an iterator over this collection, then the
  behavior of this method is undefined.

  @note **O(m*log(n))** amortized worst-case performance
  @param itr  an iterator over elements to add (takes ownership)
  @return  true iff the element was added to the list
  */
  template<typename E>
  inline bool ArrayList<E>::addAll(Iterator<const E>* itr)
  {
    // add each element one at a time since we don't know how many we will get
    bool ret = false;
    while (itr->hasNext())
    {
      ret |= this->add(itr->next());
    }
    delete itr;
    return ret;
  }

  /**
  Remove the element at the specified index from the list and delete it.

  @note **O(n)** worst-case performance
  @param index  the position of the element to be removed
  @return  true iff an element was removed
  */
  template<typename E>
  inline bool ArrayList<E>::erase(size_t index)
  {
    array_[index].~E();
    objmove(array_ + index, array_ + index + 1, size_ - index - 1);
    --size_;
    return true;
  }

  /**
  Remove all the elements at the specified indices and delete them.

  @note **O(n+m)** worst-case performance
  @param indices  the indices of elements to remove
  @return  true iff at least one element was removed
  */
  template<typename E>
  inline bool ArrayList<E>::eraseAll(const ListIndices& indices)
  {
    size_t removed = indices.size();
    for (size_t idx : indices)
    {
      array_[idx].~E();
    }
    defragment(indices);
    size_ -= removed;
    return removed;
  }

  /**
  Remove all elements of this list which are equal to the provided element.

  @note **O(n)** worst-case performance
  @param e  the element to remove from this list
  @return  true iff an element was removed
  */
  template<typename E>
  inline bool ArrayList<E>::remove(const E& e)
  {
    ListIndices toRemove;
    for (size_t idx = 0; idx < size_; ++idx)
    {
      if (this->equal(array_[idx], e)) toRemove.add(idx);
    }
    return eraseAll(toRemove);
  }

  /**
  Remove all elements of this list which are equal to any elements of the
  provided collection.
  
  @note **O(n*m)** performance
  @param c  a collection of elements to remove from the list
  @return  true iff at least one element was removed
  */
  template<typename E>
  inline bool ArrayList<E>::removeAll(const ConstIterable<E>& c)
  {
    bool ret = false;
    // Self-removal is the same as clear.
    if (this == &c)
    {
      ret = !this->isEmpty();
      clear();
    }
    // Remove each element of the other collection.
    else
    {
      ListIndices toRemove;
      size_t idx = 0;
      for (const E& elem : (*this))
      {
        for (const E& celem : c)
        {
          if (this->equal(celem, elem))
          {
            toRemove.add(idx);
            break;
          }
        }
        ++idx;
      }
      ret = eraseAll(toRemove);
    }
    return ret;
  }

  /**
  Remove elements from this list which are equal to any of the elements
  returned by the provided iterator.

  @warning If the iterator points to this list, the behavior is undefined.

  @note **O(n*m)** performance
  @param itr  an iterator over elements to remove (takes ownership)
  @return  true iff the element was removed from the list
  */
  template<typename E>
  inline bool ArrayList<E>::removeAll(Iterator<const E>* itr)
  {
    // exit early if no elements
    if (!itr->hasNext())
    {
      delete itr;
      return false;
    }
    // collect all the indices of elements to remove
    ListIndices toRemove;
    while (itr->hasNext())
    {
      const E& celem = itr->next();
      size_t idx = 0;
      for (const E& elem : (*this))
      {
        if (this->equal(elem, celem))
        {
          toRemove.add(idx);
        }
        ++idx;
      }
    }
    delete itr;
    // delete all the elements at the found indices
    return eraseAll(toRemove);
  }

  /**
  Remove all elements from the collection. Reduces the size to 0, but does not
  affect capacity.
  */
  template<typename E>
  inline void ArrayList<E>::clear(void)
  {
    for (size_t idx = 0; idx < size_; ++idx)
    {
      array_[idx].~E();
    }
    size_ = 0;
  }

  /**
  Sort the specified sub-array using quicksort (Hoare partition).

  @note **O(n^2)** worst-case performance, but **O(n*log(n))** average-case
  @param comp  the comparison operator to use to determine sort order
  */
  template<typename E>
  inline void ArrayList<E>::sort(const OrderOp<E>& comp)
  {
    quicksort(0, size_ - 1, comp);
  }

  /**
  Obtain the current size of the list.

  @return  the number of elements in the list
  */
  template<typename E>
  inline size_t ArrayList<E>::size(void) const
  {
    return size_;
  }

  /**
  Access the element at the specified index.

  @note **O(1)** performance
  @param index  the index of the element to be accessed
  @return  the element at the specified index
  */
  template<typename E>
  inline E& ArrayList<E>::at(size_t index)
  {
    return array_[index];
  }

  /**
  Const-qualified access the element at the specified index.

  @note **O(1)** performance
  @param index  the index of the element to be accessed
  @return  the element at the specified index
  */
  template<typename E>
  inline const E& ArrayList<E>::at(size_t index) const
  {
    return array_[index];
  }

  /**
  Swap the positions of two objects in memory using their move constructors.
  This method does not take ownership of the provided elements, but the
  ownership of each element would also be swapped.

  @note **O(1)** performance
  @param a  the first element to swap
  @param b  the second element to swap
  */
  template<typename E>
  inline void ArrayList<E>::objswap(E* a, E* b)
  {
    E temp((E&&)*a);
    new ((void*)a) E((E&&)*b);
    new ((void*)b) E((E&&)temp);
  }

  /**
  Move `num` objects from `src` to `dst`. Similar behavior as `memmove()`, but
  includes move-constructor operation.

  Note that this method does not implement generalized data-move operations.
  Specifically, it does not handle the case where the source and destination
  ranges overlap by less than `sizeof(E)`. This implementation is sufficient
  for moving aligned elements within an array of those objects, or between
  non-overlapping arrays.

  @note **O(m)** performance
  @param dst  the destination position for the object
  @param src  the source position for the object
  @param num  the number of elements to move
  */
  template<typename E>
  inline void ArrayList<E>::objmove(E* dst, E* src, size_t num)
  {
    if (dst < src)
    {
      for (size_t idx = 0; idx < num; ++idx)
      {
        new ((void*)(dst + idx)) E((E&&)src[idx]);
      }
    }
    else if(src < dst)
    {
      for (size_t idx = num; idx > 0; --idx)
      {
        size_t idxx = idx - 1;
        new ((void*)(dst + idxx)) E((E&&)src[idxx]);
      }
    }
    // else no-op
  }

  /**
  Shift elements of the array at the specified index or beyond by the given
  amount. The elements in the gap are then not initialized, meaning that
  the `ArrayList` *is not valid* immediately following a call to this
  method. To get back to a valid state, the gap must be filled, and the size
  must be updated to include the added elements.

  If the capacity is not sufficient for this shift, then the capacity will be
  increased and the entire array re-allocated.

  @node **O(n)** worst-case performance
  @param index  the index where we are introducing empty space
  @param amount  how many uninitialized objects to insert
  */
  template<typename E>
  inline void ArrayList<E>::split(size_t index, size_t amount)
  {
    if (size_ + amount <= capacity_)
    {
      objmove(array_ + index + amount, array_ + index, size_ - index);
    }
    else
    {
      size_t newCap = amount > capacity_ ? capacity_ + amount : 2 * capacity_;
      E* newArray = viln_hidden::vmalloc<E>(newCap);
      objmove(newArray, array_, index);
      objmove(newArray + index + amount, array_ + index, size_ - index);
      viln_hidden::vfree<E>(array_);
      array_ = newArray;
      capacity_ = newCap;
    }
  }

  /**
  This method re-condenses the array of elements after those elements have been
  deleted. The remaining elements in the array are shifted down so that there
  is only a contiguous set of elements.

  @node **O(n+m)** worst-case performance
  @param empties  the locations of uninitialized objects to remove
  */
  template<typename E>
  inline void ArrayList<E>::defragment(const ListIndices& empties)
  {
    // nothing to do if no indices to remove
    if (empties.isEmpty())
    {
      return;
    }

    // iterate over the indices to remove
    Iterator<const size_t>* itr = empties.iterator();
    size_t dstidx = itr->next(); // ok because empties is not empty
    size_t nextidx = 0;
    size_t acc = 0;
    while (nextidx < size_)
    {
      // initial guess is that we are removing one element
      size_t srcidx = dstidx + 1;
      // increase srcidx while empties form a contiguous block
      while (true)
      {
        if (!itr->hasNext())
        {
          nextidx = size_;
          break;
        }
        nextidx = itr->next();
        if (nextidx != srcidx)
        {
          break;
        }
        srcidx = nextidx + 1;
      }
      // move a single block of data
      objmove(array_ + dstidx - acc, array_ + srcidx, nextidx - srcidx);
      // shift subsequent output indices left to account for removed elements
      acc += srcidx - dstidx;
      dstidx = nextidx;
    }
    delete itr;
  }

  /**
  Sort the specified sub-array using quicksort (Hoare partition).

  @note **O(n^2)** worst-case performance, but **O(n*log(n))** average-case
  @param lo  the leftmost index to sort
  @param hi  the rightmost indext to sort
  @param comp  the comparison operator to use to determine sort order
  */
  template<typename E>
  void ArrayList<E>::quicksort(size_t lo, size_t hi, const OrderOp<E>& comp)
  {
    if (lo >= hi) return;
    size_t mid = (lo + hi) / 2;
    size_t loo = lo;
    size_t hii = hi;
    while (true)
    {
      while (comp.order(array_[loo], array_[mid])) ++loo;
      while (comp.order(array_[mid], array_[hii])) --hii;
      if (loo < hii)
      {
        objswap(array_ + loo, array_ + hii);
        if (loo == mid) mid = hii++;
        else if (hii == mid) mid = loo--;
        --hii;
        ++loo;
      }
      else break;
    }
    if(lo < mid) quicksort(lo, mid - 1, comp);
    if(hi > mid) quicksort(mid + 1, hi, comp);
  }
}

#endif
