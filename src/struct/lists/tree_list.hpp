#ifndef VILN_TREE_LIST_HPP
#define VILN_TREE_LIST_HPP

#include <stddef.h>
#include <initializer_list>

#include "list.hpp"

namespace viln
{
  /// \cond DO_NOT_DOCUMENT
  template<typename E> class AvlTree;
  template<typename E> class TreeNode;
  template<typename E> class TreeNodeIterator;
  /// \endcond

  /// A type of list using a binary search tree for storage.
  /**
  A `TreeList` is a `List` implementation which uses a binary search tree for
  storage. This implementation compromises between the various operations,
  providing good performance for random mixes of operations.

  This class uses `DetectEqual<E>` for the templated type, so either
  `operator==` must be defined for the element-type, or the `equal(E,E)`
  method should be overridden.

  This class does not implement thread-safety, nor does it implement bounds-
  checking for methods that accept an index as a parameter.

  Operations all have amortized performance of **O(m*log(n))** where **m** is
  the size of the input (if applicable), and **n** is the size of the list.

  Note that modification of the list while there are active iterators will
  cause undefined behavior in those iterators.
  */
  template<typename E>
  class TreeList : public List<E>
  {
  public:
    /// Default constructor.
    TreeList(void);
    /// Initializer-list brace-construction.
    TreeList(std::initializer_list<E> li);
    /// Copy constructor.
    TreeList(const TreeList<E>& o);
    /// Move-constructor.
    TreeList(TreeList<E>&& o);
    /// Destructor.
    virtual ~TreeList(void);

    /// Assignment operator.
    TreeList<E>& operator=(const TreeList<E>& o);
    /// Move-assign operator.
    TreeList<E>& operator=(TreeList<E>&& o);

    /// Construct a new iterator over the list starting at the given index.
    virtual ListIterator<E>* listIterator(size_t idx) override;
    /// Construct a new iterator over the list starting at the given index.
    virtual ListIterator<const E>* listIterator(size_t idx) const override;

    /// Insert an element at the given index.
    virtual bool insert(size_t index, E* e);
    /// Move an element into the list at the given index.
    virtual bool insert(size_t index, E&& e) override;
    /// Insert an additional element at the given index.
    virtual bool insert(size_t index, const E& e) override;
    /// Take the provided pointer and add it to the list.
    virtual bool add(E* e);
    /// Move an element into the list at the end.
    virtual bool add(E&& e) override;
    /// Add an element to the list at the end.
    virtual bool add(const E& e) override;
    /// Remove the element at the specified index.
    virtual bool erase(size_t index) override;
    /// Remove an element from the collection.
    virtual bool remove(const E& e) override;
    /// Remove all elements from this collection.
    virtual void clear(void) override;
    /// Sort the elements in the list according to the given ordering.
    virtual void sort(const OrderOp<E>& comp) override;
    using List<E>::sort;

    /// Determine the size of this collection.
    virtual size_t size(void) const override;

    /// Indexed access into the list elements.
    virtual E& at(size_t index) override;
    virtual const E& at(size_t index) const override;

  protected:
    /// Access the root node.
    TreeNode<E>* const root(void);
    const TreeNode<E>* root(void) const;

  private:
    /// Internally use specific iterator variant.
    TreeNodeIterator<E>* nodeIterator(size_t idx);
    // Find the node at the specified index.
    TreeNode<E>* nodeAt(size_t index) const;
    // Find the node at the specified index, starting from the right.
    TreeNode<E>* rightNodeAt(size_t index) const;
    // Find the node at the specified index, starting from the left.
    TreeNode<E>* leftNodeAt(size_t index) const;
    // Insert the specified node into the tree at position indicated by comp.
    void insertNode(TreeNode<E>* enod, const OrderOp<E>& comp);

    // auto-balancing tree storage
    AvlTree<E>* tree_;
    // size of the set
    size_t size_;
  };
}

#endif
