#ifndef VILN_LIST_NODE_ITERATOR_INL
#define VILN_LIST_NODE_ITERATOR_INL

#include "list_node.inl"

#include "list_node_iterator.hpp"

namespace viln
{
  /**
  Constructor with explicit references to next and previous nodes. If the
  iterator is being pointed at the start or end of the list, then one of the
  two arguments may be `NULL`. If the provided previous and next nodes are not
  neighbors, then the first call to `next()`/`previous()` or
  `snapToNext()`/`snapToPrevious()` will choose the next/prev element
  respectively and force consistency of the other element with it.

  @param prev  the previous node to start iteration at  (`NULL` permitted)
  @param next  the next node to start iteration at  (`NULL` permitted)
  */
  template<typename E>
  inline ListNodeIterator<E>::ListNodeIterator(
    ListNode<E>* const prev,
    ListNode<E>* const next) :
    nextptr_(next),
    prevptr_(prev)
  {
    // no-op
  }

  template<typename E>
  inline ListNodeIterator<E>::~ListNodeIterator(void)
  {
    // no-op
  }

  /**
  Move the position of the iterator to the next node, and return the element
  held by the node just incremented over. Note that behavior of this method is
  undefined if `hasNext()` has not returned `true` since the most recent call
  to `next()`.

  @return  the next element available in the list
  */
  template<typename E>
  inline E& ListNodeIterator<E>::next(void)
  {
    return nextNode()->data();
  }

  /**
  Determine if there is a next element to be incremented over.

  @return  true iff there is a next element
  */
  template<typename E>
  inline bool ListNodeIterator<E>::hasNext(void) const
  {
    return nextptr_;
  }

  /**
  Repair any possible inconsistencies in the iterator by forcing the previous
  element to be consistent with the linkages of the next node. This can be used
  if the underlying list is being modified during iteration, and
  bi-directional iteration needs to stay self-consistent.
  */
  template<typename E>
  inline void ListNodeIterator<E>::snapToNext(void)
  {
    if (nextptr_) prevptr_ = nextptr_->previousNode();
    else prevptr_ = nullptr;
  }

  /**
  Move the position of the iterator to the previous node, and return the
  element held by the node just incremented over. Note that behavior of this
  method is undefined if `hasPrevious()` has not returned `true` since the most
  recent call to `previous()`.

  @return  the next element in the list, in the reverse direction
  */
  template<typename E>
  inline E& ListNodeIterator<E>::previous(void)
  {
    return previousNode()->data();
  }

  /**
  Determine if there is a previous element to be incremented over.

  @return  true iff there is a previous element
  */
  template<typename E>
  inline bool ListNodeIterator<E>::hasPrevious(void) const
  {
    return prevptr_;
  }

  /**
  Repair any possible inconsistencies in the iterator by forcing the next
  element to be consistent with the linkages of the previous node. This can be
  used if the underlying list is being modified during iteration, and
  bi-directional iteration needs to stay self-consistent.
  */
  template<typename E>
  inline void ListNodeIterator<E>::snapToPrevious(void)
  {
    if (prevptr_) nextptr_ = prevptr_->nextNode();
    else nextptr_ = nullptr;
  }

  /**
  Return a pointer to the next node, and increment the iterator position over
  it.

  @return  the next node
  @see next()
  */
  template<typename E>
  inline ListNode<E>* const ListNodeIterator<E>::nextNode(void)
  {
    prevptr_ = nextptr_;
    nextptr_ = nextptr_->nextNode();
    return prevptr_;
  }

  /**
  Return a pointer to the next node. Does *not* modify the position of the
  iterator. Used by `LinkedList` to inspect the position of the iterator.

  @return  the next node to be iterated over
  */
  template<typename E>
  inline ListNode<E>* const ListNodeIterator<E>::peekNextNode(void)
  {
    return nextptr_;
  }

  /**
  Return a pointer to the next node, and decrement the iterator position over
  it.

  @return  the previous node
  @see previous()
  */
  template<typename E>
  inline ListNode<E>* const ListNodeIterator<E>::previousNode(void)
  {
    nextptr_ = prevptr_;
    prevptr_ = prevptr_->previousNode();
    return nextptr_;
  }

  /**
  Return a pointer to the previous node. Does *not* modify the position of the
  iterator. Used by `LinkedList` to inspect the position of the iterator.

  @return  the previous node to be iterated over
  */
  template<typename E>
  inline ListNode<E>* const ListNodeIterator<E>::peekPreviousNode(void)
  {
    return prevptr_;
  }
}

#endif
