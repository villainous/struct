#ifndef VILN_BOX_ITERATOR_INL
#define VILN_BOX_ITERATOR_INL

#include "box_iterator.hpp"

namespace viln
{
  /**
  Construct an iterator starting at the specified coordinate and continuing
  over the rectangular region spanned by the size.

  @param min  starting coordinate
  @param siz  size of the rectangle to iterate over
  */
  template<typename E, int8_t D>
  inline BoxIterator<E, D>::BoxIterator(const vvcrd& min, const vvcrd& siz) :
    min_(min),
    max_(min + siz)
  {
    next_ = min_;
    --(next_[0]);
    for (int8_t dim = 0; dim < vvcrd::SIZE; ++dim)
    {
      if (siz[dim] <= 0)
      {
        next_ = max_;
        break;
      }
    }
  }

  template<typename E, int8_t D>
  inline BoxIterator<E, D>::~BoxIterator(void)
  {
    // no-op
  }

  /**
  Obtain the next coordinate from the iterator. The returned reference is
  owned by the iterator, and may change if the iterator state is updated.
  Note that behavior is undefined if `hasNext()` has not returned `true`
  since the previous call to `next()`.

  @return  the next coordinate in the sequence
  */
  template<typename E, int8_t D>
  inline const vcrd<E, D>& BoxIterator<E, D>::next(void)
  {
    for (int8_t idx = 0; idx < D; ++idx)
    {
      if (++(next_[idx]) < max_[idx]) break;
      else next_[idx] = min_[idx];
    }
    return next_;
  }

  /**
  Determine if there are additional elements to be iterated over.
  
  @return  true iff the iterator has a next element
  */
  template<typename E, int8_t D>
  inline bool BoxIterator<E, D>::hasNext(void) const
  {
    for (int8_t idx = 0; idx < D; ++idx)
    {
      if (next_[idx] < max_[idx] - 1) return true;
    }
    return false;
  }
}

#endif
