#ifndef VILN_ARRAY_ITERATOR_HPP
#define VILN_ARRAY_ITERATOR_HPP

#include <stddef.h>

#include "list_iterator.hpp"

namespace viln
{
  /// An iterator over elements in a C-style array.
  /**
  The `ArrayIterator` provides an implementation of the `Iterator` interface
  that provides access to a C-style naked array.
  */
  template<typename E>
  class ArrayIterator : public ListIterator<E>
  {
  public:
    /// Construct an iterator over the provided array.
    ArrayIterator(E* const array, size_t size, size_t offset = 0);
    /// Copy-constructor (pointers not owned!)
    ArrayIterator(const ArrayIterator& o) = default;
    /// Destructor.
    virtual ~ArrayIterator(void);

    // not assignable!
    ArrayIterator& operator=(const ArrayIterator&) = delete;

    /// Increment the position and return access to the next element.
    virtual E& next(void) override;
    /// Determine if there are additional elements in the array.
    virtual bool hasNext(void) const override;
    /// Return the index of the next element in the list.
    virtual size_t nextIndex(void) const;

    /// Accesses the previous element and decrements the current position.
    virtual E& previous(void) override;
    /// Returns true iff there are more previous elements to be accessed.
    virtual bool hasPrevious(void) const override;
    /// Return the index of the previous element in the list.
    virtual size_t previousIndex(void) const;

  private:
    // current position of the iterator
    E* ptr_;
    // the start address of the array being iterated over
    E* const begin_;
    // the address immediately following the end of the array
    E* const end_;
  };
}

#endif
