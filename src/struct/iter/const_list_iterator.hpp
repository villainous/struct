#ifndef VILN_CONST_LIST_ITERATOR_HPP
#define VILN_CONST_LIST_ITERATOR_HPP

#include "list_iterator.hpp"

namespace viln
{
  /// A wrapper-type for adding const-qualification to a list-iterator template.
  /**
  This iterator-implementation is a thin-wrapper that solves the common
  problem of transferring const-qualification to the *template*. This is
  necessary since `ListIterator<E>` and `ListIterator<const E>` are considered
  *unrelated types* by the compiler, so a specific mechanism for converting from
  one to the other is provided here.
  */
  template<typename E>
  class ConstListIterator : public ListIterator<const E>
  {
  public:
    /// Construct an iterator wrapping the provided base (takes ownership).
    ConstListIterator(ListIterator<E>* base) : base_(base) {}
    /// Destructor.
    virtual ~ConstListIterator(void) { delete base_; }

    /// Accesses the next element and advances the current position.
    virtual const E& next(void) override { return base_->next(); }
    /// Accesses the previous element and decrements the current position.
    virtual const E& previous(void) override { return base_->previous(); }
    /// Returns true iff there are more elements to be accessed.
    virtual bool hasNext(void) const override { return base_->hasNext(); }
    /// Returns true iff there are more previous elements to be accessed.
    virtual bool hasPrevious(void) const override { return base_->hasPrevious(); }

  private:
    // the base implementation being wrapped
    ListIterator<E>* base_;
  };
}

#endif
