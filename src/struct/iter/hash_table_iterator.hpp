#ifndef VILN_HASH_SET_ITERATOR_HPP
#define VILN_HASH_SET_ITERATOR_HPP

#include <stddef.h>

#include "iterator.hpp"

namespace viln
{
  /// \cond DO_NOT_DOCUMENT
  template<typename E> class ListNode;
  /// \endcond

  /// An iterator over a table containing chains of elements.
  /**
  The `HashTableIterator` is an `Iterator` implementation that provides access
  to all elements in an array of lists of elements. The array may contain
  null elements, which will be skipped.
  */
  template<typename E>
  class HashTableIterator : public Iterator<E>
  {
  public:
    /// Create an iterator over the given array of list node pointers.
    HashTableIterator(ListNode<E>** const base, size_t count);
    /// Copy-constructor (pointers not owned!).
    HashTableIterator(const HashTableIterator& o) = default;
    /// Destructor.
    virtual ~HashTableIterator(void);

    // not assingable!
    HashTableIterator& operator=(const HashTableIterator&) = delete;

    /// Accesses the next element and advances the current position.
    virtual E& next(void) override;
    /// Returns true iff there are more elements to be accessed.
    virtual bool hasNext(void) const override;

  private:
    // current pointer into an array of lists (NOT owned by this)
    ListNode<E>** baseptr_;
    // the next node to return (NOT owned by this)
    ListNode<E>* nextptr_;
    // the number of remaining elements in the array of lists
    size_t count_;
  };
}

#endif
