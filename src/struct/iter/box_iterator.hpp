#ifndef VILN_BOX_ITERATOR_HPP
#define VILN_BOX_ITERATOR_HPP

#include "vcrd.hpp"

namespace viln
{
  // An iterator over indices within a box.
  /**
  The `BoxIterator` provides an iterator over the coordinates spanned by
  a `[min,max)` as seen in the `Box` class. This allows for easy N-dimensional
  coordinate iteration using a single loop:
  ```
  for(vcrd v : Box(start, size))
  ```
  This greatly improves readability for these kinds of loop.
  */
  template <typename E, int8_t D>
  class BoxIterator : public Iterator<const vcrd<E, D>>
  {
  public:
    typedef vcrd<E, D> vvcrd;

    /// Construct an iterator with the specified position and size.
    BoxIterator(const vvcrd& min, const vvcrd& siz);
    /// Destructor.
    ~BoxIterator(void);

    /// Accesses the next element and advances the current position.
    virtual const vvcrd& next(void) override;
    /// Returns true iff there are more elements to be accessed.
    virtual bool hasNext(void) const override;

  private:
    // setup
    // the bounds for iteration
    const vvcrd min_;
    const vvcrd max_;

    // state
    // the next coord to return
    vvcrd next_;
  };
}

#endif
