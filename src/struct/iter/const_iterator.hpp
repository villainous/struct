#ifndef VILN_CONST_ITERATOR_HPP
#define VILN_CONST_ITERATOR_HPP

#include "iterator.hpp"

namespace viln
{
  /// A wrapper-type for adding const-qualification to an iterator template.
  /**
  This iterator-implementation is a thin-wrapper that solves the common
  problem of transferring const-qualification to the *template*. This is
  necessary since `Iterator<E>` and `Iterator<const E>` are considered
  *unrelated types* by the compiler, so a specific mechanism for converting from
  one to the other is provided here.
  */
  template<typename E>
  class ConstIterator : public Iterator<const E>
  {
  public:
    /// Construct an iterator wrapping the provided base (takes ownership).
    ConstIterator(Iterator<E>* base) : base_(base) {}
    /// Destructor.
    virtual ~ConstIterator(void) { delete base_; }

    /// Accesses the next element and advances the current position.
    virtual const E& next(void) override { return base_->next(); }
    /// Returns true iff there are more elements to be accessed.
    virtual bool hasNext(void) const override { return base_->hasNext(); }

  private:
    // the base implementation being wrapped
    Iterator<E>* base_;
  };
}

#endif
