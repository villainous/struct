#ifndef VILN_ITERATOR_HPP
#define VILN_ITERATOR_HPP

namespace viln
{
  /// An interface providing incremental visitation to a collection of elements.
  /**
  An iterator over some collection of elements, allowing visitation of each
  member of that collection.

  Note that the behavior of `next()` is undefined if `hasNext()` has not
  returned `true` since the most recent call to `next()`. This implies that
  callers will alternate between calls to `hasNext()` and `next()`.
  ```
  while( itr.hasNext() )
  {
    itr.next();
  }
  ```
  */
  template<typename E>
  class Iterator
  {
  public:
    /// Virtual destructor.
    virtual ~Iterator(void) {};

    /// Accesses the next element and advances the current position.
    virtual E& next(void) = 0;
    /// Returns true iff there are more elements to be accessed.
    virtual bool hasNext(void) const = 0;
  };
}

#endif
