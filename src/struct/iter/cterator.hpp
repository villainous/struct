#ifndef VILN_CTERATOR_HPP
#define VILN_CTERATOR_HPP

namespace viln
{
  /// \cond DO_NOT_DOCUMENT
  template<typename E> class Iterator;
  /// \endcond

  /// A facade for an iterator that provides C++ iteration behavior.
  /**
  The `Cterator` (short for C++-style-iterator) is an interface providing the
  stl-style iterator interface. This allows collections providing an
  implementation of `Iterator` to be used in C++11 range-for expressions by
  wrapping it in a `Cterator`.
  */
  template<typename E>
  class Cterator
  {
  public:
    /// Create a `Cterator` wrapping the provided `Iterator`.
    Cterator(Iterator<E>* base);
    /// Move-constructor.
    Cterator(Cterator&& o);
    /// Destructor.
    ~Cterator(void);

    // not copyable!
    Cterator(const Cterator&) = delete;
    Cterator& operator=(const Cterator&) = delete;

    /// Represent some point before the first element of some collection.
    static Cterator before(void);
    /// Represent some point after the last element of some collection.
    static Cterator after(void);

    /// Increment the iterator position to the next element.
    Cterator& operator++(void);
    /// Return true iff the provided objects have matching positions.
    bool operator==(const Cterator& t) const;
    /// Return false iff the provided objects have matching positions.
    bool operator!=(const Cterator& t) const;
    /// Access the element at the current position of the iterator.
    E& operator*(void);
    /// Access the element at the current position of the iterator (const).
    const E& operator*(void) const;

  protected:
    /// Default constructor.
    Cterator(void);

  private:
    // Description of the state of a Cterator.
    enum cter_state : unsigned char
    {
      BEFORE, // before the first element of the collection
      WITHIN, // any element of the collection
      AFTER   // after the last element of the collection
    };

    // current status of the pointer
    cter_state state_;
    // underlying iterator implementation
    Iterator<E>* base_;
    // current position of the iterator
    E* curptr_;
  };
}

#endif
