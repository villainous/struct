#ifndef VILN_ITERATOR_SEQUENCE_HPP
#define VILN_ITERATOR_SEQUENCE_HPP

#include <initializer_list>

#include "iterator.hpp"
#include "linked_list.hpp"
#include "iterator.hpp"

namespace viln
{
  /// An aggregate of multiple iterators into a single long iterator.
  /**
  An `IteratorSequence` is an `Iterator` composed of other `Iterators`.
  Elements provided to the consumer proceed in order within the iterators,
  and then across them, in the order in which they are added. This can be
  used to compose a single iterator over multiple collections, or multiple
  passes over the same collection.
  */
  template <typename E>
  class IteratorSequence : public Iterator<E>
  {
  public:
    /// Construct an empty iterator.
    IteratorSequence(void);
    /// Construct an iterator wrapping the specified iterator.
    IteratorSequence(Iterator<E>* itr);
    /// Construct an iterator over all the provided iterators' elements.
    IteratorSequence(std::initializer_list<Iterator<E>*> li);
    /// Move-constructor.
    IteratorSequence(IteratorSequence<E>&& o);
    /// Destructor.
    virtual ~IteratorSequence(void);

    // not copyable!
    IteratorSequence(const IteratorSequence&) = delete;
    IteratorSequence& operator=(const IteratorSequence&) = delete;

    /// Add the specified iterator to the sequence.
    virtual bool add(Iterator<E>* itr);

    /// Accesses the next element and advances the current position.
    virtual E& next(void) override;
    /// Returns true iff there are more elements to be accessed.
    virtual bool hasNext(void) const override;

  private:
    // an iterator over the iterators
    Iterator<Iterator<E>*>* outer_;
    // the current inner-iterator
    Iterator<E>* innerPtr_;
    // the iterators being iterated over
    LinkedList<Iterator<E>*> iterators_;
  };
}

#endif
