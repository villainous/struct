#ifndef VILN_ITERATOR_SEQUENCE_INL
#define VILN_ITERATOR_SEQUENCE_INL

#include "linked_list.inl"
#include "iterator_sequence.hpp"

namespace viln
{
  /**
  Construct an empty iterator sequence. Use this with `add()` in order to
  incrementally build up a custom iterator sequence.
  */
  template<typename E>
  inline IteratorSequence<E>::IteratorSequence(void) :
    outer_(nullptr),
    innerPtr_(nullptr),
    iterators_()
  {
    // no-op
  }

  /**
  Construct an iterator sequence wrapping a single iterator.

  @param itr  an iterator to wrap (takes ownership)
  */
  template<typename E>
  inline IteratorSequence<E>::IteratorSequence(Iterator<E>* itr) :
    outer_(nullptr),
    innerPtr_(nullptr),
    iterators_()
  {
    IteratorSequence::add(itr);
  }

  /**
  Construct an iterator sequence from the provided list of iterator pointers.

  @param li  a sequence of iterators to add (takes ownership of each)
  */
  template<typename E>
  inline IteratorSequence<E>::IteratorSequence(
    std::initializer_list<Iterator<E>*> li) :
    outer_(nullptr),
    innerPtr_(nullptr),
    iterators_()
  {
    for (Iterator<E>* itr : li) IteratorSequence::add(itr);
  }

  template<typename E>
  inline IteratorSequence<E>::IteratorSequence(IteratorSequence<E>&& o) :
    outer_(o.outer_),
    innerPtr_(o.innerPtr_),
    iterators_(o.iterators_)
  {
    o.outer_ = nullptr;
    o.iterators_.clear();
  }

  template<typename E>
  inline IteratorSequence<E>::~IteratorSequence(void)
  {
    if (outer_) delete outer_;
    for (Iterator<E>* itr : iterators_) delete itr;
  }

  /**
  Add the specified iterator to the end of this sequence.

  @param itr  an iterator to add (takes ownership)
  */
  template<typename E>
  inline bool IteratorSequence<E>::add(Iterator<E>* itr)
  {
    // don't add iterators once iteration has begun
    if (outer_)
    {
      delete itr;
      return false;
    }
    // don't allow empty iterators
    if (!itr->hasNext())
    {
      delete itr;
      return false;
    }
    return iterators_.add(itr);
  }

  /**
  Locate the next element to the right and return it. The iterator's location
  is updated by this operation.

  Note that calling `next()` is only safe if `hasNext()` has returned true
  since the most recent call to `next()`.

  @return  the next *element* available
  */
  template<typename E>
  inline E& IteratorSequence<E>::next(void)
  {
    if (!outer_) outer_ = iterators_.iterator();
    if (!innerPtr_ || !innerPtr_->hasNext()) innerPtr_ = outer_->next();
    return innerPtr_->next();
  }

  /**
  Determine if there is a next element available or not.

  @return  true iff there is a next node to iterate over
  */
  template<typename E>
  inline bool IteratorSequence<E>::hasNext(void) const
  {
    if (innerPtr_ && innerPtr_->hasNext()) return true;
    if (outer_ && outer_->hasNext()) return true;
    if (outer_) return false;
    return !iterators_.isEmpty();
  }
}

#endif
