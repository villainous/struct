#ifndef VILN_TREE_NODE_ITERATOR_INL
#define VILN_TREE_NODE_ITERATOR_INL

#include "tree_node.hpp"
#include "tree_node_iterator.hpp"

namespace viln
{
  /**
  Construct an iterator over all nodes below the specified root. Iteration
  will begin with the leftmost element below the chosen node, *not* the node
  itself. If the root node is `NULL` then there are no elements it iterate
  over.

  @param root  root of the tree to iterate over (`NULL` permitted)
  @param prev  the previous node to start at (`NULL` means start at left)
  */
  template<typename E>
  inline TreeNodeIterator<E>::TreeNodeIterator(
    TreeNode<E>* const root,
    TreeNode<E>* const prev) :
    root_(root),
    next_(nullptr),
    previous_(nullptr)
  {
    if (root)
    {
      // initially the "next" node is the leftmost node
      if (!prev)
      {
        next_ = root;
        while (next_->leftChild())
        {
          next_ = next_->leftChild();
        }
      }
      // start the given previous node as the next node, and advance one
      else
      {
        next_ = prev;
        this->nextNode();
      }
    }
  }

  template<typename E>
  inline TreeNodeIterator<E>::~TreeNodeIterator(void)
  {
    // no-op
  }

  /**
  Locate the next element to the right and return it. The iterator's location
  is updated by this operation.

  Note that calling `next()` is only safe if `hasNext()` has returned true
  since the most recent call to `next()`.

  @return  the next *element* in the tree to the right
  */
  template<typename E>
  inline E& TreeNodeIterator<E>::next(void)
  {
    return nextNode()->data();
  }

  /**
  Determine if there is a next element available or not.

  @return  true iff there is a next node to iterate over
  */
  template<typename E>
  inline bool TreeNodeIterator<E>::hasNext(void) const
  {
    return next_;
  }

  /**
  Locate the previous element to the right and return it. The iterator's
  location is updated by this operation.

  Note that calling `previous()` is only safe if `hasPrevious()` has returned
  true since the most recent call to `previous()`.

  @return  the previous *element* in the tree to the right
  */
  template<typename E>
  inline E& TreeNodeIterator<E>::previous(void)
  {
    return previousNode()->data();
  }

  /**
  Determine if there is a previous element available or not.

  @return  true iff there is a previous node to iterate over
  */
  template<typename E>
  inline bool TreeNodeIterator<E>::hasPrevious(void) const
  {
    return previous_;
  }

  /**
  Equivalent to `next()` but returns the node containing the next element,
  rather than the element itself.

  Note that calling `nextNode()` is only safe if `hasNext()` has returned true
  since the most recent call to `nextNode()`.

  @return  the next *node* in the tree to the right
  */
  template<typename E>
  inline TreeNode<E>* const TreeNodeIterator<E>::nextNode(void)
  {
    previous_ = next_;
    // if there is a right node, then the next node is that way
    if (next_->rightChild())
    {
      // the leftmost child of the right node is the next node
      next_ = next_->rightChild();
      while (next_->leftChild())
      {
        next_ = next_->leftChild();
      }
    }
    // if there is not a right node, then climb parents until we move right
    else
    {
      while (true)
      {
        TreeNode<E>* tmp = next_;
        next_ = next_->parent();
        // do not try to ascend past the root
        if (!next_ || tmp == root_)
        {
          next_ = nullptr;
          break;
        }
        // if we moved right at least once, we have reached the next node
        if (next_->leftChild() == tmp)
        {
          break;
        }
      }
    }
    return previous_;
  }

  /**
  Return a pointer to the next node. Does *not* modify the position of the
  iterator.

  @return  the next node to be iterated over
  */
  template<typename E>
  inline TreeNode<E>* const TreeNodeIterator<E>::peekNextNode(void)
  {
    return next_;
  }

  /**
  Equivalent to `previous()` but returns the node containing the previous
  element, rather than the element itself.

  Note that calling `previousNode()` is only safe if `hasPrevious()` has
  returned true since the most recent call to `previousNode()`.

  @return  the previous *node* in the tree to the left
  */
  template<typename E>
  inline TreeNode<E>* const TreeNodeIterator<E>::previousNode(void)
  {
    next_ = previous_;
    // if there is a left node, then the previous node is that way
    if (previous_->leftChild())
    {
      // the rightmost child of the left node is the previous node
      previous_ = previous_->leftChild();
      while (previous_->rightChild())
      {
        previous_ = previous_->rightChild();
      }
    }
    // if there is not a left node, then climb parents until we move left
    else
    {
      while (true)
      {
        TreeNode<E>* tmp = previous_;
        previous_ = previous_->parent();
        // do not try to ascend past the root
        if (!previous_ || tmp == root_)
        {
          previous_ = nullptr;
          break;
        }
        // if we moved left at least once, we have reached the previous node
        if (previous_->rightChild() == tmp)
        {
          break;
        }
      }
    }
    return next_;
  }

  /**
  Return a pointer to the previous node. Does *not* modify the position of the
  iterator.

  @return  the previous node to be iterated over
  */
  template<typename E>
  inline TreeNode<E>* const TreeNodeIterator<E>::peekPreviousNode(void)
  {
    return previous_;
  }
}

#endif
