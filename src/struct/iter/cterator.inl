#ifndef VILN_CTERATOR_INL
#define VILN_CTERATOR_INL

#include "iterator.hpp"
#include "cterator.hpp"

namespace viln
{
  /**
  Create a `Cterator` wrapping the provided `Iterator` instance. Takes
  ownership of the provided pointer. Behavior is undefined if the provided
  `Iterator` is modified by the caller after providing it to this constructor.

  Begins in a state where it is pointing at the first element of the collection
  if present, and past the end otherwise.

  @param base  the iterator to wrap with this object (cterator takes ownership)
  */
  template<typename E>
  inline Cterator<E>::Cterator(Iterator<E>* base) :
    state_(WITHIN),
    base_(base),
    curptr_(nullptr)
  {
    // load the first element if available
    operator++();
  }

  template<typename E>
  inline Cterator<E>::Cterator(Cterator&& o) :
    state_(o.state_),
    base_(o.base_),
    curptr_(o.curptr_)
  {
    o.base_ = nullptr;
  }

  /**
  Creates a `Cterator` with no underlying iterator or collection. The resulting
  object is considered to be after any iterator with a valid next element.
  */
  template<typename E>
  inline Cterator<E>::Cterator(void) :
    state_(AFTER),
    base_(nullptr),
    curptr_(nullptr)
  {
    // no-op
  }

  template<typename E>
  inline Cterator<E>::~Cterator(void)
  {
    if (base_) delete(base_);
  }

  /**
  Create a `Cterator` instance that represents a position before the beginning
  of some arbitrary collection.

  @return  an iterator *marker* pointing before all elements
  */
  template<typename E>
  inline Cterator<E> Cterator<E>::before(void)
  {
    Cterator<E> ret;
    ret.state_ = BEFORE;
    return ret;
  }

  /**
  Create a `Cterator` instance that represents a position after the end of some
  arbitrary collection.

  @return  an iterator *marker* pointing after all elements
  */
  template<typename E>
  inline Cterator<E> Cterator<E>::after(void)
  {
    Cterator<E> ret;
    ret.state_ = AFTER;
    return ret;
  }

  /**
  Increment the position of the pointer within the collection. If this would
  result in incrementing past the end of the collection, then the object is
  instead updated such that its state represents pointing past the end of the
  collection.

  @return  a reference to this iterator, pointing at the subsequent element
  */
  template<typename E>
  inline Cterator<E>& Cterator<E>::operator++(void)
  {
    if (base_->hasNext())
    {
      curptr_ = &(base_->next());
    }
    else
    {
      curptr_ = nullptr;
      state_ = AFTER;
    }
    return *this;
  }

  /**
  Comparison between two `Cterator` objects. These are considered equal if
  they are pointing at the same object within the same collection, OR they
  are both before the beginning or both after the end of some collection.

  @return  true iff the two iterators represent the same position
  */
  template<typename E>
  inline bool Cterator<E>::operator==(const Cterator<E>& t) const
  {
    if (t.state_ != state_)
    {
      return false;
    }

    if (state_ != WITHIN)
    {
      return true;
    }

    return t.curptr_ == curptr_;
  }

  /**
  Logical negation of `operator==()`.
  */
  template<typename E>
  inline bool Cterator<E>::operator!=(const Cterator<E>& t) const
  {
    return !(operator==(t));
  }

  /**
  Access to the object currently pointed at by the iterator. Has undefined
  behavior if pointing beyond the bounds of the underlying collection.

  @return  the element at the current position of the iterator
  */
  template<typename E>
  inline E& Cterator<E>::operator*(void)
  {
    return *curptr_;
  }

  /**
  Access to the object currently pointed at by the iterator. Has undefined
  behavior if pointing beyond the bounds of the underlying collection.

  @return  the element at the current position of the iterator
  */
  template<typename E>
  inline const E& Cterator<E>::operator*(void) const
  {
    return *curptr_;
  }
}

#endif
