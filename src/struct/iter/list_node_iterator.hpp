#ifndef VILN_LIST_NODE_ITERATOR_HPP
#define VILN_LIST_NODE_ITERATOR_HPP

#include "list_iterator.hpp"

namespace viln
{
  /// \cond DO_NOT_DOCUMENT
  template<typename E> class ListNode;
  template<typename E> class LinkedList;
  /// \endcond

  /// An iterator over elements in a linked-list structure.
  /**
  The `ListNodeIterator` impltments the `ListIterator` interface over
  `ListNode` elements. One benefit of this type of iterator over possible
  alternatives is that it can be repaired following modification of one of the
  neighboring nodes using `snapToNext()`/`snapToPrevious()`.
  */
  template<typename E>
  class ListNodeIterator : public ListIterator<E>
  {
  public:
    /// Create an iterator pointing between the given elements.
    ListNodeIterator(ListNode<E>* const prev, ListNode<E>* const next);
    /// Copy-constructor (pointers not owned!).
    ListNodeIterator(const ListNodeIterator& o) = default;
    /// Destructor.
    virtual ~ListNodeIterator(void);

    // not assignable!
    ListNodeIterator& operator=(const ListNodeIterator&) = delete;

    /// Accesses the next element and advances the current position.
    virtual E& next(void) override;
    /// Returns true iff there are more elements to be accessed.
    virtual bool hasNext(void) const override;
    /// Forces the previous pointer to be consistent with next pointer.
    virtual void snapToNext(void);

    /// Accesses the previous element and decrements the current position.
    virtual E& previous(void) override;
    /// Returns true iff there are more previous elements to be accessed.
    virtual bool hasPrevious(void) const override;
    /// Forces the next pointer to be consistent with previous pointer.
    virtual void snapToPrevious(void);

  protected:
    /// Access the next node and advances the current position.
    ListNode<E>* const nextNode(void);
    /// Access the next node.
    ListNode<E>* const peekNextNode(void);
    /// Accesses the previous node and decrements the current position.
    ListNode<E>* const previousNode(void);
    /// Accesses the previous node.
    ListNode<E>* const peekPreviousNode(void);

    friend class LinkedList<E>;

  private:
    // the next node after the position of the iterator
    ListNode<E>* nextptr_;
    // the previous node before the position of the iterator
    ListNode<E>* prevptr_;
  };
}

#endif
