#ifndef VILN_PAIR_ITERATORS_INL
#define VILN_PAIR_ITERATORS_INL

#include "key_value_pair.inl"
#include "pair_iterators.hpp"

namespace viln
{
  // ConstKeyIterator

  template<typename K, typename V>
  inline ConstKeyIterator<K,V>::ConstKeyIterator(
    Iterator<const KeyVal<K,V>>* base) :
    base_(base)
  {
    // no-op
  }

  template<typename K, typename V>
  inline ConstKeyIterator<K, V>::ConstKeyIterator(ConstKeyIterator&& o) :
    base_(o.base_)
  {
    o.base_ = nullptr;
  }

  template<typename K, typename V>
  inline ConstKeyIterator<K,V>::~ConstKeyIterator(void)
  {
    if(base_) delete base_;
  }

  template<typename K, typename V>
  inline const K& ConstKeyIterator<K,V>::next(void)
  {
    const KeyVal<K,V>& npair = base_->next();
    return npair.key();
  }

  template<typename K, typename V>
  inline bool ConstKeyIterator<K,V>::hasNext(void) const
  {
    return base_->hasNext();
  }

  // ConstValueIterator

  template<typename K, typename V>
  inline ConstValueIterator<K,V>::ConstValueIterator(
    Iterator<const KeyVal<K,V>>* base) :
    base_(base)
  {
    // no-op
  }

  template<typename K, typename V>
  inline ConstValueIterator<K, V>::ConstValueIterator(ConstValueIterator&& o) :
    base_(o.base_)
  {
    o.base_ = nullptr;
  }

  template<typename K, typename V>
  inline ConstValueIterator<K,V>::~ConstValueIterator(void)
  {
    if(base_) delete base_;
  }

  template<typename K, typename V>
  inline const V& ConstValueIterator<K,V>::next(void)
  {
    const KeyVal<K,V>& npair = base_->next();
    return npair.value();
  }

  template<typename K, typename V>
  inline bool ConstValueIterator<K,V>::hasNext(void) const
  {
    return base_->hasNext();
  }

  // MutableValueIterator

  template<typename K, typename V>
  inline MutableValueIterator<K,V>::MutableValueIterator(
    Iterator<KeyVal<K,V>>* base) :
    base_(base)
  {
    // no-op
  }

  template<typename K, typename V>
  inline MutableValueIterator<K, V>::MutableValueIterator(
    MutableValueIterator&& o) :
    base_(o.base_)
  {
    o.base_ = nullptr;
  }

  template<typename K, typename V>
  inline MutableValueIterator<K,V>::~MutableValueIterator(void)
  {
    if(base_) delete base_;
  }

  template<typename K, typename V>
  inline V& MutableValueIterator<K,V>::next(void)
  {
    KeyVal<K,V>& npair = base_->next();
    return npair.value();
  }

  template<typename K, typename V>
  inline bool MutableValueIterator<K,V>::hasNext(void) const
  {
    return base_->hasNext();
  }
}

#endif
