#ifndef VILN_PAIR_ITERATORS_HPP
#define VILN_PAIR_ITERATORS_HPP

#include "key_value_pair.hpp"
#include "iterator.hpp"

namespace viln
{
  /// An iterator over the keys in some collection of pairs.
  /**
  A wrapper-type for a pair-iterator that returns only the keys.
  Since the keys are immutable components of the pair, the keys
  returned by the iterator are always const-qualified.
  */
  template<typename K, typename V>
  class ConstKeyIterator : public Iterator<const K>
  {
  public:
    /// Construct an iterator over the base iterator's first entries.
    ConstKeyIterator(Iterator<const KeyVal<K,V>>* base);
    /// Move-constructor.
    ConstKeyIterator(ConstKeyIterator&& o);
    /// Destructor.
    virtual ~ConstKeyIterator(void);

    // not copyable!
    ConstKeyIterator(const ConstKeyIterator&) = delete;
    ConstKeyIterator& operator=(const ConstKeyIterator&) = delete;

    /// Accesses the next element and advances the current position.
    virtual const K& next(void) override;
    /// Returns true iff there are more elements to be accessed.
    virtual bool hasNext(void) const override;

  private:
    // the base iterator over the pairs of elements
    Iterator<const KeyVal<K,V>>* base_;
  };

  /// An iterator over the values in some collection of pairs.
  /**
  A wrapper-type for a pair-iterator that returns only the values.
  This type specifically returns only immutable values in order to
  prevent modification of elements.
  */
  template<typename K, typename V>
  class ConstValueIterator : public Iterator<const V>
  {
  public:
    /// Construct an iterator over the base iterator's second entries.
    ConstValueIterator(Iterator<const KeyVal<K,V>>* base);
    /// Move-constructor.
    ConstValueIterator(ConstValueIterator&& o);
    /// Destructor.
    virtual ~ConstValueIterator(void);

    // not copyable!
    ConstValueIterator(const ConstValueIterator&) = delete;
    ConstValueIterator& operator=(const ConstValueIterator&) = delete;

    /// Accesses the next element and advances the current position.
    virtual const V& next(void) override;
    /// Returns true iff there are more elements to be accessed.
    virtual bool hasNext(void) const override;

  private:
    // the base iterator over the pairs of elements
    Iterator<const KeyVal<K,V>>* base_;
  };

  /// An iterator over the mutable values in some collection of pairs.
  /**
  A wrapper-type for a pair-iterator that returns only the values.
  This type allows modification of the values being iterated over.
  */
  template<typename K, typename V>
  class MutableValueIterator : public Iterator<V>
  {
  public:
    /// Construct an iterator over the base iterator's second entries.
    MutableValueIterator(Iterator<KeyVal<K,V>>* base);
    /// Move-constructor.
    MutableValueIterator(MutableValueIterator&& o);
    /// Destructor.
    virtual ~MutableValueIterator(void);

    // not copyable!
    MutableValueIterator(const MutableValueIterator&) = delete;
    MutableValueIterator& operator=(const MutableValueIterator&) = delete;

    /// Accesses the next element and advances the current position.
    virtual V& next(void) override;
    /// Returns true iff there are more elements to be accessed.
    virtual bool hasNext(void) const override;

  private:
    // the base iterator over the pairs of elements
    Iterator<KeyVal<K,V>>* base_;
  };
}

#endif
