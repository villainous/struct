#ifndef VILN_ARRAY_ITERATOR_INL
#define VILN_ARRAY_ITERATOR_INL

#include <stddef.h>

#include "array_iterator.hpp"

namespace viln
{
  /**
  Construct an `ArrayIterator` providing access to the array described by
  the provided base-pointer and size.

  @param array  the array of elements being iterated over
  @param size  the number of elements in the array
  @param offset  the initial position of the iterator within the array
  */
  template<typename E>
  inline ArrayIterator<E>::ArrayIterator(
    E* const array,
    size_t size,
    size_t offset):
    ptr_(array + offset),
    begin_(array),
    end_(array + size)
  {
    // no-op
  }

  template<typename E>
  inline ArrayIterator<E>::~ArrayIterator(void)
  {
    // no-op
  }

  /**
  Provide access to the next element of the array, and increment the position
  of the iterator. Note that this has undefined behavior unless `hasNext()`
  has returned `true` since the previous call to `next()`.

  @return  the next element
  */
  template<typename E>
  inline E& ArrayIterator<E>::next(void)
  {
    return *(ptr_++);
  }

  /**
  Determine if there are additional elements in the array available to be
  iterated over in the forward direction.

  @return  true iff the iterator has a next element
  */
  template<typename E>
  inline bool ArrayIterator<E>::hasNext(void) const
  {
    return ptr_ < end_;
  }

  /**
  Determine the index of the element which would be returned by the next call
  to `next()`, or the previous call to `previous()`.

  @return  the index of the next element
  */
  template<typename E>
  inline size_t ArrayIterator<E>::nextIndex(void) const
  {
    return ptr_ - begin_;
  }

  /**
  Provide access to the previous element of the array, and decrement the
  position of the iterator. Note that this has undefined behavior unless
  `hasPrevious()` has returned `true` since the previous call to `previous()`.

  @return  the previous element
  */
  template<typename E>
  inline E& ArrayIterator<E>::previous(void)
  {
    return *(--ptr_);
  }

  /**
  Determine if there are additional elements in the array available to be
  iterated over in the reverse direction.

  @return  true iff the iterator has a previous element
  */
  template<typename E>
  inline bool ArrayIterator<E>::hasPrevious(void) const
  {
    return ptr_ > begin_;
  }


  /**
  Determine the index of the element which would be returned by the next call
  to `previous()`, or by the previous call to `next()`.

  @return  the index of the previous element
  */
  template<typename E>
  inline size_t ArrayIterator<E>::previousIndex(void) const
  {
    return ptr_ - begin_ - 1;
  }
}

#endif
