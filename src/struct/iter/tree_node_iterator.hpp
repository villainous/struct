#ifndef VILN_TREE_NODE_ITERATOR_HPP
#define VILN_TREE_NODE_ITERATOR_HPP

#include "list_iterator.hpp"

namespace viln
{
  /// \cond DO_NOT_DOCUMENT
  template<typename E> class TreeNode;
  template<typename E> class TreeSet;
  template<typename E> class TreeList;
  /// \endcond

  /// An iterator over elements in a binary-search-tree.
  /**
  The `TreeNodeIterator` is an `Iterator` implementation which uses the
  links between `TreeNode` elements to navigate a set. Iterators are
  constructed with a specified root, and will iterate over all elements below
  said root. Iteration proceeds in order of leftmost to rightmost element.

  Note that modification of the underlying set of nodes while
  the iterator is active leads to unspecified behavior.

  Assuming a balanced tree, the `next()` method is expected to have constant
  cost, and worst-case **O(log(n))**.
  */
  template<typename E>
  class TreeNodeIterator : public ListIterator<E>
  {
  public:
    /// Construct an iterator over elements below a root node.
    TreeNodeIterator(
      TreeNode<E>* const root,
      TreeNode<E>* const prev = nullptr);
    /// Copy-constructor (pointers are not owned).
    TreeNodeIterator(const TreeNodeIterator& o) = default;
    /// Destructor.
    virtual ~TreeNodeIterator(void);

    // not assignable!
    TreeNodeIterator& operator=(const TreeNodeIterator&) = delete;

    /// Accesses the next element and advances the current position.
    virtual E& next(void) override;
    /// Returns true iff there are more elements to be accessed.
    virtual bool hasNext(void) const override;

    /// Accesses the next element and decrements the current position.
    virtual E& previous(void) override;
    /// Returns true iff there are more elements to be accessed.
    virtual bool hasPrevious(void) const override;

  protected:
    /// Access the next node and advances the current position.
    TreeNode<E>* const nextNode(void);
    /// Access the next node.
    TreeNode<E>* const peekNextNode(void);
    /// Accesses the previous node and decrements the current position.
    TreeNode<E>* const previousNode(void);
    /// Accesses the previous node.
    TreeNode<E>* const peekPreviousNode(void);

    friend class TreeSet<E>;
    friend class TreeList<E>;

  private:
    // the root of the tree
    TreeNode<E>* const root_;
    // the next node to iterate over (not owned by this)
    TreeNode<E>* next_;
    // the previous node to iterate over (not owned by this)
    TreeNode<E>* previous_;
  };
}

#endif
