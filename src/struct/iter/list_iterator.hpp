#ifndef VILN_LIST_ITERATOR_HPP
#define VILN_LIST_ITERATOR_HPP

#include "iterator.hpp"

namespace viln
{
  /// An iterator sub-interface adding reverse-iteration.
  /**
  An iterator over a list of elements, allowing traversal in either direction.

  Conceptually, a list-iterator can be thought of as having a "cursor" that
  points *between* elements of the list being traversed. Calls to `next()` and
  `previous()` hop over elements in either direction.

  Note that the behavior of `next()` is undefined if `hasNext()` has not
  returned true since the last call to `next()`, and likewise for `previous()`
  and `hasPrevious()`.
  */
  template<typename E>
  class ListIterator : public Iterator<E>
  {
  public:
    /// Virtual destructor.
    virtual ~ListIterator(void) {};

    /// Accesses the previous element and decrements the current position.
    virtual E& previous(void) = 0;
    /// Returns true iff there are more previous elements to be accessed.
    virtual bool hasPrevious(void) const = 0;
  };
}

#endif
