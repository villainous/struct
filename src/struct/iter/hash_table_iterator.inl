#ifndef VILN_HASH_SET_ITERATOR_INL
#define VILN_HASH_SET_ITERATOR_INL

#include <stddef.h>

#include "list_node.inl"

#include "hash_table_iterator.hpp"

namespace viln
{
  /**
  Create a hash set iterator that starts at the given `base` table position,
  and iterates over `count` list-chains in that table. Note that the count
  specified here is the number of hash table entries, which may or may not be
  populated with chains of one or more elements.

  @param base  the table of chains to iterate over
  @param count  the number of elements in the table to iterate over
  */
  template<typename E>
  HashTableIterator<E>::HashTableIterator(
    ListNode<E>** const base,
    size_t count) :
    baseptr_(base),
    nextptr_(nullptr),
    count_(count)
  {
    // search for the first "next" element
    while (count_ > 0 && !nextptr_)
    {
      nextptr_ = *baseptr_;
      ++baseptr_;
      --count_;
    }
  }

  template<typename E>
  inline HashTableIterator<E>::~HashTableIterator(void)
  {
    // no-op
  }

  /**
  Move the position of the iterator to the next element. The "next" element is
  the following element in the chain if it exists, or the first element of the
  next chain following the base pointer otherwise.

  Note that it is only safe to call `next()` if `hasNext()` has returned true
  since the previous call to `next()`.

  @return  the next element
  */
  template<typename E>
  inline E& HashTableIterator<E>::next(void)
  {
    E& dat = nextptr_->data();
    // next element in the current chain
    nextptr_ = nextptr_->nextNode();
    // if chain ends, then search for the next chain
    if (!nextptr_)
    {
      while (count_ > 0 && !nextptr_)
      {
        nextptr_ = *baseptr_;
        ++baseptr_;
        --count_;
      }
    }
    // return the data
    return dat;
  }

  /**
  Determine if there is a next element to be incremented over.

  @return  true iff the iterator has a next element
  */
  template<typename E>
  inline bool HashTableIterator<E>::hasNext(void) const
  {
    return nextptr_;
  }
}

#endif
