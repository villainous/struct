#ifndef VILN_SORTED_SET_INL
#define VILN_SORTED_SET_INL

#include <stddef.h>

#include "tree_node.hpp"
#include "avl_tree.inl"
#include "tree_node_iterator.inl"
#include "detect_order.inl"
#include "set.inl"

#include "const_iterator.hpp"
#include "tree_set.hpp"

namespace viln
{
  template<typename E>
  TreeSet<E>::TreeSet(void) :
    tree_(nullptr),
    size_(0)
  {
    // no-op
  }

  template<typename E>
  TreeSet<E>::TreeSet(const TreeSet<E>& o) :
    tree_(nullptr),
    size_(0)
  {
    TreeSet::addAll(o);
  }

  template<typename E>
  inline TreeSet<E>::TreeSet(TreeSet<E>&& o) :
    tree_(o.tree_),
    size_(o.size_)
  {
    o.tree_ = nullptr;
  }

  template<typename E>
  TreeSet<E>::~TreeSet(void)
  {
    if (tree_) delete tree_;
  }

  template<typename E>
  inline TreeSet<E>& TreeSet<E>::operator=(const TreeSet<E>& o)
  {
    if (this == &o) return *this;
    TreeSet::clear();
    TreeSet::addAll(o);
    return *this;
  }

  template<typename E>
  inline TreeSet<E>& TreeSet<E>::operator=(TreeSet<E>&& o)
  {
    if (this == &o) return *this;
    if (tree_) delete tree_;
    size_ = o.size_;
    tree_ = o.tree_;
    o.tree_ = nullptr;
    return *this;
  }

  /**
  Create an iterator over the elements in the set. Elements will be iterated
  over in increasing order within the set. Be aware that modification of the
  set while an iterator is active may invalidate the state of the iterator.

  @return  a new iterator over the set elements (caller takes ownership)
  */
  template<typename E>
  inline Iterator<const E>* TreeSet<E>::iterator(void) const
  {
    TreeNode<E>* nod = tree_ ? tree_->root() : nullptr;
    TreeNodeIterator<E>* itr = new TreeNodeIterator<E>(nod);
    return new ConstIterator<E>(itr);
  }

  /**
  Add an element to the set if the set does not already contain the element.

  This version takes ownership of the provided element pointer.

  @note **O(log(n))** worst-case performance
  @return  true iff the element was added to the set
  */
  template<typename E>
  inline bool TreeSet<E>::add(E* e)
  {
    return addHelper(e, searchHelper(*e));
  }

  /**
  Add an element to the set if the set does not already contain the element.

  This version calls the move-constructor to take ownership of the provided
  element. This version of insertion is used when the object being inserted
  is constructed in-line with the insert operation.

  @note **O(log(n))** worst-case performance
  @return  true iff the element was added to the set
  */
  template<typename E>
  inline bool TreeSet<E>::add(E&& e)
  {
    search_results fn = searchHelper(e);
    return (!fn.found) && addHelper(new E((E&&)e), fn);
  }

  /**
  Add an element to the set if the set does not already contain the element.

  This version makes a copy of the provided element reference using its copy-
  constructor.

  @note **O(log(n))** worst-case performance
  @return  true iff the element was added to the set
  */
  template<typename E>
  inline bool TreeSet<E>::add(const E& e)
  {
    search_results fn = searchHelper(e);
    return (!fn.found) && addHelper(new E(e), fn);
  }

  /**
  Remove an element equal to the specified element from the set if it exists.

  @note **O(log(n))** worst-case performance
  @return  true iff the element was removed the set
  */
  template<typename E>
  inline bool TreeSet<E>::remove(const E& e)
  {
    search_results fn = searchHelper(e);
    if (!fn.found) return false;
    tree_->remove(fn.node);
    --size_;
    return true;
  }

  template<typename E>
  inline void TreeSet<E>::clear(void)
  {
    if (tree_) delete tree_;
    tree_ = nullptr;
    size_ = 0;
  }

  /**
  Determine if the set contains an element that would be sorted into the same
  position as the provided node.

  @note **O(log(n))** worst-case performance
  @return  true iff the given element is in the set
  */
  template<typename E>
  inline bool TreeSet<E>::contains(const E& e) const
  {
    return searchHelper(e).found;
  }

  /**
  Find the first element in the set. This would be the "smallest" element in
  the set, and the first one returned by a new iterator over the set.

  @note **O(log(n))** worst-case performance
  @return  the first element in the set
  */
  template<typename E>
  inline const E& TreeSet<E>::first(void) const
  {
    const TreeNode<E>* nod = tree_->root();
    while (nod->leftChild()) nod = nod->leftChild();
    return nod->data();
  }

  /**
  Find the last element in the set. This would be the "largest" element in
  the set, and the last one returned by a new iterator over the set.

  @note **O(log(n))** worst-case performance
  @return  the last element in the set
  */
  template<typename E>
  inline const E& TreeSet<E>::last(void) const
  {
    const TreeNode<E>* nod = tree_->root();
    while (nod->rightChild()) nod = nod->rightChild();
    return nod->data();
  }

  /**
  Find the successor to the specified element. This means that the returned
  value is the next element in the set that is "larger" than the specified
  element. If no element in the set is larger than the specified one, then
  this returns the last element, even if it is not larger than the specified
  one.

  @note **O(log(n))** worst-case performance
  @param e  an element to search for
  @return  the next element, or the last element
  */
  template<typename E>
  inline const E& TreeSet<E>::successor(const E& e) const
  {
    search_results result = searchHelper(e);
    // result is not successor, so try to find one
    if (result.found || !result.left)
    {
      TreeNodeIterator<E> itr(tree_->root(), result.node);
      if (itr.hasNext()) return itr.next();
    }
    // otherwise, searched node is the best result
    return result.node->data();
  }

  /**
  Find the predecessor to the specified element. This means that the returned
  value is the next element in the set that is "smaller" than the specified
  element. If no element in the set is smaller than the specified one, then
  this returns the first element, even if it is not smaller than the specified
  one.

  @note **O(log(n))** worst-case performance
  @param e  an element to search for
  @return  the previous element, or the first element
  */
  template<typename E>
  inline const E& TreeSet<E>::predecessor(const E& e) const
  {
    search_results result = searchHelper(e);
    // result is not successor, so try to find one
    if (result.found || result.left)
    {
      TreeNodeIterator<E> itr(tree_->root(), result.node);
      itr.previous(); // iterator moves to result.node
      if (itr.hasPrevious()) return itr.previous();
    }
    // otherwise, searched node is the best result
    return result.node->data();
  }

  template<typename E>
  inline size_t TreeSet<E>::size(void) const
  {
    return size_;
  }

  template<typename E>
  inline TreeNode<E>* const TreeSet<E>::root(void)
  {
    return tree_ ? tree_->root() : nullptr;
  }

  template<typename E>
  inline const TreeNode<E>* TreeSet<E>::root(void) const
  {
    return tree_ ? tree_->root() : nullptr;
  }

  /**
  Search the set for an element equal to the specified element, and return
  the `TreeNode` that contains the found element. An element is considered
  "found" if niether ordering of the `<` operator returns true for a given
  element. If the element is not found, then the last position searched is
  indicated by the returned node, as well as if the specified element would
  be found as the left or right child of that node.

  @note **O(log(n))** worst-case performance
  @return  the found node or the location one would be added
  */
  template<typename E>
  inline typename TreeSet<E>::search_results
    TreeSet<E>::searchHelper(const E& e) const
  {
    TreeNode<E>* nod = tree_ ? tree_->root() : nullptr;
    while (nod)
    {
      const E& ndata = nod->data();
      if (this->order(e, ndata))
      {
        if (nod->leftChild())
        {
          nod = nod->leftChild();
        }
        else
        {
          return search_results{ nod, false, true };
        }
      }
      else if (this->order(ndata, e))
      {
        if (nod->rightChild())
        {
          nod = nod->rightChild();
        }
        else
        {
          return search_results{ nod, false, false };
        }
      }
      else
      {
        return search_results{ nod, true };
      }
    }
    return search_results{ nullptr, false };
  }

  /**
  A helper for the `add()` methods. Allows us to use `search()` for adding
  elements.

  @param e  the element to add (take ownership)
  @param fn  results from `search()`
  @return  true iff the element was added
  */
  template<typename E>
  inline bool TreeSet<E>::addHelper(E* e, const search_results& fn)
  {
    // if found then nothing to do
    if (fn.found)
    {
      delete e; // took ownership so dispose of it I guess?
      return false;
    }
    // no node found means no root
    if (!fn.node)
    {
      if (tree_) delete tree_;
      tree_ = new AvlTree<E>(e);
      size_ = 1;
    }
    else if (fn.left)
    {
      tree_->addPredecessor(fn.node, e);
      ++size_;
    }
    else // right
    {
      tree_->addSuccessor(fn.node, e);
      ++size_;
    }
    return true;
  }

}

#endif
