#ifndef VILN_SORTED_SET_HPP
#define VILN_SORTED_SET_HPP

#include <stddef.h>

#include "detect_order.hpp"
#include "set.hpp"

namespace viln
{
  /// \cond DO_NOT_DOCUMENT
  template<typename E> class TreeNode;
  template<typename E> class AvlTree;
  /// \endcond

  /// A type of set which provides order iteration over its elements.
  /**
  The `TreeSet` is a `Set` implementation which uses an auto-balanced
  binary search tree to store elements. This allows iteration over the set to
  progress *in ascending order* of elements.

  This class uses `DetectOrder<E>` for the templated type, so either
  `operator<` must be defined for the element-type, or the `order(E,E)`
  method should be overridden. Equality is inferred from the less-than
  operation, so there is no requirement for a separate equality operation to
  be defined.

  This class does not implement thread-safety.

  Operations all have worst-case performance of **O(m*log(n))** where **m** is
  the size of the input (if applicable), and **n** is the size of the set.

  Note that modification of the set will invalidate any active iterators over
  the set.
  */
  template<typename E>
  class TreeSet :
    public Set<E>,
    protected DetectOrder<E>
  {
  public:
    /// Default constructor.
    TreeSet(void);
    /// Copy constructor.
    TreeSet(const TreeSet<E>& o);
    /// Move-constructor.
    TreeSet(TreeSet<E>&& o);
    /// Destructor.
    virtual ~TreeSet(void);

    /// Assignment operator.
    TreeSet<E>& operator=(const TreeSet<E>& o);
    /// Move-assign operator.
    TreeSet<E>& operator=(TreeSet<E>&& o);

    /// Construct a new tree-iterator over the elements of this object.
    virtual Iterator<const E>* iterator(void) const override;

    /// Add an element to the collection, taking ownership of the pointer.
    virtual bool add(E* e);
    /// Move an element into the collection.
    virtual bool add(E&& e) override;
    /// Add an element to the collection.
    virtual bool add(const E& e) override;
    /// Remove an element from the collection.
    virtual bool remove(const E& e) override;
    /// Remove all elements from this collection.
    virtual void clear(void) override;

    /// Determine if the provided element is contained in this collection.
    virtual bool contains(const E& e) const override;
    /// Find the first element in the set.
    virtual const E& first(void) const;
    /// Find the last element in the set.
    virtual const E& last(void) const;
    /// Find the smallest set element that is larger than the specified one.
    virtual const E& successor(const E& e) const;
    /// Find the largest set element that is smaller than the specified one.
    virtual const E& predecessor(const E& e) const;
    /// Determine the size of this collection.
    virtual size_t size(void) const override;

  protected:
    /// Access the root node.
    TreeNode<E>* const root(void);
    const TreeNode<E>* root(void) const;

  private:
    // multiple return states for the find operation
    struct search_results
    {
      // node containing the element or potential parent
      TreeNode<E>* node;
      // does the specified element exist in the set?
      bool found;
      // if not found, should add element as left child of node?
      bool left;
    };
    // find a tree node containing the specified element
    search_results searchHelper(const E& e) const;
    // helper for the various add methods
    bool addHelper(E* e, const search_results& fn);

    // auto-balancing tree storage
    AvlTree<E>* tree_;
    // size of the set
    size_t size_;
  };
}

#endif
