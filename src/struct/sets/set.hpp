#ifndef VILN_SET_HPP
#define VILN_SET_HPP

#include "collection.hpp"

namespace viln
{
  /// A type of collection that only contains unique elements.
  /**
  A `Set` is a type of `Collection` which permits at most one instance of each
  element.
  */
  template<typename E>
  class Set : public Collection<E>
  {
  public:
    /// Virtual destructor.
    virtual ~Set(void) {};

    /// Sets are equal if they contain the same elements.
    virtual bool operator==(const Set<E>& o) const;
    /// Sets are not equal if they do not contain the same elements.
    virtual bool operator!=(const Set<E>& o) const;

    /// Add all the elements from another collection to this set.
    virtual bool addAll(const ConstIterable<E>& c) override;
    /// Add all elements returned by the iterator to this set.
    virtual bool addAll(Iterator<const E>* itr) override;
  };
}

#endif
