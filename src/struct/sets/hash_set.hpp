#ifndef VILN_HASH_SET_HPP
#define VILN_HASH_SET_HPP

#include <stddef.h>

#include "detect_hash.hpp"
#include "detect_equal.hpp"
#include "set.hpp"

namespace viln
{
  /// \cond DO_NOT_DOCUMENT
  template<typename E> class ListNode;
  /// \endcond

  /// A type of set implemented using a hash-function for element accessing.
  /**
  The `HashSet` is an un-order `Set` implementation, which provides average-
  case **O(1)** access to elements through use of a *hash function*.

  This class uses `DetectEqual<E>` and `DetectHash<E>`for the templated type,
  so either `operator==` and `hash()` must be defined for the element-type,
  or the `equal(E,E)` and `hash()` methods should be overridden to provide
  that capability.

  This class does not implement thread-safety.

  Assuming a perfect hash-function and that the storage array does not require
  resizing, the performance of hash-set operations would be **O(m)**. The
  storage resizing, if needed, will add overhead, making the
  performance **O(m*log(n))** for operations that modify the set. If the
  provided hash-function is poor, the performance could be as bad as **O(m*n)**
  in the worst case. To ensure the best performance, a good hash-function must
  be provided, and the initial capacity should provide excess space beyond the
  anticipated size of the set.

  Note that modification of the list will invalidate any active iterators over
  the set.
  */
  template<typename E>
  class HashSet :
    public Set<E>,
    protected DetectHash<E>,
    protected DetectEqual<E>
  {
  public:
    /// Default constructor.
    HashSet(size_t cap = 2);
    /// Copy constructor.
    HashSet(const HashSet<E>& o);
    /// Move-constructor.
    HashSet(HashSet<E>&& o);
    /// Destructor.
    virtual ~HashSet(void);

    /// Assignment operator.
    HashSet<E>& operator=(const HashSet<E>& o);
    /// Move-assign operator.
    HashSet<E>& operator=(HashSet<E>&& o);

    /// Construct a new iterator over the elements of this object.
    virtual Iterator<const E>* iterator(void) const override;

    /// Add an element to the collection, taking ownership of the pointer.
    virtual bool add(E* e);
    /// Move an element into the collection.
    virtual bool add(E&& e) override;
    /// Add an element to the collection.
    virtual bool add(const E& e) override;
    /// Remove an element from the collection.
    virtual bool remove(const E& e) override;
    /// Remove all elements from this collection.
    virtual void clear(void) override;

    /// Determine if the provided element is contained in this collection.
    virtual bool contains(const E& e) const override;
    /// Determine the size of this collection.
    virtual size_t size(void) const override;

  protected:
    /// Obtain the hash table's capacity.
    virtual size_t capacity(void) const;
    // Update the hash table capacity to the given size.
    void setCapacity(size_t cap);
    // Determine the table index for the given element.
    size_t index(const E& e) const;

  private:
    // add an element with no checks to see if it is present
    void addHelper(E* e);

    // the actual storage for the set
    ListNode<E>** data_;
    // the size of the data array
    size_t capacity_;
    // the number of nodes in the table
    size_t size_;
  };
}

#endif
