#ifndef VILN_SET_INL
#define VILN_SET_INL

#include "collection.inl"

#include "set.hpp"

namespace viln
{
  /**
  Two sets are considered equal if they contain the same elements.

  @param o  the target set to compare elements
  @return  true iff each set contains the same elements
  */
  template<typename E>
  inline bool Set<E>::operator==(const Set<E>& o) const
  {
    // set elements are unique, so sufficient to check size and containment
    return this->size() == o.size() && this->containsAll(o);
  }

  /**
  Two sets are not considered equal if they do not contain the same elements.

  @param o  the target set to compare elements
  @return  true iff the sets do not contain the same elements
  */
  template<typename E>
  inline bool Set<E>::operator!=(const Set<E>& o) const
  {
    return !this->Set<E>::operator==(o);
  }

  /**
  Add a copy of each element from the provided collection to the set. This can
  be thought of as a union operation.

  @param c  the collection of elements to copy
  @return  true iff at least one element was added
  */
  template<typename E>
  inline bool Set<E>::addAll(const ConstIterable<E>& c)
  {
    if (&c == this) return false;
    return addAll(c.iterator());
  }

  /**
  Add all the elements provided by the given iterator to this set.
  Note that if the iterator is not finite, then this operation will never
  terminate.

  @warning If the iterator points to this set, the behavior is undefined.

  @param itr  an iterator over elements to add (takes ownership)
  @return  true iff at least one element was added
  */
  template<typename E>
  inline bool Set<E>::addAll(Iterator<const E>* itr)
  {
    bool ret = false;
    while (itr->hasNext())
    {
      ret |= this->add(itr->next());
    }
    delete itr;
    return ret;
  }
}

#endif
