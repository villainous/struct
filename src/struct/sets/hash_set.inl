#ifndef VILN_HASH_SET_INL
#define VILN_HASH_SET_INL

#include <stddef.h>

#include "list_node.inl"
#include "hash_table_iterator.inl"
#include "detect_hash.inl"
#include "detect_equal.inl"
#include "set.inl"

#include "const_iterator.hpp"
#include "hash_set.hpp"

namespace viln
{
  /**
  Default constructor creates an empty set with the given inital capacity.

  @param cap  initial capacity of the hash table
  */
  template<typename E>
  HashSet<E>::HashSet(size_t cap) :
    capacity_(cap < 2 ? 2 : cap),
    size_(0)
  {
    data_ = new ListNode<E>*[capacity_]();
  }

  /**
  The copy constructor creates a deep copy of all elements and creates a
  new set containing all those elements.

  @param o  the hash-set to copy
  */
  template<typename E>
  HashSet<E>::HashSet(const HashSet<E>& o) :
    data_(new ListNode<E>*[o.capacity_]()),
    capacity_(o.capacity_),
    size_(0)
  {
    HashSet::addAll(o);
  }

  template<typename E>
  inline HashSet<E>::HashSet(HashSet<E>&& o) :
    data_(o.data_),
    capacity_(o.capacity_),
    size_(o.size_)
  {
    o.data_ = nullptr;
  }

  template<typename E>
  HashSet<E>::~HashSet(void)
  {
    if (data_)
    {
      HashSet::clear();
      delete[] data_;
    }
  }

  template<typename E>
  inline HashSet<E>& HashSet<E>::operator=(const HashSet<E>& o)
  {
    if (this == &o) return *this;
    HashSet::clear();
    HashSet::addAll(o);
    return *this;
  }

  template<typename E>
  inline HashSet<E>& HashSet<E>::operator=(HashSet<E>&& o)
  {
    if (this == &o) return *this;
    if (data_)
    {
      HashSet::clear();
      delete[] data_;
    }
    capacity_ = o.capacity_;
    size_ = o.size_;
    data_ = o.data_;
    o.data_ = nullptr;
    return *this;
  }

  /**
  Create an iterator over the hash set's elements. Since the hash-set is not
  order, this iteration will progress through the elements in a pseudo-random
  order.

  Note that modification of the set while an iterator is active may lead to
  undefined behavior of any active iterators.

  @return  a new iterator over all the elements in this set
  */
  template<typename E>
  inline Iterator<const E>* HashSet<E>::iterator(void) const
  {
    HashTableIterator<E>* itr = new HashTableIterator<E>(data_, capacity_);
    return new ConstIterator<E>(itr);
  }

  /**
  Add an element to the set if the set does not already contain the element.

  This version takes ownership of the provided element pointer.

  @note **O(1)** average-case, **O(n)** worst-case performance
  @param e  the element to add to the set (takes ownership)
  @return  true iff the element was added to the set
  */
  template<typename E>
  inline bool HashSet<E>::add(E* e)
  {
    if (this->contains(*e)) return false;
    addHelper(e);
    return true;
  }

  /**
  Add an element to the set if the set does not already contain the element.

  This version calls the move-constructor to take ownership of the provided
  element. This version of insertion is used when the object being inserted
  is constructed in-line with the insert operation.
  
  @note **O(1)** average-case, **O(n)** worst-case performance
  @param e  the element to add to the set (moves element)
  @return  true iff the element was added to the set
  */
  template<typename E>
  inline bool HashSet<E>::add(E&& e)
  {
    if (this->contains(e)) return false;
    addHelper(new E((E&&)e));
    return true;
  }

  /**
  Add an element to the set if the set does not already contain the element.

  This version makes a copy of the provided element reference using its copy-
  constructor.

  @note **O(1)** average-case, **O(n)** worst-case performance
  @param e  the element to add to the set (copies element)
  @return  true iff the element was added to the set
  */
  template<typename E>
  inline bool HashSet<E>::add(const E& e)
  {
    if (this->contains(e)) return false;
    addHelper(new E(e));
    return true;
  }

  /**
  Remove an element equal to the specified element from the set if it exists.
  If an element is removed, this method returns true, and false otherwise.

  @note **O(1)** average-case, **O(n)** worst-case performance
  @param e  the element to remnove from the set
  @return  true iff the element was removed from the set
  */
  template<typename E>
  inline bool HashSet<E>::remove(const E& e)
  {
    size_t idx = index(e);
    ListNode<E>* nod = data_[idx];
    while (nod)
    {
      if (this->equal(nod->data(), e))
      {
        // update the start of the chain
        if (data_[idx] == nod)
        {
          data_[idx] = nod->nextNode();
        }
        // delete the found node
        nod->detach();
        delete nod;

        // if less than 1/4 full, then re-hash the table
        --size_;
        if (4 * size_ < capacity_)
        {
          setCapacity(2 * size_);
        }
        return true;
      }
      nod = nod->nextNode();
    }

    return false;
  }

  template<typename E>
  inline void HashSet<E>::clear(void)
  {
    for (size_t idx = 0; idx < capacity_; ++idx)
    {
      if (data_[idx])
      {
        delete data_[idx];
        data_[idx] = nullptr;
      }
    }
    size_ = 0;
  }

  /**
  Determine if this set contains the given element.

  @note **O(1)** average-case, **O(n)** worst-case performance
  @param e  the element to detect
  @return  true iff the set contains the given element
  */
  template<typename E>
  inline bool HashSet<E>::contains(const E& e) const
  {
    size_t idx = index(e);
    ListNode<E>* nod = data_[idx];
    while (nod)
    {
      if (this->equal(nod->data(), e))
      {
        return true;
      }
      nod = nod->nextNode();
    }
    return false;
  }

  template<typename E>
  inline size_t HashSet<E>::size(void) const
  {
    return size_;
  }

  /**
  Obtain the current capacity of the hash table. The `add()` and `remove()`
  methods will automatically adjust the capacity to be appropriate.

  @return  the current capacity of the hash table
  */
  template<typename E>
  inline size_t HashSet<E>::capacity(void) const
  {
    return capacity_;
  }

  /**
  Setting the capacity re-indexes the table. This may re-order and re-structure
  the chains.

  @note **O(n)** average-case, **O(n²)** worst-case performance
  @param cap  the capacity of the hash table
  */
  template<typename E>
  inline void HashSet<E>::setCapacity(size_t cap)
  {
    // re-setting capacity is a no-op
    if (cap == capacity_) return;
    // capacity must always be at least 2
    if (cap < 2) cap = 2;
    // keep the old table while performing the update
    ListNode<E>** oldData = data_;
    size_t oldCap = capacity_;
    // create a new empty table
    data_ = new ListNode<E> * [cap]();
    capacity_ = cap;
    for (size_t idx = 0; idx < oldCap; ++idx)
    {
      // keep track of current and next node in the chain
      ListNode<E>* nod = oldData[idx];
      ListNode<E>* nxt = nullptr;
      while (nod)
      {
        nxt = nod->nextNode();
        // move the node over to the new table
        size_t jdx = index(nod->data());
        nod->detach();
        if (data_[jdx])
        {
          nod->append(data_[jdx]);
        }
        data_[jdx] = nod;
        // increment the node to the next in the chain
        nod = nxt;
      }
    }
    delete[] oldData;
  }

  /**
  Compute the index in the hash-table for the specified element using
  the hash function and current table capacity.

  @param e  the element to locate in the hash table
  @return  the appropriate index in the hash table for this element
  */
  template<typename E>
  inline size_t HashSet<E>::index(const E& e) const
  {
    return this->hash(e) % capacity_;
  }

  template<typename E>
  inline void HashSet<E>::addHelper(E* e)
  {
    size_t idx = index(*e);
    // add the element to the start of the chain
    ListNode<E>* enod = new ListNode<E>(e);
    if (data_[idx])
    {
      enod->append(data_[idx]);
    }
    data_[idx] = enod;

    // if more than 3/4 full, then re-hash the table before insertion
    ++size_;
    if (4 * size_ > 3 * capacity_)
    {
      setCapacity(2 * size_);
    }
  }

}

#endif
