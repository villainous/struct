# Structures Library {#mainpage}

@tableofcontents

[![project homepage][home badge]][home page]

## Introduction

The core Structures library provides memory-managed aggregate types, and aims
to provide familiar and easy-to-use interfaces for those types. The Java
Structures Framework is a major inspiration for this library, however there is
not necessarily a 1:1 correspondence between the types or methods provided.

## Goals

The major goals this library attempts to address are as follows:

- provide useful abstract aggregate types
- implement performant concrete types of each aggregate
- provide options between implementations with differing performance profiles
- avoid unnecessary header-leakage
- provide headers that are plain-text readable
- promote readable code by presenting readable types and methods
- implement C++-standard-compliant cross-platform code
- rely only on C standard library functions and constants

## Provided Types

The core types of interest to developers are:

| Type             | Description                                 |
|------------------|---------------------------------------------|
| `viln::Array`    | a lightly-wrapped C-style array             |
| `viln::List`     | resizable storage accessed by index         |
| `viln::Set`      | unique non-indexed elements                 |
| `viln::Map`      | arbitrary elements indexed by another type  |
| `viln::Iterator` | can visit each element in an aggregate type |
| `viln::vcrd`     | small integer-arrays used for indexing      |

The other relevant types can be found via references in the documentation
of those interfaces or the provided implementations thereof.

*All templated types must provide a copy-constructor in order to be used as
elements of any of the provided aggregate types.*

### Array

Arrays are a wrapper for fixed-size storage. Elements of arrays must by
*default*-initializable and provide copy-construction and an assignment-
operator.

### List

Lists are intended to provide behavior similar to an array, but with automatic
growth and memory management. Elements may be arbitrary, but if they implement
`operator==` then the `contains()` methods will return useful results.

### Set

Sets provide a mechanism for collecting unique elements. Specific
implementations may require specific operators or methods to be defined in
order to compare elements. Check the set implementation for its requirements.

### Map

Maps provide a mechanism for correlating a set of *keys* with some collection
of *values*. Note that since the keys must form a set, they have similar
restrictions as described in the previous section. The values on the other hand
have no such restructions. This allows values to be accesed via lookup
according to their corresponding key.

### vcrd

This type is a wrapper for static-arrays of integer-types. They are intended
for use for indexing `viln::Map` or `viln::MultiArray` types, or to simply
bundle coordinate-indices together.

## Custom Types

If you would like to create your own collection-interoperable types, then
it is suggested to inherit the `viln::Iterable` interface and create an
appropriate `viln::Iterator` type. This has the added advantage of
allowing you to use your type with C++ range-for loops, like the provided
types, for example:

```cpp
MyIterable<float> things;
// add some values to things
for(const float& thing : things)
{
  // do something with a thing
}
```

[home badge]: https://img.shields.io/badge/project%20page-gitlab.com-informational
[home page]: https://gitlab.com/villainous/struct/
