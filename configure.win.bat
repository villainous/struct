@echo off
cls

if not exist vcpkg\vcpkg.exe (
  vcpkg\bootstrap-vcpkg.bat
)
vcpkg\vcpkg.exe version
vcpkg\vcpkg.exe install --triplet=x64-windows gtest
if errorlevel 1 (
  echo ERROR: vcpkg dependency installation failure
  pause
  exit
)
vcpkg\vcpkg.exe update

cmake --version
cmake -B _build -DCPACK_GENERATOR=ZIP -DBUILD_SHARED_LIBS=ON -DCMAKE_INSTALL_PREFIX=_install -DCMAKE_TOOLCHAIN_FILE="vcpkg\scripts\buildsystems\vcpkg.cmake" . -Ax64
if errorlevel 1 (
  echo ERROR: cmake build failure
  pause
  exit
) else (
  cd _build
  cmake-gui ..
)
